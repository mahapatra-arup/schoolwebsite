﻿using System;
using System.Data;
using System.Net.Mail;

namespace SchoolWebLibrary
{
    public static class SendMailTools
    {
        public static bool SendMail(string Name, string Email, string Subjects, string Message)
        {
            //own Details
            DataTable dt = MailUtils.GetMailDetails();
            if (dt.IsValidDataTable())
            {
                try
                {
                    string smtp_server = dt.Rows[0]["SmtpServer"].ToString();
                    int smtp_Port = 0;
                    int.TryParse(dt.Rows[0]["Port"].ToString(), out smtp_Port);
                    string smtp_EmailId = dt.Rows[0]["EmailId"].ToString();
                    string smtp_Password = dt.Rows[0]["Password"].ToString();

                    // Specify the from and to email address
                    MailMessage mailMessage = new MailMessage(smtp_EmailId, smtp_EmailId);
                    // Specify the email body
                    mailMessage.Body = Message;
                    // Specify the email Subject
                    mailMessage.Subject = Subjects + "(" + Name + ")";


                    // Specify the SMTP server name and post number
                    SmtpClient smtpClient = new SmtpClient(smtp_server, smtp_Port);
                    // Specify your gmail address and password
                    smtpClient.Credentials = new System.Net.NetworkCredential()
                    {
                        UserName = smtp_EmailId,
                        Password = smtp_Password
                    };
                    // Gmail works on SSL, so set this property to true
                    smtpClient.EnableSsl = false;
                    // Finall send the email message using Send() method
                    smtpClient.Send(mailMessage);
                    return true;
                }
                catch (Exception)
                {

                }
            }
            return false;
        }
    }
}