﻿using System;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary
{
    public static class GeneratePath
    {
        public static string GenerateFileUploadPath(this FileUpload FileUploadImage, string _SavePath)
        {
            string UploadPath = "";
            if (FileUploadImage.HasFile)
            {
                // Get the file extension
                string fileExtension = System.IO.Path.GetExtension(FileUploadImage.FileName);

                // Generate 
                UploadPath = _SavePath + "/" + Guid.NewGuid() + fileExtension;
            }

            // Create the path 
            return UploadPath;
        }
    }
}
