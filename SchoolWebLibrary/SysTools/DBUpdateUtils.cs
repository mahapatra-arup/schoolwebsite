﻿namespace SchoolWebLibrary
{
    public static class DBUpdateUtils
    {
        public static void Execute()
        {
            CreatenavcarouselImageGroup();
        }
        /// <summary>
        /// Create Home page slider banner gallery group
        /// </summary>
        public static void CreatenavcarouselImageGroup()
        {

            string img_64 = "../Web/images/BannerLogo.png";
            string query = "SELECT     GroupName FROM Gallery_Thumbnail WHERE(GroupName = 'navcarouselImage')";
            object obj = SqlHelper.GetInstance().ExcuteScalar(query);
            if (!obj.ISValidObject())
            {
                query = "Insert into Gallery_Thumbnail(GroupName,SlNo,ImagePath,ACtivity) Values('navcarouselImage','0','" + img_64 + "','True')";
                SqlHelper.GetInstance().ExcuteQuery(query);
            }
        }
    }
}