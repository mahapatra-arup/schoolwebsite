﻿using System;
using System.Data.Entity.Validation;
using System.Text;

namespace SchoolWebLibrary
{
    public class ExceptionUtils
    {
        public static ArgumentException DBEntityValidation(DbEntityValidationException e)
        {
            StringBuilder str = new StringBuilder();
            foreach (var eve in e.EntityValidationErrors)
            {
                str.Append(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    eve.Entry.Entity.GetType().Name, eve.Entry.State));
                foreach (var ve in eve.ValidationErrors)
                {
                    str.Append(string.Format("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                        ve.PropertyName,
                        eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                        ve.ErrorMessage));
                }
            }

            System.ArgumentException argEx = new System.ArgumentException(str.ToString(), "Error ");
            return argEx;
        }
    }
}
