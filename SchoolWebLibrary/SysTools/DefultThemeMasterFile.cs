﻿using System.Data;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class DefultThemeMasterFile
    {

        public static long Id { get { return SqlHelper.DbContext().Theme_Master.Where(a => a.Activity == true).Select(s => s.Id).FirstOrDefault(); } }
        public static string MasterfileName { get { return SqlHelper.DbContext().Theme_Master.Where(a => a.Activity == true).Select(s => s.MasterfileName).FirstOrDefault(); } }
        public static bool Activity { get { return SqlHelper.DbContext().Theme_Master.Where(a => a.Activity == true).Select(s => s.Activity).FirstOrDefault().ConvertObjectToBool(); } }

    }
}