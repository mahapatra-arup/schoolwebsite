﻿using System.ComponentModel.DataAnnotations;

namespace SchoolWebLibrary
{
    public class _SectionEnum
    {
        public enum _EnumOfSection
        {
            //Dont Change
            [Display(Name = "HEADER")]
            HEADER,
            [Display(Name = "NAV")]
            NAV,
            [Display(Name = "BODY")]
            BODY,
            [Display(Name = "FOOTER")]
            FOOTER,
            [Display(Name = "LEFT")]
            LEFT,
            [Display(Name = "RIGHT")]
            RIGHT

        }

    }
}