﻿using System;
using System.IO;
using System.Web;

namespace SchoolWebLibrary
{
    public static class ExceptionExtensions
    {
        /// <summary>
        ///  Provides full stack trace for the exception that occurred.
        /// </summary>
        /// <param name="exception">Exception object.</param>
        /// <param name="environmentStackTrace">Environment stack trace, for pulling additional stack frames.</param>
        /// 
        public static void ToWriteLog(this Exception exc, string source)
        {

            // Include logic for logging exceptions
            // Get the absolute path to the log file
            string logFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ErrorLog.txt");
            logFile = HttpContext.Current.Server.MapPath(logFile);

            // Open the log file for append and write the log
            System.IO.StreamWriter sw = new System.IO.StreamWriter(logFile, true);
            sw.WriteLine("**************** {0} ****************", DateTime.Now);
            sw.Write(Environment.NewLine);
            sw.Write("*******************************************************");
            sw.Write(Environment.NewLine);

            if (exc.InnerException != null)
            {
                sw.Write("Inner Exception Type: ");
                sw.WriteLine(exc.InnerException.GetType().ToString());
                sw.Write("<------------------------------------------------->");

                sw.Write(Environment.NewLine);
                sw.Write("Inner Exception: ");
                sw.WriteLine(exc.InnerException.Message);
                sw.Write("<------------------------------------------------->");

                sw.Write(Environment.NewLine);
                sw.Write("Inner Source: ");
                sw.WriteLine(exc.InnerException.Source);
                sw.Write("<------------------------------------------------->");

                sw.Write(Environment.NewLine);
                if (exc.InnerException.StackTrace != null)
                {
                    sw.WriteLine("Inner Stack Trace: ");
                    sw.WriteLine(exc.InnerException.StackTrace);
                }
            }
            sw.Write("<------------------------------------------------->");

            sw.Write(Environment.NewLine);
            sw.Write("Exception Type: ");
            sw.WriteLine(exc.GetType().ToString());
            sw.Write("<------------------------------------------------->");

            sw.Write(Environment.NewLine);
            sw.WriteLine("Exception: " + exc.Message);
            sw.Write("<------------------------------------------------->");

            sw.Write(Environment.NewLine);
            sw.WriteLine("Source: " + source);
            sw.Write("<------------------------------------------------->");

            sw.Write(Environment.NewLine);
            sw.WriteLine("Stack Trace: ");
            if (exc.StackTrace != null)
            {
                sw.WriteLine(exc.StackTrace);
                sw.WriteLine();
            }
            sw.Write("<---------------------End--------------------->");
            sw.Write(Environment.NewLine);
            sw.Close();
        }
    }

}

