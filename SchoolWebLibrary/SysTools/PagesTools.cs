﻿using System.Web.UI;
namespace SchoolWebLibrary
{
    public static class PagesTools
    {
        public static string GetCurrentPageName(this MasterPage p)
        {
            string sPath = p.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

        public static string GetCurrentPageName(this Page p)
        {
            string sPath = p.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }

        public static string GetCurrentPageName(this UserControl p)
        {
            string sPath = p.Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }
    }
}