﻿namespace SchoolWebLibrary
{
    public class DefultPaths
    {
        //--------------------------------
        public static string AboutUs_Folder_Path { get => "../FilesAndFolder/UploadImage"; }
        public static string Gallery_Folder_Path { get => "../FilesAndFolder/GalleryFile"; }
        public static string Notice_Folder_Path { get => "../FilesAndFolder/NoticeFile"; }
        public static string Staff_Folder_Path { get => "../FilesAndFolder/StaffImage"; }
        public static string Post_Folder_Path { get => "../FilesAndFolder/PostFile"; }

        //Dynamic Pages Folder------------
        public static string DynamicPages_ImageFile_Path { get => "../FilesAndFolder/StudentResultMarksheet"; }
        public static string DynamicPages_PdfFile_Path { get => "../FilesAndFolder/StudentResultMarksheet"; }

        //Exam----------------------------
        public static string Marksheet_Folder_Path { get => "../FilesAndFolder/StudentResultMarksheet"; }
        public static string Temp_Folder_Path { get => "../FilesAndFolder/TempFile"; }

    }
}
