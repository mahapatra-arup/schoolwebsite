//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolWebLibrary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SC_SiteMeta
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public byte[] Title_Ico { get; set; }
        public string KeyWords { get; set; }
        public string PageId { get; set; }
        public string Description { get; set; }
    
        public virtual SC_MenuMaster SC_MenuMaster { get; set; }
    }
}
