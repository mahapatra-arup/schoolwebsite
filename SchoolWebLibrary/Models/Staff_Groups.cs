//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolWebLibrary.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Staff_Groups
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Staff_Groups()
        {
            this.Staff_SubGroup = new HashSet<Staff_SubGroup>();
        }
    
        public int Id { get; set; }
        public string GroupName { get; set; }
        public Nullable<long> slNo { get; set; }
        public bool Activity { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Staff_SubGroup> Staff_SubGroup { get; set; }
    }
}
