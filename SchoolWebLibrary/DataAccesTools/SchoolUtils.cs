﻿using SchoolWebLibrary.Models;
using System.Data.Entity.Validation;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class SchoolUtils
    {

        public static bool UpdateSchoolProfile(SchoolProfile schoolProfile)
        {
            if (schoolProfile.ISValidObject())
            {
                //Db
                var db = SqlHelper.DbContext();
                try
                {
                    //Get Single course which need to update
                    var objCourse = db.SchoolProfiles.FirstOrDefault();

                    objCourse.SchoolName = schoolProfile.SchoolName;
                    objCourse.At = schoolProfile.At;
                    objCourse.PO = schoolProfile.PO;
                    objCourse.Block = schoolProfile.Block;
                    objCourse.SubDivission = schoolProfile.SubDivission;
                    objCourse.PS = schoolProfile.PS;
                    objCourse.DIST = schoolProfile.DIST;
                    objCourse.PIN = schoolProfile.PIN;
                    objCourse.DISECode = schoolProfile.DISECode;
                    objCourse.InstitutionalCode = schoolProfile.InstitutionalCode;
                    objCourse.HSCode = schoolProfile.HSCode;
                    objCourse.SecondaryCode = schoolProfile.SecondaryCode;
                    objCourse.ESTD = schoolProfile.ESTD;
                    objCourse.Category = schoolProfile.Category;
                    objCourse.Ph = schoolProfile.Ph;
                    objCourse.Email = schoolProfile.Email;
                    objCourse.Website = schoolProfile.Website;

                    //Logo
                    objCourse.Logo = schoolProfile.Logo;
                    objCourse.Logo1 = schoolProfile.Logo1;
                    objCourse.Logo2 = schoolProfile.Logo2;

                    //School
                    objCourse.SchoolType = schoolProfile.SchoolType;
                    objCourse.SignatureArea = schoolProfile.SignatureArea;
                    objCourse.GP = schoolProfile.GP;
                    objCourse.VocationalCode = schoolProfile.VocationalCode;
                    objCourse.Region = schoolProfile.Region;
                    objCourse.SCentreCode = schoolProfile.SCentreCode;
                    objCourse.HSCentreCode = schoolProfile.HSCentreCode;
                    objCourse.Area = schoolProfile.Area;
                    objCourse.DistrictCode = schoolProfile.DistrictCode;
                    objCourse.GoogleMapUrl = schoolProfile.GoogleMapUrl;

                    db.SaveChanges();
                    return true;
                }
                catch (DbEntityValidationException e)
                {
                    throw ExceptionUtils.DBEntityValidation(e);
                }
            }
            return false;
        }
        public static SchoolProfile GetSchoolDetails()
        {
            return SqlHelper.DbContext().SchoolProfiles.FirstOrDefault();
        }

    }
}