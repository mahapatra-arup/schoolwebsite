﻿using SchoolWebLibrary.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class WebsiteAnalyticsTools
    {
        public static List<WebsiteAnalytic> GetWebsiteAnalytics()
        {
            var WebsiteAnalytics = SqlHelper.DbContext().WebsiteAnalytics;
            var query = from wa in WebsiteAnalytics
                        select wa;
            return query.ToList();
        }

        public static string GetWebsiteAnalyticsPath()
        {
            var _WebsiteAnalytics = SqlHelper.DbContext().WebsiteAnalytics.FirstOrDefault();
            if (_WebsiteAnalytics.ISValidObject())
            {
                return _WebsiteAnalytics.AnalyticsPath;
            }
            return string.Empty;
        }

        public static string GetWebsiteAnalyticsTrackingId()
        {
            var _WebsiteAnalytics = SqlHelper.DbContext().WebsiteAnalytics.FirstOrDefault();
            if (_WebsiteAnalytics.ISValidObject())
            {
                return _WebsiteAnalytics.TrackingId;
            }
            return string.Empty;
        }
        public static bool UpdateWebsiteAnalytics(WebsiteAnalytic wa)
        {
            if (wa.ISValidObject())
            {
                try
                {
                    var ParamList = new List<SqlParameter>();
                    string query = "Update WebsiteAnalytics set TrackingId=@TrackingId, AnalyticsPath=@AnalyticsPath";

                    ParamList.Add(new SqlParameter("@TrackingId", wa.TrackingId));
                    ParamList.Add(new SqlParameter("@AnalyticsPath", wa.AnalyticsPath));
                    SqlHelper.MParameterList = ParamList;
                    if (SqlHelper.GetInstance().ExcuteQuery(query))
                    {
                        return true;
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    ExceptionUtils.DBEntityValidation(dbEx);
                }
            }
            return false;

        }
    }
}
