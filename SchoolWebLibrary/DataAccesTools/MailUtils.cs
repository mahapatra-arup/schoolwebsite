﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SchoolWebLibrary
{
    public static class MailUtils
    {

        public static DataTable GetMailDetails()
        {
            string sqry = string.Empty;
            string Query = "select  * from MailDetails";
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }

        public static string UpdateMailQry(string EmailId, string Password, string Name, string SmtpServer, string Port, bool SSL, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();
            string query = "update  MailDetails set EmailId=@EmailId, Password=@Password, Name=@Name, SmtpServer=@SmtpServer, Port=@Port, SSL=@SSL";

            ParamList.Add(new SqlParameter("@EmailId", EmailId));
            ParamList.Add(new SqlParameter("@Password", Password));
            ParamList.Add(new SqlParameter("@Name", Name));
            ParamList.Add(new SqlParameter("@SmtpServer", SmtpServer));
            ParamList.Add(new SqlParameter("@Port", Port));
            ParamList.Add(new SqlParameter("@SSL", SSL));
            return query;
        }

    }
}