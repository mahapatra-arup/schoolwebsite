﻿using System.Collections.Generic;

namespace SchoolWebLibrary
{
    public static class StudentClassTools
    {
        public static List<string> ClassMp { get => new List<string> { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X" }; }
        public static List<string> ClassHs { get => new List<string> { "XI", "XII" }; }
    }
}
