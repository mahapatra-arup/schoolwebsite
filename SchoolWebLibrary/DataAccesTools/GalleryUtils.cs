﻿using SchoolWebLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary
{
    public static class GalleryUtils
    {


        #region =============Gallery============
        public static string InsertGalleryQry(string ImgPath, string Slno, bool Activity, string ToolTip, string AlternateText, object ImageUrl, object Contents, string GalleryThumbnailId, object Subjects, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO  Gallery( ImgPath, Slno, Activity, ToolTip, AlternateText, ImageUrl, Contents, GalleryThumbnailId, Subjects) " +
            "VALUES(@ImgPath,@Slno,@Activity,@ToolTip,@AlternateText,@ImageUrl,@Contents,@GalleryThumbnailId,@Subjects)";

            ParamList.Add(new SqlParameter("@ImgPath", ImgPath));
            ParamList.Add(new SqlParameter("@Slno", Slno));
            ParamList.Add(new SqlParameter("@Activity", Activity));
            ParamList.Add(new SqlParameter("@ToolTip", ToolTip));
            ParamList.Add(new SqlParameter("@AlternateText", AlternateText));
            ParamList.Add(new SqlParameter("@ImageUrl", ImageUrl));
            ParamList.Add(new SqlParameter("@Contents", Contents));
            ParamList.Add(new SqlParameter("@GalleryThumbnailId", GalleryThumbnailId));
            ParamList.Add(new SqlParameter("@Subjects", Subjects));
            return query;
        }
        public static string UpdateGalleryQry(string Id, bool Activity, string ToolTip, string AlternateText, object ImageUrl, object Contents, string GalleryThumbnailId, object Subjects, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();
            string query = "update  Gallery set Activity=@Activity, ToolTip=@ToolTip,  " +
           "AlternateText=@AlternateText, ImageUrl=@ImageUrl, Contents=@Contents, GalleryThumbnailId=@GalleryThumbnailId, " +
           "Subjects=@Subjects where Id=@Id";

            ParamList.Add(new SqlParameter("@Id", Id));
            ParamList.Add(new SqlParameter("@Activity", Activity));
            ParamList.Add(new SqlParameter("@ToolTip", ToolTip));
            ParamList.Add(new SqlParameter("@AlternateText", AlternateText));
            ParamList.Add(new SqlParameter("@ImageUrl", ImageUrl));
            ParamList.Add(new SqlParameter("@Contents", Contents));
            ParamList.Add(new SqlParameter("@GalleryThumbnailId", GalleryThumbnailId));
            ParamList.Add(new SqlParameter("@Subjects", Subjects));
            return query;
        }
        public static bool UpdateGalleryImgPath(string id, string ImgPath)
        {
            SqlHelper.MParameterList = new List<SqlParameter>();
            string Query = "Update Gallery set ImgPath=@ImgPath  where  id=@id";
            SqlHelper.MParameterList.Add(new SqlParameter("@id", id));
            SqlHelper.MParameterList.Add(new SqlParameter("@ImgPath", ImgPath));
            return SqlHelper.GetInstance().ExcuteQuery(Query);
        }
        public static bool DeleteAbouts(string Id)
        {

            string Query = "Delete FROM  Gallery where Id=@Id";
            SqlHelper.MParameterList.Add(new SqlParameter("@Id", Id));
            bool issuccess = SqlHelper.GetInstance().ExcuteQuery(Query);
            if (issuccess)
            {
                return true;
            }
            return false;
        }

        public static string GetImagePath(string Id)
        {
            string Query = "SELECT  ImgPath FROM  Gallery where  Id=@Id";
            SqlHelper.MParameterList.Add(new SqlParameter("@Id", Id));
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                return Convert.ToString(obj);
            }
            return string.Empty;
        }


        public static int GetMaxGallerySlno()
        {
            int slno = 0;
            string Query = "SELECT  Max(SlNo) FROM  Gallery ";
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                int.TryParse(obj.ToString(), out slno);
            }
            return slno;
        }


        public static List<GalleryView> GetGalleryByGroup(string groupName)
        {
            List<GalleryView> lstGallery = new List<GalleryView>();
            string sqry = string.Empty;
            var db = SqlHelper.DbContext().GalleryViews;
            if (db.ISValidObject())
            {
                lstGallery = (from galleryview in db
                              where galleryview.Activity == true &&
                              galleryview.GroupName == groupName
                              select galleryview).ToList();

            }
            return lstGallery;
        }

        public static DataTable GetGallery(string id)
        {
            string sqry = string.Empty;

            string Query =  //Gallery Details
                "SELECT    Gallery.Id as Gallery_Id, Gallery.ImgPath as Gallery_Imgpath , Gallery.Slno as Gallery_slno, " +
                "Gallery.Activity as Gallery_Activity, Gallery.ToolTip as Gallery_ToolTip, Gallery.AlternateText as Gallery_AlternateText,  Gallery.Contents  as Gallery_Contents, " +
                " Gallery.Subjects as Gallery_Subjects,  Gallery.Type as Gallery_Type,\r\n" +
 //thumNail Details
 "dbo.Gallery_Thumbnail.ImagePath AS Thumbnail_ImagePath, dbo.Gallery_Thumbnail.Tooltip AS Thumbnail_Tooltip,\r\n" +
 "dbo.Gallery_Thumbnail.Subjects AS Thumbnail_Subjects,\r\n" +
 "dbo.Gallery_Thumbnail.contents AS Thumbnail_contents,  dbo.Gallery_Thumbnail.GroupName AS Thumbnail_GroupName,\r\n" +
 "dbo.Gallery_Thumbnail.ImageUrl AS Thumbnail_ImageUrl FROM         dbo.Gallery INNER JOIN\r\n" +
 "dbo.Gallery_Thumbnail ON dbo.Gallery.GalleryThumbnailId = dbo.Gallery_Thumbnail.Id\r\n" +
 "where  dbo.Gallery.Activity='True' and dbo.Gallery.id='" + id + "'";
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }
        #endregion

        #region ============Thumbnail===========
        public static string InsertThumbnailQry(string ImagePath, string Tooltip, string Subjects, string contents, string GroupName, bool ACtivity, string SlNo, object ImageUrl, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO    Gallery_Thumbnail(ImagePath, Tooltip, Subjects, contents, GroupName, ACtivity, SlNo, ImageUrl) " +
            "VALUES(@ImagePath,@Tooltip,@Subjects,@contents,@GroupName,@ACtivity,@SlNo,@ImageUrl)";

            ParamList.Add(new SqlParameter("@ImagePath", ImagePath));
            ParamList.Add(new SqlParameter("@Tooltip", Tooltip));
            ParamList.Add(new SqlParameter("@Subjects", Subjects));
            ParamList.Add(new SqlParameter("@contents", contents));
            ParamList.Add(new SqlParameter("@GroupName", GroupName));
            ParamList.Add(new SqlParameter("@ACtivity", ACtivity));
            ParamList.Add(new SqlParameter("@SlNo", SlNo));
            ParamList.Add(new SqlParameter("@ImageUrl", ImageUrl));

            return query;
        }

        public static int GetMaxThumbnailSlno()
        {
            int slno = 0;
            string Query = "SELECT  Max(SlNo) FROM    Gallery_Thumbnail ";
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                int.TryParse(obj.ToString(), out slno);
            }
            return slno;
        }

        public static bool IsExistGroup(string GroupName)
        {
            string Query = "SELECT GroupName from  Gallery_Thumbnail where GroupName=@GroupName";

            SqlHelper.MParameterList.Add(new SqlParameter("@GroupName", GroupName));
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// ///GET THUMBLE AND GROUP NAME
        /// </summary>

        public static DataTable GetThumbnail()
        {
            string sqry = string.Empty;
            string Query = "SELECT * FROM Gallery_Thumbnail where ACtivity='True'";
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }

        public static void AddThumbnailGroup(this DropDownList cmb)
        {
            Dictionary<int, string> cmbDictCls = new Dictionary<int, string>();
            if (cmb.Items.Count > 0)
            {
                cmb.DataSource = null;
            }
            string query = "Select Id,GroupName from  Gallery_Thumbnail   order by slno";
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                foreach (DataRow item in dt.Rows)
                {
                    cmbDictCls.Add(int.Parse(item["id"].ToString()), item["GroupName"].ToString());
                }
                cmb.DataSource = cmbDictCls;
                cmb.DataTextField = "Value";
                cmb.DataValueField = "Key";
                cmb.DataBind();
            }
        }
        #endregion
    }
}