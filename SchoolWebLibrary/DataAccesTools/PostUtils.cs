﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace SchoolWebLibrary
{
    public static class PostUtils
    {
        public enum _PostType { Event, Post }
        public static DataTable GetTop10PostDetails(_PostType postType)
        {
            string posttyp = Enum.GetName(typeof(_PostType), postType);
            DataTable dt = new DataTable();
            string query = "Select Top(4) * from Sc_Posts where Post_Type='" + posttyp + "' and Activity='True' order by Publish_Date DESC, menu_order";
            dt = SqlHelper.GetInstance().ExcuteNonQuery(query);
            return dt;
        }

        public static DataTable GetPostDetails(_PostType postType)
        {
            string posttyp = Enum.GetName(typeof(_PostType), postType);
            DataTable dt = new DataTable();
            string query = "Select * from Sc_Posts where Post_Type='" + posttyp + "' and Activity='True' order by Publish_Date,menu_order";
            dt = SqlHelper.GetInstance().ExcuteNonQuery(query);
            return dt;
        }

        public static DataTable GetPostDetailsById(string id)
        {
            DataTable dt = new DataTable();
            string query = "Select * from Sc_Posts where id='" + id + "'";
            dt = SqlHelper.GetInstance().ExcuteNonQuery(query);
            return dt;
        }
        public static int GetMaxPostSlno()
        {
            int slno = 0;
            string Query = "SELECT  Max(menu_order) FROM  Sc_Posts ";
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                int.TryParse(obj.ToString(), out slno);
            }
            return slno;
        }
        public static string InsertQryPost(string post_author, string Publish_Date, object post_dateFrom, object Post_Date_to, string post_content, string post_title, string Post_Header, bool Activity, object post_password, string post_name,
                string post_content_filtered, object post_parent, string menu_order, string post_type, string Post_ImgPath, object Post_Link_Id, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO Sc_Posts(post_author, Publish_Date, post_dateFrom, Post_Date_to, post_content, post_title, Post_Header, Activity, post_password, post_name, " +
                "post_content_filtered, post_parent, menu_order,post_type, Post_ImgPath, Post_Link_Id) " +

                  "VALUES(@post_author, @Publish_Date, @post_dateFrom, @Post_Date_to, @post_content, @post_title, @Post_Header, @Activity, @post_password, @post_name, " +
                "@post_content_filtered, @post_parent, @menu_order,@post_type, @Post_ImgPath, @Post_Link_Id)";

            ParamList.Add(new SqlParameter("@post_author", post_author));
            ParamList.Add(new SqlParameter("@Publish_Date", Publish_Date));
            ParamList.Add(new SqlParameter("@post_dateFrom", post_dateFrom));
            ParamList.Add(new SqlParameter("@Post_Date_To", Post_Date_to));
            ParamList.Add(new SqlParameter("@post_content", post_content));
            ParamList.Add(new SqlParameter("@post_title", post_title));
            ParamList.Add(new SqlParameter("@Post_Header", Post_Header));

            ParamList.Add(new SqlParameter("@Activity", Activity));
            ParamList.Add(new SqlParameter("@post_password", post_password));
            ParamList.Add(new SqlParameter("@post_name", post_name));
            ParamList.Add(new SqlParameter("@post_content_filtered", post_content_filtered));
            ParamList.Add(new SqlParameter("@post_parent", post_parent));
            ParamList.Add(new SqlParameter("@menu_order", menu_order));
            ParamList.Add(new SqlParameter("@post_type", post_type));
            ParamList.Add(new SqlParameter("@Post_ImgPath", Post_ImgPath));
            ParamList.Add(new SqlParameter("@Post_Link_Id", Post_Link_Id));
            return query;
        }

        public static string GetPostFilePath(string Id)
        {
            string Query = "SELECT  Post_ImgPath FROM   Sc_Posts where  Id=@Id";
            SqlHelper.MParameterList.Add(new SqlParameter("@Id", Id));
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                return Convert.ToString(obj);
            }
            return string.Empty;
        }

        public static bool DeletePost(string Id)
        {
            string Query = "Delete FROM  Sc_Posts where  Id=@Id";
            SqlHelper.MParameterList.Add(new SqlParameter("@Id", Id));
            bool obj = SqlHelper.GetInstance().ExcuteQuery(Query);
            if (obj)
            {
                return true;
            }
            return false;
        }
    }
}