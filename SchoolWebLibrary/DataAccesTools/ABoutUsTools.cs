﻿using SchoolWebLibrary.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class AboutUsTools
    {

        #region <------Insert Update Delete------->
        public static bool InsertAboutUs(About_Details _About_Details)
        {
            if (_About_Details.ISValidObject())
            {
                var db = SqlHelper.DbContext();
                var _Table = db.About_Details;
                if (_Table.ISValidObject())
                {
                    _Table.Add(_About_Details);
                    db.SaveChanges();
                    return true;
                }
            }
            return false;
        }
        public static string UpdateAboutUsQry(string Title, string Subjects, string Contents, string Footer, object Link_Id, string ToolsTrip, string Id, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "Update About_Details set Title=@Title, Subjects=@Subjects, Contents=@Contents, Footer=@Footer," +
                " Link_Id=@Link_Id, ToolsTrip=@ToolsTrip where Id=@Id";

            ParamList.Add(new SqlParameter("@Id", Id));
            ParamList.Add(new SqlParameter("@Title", Title));
            ParamList.Add(new SqlParameter("@Subjects", Subjects));
            ParamList.Add(new SqlParameter("@Contents", Contents));
            ParamList.Add(new SqlParameter("@Footer", Footer));
            ParamList.Add(new SqlParameter("@Link_Id", Link_Id));
            ParamList.Add(new SqlParameter("@ToolsTrip", ToolsTrip));
            return query;
        }
        public static bool UpdateAboutsImgPath(string id, string ImgPath)
        {
            SqlHelper.MParameterList = new List<SqlParameter>();
            string Query = "Update About_Details set ImgPath=@ImgPath  where  id=@id";
            SqlHelper.MParameterList.Add(new SqlParameter("@id", id));
            SqlHelper.MParameterList.Add(new SqlParameter("@ImgPath", ImgPath));
            return SqlHelper.GetInstance().ExcuteQuery(Query);
        }
        public static bool DeleteAbouts(string Id)
        {

            string Query = "Delete FROM  About_Details where Id=@Id";
            SqlHelper.MParameterList.Add(new SqlParameter("@Id", Id));
            bool issuccess = SqlHelper.GetInstance().ExcuteQuery(Query);
            if (issuccess)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region <--------------Get---------------->
        public static string GetImagePath(string Id)
        {
            var db = SqlHelper.DbContext().About_Details;
            return db.ISValidObject() ? (from about in db select about).Max(s => s.ImgPath) : "";
        }

        /// <summary>
        /// ///
        /// </summary>
        public static int GetMaxAboutSlno()
        {
            var db = SqlHelper.DbContext().About_Details;
            return db.ISValidObject() ? (from about in db select about).Max(s => s.slno).ConvertObjectToInt() : 0;
        }
        public static DataTable GetAboutUs()
        {
            string sqry = string.Empty;

            string Query = "select  * from About_Details   \r\n" +
            "left join Sc_links on Sc_links.Link_Id = About_Details.Link_Id \r\n" +
            "where About_Details.Activity = 'True'  order by About_Details.SlNo";
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }
        public static DataTable GetAboutUs(string Id)
        {
            string sqry = string.Empty;

            string Query = "select  * from About_Details   \r\n" +
            "left join Sc_links on Sc_links.Link_Id = About_Details.Link_Id \r\n" +
            "where About_Details.Activity = 'True' and About_Details.Id=@Id order by About_Details.SlNo";
            SqlHelper.MParameterList.Add(new SqlParameter("@Id", Id));
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }
        #endregion
    }
}
