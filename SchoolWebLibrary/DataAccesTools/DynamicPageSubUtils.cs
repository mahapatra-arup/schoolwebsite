﻿using SchoolWebLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class DynamicPageSubUtils
    {
        #region <-----------Post Request--------->
        public static bool InsertData(DynamicPages_Sub dynamicPages_Sub)
        {
            using (var db = SqlHelper.DbContext())
            {
                db.Database.Log = Console.Write;

                try
                {
                    //SC_links
                    db.DynamicPages_Sub.Add(dynamicPages_Sub);
                    var isSuccess = db.SaveChanges();


                    if (isSuccess > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (DbEntityValidationException e)
                {
                    throw e;
                }
            }
        }

        public static bool UpdateData(DynamicPages_Sub dynamicPages_Sub, bool isImageSave, bool isPdfSave)
        {
            if (dynamicPages_Sub.ISValidObject())
            {
                try
                {
                    var db = SqlHelper.DbContext();
                    //Get Single course which need to update
                    var objCourse = db.DynamicPages_Sub.Where(s => s.Id == dynamicPages_Sub.Id).FirstOrDefault();


                    //Text
                    objCourse.Publisher = dynamicPages_Sub.Publisher;
                    objCourse.PublishDate = dynamicPages_Sub.PublishDate;
                    objCourse.DateFrom = dynamicPages_Sub.DateFrom;
                    objCourse.DateTo = dynamicPages_Sub.DateTo;
                    objCourse.Subjects = dynamicPages_Sub.Subjects;
                    objCourse.Contents = dynamicPages_Sub.Contents;
                    objCourse.Footer = dynamicPages_Sub.Footer;
                    objCourse.Activity = dynamicPages_Sub.Activity;
                    objCourse.DynamicPageId = dynamicPages_Sub.DynamicPageId;


                    if (isImageSave)
                    {
                        objCourse.ImageFileUrl = dynamicPages_Sub.ImageFileUrl;
                    }
                    if (isPdfSave)
                    {
                        objCourse.PdfFileUrl = dynamicPages_Sub.PdfFileUrl;
                    }

                    // executes the appropriate commands to implement the changes to the database
                    var issucc = db.SaveChanges();
                    if (issucc > 0)
                    {
                        return true;
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    throw ExceptionUtils.DBEntityValidation(e);
                }
            }
            return false;
        }

        public static bool DeleteData(long Id)
        {
            if (Id.ISValidObject())
            {
                try
                {
                    var db = SqlHelper.DbContext();
                    //Get Single course which need to update
                    var objCourse = db.DynamicPages_Sub.Where(s => s.Id == Id).FirstOrDefault();

                    // executes the appropriate commands to implement the changes to the database
                    db.DynamicPages_Sub.Remove(objCourse);
                    var issucc = db.SaveChanges();
                    if (issucc > 0)
                    {
                        return true;
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    throw ExceptionUtils.DBEntityValidation(e);
                }
            }

            return false;
        }
        #endregion

        #region <------------Get Request----------->
        public static int GetMaxSlno(Guid _DynamicPageId)
        {
            var db = SqlHelper.DbContext().DynamicPages_Sub;
            int slno = 0;
            if (db.ISValidObject())
            {
                slno = db.Where(q => q.DynamicPageId == _DynamicPageId).Max(s => s.Slno).ConvertObjectToInt();
            }
            return slno;
        }

        /// <summary>
        /// ///get notice between date
        /// </summary>

        public static List<DynamicPages_Sub> GetData_BetweenDate(Guid _DynamicPageId)
        {
            var db = SqlHelper.DbContext().DynamicPages_Sub;
            List<DynamicPages_Sub> lstntc = new List<DynamicPages_Sub>();
            if (db.ISValidObject())
            {
                //(dp.DateFrom <= DateTime.Today.Date) && (dp.DateTo >= DateTime.Today.Date) &&
                lstntc = (from dp in db
                          where dp.DynamicPageId == _DynamicPageId
                          select dp).OrderByDescending(p => p.PublishDate).ThenBy(q => q.Slno).ToList();
            }

            return lstntc;
        }

        public static List<DynamicPages_Sub> GetAllData(Guid _DynamicPageId)
        {
            var db = SqlHelper.DbContext().DynamicPages_Sub;
            List<DynamicPages_Sub> lstntc = new List<DynamicPages_Sub>();
            if (db.ISValidObject())
            {
                lstntc = db.Where(q => q.DynamicPageId == _DynamicPageId).OrderByDescending(p => p.PublishDate).ThenBy(q => q.Slno).ToList();
            }
            return lstntc;
        }
        public static DynamicPages_Sub GetData(long id)
        {
            var db = SqlHelper.DbContext().DynamicPages_Sub;
            DynamicPages_Sub lstntc = new DynamicPages_Sub();
            if (db.ISValidObject())
            {
                lstntc = (from sd in db
                          where sd.Id == id
                          select sd).FirstOrDefault();
            }

            return lstntc;
        }

        #endregion
    }
}