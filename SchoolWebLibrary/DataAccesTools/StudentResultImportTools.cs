﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;


namespace SchoolWebLibrary
{
    public static class StudentResultImportTools
    {
        public static string _ImportErrorMessageShow = "";

        #region ------------------Import Excel Table : RStudent_HSResult & RStudent_MpResult---------------------

        #region -----------------Mp----------------
        /// <summary>
        /// column are static
        /// </summary>
        /// <returns></returns>
        public static DataTable GetMpresultDetails(string cls)
        {
            string Query = "SELECT  * FROM  RStudent_MpResult where Class=@clas order by Class,Section,RollNo";
            SqlHelper.MParameterList.Add(new SqlParameter("@clas", cls));
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }

        public static DataTable GetMpresultDetails(string clas, string sec, string rollno, string dob)
        {
            string Query = "SELECT  * FROM  RStudent_MpResult where Class=@clas and Section=@sec and RollNo=@rollno and DOB=@dob";
            SqlHelper.MParameterList.Add(new SqlParameter("@clas", clas));
            SqlHelper.MParameterList.Add(new SqlParameter("@sec", sec));
            SqlHelper.MParameterList.Add(new SqlParameter("@rollno", rollno));
            SqlHelper.MParameterList.Add(new SqlParameter("@dob", dob));

            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }
        public static bool ImportMpResult(string path)
        {
            List<string> lstQuery = new List<string>();
            List<List<SqlParameter>> sqlparammain = new List<List<SqlParameter>>();

            try
            {
                DataTable mDt = new DataTable();
                if (!path.ISNullOrWhiteSpace())
                {
                    // Connection String to Excel Workbook
                    //=================only for <xls 2003 version excel file are import>================
                    string excelCS = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0", path);
                    using (OleDbConnection con = new OleDbConnection(excelCS))
                    {
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }

                        //Get Sheet Name
                        // Get the data table containg the schema guid.
                        DataTable _sheetnameDt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        if (_sheetnameDt.IsValidDataTable())
                        {
                            //Get  Sheet Name
                            // object firstsheet = _sheetnameDt.Rows[0]["TABLE_NAME"];
                            object firstsheet = "Student List$";
                            OleDbDataAdapter dat = new OleDbDataAdapter("select * from [" + firstsheet + "]", con);
                            dat.Fill(mDt);
                            if (con.State == ConnectionState.Open)
                            {
                                con.Close();
                            }
                            if (mDt.IsValidDataTable())
                            {
                                foreach (DataRow row in mDt.Rows)
                                {
                                    List<SqlParameter> lstchild = new List<SqlParameter>();
                                    try
                                    {
                                        string[] _clsMp = { "IX", "X" };

                                        #region --------variable create and assign----------
                                        object RegisterNo = Convert.ToString(row["Register No"]);
                                        string Session = Convert.ToString(row["Session"]);
                                        string Class = Convert.ToString(row["Class"]);
                                        string Section = Convert.ToString(row["Section"]);
                                        string RollNo = Convert.ToString(row["Roll No"]);

                                        string Name = Convert.ToString(row["Student Name"]);
                                        string FathersName = Convert.ToString(row["Fathers Name"]);
                                        string MothersName = Convert.ToString(row["Mothers Name"]);
                                        string DOB = Convert.ToString(row["DOB"]);
                                        string SubjectsName = "<pre align=\"left\">" + Convert.ToString(row["Subject"]) + "</pre>";
                                        string Unit1_Marks = "<pre align=\"center\">" + Convert.ToString(row["Unit1"]) + "</pre>";
                                        string Unit2_Marks = "<pre align=\"center\">" + Convert.ToString(row["Unit2"]) + "</pre>";
                                        string Unit3_Marks = "<pre align=\"center\">" + Convert.ToString(row["Unit3"]) + "</pre>";
                                        string Total = "<pre align=\"center\">" + Convert.ToString(row["Total"]) + "</pre>";
                                        string AvgAndGrade = "<pre align=\"center\">" + Convert.ToString(row["Per and Grade"]) + "</pre>";
                                        string GrandTotal = Convert.ToString(row["Grand Total"]);
                                        string Result = Convert.ToString(row["Result"]);

                                        //For IX and X
                                        object RegistrationNo = _clsMp.Contains(Class.ToUpper().Trim()) ? row["Registration No"] : DBNull.Value;

                                        #endregion

                                        #region ==========Paraneter Define with query=============
                                        //Query
                                        string query = "INSERT INTO  RStudent_MpResult(RegisterNo, Session, Class, Section, RollNo, RegistrationNo, Name, FathersName, MothersName, DOB, SubjectsName, " +
                                            "Unit1_Marks, Unit2_Marks, Unit3_Marks, Total, AvgAndGrade, GrandTotal, Result) " +
                                             "VALUES(@RegisterNo, @Session, @Class, @Section, @RollNo, @RegistrationNo, @Name, @FathersName, @MothersName, @DOB, @SubjectsName, " +
                                            "@Unit1_Marks, @Unit2_Marks, @Unit3_Marks, @Total, @AvgAndGrade, @GrandTotal, @Result)";
                                        //Student Details
                                        lstchild.Add(new SqlParameter("@RegisterNo", RegisterNo));
                                        lstchild.Add(new SqlParameter("@Session", Session));
                                        lstchild.Add(new SqlParameter("@Class", Class));
                                        lstchild.Add(new SqlParameter("@Section", Section));
                                        lstchild.Add(new SqlParameter("@RollNo", RollNo));
                                        lstchild.Add(new SqlParameter("@RegistrationNo", RegistrationNo));
                                        lstchild.Add(new SqlParameter("@Name", Name));
                                        lstchild.Add(new SqlParameter("@FathersName", FathersName));
                                        lstchild.Add(new SqlParameter("@MothersName", MothersName));
                                        lstchild.Add(new SqlParameter("@DOB", DOB));
                                        lstchild.Add(new SqlParameter("@SubjectsName", SubjectsName));
                                        //Marks
                                        lstchild.Add(new SqlParameter("@Unit1_Marks", Unit1_Marks));
                                        lstchild.Add(new SqlParameter("@Unit2_Marks", Unit2_Marks));
                                        lstchild.Add(new SqlParameter("@Unit3_Marks", Unit3_Marks));
                                        lstchild.Add(new SqlParameter("@Total", Total));
                                        lstchild.Add(new SqlParameter("@AvgAndGrade", AvgAndGrade));
                                        lstchild.Add(new SqlParameter("@GrandTotal", GrandTotal));
                                        lstchild.Add(new SqlParameter("@Result", Result));

                                        //==Assign list==
                                        lstQuery.Add(query);
                                        sqlparammain.Add(lstchild);

                                        #endregion
                                    }
                                    catch (Exception ex1)
                                    {
                                        _ImportErrorMessageShow = ex1.Message.ToString();
                                    }

                                }
                                if (SqlHelper.GetInstance().ExecuteTransection(lstQuery, sqlparammain))
                                {
                                    return true;
                                }
                            }

                        }

                        else
                        {
                            _ImportErrorMessageShow = "Sheet Are not available in this Workbook";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _ImportErrorMessageShow = ex.Message.ToString();
            }
            return false;
        }

        public static bool DeleteMpresultDetails(string clas)
        {
            string Query = "Delete from RStudent_MpResult where Class=@clas";
            SqlHelper.MParameterList.Add(new SqlParameter("@clas", clas));
            bool issuccess = SqlHelper.GetInstance().ExcuteQuery(Query);
            return issuccess;
        }

        #endregion


        #region -=---------------Hs-------------------
        public static DataTable GetHSresultDetails(string cls)
        {
            string Query = "SELECT * FROM  RStudent_HSResult where Class=@clas order by Class,Section,RollNo";
            SqlHelper.MParameterList.Add(new SqlParameter("@clas", cls));
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }

        public static DataTable GetHSresultDetails(string clas, string sec, string rollno, string dob)
        {
            string Query = "SELECT  * FROM  RStudent_HSResult where Class=@clas and Section=@sec and RollNo=@rollno and DOB=@dob";
            SqlHelper.MParameterList.Add(new SqlParameter("@clas", clas));
            SqlHelper.MParameterList.Add(new SqlParameter("@sec", sec));
            SqlHelper.MParameterList.Add(new SqlParameter("@rollno", rollno));
            SqlHelper.MParameterList.Add(new SqlParameter("@dob", dob));

            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }

        public static bool ImportHSResult(string path)
        {
            List<string> lstQuery = new List<string>();
            List<List<SqlParameter>> sqlparammain = new List<List<SqlParameter>>();

            try
            {
                DataTable mDt = new DataTable();
                if (!path.ISNullOrWhiteSpace())
                {
                    // Connection String to Excel Workbook
                    //=================only foer <xlsx>================
                    string excelCS = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0", path);
                    using (OleDbConnection con = new OleDbConnection(excelCS))
                    {
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }

                        //Get Sheet Name
                        // Get the data table containg the schema guid.
                        DataTable _sheetnameDt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                        if (_sheetnameDt.IsValidDataTable())
                        {
                            //Get  Sheet Name
                            // object firstsheet = _sheetnameDt.Rows[0]["TABLE_NAME"];
                            object firstsheet = "Student List$";
                            OleDbDataAdapter dat = new OleDbDataAdapter("select * from [" + firstsheet + "]", con);
                            dat.Fill(mDt);
                            if (con.State == ConnectionState.Open)
                            {
                                con.Close();
                            }

                            if (mDt.IsValidDataTable())
                            {
                                foreach (DataRow row in mDt.Rows)
                                {
                                    List<SqlParameter> lstchild = new List<SqlParameter>();
                                    try
                                    {

                                        #region --------variable create and assign----------
                                        object RegisterNo = Convert.ToString(row["Register No"]);
                                        string Session = Convert.ToString(row["Session"]);
                                        string Class = Convert.ToString(row["Class"]);
                                        string Section = Convert.ToString(row["Section"]);
                                        string RollNo = Convert.ToString(row["Roll No"]);
                                        object RegistrationNo = Convert.ToString(row["Registration No"]);
                                        string Name = Convert.ToString(row["Student Name"]);
                                        string FathersName = Convert.ToString(row["Fathers Name"]);
                                        string MothersName = Convert.ToString(row["Mothers Name"]);
                                        string DOB = Convert.ToString(row["DOB"]);
                                        string SubjectsName = "<pre align=\"left\">" + Convert.ToString(row["Subject"]) + "</pre>";
                                        ///----------
                                        object Marks = DBNull.Value;
                                        object Total = DBNull.Value;
                                        string PercentageandGrade = "<pre align=\"center\">" + Convert.ToString(row["Per and Grade"]) + "</pre>";
                                        string GrandTotal = Convert.ToString(row["Grand Total"]);
                                        string Result = Convert.ToString(row["Result"]);

                                        #endregion



                                        #region ==========Paraneter Define with query=============
                                        //Query
                                        string query = "INSERT INTO  RStudent_HSResult(RegisterNo, Session, Class, Section, RollNo, RegistrationNo, Name, FathersName, MothersName, DOB, SubjectsName, " +
                                            " Marks, Total, PercentageandGrade, GrandTotal, Result) " +
                                             "VALUES(@RegisterNo, @Session, @Class, @Section, @RollNo, @RegistrationNo, @Name, @FathersName, @MothersName, @DOB, @SubjectsName, " +
                                            "@Marks, @Total, @PercentageandGrade, @GrandTotal, @Result)";
                                        //Student Details
                                        lstchild.Add(new SqlParameter("@RegisterNo", RegisterNo));
                                        lstchild.Add(new SqlParameter("@Session", Session));
                                        lstchild.Add(new SqlParameter("@Class", Class));
                                        lstchild.Add(new SqlParameter("@Section", Section));
                                        lstchild.Add(new SqlParameter("@RollNo", RollNo));
                                        lstchild.Add(new SqlParameter("@RegistrationNo", RegistrationNo));
                                        lstchild.Add(new SqlParameter("@Name", Name));
                                        lstchild.Add(new SqlParameter("@FathersName", FathersName));
                                        lstchild.Add(new SqlParameter("@MothersName", MothersName));
                                        lstchild.Add(new SqlParameter("@DOB", DOB));
                                        lstchild.Add(new SqlParameter("@SubjectsName", SubjectsName));
                                        //Marks  
                                        lstchild.Add(new SqlParameter("@Marks", Marks));
                                        lstchild.Add(new SqlParameter("@Total", Total));
                                        lstchild.Add(new SqlParameter("@PercentageandGrade", PercentageandGrade));
                                        lstchild.Add(new SqlParameter("@GrandTotal", GrandTotal));
                                        lstchild.Add(new SqlParameter("@Result", Result));

                                        //==Assign list==
                                        lstQuery.Add(query);
                                        sqlparammain.Add(lstchild);
                                        #endregion
                                    }
                                    catch (Exception ex1)
                                    {
                                        _ImportErrorMessageShow = ex1.Message.ToString();
                                    }

                                }
                                if (SqlHelper.GetInstance().ExecuteTransection(lstQuery, sqlparammain))
                                {
                                    return true;
                                }
                            }

                        }

                        else
                        {
                            _ImportErrorMessageShow = "Sheet Are not available in this Workbook";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _ImportErrorMessageShow = ex.Message.ToString();
            }
            return false;
        }
        public static bool DeleteHSpresultDetails(string clas)
        {
            string Query = "Delete from RStudent_HSResult where Class=@clas";
            SqlHelper.MParameterList.Add(new SqlParameter("@clas", clas));
            bool issuccess = SqlHelper.GetInstance().ExcuteQuery(Query);
            return issuccess;
        }
        #endregion

        #endregion

        #region ------------Import MarkSheet Table : RStudentMarksheetReport--------------

        public static string InsertRStudentsMarksheetReportsQry(object RegisterNo, object Session, object Class, object Section, object RollNo, object RegistrationNo, object Name, object FathersName, object MothersName, DateTime DOB, object MarksheetPath, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO  RStudentsMarksheetReports(RegisterNo, Session, Class, Section, RollNo, RegistrationNo, Name, FathersName, MothersName,DOB,MarksheetPath) " +
            "VALUES(@RegisterNo, @Session, @Class, @Section, @RollNo, @RegistrationNo, @Name, @FathersName, @MothersName,@DOB,@MarksheetPath)";

            ParamList.Add(new SqlParameter("@RegisterNo", RegisterNo));
            ParamList.Add(new SqlParameter("@Session", Session));
            ParamList.Add(new SqlParameter("@Class", Class));
            ParamList.Add(new SqlParameter("@Section", Section));
            ParamList.Add(new SqlParameter("@RollNo", RollNo));
            ParamList.Add(new SqlParameter("@RegistrationNo", RegistrationNo));
            ParamList.Add(new SqlParameter("@Name", Name));
            ParamList.Add(new SqlParameter("@FathersName", FathersName));
            ParamList.Add(new SqlParameter("@MothersName", MothersName));
            ParamList.Add(new SqlParameter("@DOB", DOB));
            ParamList.Add(new SqlParameter("@MarksheetPath", MarksheetPath));
            return query;
        }
        public static bool DeleteRStudentsMarksheetReports(string clas, string sec, string rollno, string dob)
        {
            string Query = "Delete from RStudentsMarksheetReports where Class=@clas and Section=@sec and RollNo=@rollno and DOB=@dob";
            SqlHelper.MParameterList.Add(new SqlParameter("@clas", clas));
            SqlHelper.MParameterList.Add(new SqlParameter("@sec", sec));
            SqlHelper.MParameterList.Add(new SqlParameter("@rollno", rollno));
            SqlHelper.MParameterList.Add(new SqlParameter("@dob", dob));
            bool issuccess = SqlHelper.GetInstance().ExcuteQuery(Query);
            return issuccess;
        }

        public static DataTable GetRStudentsMarksheetReports(string clas)
        {
            string Query = "select * from RStudentsMarksheetReports where Class=@clas";
            SqlHelper.MParameterList.Add(new SqlParameter("@clas", clas));

            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }

        public static DataTable GetRStudentsMarksheetReports(string clas, string sec, string rollno, string dob)
        {
            string Query = "select * from RStudentsMarksheetReports where Class=@clas and Section=@sec and RollNo=@rollno and DOB=@dob";
            SqlHelper.MParameterList.Add(new SqlParameter("@clas", clas));
            SqlHelper.MParameterList.Add(new SqlParameter("@sec", sec));
            SqlHelper.MParameterList.Add(new SqlParameter("@rollno", rollno));
            SqlHelper.MParameterList.Add(new SqlParameter("@dob", dob));
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }

        #endregion


        /// <summary>
        /// Get  Classes from Table : RStudentsMarksheetReports,RStudent_MpResul,RStudent_HSResult
        /// 
        /// </summary>
        /// <param name="cmb"></param>
        public static void GetMpHsClasses(this DropDownList cmb)
        {
            if (cmb.Items.Count > 0)
            {
                cmb.Items.Clear();
            }

            cmb.Items.Add("--Select--");
            string query = "select class from RStudent_MpResult " +//RStudent_MpResult
                             "union " +
                             "select class from RStudent_HSResult " +//RStudent_HSResult
                              "union " +
                             "select class from RStudentsMarksheetReports order by class ";//RStudentsMarksheetReports
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                foreach (DataRow item in dt.Rows)
                {
                    cmb.Items.Add(Convert.ToString(item["class"]));
                }
                cmb.DataBind();
            }

        }

        /// <summary>
        /// Get Sec use by class and both table relation are union
        /// </summary>
        /// <param name="cmb"></param>
        /// <param name="clas"></param>
        public static void GetMpHsSecByClass(this DropDownList cmb, string clas)
        {
            if (cmb.Items.Count > 0)
            {
                cmb.Items.Clear();
            }
            cmb.Items.Add("--Select--");
            string query = "select * from (select Section,Class from RStudent_MpResult " +//RStudent_MpResult
            "union " +
            "select Section,Class from RStudent_HSResult  " +//RStudent_HSResult
            "union " +
            "select Section,Class from RStudentsMarksheetReports) as tab " +//RStudentsMarksheetReports
            "where Class = '" + clas + "' ";
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                foreach (DataRow item in dt.Rows)
                {
                    cmb.Items.Add(Convert.ToString(item["Section"]));
                }
                cmb.DataBind();
            }
        }
    }
}