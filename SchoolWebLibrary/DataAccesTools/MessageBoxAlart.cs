﻿using System;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary
{
    public static class MessageBoxAlart
    {
        #region Messege Alart
        public enum _messegeType { danger, info, warning, success }
        public enum _alartTopIndex { Simple, Topindex }
        public static void Show(string msg, string msgHeader, _messegeType type, _alartTopIndex ltop, ref Literal ltr)
        {
            //------------Icon Set--------
            string ico = IcoSet(type);


            //------------Generate Message String--------
            string strmsgValue = Enum.GetName(typeof(_messegeType), type);
            switch (ltop)
            {
                #region ========tOP iNDEX==============
                case _alartTopIndex.Topindex:
                    string sb =
                        //=============Messege box=============-
                        "<div class = \"alert alert-" + strmsgValue + " fade in w3-card-4\" style=\"position:absolute;right:0;top:0; z-index:999; width:95%;\">" +
                  "<a href =\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>" +
                  "<strong>" + ico + " " + msgHeader + "</strong> <br\\> " + msg + "." +
                   //=============//Messege box=============-

                   //=============Script=============-
                   //"<link rel = \"stylesheet\" href =\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"> " +
                   //"<script src = \"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></ script> " +
                   // "< script src = \"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></ script> " +
                   // //=============//Script=============-

                   "</div>";
                    ltr.Text = sb;
                    break;
                #endregion

                #region --------sIMPLE-----------
                case _alartTopIndex.Simple:
                    string sb1Simple =
                        //=============Messege box=============-
                        "<div class = \"alert alert-" + strmsgValue + "  fade in w3-card-4\">" +
                "<a href =\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>" +
                 "<strong>" + ico + " " + msgHeader + "</strong>  " + msg + "." +
                 "</div>";
                    //=============Messege box=============-

                    ltr.Text = sb1Simple;
                    break;
                    #endregion

            }
        }

        private static string IcoSet(_messegeType type)
        {
            switch (type)
            {
                case _messegeType.danger:
                    return "&#10007;";
                case _messegeType.info:
                    return "&#x1F603;";
                case _messegeType.warning:
                    return "&#x26A0;";
                case _messegeType.success:
                    return "&#10004;";
            }
            return string.Empty;
        }
        #endregion
    }
}