﻿using SchoolWebLibrary.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class StaffUtils
    {


        #region ------Post Request------
        public static string QInsertryStaffDetails(string EmpName, string EmpCode, string Gender, string Dob,
      string EducationalQualification, string Subjects, string Address, string Description, string SlNo,
      bool Activity, string PhNo, string designation, string Email, string SubGroupId, string ImagePath, string Opinion, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO  Staff_Details(EmpCode,EmpName,Gender,Dob,EducationalQualification,Subjects,Address,Description,SlNo,Activity,PhNo,designation,Email,SubGroupId,ImagePath,Opinion) " +
            "VALUES(@EmpCode,@EmpName,@Gender,@Dob,@EducationalQualification,@Subjects,@Address,@Description,@SlNo,@Activity,@PhNo,@designation,@Email,@SubGroupId,@ImagePath,@Opinion)";

            ParamList.Add(new SqlParameter("@EmpCode", EmpCode));
            ParamList.Add(new SqlParameter("@EmpName", EmpName));
            ParamList.Add(new SqlParameter("@Gender", Gender));
            ParamList.Add(new SqlParameter("@Dob", Dob));
            ParamList.Add(new SqlParameter("@EducationalQualification", EducationalQualification));
            ParamList.Add(new SqlParameter("@Subjects", Subjects));
            ParamList.Add(new SqlParameter("@Address", Address));
            ParamList.Add(new SqlParameter("@Description", Description));
            ParamList.Add(new SqlParameter("@SlNo", SlNo));
            ParamList.Add(new SqlParameter("@Activity", Activity));
            ParamList.Add(new SqlParameter("@PhNo", PhNo));
            ParamList.Add(new SqlParameter("@designation", designation));
            ParamList.Add(new SqlParameter("@Email", Email));
            ParamList.Add(new SqlParameter("@SubGroupId", SubGroupId));
            ParamList.Add(new SqlParameter("@ImagePath", ImagePath));
            ParamList.Add(new SqlParameter("@Opinion", Opinion));

            return query;
        }



        public static string QUpdateStaffDetails(string EmpName, string EmpCode, string Gender, string Dob,
        string EducationalQualification, string Subjects, string Address, string Description,
        bool Activity, string PhNo, string designation, string Email, string SubGroupId, string Opinion, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "update Staff_Details set EmpName=@EmpName,Gender=@Gender,Dob=@Dob,EducationalQualification=@EducationalQualification, " +
            "Subjects=@Subjects,Address=@Address,Description=@Description,Activity=@Activity,PhNo=@PhNo,designation=@designation,Email=@Email,SubGroupId=@SubGroupId,Opinion=@Opinion " +
            "where EmpCode=@EmpCode";

            ParamList.Add(new SqlParameter("@EmpCode", EmpCode));
            ParamList.Add(new SqlParameter("@EmpName", EmpName));
            ParamList.Add(new SqlParameter("@Gender", Gender));
            ParamList.Add(new SqlParameter("@Dob", Dob));
            ParamList.Add(new SqlParameter("@EducationalQualification", EducationalQualification));
            ParamList.Add(new SqlParameter("@Subjects", Subjects));
            ParamList.Add(new SqlParameter("@Address", Address));
            ParamList.Add(new SqlParameter("@Description", Description));
            ParamList.Add(new SqlParameter("@Activity", Activity));
            ParamList.Add(new SqlParameter("@PhNo", PhNo));
            ParamList.Add(new SqlParameter("@designation", designation));
            ParamList.Add(new SqlParameter("@Email", Email));
            ParamList.Add(new SqlParameter("@SubGroupId", SubGroupId));
            ParamList.Add(new SqlParameter("@Opinion", Opinion));

            return query;
        }


        public static bool UpdateStaffImgPath(string _EmpCode, string ImagePath)
        {
            using (var db = SqlHelper.DbContext())
            {
                var x = db.Staff_Details.Where(em => em.EmpCode == _EmpCode).FirstOrDefault();
                x.ImagePath = ImagePath;
                var succ = db.SaveChanges();
                if (succ > 0)
                {
                    return true;
                }
                return false;
            }

            //SqlHelper.MParameterList = new List<SqlParameter>();
            //string Query = "Update StaffDetails set ImagePath=@ImagePath  where  EmpCode=@EmpCode";
            //SqlHelper.MParameterList.Add(new SqlParameter("@EmpCode", EmpCode));
            //SqlHelper.MParameterList.Add(new SqlParameter("@ImagePath", ImagePath));
            //return SqlHelper.GetInstance().ExcuteQuery(Query);
        }

        public static bool DeleteStaff(string _EmpCode)
        {
            using (var ctx = SqlHelper.DbContext())
            {
                var x = ctx.Staff_Details.Where(em => em.EmpCode == _EmpCode).FirstOrDefault();
                ctx.Staff_Details.Remove(x);
                var succ = ctx.SaveChanges();
                if (succ > 0)
                {
                    return true;
                }
                return false;
            }

            //string Query = "Delete FROM  StaffDetails where  EmpCode=@EmpCode";
            //SqlHelper.MParameterList.Add(new SqlParameter("@EmpCode", EmpCode));
            //bool obj = SqlHelper.GetInstance().ExcuteQuery(Query);
        }

        #endregion


        #region -----Get Request----
        public static int GetMaxMenuSlno()
        {
            var db = SqlHelper.DbContext().Staff_Details.Max(sl => sl.SlNo);
            return db.ConvertObjectToInt() + 1;
        }

        public static string GetImagePath(string _EmpCode)
        {
            Staff_Details staff_Details = new Staff_Details();
            var db = SqlHelper.DbContext().Staff_Details.ToList();

            if (db.IsValidList())
            {
                staff_Details = (from sd in db
                                 where sd.EmpCode == _EmpCode
                                 orderby sd.SlNo
                                 select sd).FirstOrDefault();

            }
            if (staff_Details.ISValidObject())
            {
                return staff_Details.ImagePath;
            }
            return string.Empty;
        }


        public static bool IsExistEmployee(string empcode)
        {
            Staff_Details staff_Details = new Staff_Details();
            var db = SqlHelper.DbContext().Staff_Details.ToList();

            if (db.IsValidList())
            {
                staff_Details = (from sd in db
                                 where sd.EmpCode == empcode
                                 select sd).FirstOrDefault();

            }

            if (staff_Details.ISValidObject() && !staff_Details.EmpCode.ISNullOrWhiteSpace())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Get Staff Details Where Staff Opinion Is present
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Models.StaffView> OpinionPresentStaffDetails()
        {
            var _DbSetStaffView = SqlHelper.DbContext().StaffViews.ToList();
            IEnumerable<Models.StaffView> lst = null;
            if (_DbSetStaffView.IsValidList())
            {
                lst = from StaffView in _DbSetStaffView
                      where StaffView.Opinion.ConvertObjectToString() != string.Empty
                      select StaffView;
            }
            return lst;
        }


        public static List<Staff_Details> GetStaffDetails()
        {
            List<Staff_Details> lstView = new List<Staff_Details>();
            var db = SqlHelper.DbContext().Staff_Details.ToList();

            if (db.IsValidList())
            {
                lstView = (from sd in db
                           where sd.Activity == true
                           orderby sd.SlNo
                           select sd).ToList();

            }

            return lstView;
        }

        public static List<Staff_Details> GetStaffDetailsBySubGroup(int _SubGroupId)
        {
            List<Staff_Details> lstView = new List<Staff_Details>();
            var db = SqlHelper.DbContext().Staff_Details.ToList();

            if (db.IsValidList())
            {
                lstView = (from sd in db
                           where sd.Activity == true && sd.SubGroupId == _SubGroupId
                           orderby sd.SlNo
                           select sd).ToList();

            }

            return lstView;
        }

        public static Staff_Details GetStaffDetails(string _EmpCode)
        {
            Staff_Details staff_Details = new Staff_Details();
            var db = SqlHelper.DbContext().Staff_Details.ToList();

            if (db.IsValidList())
            {
                staff_Details = (from sd in db
                                 where sd.Activity == true && sd.EmpCode == _EmpCode
                                 orderby sd.SlNo
                                 select sd).FirstOrDefault();

            }

            return staff_Details;
        }

        public static List<StaffView> GetStaffView()
        {
            List<StaffView> lstView = new List<StaffView>();
            var db = SqlHelper.DbContext().StaffViews.ToList();

            if (db.IsValidList())
            {
                lstView = (from sd in db
                           where sd.Activity == true
                           orderby sd.SlNo
                           select sd).ToList();

            }

            return lstView;
        }
        #endregion


    }
}