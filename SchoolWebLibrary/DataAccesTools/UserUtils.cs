﻿using SchoolWebLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class UserUtils
    {
        public static string _EncriptCurrentUser { get; set; }
        public static List<SC_Users> GetUserDetails(bool _Activity)
        {
            var users = SqlHelper.DbContext().SC_Users;
            var query = from user in users
                        where user.Activity == _Activity
                        select user;
            return query.ToList();
        }

        public static SC_Users GetUserDetails(bool _Activity, string _UserName)
        {
            var users = SqlHelper.DbContext().SC_Users;
            var query = from user in users
                        where user.Activity == _Activity &&
                       user.user_Name == _UserName
                        select user;
            return query.FirstOrDefault();
        }

        public static SC_Users GetUserDetails(string _UserName)
        {
            var users = SqlHelper.DbContext().SC_Users;
            var query = from user in users
                        where user.user_Name == _UserName
                        select user;
            return query.FirstOrDefault();
        }


        public static bool UpdateUserDetails(SC_Users sC_Users)
        {
            if (sC_Users.ISValidObject())
            {
                var db = SqlHelper.DbContext();
                try
                {
                    //Get Single course which need to update
                    var objCourse = db.SC_Users.Single(u => u.user_Name == sC_Users.user_Name);

                    objCourse.user_FirstName = sC_Users.user_FirstName;
                    objCourse.User_LastName = sC_Users.User_LastName;
                    objCourse.user_email = sC_Users.user_email;
                    objCourse.Activity = sC_Users.Activity;
                    objCourse.display_name = sC_Users.display_name;
                    objCourse.User_Image = sC_Users.User_Image;
                    objCourse.User_Address = sC_Users.User_Address;
                    objCourse.User_Country = sC_Users.User_Country;
                    objCourse.User_PostalCode = sC_Users.User_PostalCode;
                    objCourse.User_Abouts = sC_Users.User_Abouts;

                    db.SaveChanges();
                    return true;
                }
                catch (DbEntityValidationException e)
                {
                    throw ExceptionUtils.DBEntityValidation(e);
                }
            }
            return false;
        }

        public static bool UpdateUserPassword(SC_Users _SC_Users)
        {
            if (_SC_Users.ISValidObject())
            {


                try
                {
                    var db = SqlHelper.DbContext();
                    //Get Single course which need to update
                    var objCourse = db.SC_Users.Single(u => u.user_Name == _SC_Users.user_Name);
                    //Field which will be update
                    objCourse.user_passWord = _SC_Users.user_passWord;
                    objCourse.SALT = _SC_Users.SALT;
                    objCourse.ModifyDate = _SC_Users.ModifyDate;
                    // executes the appropriate commands to implement the changes to the database
                    db.SaveChanges();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException)
                {

                }

            }
            return false;
        }

        public static bool IsValid(string _UserNameorMail, string _Password, out string _Messege)
        {
            _Messege = "";
            try
            {
                var db = SqlHelper.DbContext().SC_Users;

                if (db.ISValidObject())
                {
                    var getUser = (from user in db
                                   where user.user_Name == _UserNameorMail || user.user_email == _UserNameorMail
                                   select user).FirstOrDefault();
                    if (getUser != null)
                    {
                        var salt = getUser.SALT.ToString();
                        //Password Hasing Process Call Helper Class Method    
                        var encodingPasswordString = HashHelper.EncodePassword(_Password, salt);

                        //Check Login Detail User Name Or Password    
                        var query = (from user in db
                                     where (user.user_Name == _UserNameorMail || user.user_email == _UserNameorMail) && user.user_passWord.Equals(encodingPasswordString)
                                     select user).FirstOrDefault();
                        if (query != null)
                        {
                            _Messege = "Success";
                            return true;
                        }

                        _Messege = "Invallid User Name or Password";
                        return false;
                    }
                    _Messege = "Invallid User Name";
                    return false;
                }

            }
            catch (Exception)
            {
                _Messege = " Error!!! Contact To Admin";
                return false;
            }
            _Messege = "Invallid User Name or Password";
            return false;
        }
    }
}