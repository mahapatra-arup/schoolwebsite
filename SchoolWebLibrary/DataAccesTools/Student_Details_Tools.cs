﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace SchoolWebLibrary
{
    public static class Student_Details_Tools
    {
        public static string InsertStudentDetailsQry(string userName, string password, string securityQuestion,
                             string securityAnswer, string date, string studentName, string dob, string gender,
                             string caste, string religion, string nationality, string fathersName,
                             string mothersName, string vill, string po, string dist, string state, string pin,
                             string mailID, string contactNo, string aadhaarNo, bool isXstudent, object imagePath, string PS, string Block, string SubDivision, bool Activity, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO  Student_Details(UserName,Password,SecurityQuestion,SecurityAnswer,Date, " +
                           "StudentName,DOB,Gender,Caste,Religion,Nationality,Fathersname,MothersName,Vill,PO, " +
                           "Dist,State,PIN,MailID,ContactNo,AadhaarNo,XStudent,ImagePath,PS,Block,SubDivision,Activity) " +
                           "VALUES(@UserName,@Password,@SecurityQuestion,@SecurityAnswer,@Date,@StudentName " +
                           ",@DOB,@Gender,@Gender,@Caste,@Religion,@Nationality,@Fathersname,@MothersName,@Vill " +
                           ",@PO,@Dist,@State,@PIN,@MailID,@ContactNo,@AadhaarNo,@XStudent,@Image,@PS,@Block,@SubDivision,@Activity)";

            ParamList.Add(new SqlParameter("@UserName", userName));
            ParamList.Add(new SqlParameter("@Password", password));
            ParamList.Add(new SqlParameter("@SecurityQuestion", securityQuestion));
            ParamList.Add(new SqlParameter("@SecurityAnswer", securityAnswer));
            ParamList.Add(new SqlParameter("@Date", date));
            ParamList.Add(new SqlParameter("@StudentName", studentName));
            ParamList.Add(new SqlParameter("@DOB", dob));
            ParamList.Add(new SqlParameter("@Gender", gender));
            ParamList.Add(new SqlParameter("@Caste", caste));
            ParamList.Add(new SqlParameter("@Religion", religion));
            ParamList.Add(new SqlParameter("@Nationality", nationality));
            ParamList.Add(new SqlParameter("@Fathersname", mothersName));
            ParamList.Add(new SqlParameter("@MothersName", mothersName));
            ParamList.Add(new SqlParameter("@Vill", vill));
            ParamList.Add(new SqlParameter("@PO", po));
            ParamList.Add(new SqlParameter("@Dist", dist));
            ParamList.Add(new SqlParameter("@State", state));
            ParamList.Add(new SqlParameter("@PIN", pin));
            ParamList.Add(new SqlParameter("@MailID", mailID));
            ParamList.Add(new SqlParameter("@ContactNo", contactNo));
            ParamList.Add(new SqlParameter("@AadhaarNo", aadhaarNo));
            ParamList.Add(new SqlParameter("@XStudent", isXstudent));
            ParamList.Add(new SqlParameter("@ImagePath", imagePath));

            ParamList.Add(new SqlParameter("@PS", PS));
            ParamList.Add(new SqlParameter("@Block", Block));
            ParamList.Add(new SqlParameter("@SubDivision", SubDivision));
            ParamList.Add(new SqlParameter("@Activity", Activity));
            return query;
        }
        public static string UpdateStudentDetailsQry(string userName, string date, string studentName, string dob, string gender,
                            string caste, string religion, string nationality, string fathersName,
                            string mothersName, string vill, string po, string dist, string state, string pin,
                            string mailID, string contactNo, string aadhaarNo, bool isXstudent, string PS, string Block, string SubDivision, bool Activity, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "Update Student_Details set Date=@Date,StudentName=@StudentName,DOB=@DOB,Gender=@Gender, " +
                           "Caste=@Caste,Religion=@Religion,Nationality=@Nationality,Fathersname=@Fathersname, " +
                           "MothersName=@MothersName,Vill=@Vill,PO=@PO,Dist=@Dist,State=@State,PIN=@PIN, " +
                           "MailID=@MailID,ContactNo=@ContactNo,AadhaarNo=@AadhaarNo,XStudent=@XStudent " +
                           ",PS=@PS,Block=@Block,SubDivision=@SubDivision,Activity=@Activity Where UserName=@UserName";
            ParamList.Add(new SqlParameter("@UserName", userName));
            ParamList.Add(new SqlParameter("@Date", date));
            ParamList.Add(new SqlParameter("@StudentName", studentName));
            ParamList.Add(new SqlParameter("@DOB", dob));
            ParamList.Add(new SqlParameter("@Gender", gender));
            ParamList.Add(new SqlParameter("@Caste", caste));
            ParamList.Add(new SqlParameter("@Religion", religion));
            ParamList.Add(new SqlParameter("@Nationality", nationality));
            ParamList.Add(new SqlParameter("@Fathersname", mothersName));
            ParamList.Add(new SqlParameter("@MothersName", mothersName));
            ParamList.Add(new SqlParameter("@Vill", vill));
            ParamList.Add(new SqlParameter("@PO", po));
            ParamList.Add(new SqlParameter("@Dist", dist));
            ParamList.Add(new SqlParameter("@State", state));
            ParamList.Add(new SqlParameter("@PIN", pin));
            ParamList.Add(new SqlParameter("@MailID", mailID));
            ParamList.Add(new SqlParameter("@ContactNo", contactNo));
            ParamList.Add(new SqlParameter("@AadhaarNo", aadhaarNo));
            ParamList.Add(new SqlParameter("@XStudent", isXstudent));
            ParamList.Add(new SqlParameter("@PS", PS));
            ParamList.Add(new SqlParameter("@Block", Block));
            ParamList.Add(new SqlParameter("@SubDivision", SubDivision));
            ParamList.Add(new SqlParameter("@Activity", Activity));
            return query;
        }

        public static string GetSecurityQuestion(string userName)
        {
            string Query = "SELECT  SecurityQuestion FROM  Student_Details where  UserName=@UserName";
            SqlHelper.MParameterList.Add(new SqlParameter("@UserName", userName));
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                return obj.ToString();
            }
            return null;
        }
        public static bool PasswordResetAuthentication(string userName, string studentName, string securityAnswer, string dob)
        {
            string Query = "SELECT  SecurityAnswer FROM  Student_Details where  UserName=@UserName " +
                           "and StudentName=@StudentName and SecurityAnswer=@SecurityAnswer and DOB=@DOB";
            SqlHelper.MParameterList.Add(new SqlParameter("@UserName", userName));
            SqlHelper.MParameterList.Add(new SqlParameter("@StudentName", studentName));
            SqlHelper.MParameterList.Add(new SqlParameter("@SecurityAnswer", securityAnswer));
            SqlHelper.MParameterList.Add(new SqlParameter("@DOB", dob));
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                return true;
            }
            return false;
        }
        public static bool DuplicateStudent(string studentName, string fathersName, string dob)
        {
            string Query = "SELECT  UserName FROM  Student_Details " +
                           "where StudentName=@StudentName and FathersName=@FathersName and DOB=@DOB";
            SqlHelper.MParameterList.Add(new SqlParameter("@StudentName", studentName));
            SqlHelper.MParameterList.Add(new SqlParameter("@FathersName", fathersName));
            SqlHelper.MParameterList.Add(new SqlParameter("@DOB", dob));
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                return true;
            }
            return false;
        }
        public static bool DuplicateUserName(string userName)
        {
            string Query = "SELECT  UserName FROM  Student_Details where UserName=@UserName";
            SqlHelper.MParameterList.Add(new SqlParameter("@UserName", userName));
            object obj = SqlHelper.GetInstance().ExcuteScalar(Query);
            if (obj.ISValidObject())
            {
                return true;
            }
            return false;
        }

    }
}