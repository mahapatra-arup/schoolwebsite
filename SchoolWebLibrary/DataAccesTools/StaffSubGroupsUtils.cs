﻿using SchoolWebLibrary.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary
{
    public static class StaffSubGroupsUtil
    {

        public static string QryInsertSubGroupUtils(string SubGroupName, int GroupId, string slNo, bool Activity, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO Staff_SubGroup(SubGroupName,GroupId,slNo,Activity) " +
              "VALUES(@SubGroupName,@GroupId,@slNo,@Activity)";
            ParamList.Add(new SqlParameter("@SubGroupName", SubGroupName));
            ParamList.Add(new SqlParameter("@GroupId", GroupId));
            ParamList.Add(new SqlParameter("@slNo", slNo));
            ParamList.Add(new SqlParameter("@Activity", Activity));

            return query;
        }

        public static bool UpdateSubGroups(int Id, bool Activity, string SubGroupName)
        {
            if (Id.ISValidObject())
            {
                try
                {
                    var db = SqlHelper.DbContext();
                    //Get Single course which need to update
                    var objCourse = db.Staff_SubGroup.Single(u => u.Id == Id);
                    //Field which will be update
                    objCourse.Activity = Activity;
                    objCourse.SubGroupName = SubGroupName;
                    // executes the appropriate commands to implement the changes to the database
                    db.SaveChanges();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException)
                {

                }

            }
            return false;
        }
        public static bool UpdateSubGroupSlNo(int Id, int SlnO)
        {
            if (Id.ISValidObject())
            {
                try
                {
                    var db = SqlHelper.DbContext();
                    //Get Single course which need to update
                    var objCourse = db.Staff_SubGroup.Single(u => u.Id == Id);
                    //Field which will be update
                    objCourse.slNo = SlnO;
                    // executes the appropriate commands to implement the changes to the database
                    db.SaveChanges();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException)
                {

                }

            }
            return false;
        }
        /// <summary>
        /// With Out Activity
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Staff_SubGroup> GetSubGroupDetails()
        {
            return SqlHelper.DbContext().Staff_SubGroup.Where(s => s.Activity == true).OrderBy(s => s.slNo);
        }
        /// <summary>
        /// only get  Activity equals to True
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Staff_SubGroup> GetSubGroupDetails(int groupId)
        {
            return SqlHelper.DbContext().Staff_SubGroup.Where(s => s.Activity == true).Where(g => g.GroupId == groupId).OrderBy(s => s.slNo);
        }
        public static int GetMaxMenuSlno()
        {
            var data = SqlHelper.DbContext().Staff_SubGroup;
            if (data.IsValidIEnumerable())
            {
                return data.Max(s => s.slNo).ConvertObjectToInt();
            }
            return 0;
        }
        public static void AddStaffSubGroup(this DropDownList cmb)
        {
            Dictionary<int, string> cmbDictCls = new Dictionary<int, string>();
            if (cmb.Items.Count > 0)
            {
                cmb.DataSource = null;
            }
            var data = GetSubGroupDetails();
            if (data.IsValidIEnumerable())
            {
                foreach (var item in data)
                {
                    cmbDictCls.Add(item.Id, item.SubGroupName);
                }
                cmb.DataSource = cmbDictCls;
                cmb.DataTextField = "Value";
                cmb.DataValueField = "Key";
                cmb.DataBind();
            }
        }

        public static bool IsExistSubGroup(string SubgroupName)
        {
            var data = SqlHelper.DbContext().Staff_SubGroup.Where(s => s.SubGroupName == SubgroupName).FirstOrDefault();

            if (data.ISValidObject())
            {
                return true;
            }
            return false;
        }
    }
}