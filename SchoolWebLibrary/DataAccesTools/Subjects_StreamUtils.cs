﻿using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary
{
    public static class Subjects_StreamUtils
    {

        #region ===========Stream==============
        /// <summary>
        /// Get all Stream in table stream
        /// </summary>
        /// <param name="cmb"></param>
        public static void GetStream(this DropDownList cmb)
        {
            Dictionary<int, string> cmbDictCls = new Dictionary<int, string>();
            cmbDictCls.Add(-1, "--Select--");

            if (cmb.Items.Count > 0)
            {
                cmb.DataSource = null;
            }
            string Query = "select  * from Stream";

            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            if (dt.IsValidDataTable())
            {
                foreach (DataRow item in dt.Rows)
                {
                    cmbDictCls.Add(int.Parse(item["StreamId"].ToString()), item["Stream_Name"].ToString());
                }
                cmb.DataSource = cmbDictCls;
                cmb.DataTextField = "Value";
                cmb.DataValueField = "Key";
                cmb.DataBind();
            }
        }
        #endregion


        #region =================Subjects=================

        /// <summary>
        /// Get All Subjects in table Subjects_Details
        /// </summary>
        /// <param name="cmb"></param>
        public static void GetSubjects(this DropDownList cmb)
        {
            Dictionary<int, string> cmbDictCls = new Dictionary<int, string>();
            if (cmb.Items.Count > 0)
            {
                cmb.DataSource = null;
            }
            string Query = "select  * from Subjects_Details";

            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            if (dt.IsValidDataTable())
            {
                foreach (DataRow item in dt.Rows)
                {
                    cmbDictCls.Add(int.Parse(item["Id"].ToString()), item["Subject_Name"].ToString());
                }
                cmb.DataSource = cmbDictCls;
                cmb.DataTextField = "Value";
                cmb.DataValueField = "Key";
                cmb.DataBind();
            }
        }
        /// <summary>
        /// Get Subject By Group Id
        /// </summary>
        /// <param name="groupid"></param>
        /// <returns></returns>
        public static DataTable GetSubject(string groupid)
        {
            string Query = "select * from Subjects_In_Group inner join " +
           "Subjects_Details on Subjects_Details.Id = Subjects_In_Group.SubjectId " +
           "where Subjects_In_Group.GroupId='" + groupid + "' ";

            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }
        #endregion

        #region ========================Subject Group==================
        /// <summary>
        /// Get Group By Stream Id
        /// </summary>
        /// <param name="streamid"></param>
        /// <returns></returns>
        public static DataTable GetStreamGroup(string streamid)
        {
            string Query = "select  * from Stream_Group where StreamId='" + streamid + "'";
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(Query);
            return dt;
        }
        #endregion
    }
}