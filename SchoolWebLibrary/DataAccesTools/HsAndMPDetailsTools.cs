﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace SchoolWebLibrary
{
    public static class HsAndMPDetailsTools
    {
        #region ===============HsDetails=====================
        public static string InsertHsDetailsDetailsQry(string StreamId, string UserName, string Stream, string Lang1,
           string Lang2, string Elective1, string Elective2, string Elective3, string Optional, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO  HS_Details(StreamId, UserName, Stream, Lang1, Lang2, Elective1, Elective2, Elective3, Optional) " +
                           "VALUES(@StreamId, @UserName, @Stream, @Lang1, @Lang2, @Elective1, @Elective2, @Elective3, @Optiona1)";


            ParamList.Add(new SqlParameter("@StreamId", StreamId));
            ParamList.Add(new SqlParameter("@UserName", UserName));
            ParamList.Add(new SqlParameter("@Stream", Stream));
            ParamList.Add(new SqlParameter("@Lang1", Lang1));
            ParamList.Add(new SqlParameter("@Lang2", Lang2));
            ParamList.Add(new SqlParameter("@Elective1", Elective1));
            ParamList.Add(new SqlParameter("@Elective2", Elective2));
            ParamList.Add(new SqlParameter("@Elective3", Elective3));
            ParamList.Add(new SqlParameter("@Optional", Optional));

            return query;
        }


        public static string UpdateHsDetailsDetailsQry(string id, string StreamId, string UserName, string Stream, string Lang1,
           string Lang2, string Elective1, string Elective2, string Elective3, string Optional, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "update  HS_Details set StreamId=@StreamId, UserName=@UserName, Stream=@Stream, Lang1=@Lang1, Lang2=@Lang2, Elective1=@Elective1, " +
                "Elective2=@Elective2, Elective3=@Elective3, Optional=@Optional  where id=@Id";


            ParamList.Add(new SqlParameter("@Id", id));
            ParamList.Add(new SqlParameter("@StreamId", StreamId));
            ParamList.Add(new SqlParameter("@UserName", UserName));
            ParamList.Add(new SqlParameter("@Stream", Stream));
            ParamList.Add(new SqlParameter("@Lang1", Lang1));
            ParamList.Add(new SqlParameter("@Lang2", Lang2));
            ParamList.Add(new SqlParameter("@Elective1", Elective1));
            ParamList.Add(new SqlParameter("@Elective2", Elective2));
            ParamList.Add(new SqlParameter("@Elective3", Elective3));
            ParamList.Add(new SqlParameter("@Optional", Optional));

            return query;
        }

        #endregion

        #region ======================Mp Details==============
        public static string InsertMpDetailsDetailsQry(string UserName, string Passing_Year, string Lang1_Subject, string Lang1_Marks, string Lang2_Subject,
           string Lang2_Marks, string Sub1, string Sub1_Marks, string Sub2, string Sub2_Marks, string Sub3, string Sub3_Marks, string Sub4, string Sub4_Marks, string Sub5, string Sub5_Marks, bool IsRegular, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO  Mp_Details(UserName, Passing_Year, Lang1_Subject, Lang1_Marks," +
                " Lang2_Subject, Lang2_Marks, Sub1, Sub1_Marks, Sub2, Sub2_Marks, Sub3, Sub3_Marks, Sub4, Sub4_Marks, Sub5, Sub5_Marks,IsRegular) " +
                           "VALUES(@UserName, @Passing_Year, @Lang1_Subject, @Lang1_Marks, @Lang2_Subject, @Lang2_Marks, @Sub1, @Sub1_Marks, " +
                           "@Sub2, @Sub2_Marks, @Sub3, @Sub3_Marks, @Sub4, @Sub4_Marks, @Sub5, @Sub5_Marks,@IsRegular)";



            ParamList.Add(new SqlParameter("@UserName", UserName));
            ParamList.Add(new SqlParameter("@Passing_Year", Passing_Year));
            ParamList.Add(new SqlParameter("@Lang1_Subject", Lang1_Subject));
            ParamList.Add(new SqlParameter("@Lang1_Marks", Lang1_Marks));
            ParamList.Add(new SqlParameter("@Lang2_Subject", Lang2_Subject));
            ParamList.Add(new SqlParameter("@Lang2_Marks", Lang2_Marks));

            ParamList.Add(new SqlParameter("@Sub1", Sub1));
            ParamList.Add(new SqlParameter("@Sub1_Marks", Sub1_Marks));
            ParamList.Add(new SqlParameter("@Sub2", Sub2));
            ParamList.Add(new SqlParameter("@Sub2_Marks", Sub2_Marks));
            ParamList.Add(new SqlParameter("@Sub3", Sub3));
            ParamList.Add(new SqlParameter("@Sub3_Marks", Sub3_Marks));
            ParamList.Add(new SqlParameter("@Sub4", Sub4));
            ParamList.Add(new SqlParameter("@Sub4_Marks", Sub4_Marks));
            ParamList.Add(new SqlParameter("@Sub5", Sub5));
            ParamList.Add(new SqlParameter("@Sub5_Marks", Sub5_Marks));

            ParamList.Add(new SqlParameter("@IsRegular", IsRegular));

            return query;
        }


        public static string UpdateMpDetailsDetailsQry(string id, string UserName, string Passing_Year, string Lang1_Subject, string Lang1_Marks, string Lang2_Subject,
          string Lang2_Marks, string Sub1, string Sub1_Marks, string Sub2, string Sub2_Marks, string Sub3, string Sub3_Marks, string Sub4, string Sub4_Marks, string Sub5, string Sub5_Marks, bool IsRegular, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "Update  Mp_Details set UserName=@UserName, Passing_Year=@Passing_Year, Lang1_Subject=@Lang1_Subject, Lang1_Marks=@Lang1_Marks," +
                " Lang2_Subject=@Lang2_Subject, Lang2_Marks=@Lang2_Marks, Sub1=@Sub1, Sub1_Marks=@Sub1_Marks, Sub2=@Sub2, Sub2_Marks=@Sub2_Marks, Sub3=@Sub3, " +
                "Sub3_Marks=@Sub3_Marks, Sub4=@Sub4, Sub4_Marks=@Sub4_Marks, Sub5=@Sub5, Sub5_Marks=@Sub5_Marks,IsRegular=@IsRegular where id=@Id";


            ParamList.Add(new SqlParameter("@Id", id));
            ParamList.Add(new SqlParameter("@UserName", UserName));
            ParamList.Add(new SqlParameter("@Passing_Year", Passing_Year));
            ParamList.Add(new SqlParameter("@Lang1_Subject", Lang1_Subject));
            ParamList.Add(new SqlParameter("@Lang1_Marks", Lang1_Marks));
            ParamList.Add(new SqlParameter("@Lang2_Subject", Lang2_Subject));
            ParamList.Add(new SqlParameter("@Lang2_Marks", Lang2_Marks));

            ParamList.Add(new SqlParameter("@Sub1", Sub1));
            ParamList.Add(new SqlParameter("@Sub1_Marks", Sub1_Marks));
            ParamList.Add(new SqlParameter("@Sub2", Sub2));
            ParamList.Add(new SqlParameter("@Sub2_Marks", Sub2_Marks));
            ParamList.Add(new SqlParameter("@Sub3", Sub3));
            ParamList.Add(new SqlParameter("@Sub3_Marks", Sub3_Marks));
            ParamList.Add(new SqlParameter("@Sub4", Sub4));
            ParamList.Add(new SqlParameter("@Sub4_Marks", Sub4_Marks));
            ParamList.Add(new SqlParameter("@Sub5", Sub5));
            ParamList.Add(new SqlParameter("@Sub5_Marks", Sub5_Marks));

            ParamList.Add(new SqlParameter("@IsRegular", IsRegular));

            return query;
        }


        #endregion
    }
}