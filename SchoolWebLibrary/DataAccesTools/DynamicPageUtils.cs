﻿using SchoolWebLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary.DataAccesTools
{
    public static class DynamicPageUtils
    {

        #region Post Request
        public static bool AddNavigation(SC_links sC_Links, SC_MenuMaster sC_MenuMaster, DynamicPage dynamicPage)
        {
            using (var db = SqlHelper.DbContext())
            {
                db.Database.Log = Console.Write;


                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //SC_links
                        db.SC_links.Add(sC_Links);
                        db.SaveChanges();

                        //SC_MenuMaster
                        db.SC_MenuMaster.Add(sC_MenuMaster);
                        db.SaveChanges();

                        //DynamicPages
                        db.DynamicPages.Add(dynamicPage);
                        db.SaveChanges();


                        //Commit
                        transaction.Commit();

                        return true;
                    }
                    catch (DbEntityValidationException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public static bool UpdateNavigation(SC_links sC_Links, SC_MenuMaster sC_MenuMaster)
        {
            using (var db = SqlHelper.DbContext())
            {
                db.Database.Log = Console.Write;

                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        ////SC_MenuMaster
                        var sMenuMaster = db.SC_MenuMaster.Where(s => s.Module_Id == sC_MenuMaster.Module_Id).FirstOrDefault();
                        sMenuMaster.Title = sC_MenuMaster.Title;
                        sMenuMaster.Parent_Module_Id = sC_MenuMaster.Parent_Module_Id;
                        sMenuMaster.ToolsTrip = sC_MenuMaster.Title;
                        sMenuMaster.Activity = sC_MenuMaster.Activity;
                        sMenuMaster.IsImageShow = sC_MenuMaster.IsImageShow;
                        sMenuMaster.IsTitleShow = sC_MenuMaster.IsTitleShow;
                        sMenuMaster.Image = sC_MenuMaster.Image;


                        ////SC_links
                        var slink = db.SC_links.Where(s => s.Link_Id == sMenuMaster.Link_Id).FirstOrDefault();
                        slink.link_name = sC_Links.link_name;

                        // If Dynamic Page Then Update Other Wise Not
                        if (sMenuMaster.IsDynamicPage.ConvertObjectToBool())
                        {
                            ////SC_links
                            slink.link_url = sC_Links.link_url;

                            ////DynamicPages
                            var dynamicpage = db.DynamicPages.Where(s => s.ModuleId == sC_MenuMaster.Module_Id).FirstOrDefault();
                            dynamicpage.PageName = sC_MenuMaster.Title;
                        }

                        var isSuccess = db.SaveChanges();

                        //Commit
                        transaction.Commit();

                        if (isSuccess > 0)
                        {
                            return true;
                        }
                        return false;
                    }
                    catch (DbEntityValidationException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public static bool UpdateDynamicpage(Guid _Id, string _Content, string _Title, string _Css)
        {
            using (var db = SqlHelper.DbContext())
            {
                db.Database.Log = Console.Write;
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //DynamicPages
                        var dynamicpage = db.DynamicPages.Where(s => s.Id == _Id).FirstOrDefault();
                        dynamicpage.PageContent = _Content;
                        dynamicpage.PageTitle = _Title;
                        dynamicpage.css = _Css;

                        var isSuccess = db.SaveChanges();

                        //Commit
                        transaction.Commit();

                        if (isSuccess > 0)
                        {
                            return true;
                        }
                        return false;
                    }
                    catch (DbEntityValidationException e)
                    {
                        throw e;
                    }
                }
            }
        }

        public static bool UpdateMenuSlNo(string menuId, int SlnO)
        {
            if (menuId.ISValidObject())
            {
                try
                {
                    var db = SqlHelper.DbContext();
                    //Get Single course which need to update
                    var objCourse = db.SC_MenuMaster.Single(u => u.Module_Id == menuId);
                    //Field which will be update
                    objCourse.SlNo = SlnO;
                    // executes the appropriate commands to implement the changes to the database
                    db.SaveChanges();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    throw ExceptionUtils.DBEntityValidation(e);
                }

            }
            return false;
        }

        #endregion

        #region Get Request
        public static DynamicPage GetDynamicPage(string pageName)
        {
            return SqlHelper.DbContext().DynamicPages.Where(p => p.PageName == pageName).FirstOrDefault();
        }

        public static DynamicPage GetDynamicPageByModuleId(string Module_Id)
        {
            return SqlHelper.DbContext().DynamicPages.Where(p => p.ModuleId == Module_Id).FirstOrDefault();
        }

        public static void cmbDynamicPages(this DropDownList cmb)
        {
            Dictionary<Guid, string> cmbDictCls = new Dictionary<Guid, string>();
            if (cmb.Items.Count > 0)
            {
                cmb.DataSource = null;
                cmb.Items.Clear();
            }

            var dynamicPages = SqlHelper.DbContext().DynamicPages.ToList();
            if (dynamicPages.IsValidList())
            {
                foreach (var item in dynamicPages)
                {
                    cmbDictCls.Add(item.Id, item.PageName);
                }
                cmb.DataSource = cmbDictCls;
                cmb.DataTextField = "Value";
                cmb.DataValueField = "Key";
                cmb.DataBind();
            }
        }
        #endregion

    }
}
