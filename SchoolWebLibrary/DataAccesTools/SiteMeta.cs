﻿using SchoolWebLibrary.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class SiteMeta
    {

        public static string InsertSitemeta(string title, byte[] img, string KeyWords, string PageId, string Description, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO Sc_SiteMeta(Title,Title_Ico,KeyWords,PageId,Description) " +
              "VALUES(@Title,@Title_Ico,@KeyWords,@PageId,@Description)";
            ParamList.Add(new SqlParameter("@Title", title));
            ParamList.Add(new SqlParameter("@Title_Ico", img));
            ParamList.Add(new SqlParameter("@KeyWords", KeyWords));
            ParamList.Add(new SqlParameter("@PageId", PageId));
            ParamList.Add(new SqlParameter("@Description", Description));

            return query;
        }

        public static string UpdateQrySitemeta(string title, byte[] img, string KeyWords, string PageId, string Description, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "Update  Sc_SiteMeta set Title=@Title,Title_Ico=@Title_Ico,KeyWords=@KeyWords,Description=@Description where PageId=@PageId";


            ParamList.Add(new SqlParameter("@Title", title));
            ParamList.Add(new SqlParameter("@Title_Ico", img));
            ParamList.Add(new SqlParameter("@KeyWords", KeyWords));
            ParamList.Add(new SqlParameter("@PageId", PageId));
            ParamList.Add(new SqlParameter("@Description", Description));
            return query;

        }
        /// <summary>
        /// Page Means Sc_MenuMaster module Id
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static SC_SiteMeta GetSiteMetaDetails(string moduleid)
        {
            return SqlHelper.DbContext().SC_SiteMeta.Where(s => s.PageId == moduleid).FirstOrDefault();
        }

        public static bool IsExist(string pageId)
        {
            var sitemeta = SqlHelper.DbContext().SC_SiteMeta.Where(s => s.PageId == pageId).FirstOrDefault();
            if (sitemeta.ISValidObject())
            {
                return true;
            }
            return false;
        }
    }
}