﻿using SchoolWebLibrary.Models;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class MasterSectionPartTools
    {
        public static MasterSectionPart GetDetails(string _SectionName)
        {
            return SqlHelper.DbContext().MasterSectionParts.Where(a => a.SectionName == _SectionName).Where(b => b.Activity == true).FirstOrDefault();
        }
    }
}
