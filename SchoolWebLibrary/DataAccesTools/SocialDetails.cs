﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary
{
    public static class SocialDetails
    {
        public static DataTable GetSocialDetails()
        {
            DataTable dt = new DataTable();
            string query = "Select * from SocialDetails where Activity='True' Order by slNo";
            dt = SqlHelper.GetInstance().ExcuteNonQuery(query);
            return dt;
        }

        public static DataTable GetSocialDetails(string SocialMediaName)
        {
            DataTable dt = new DataTable();
            string query = "Select * from SocialDetails where SocialMediaName=@SocialMediaName";
            SqlHelper.MParameterList.Add(new SqlParameter("@SocialMediaName", SocialMediaName));
            dt = SqlHelper.GetInstance().ExcuteNonQuery(query);
            return dt;
        }

        public static string UpdateSocialDetailsQry(string SocialMediaName, bool Activity, string IClass, string AClass, string Tooltip, string SocialLink, object CssStyle, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();
            string query = "update  SocialDetails set  Activity=@Activity, IClass=@IClass, AClass=@AClass, Tooltip=@Tooltip, SocialLink=@SocialLink, CssStyle=@CssStyle  where SocialMediaName=@SocialMediaName";

            ParamList.Add(new SqlParameter("@SocialMediaName", SocialMediaName));
            ParamList.Add(new SqlParameter("@IClass", IClass));
            ParamList.Add(new SqlParameter("@Activity", Activity));
            ParamList.Add(new SqlParameter("@AClass", AClass));
            ParamList.Add(new SqlParameter("@Tooltip", Tooltip));
            ParamList.Add(new SqlParameter("@SocialLink", SocialLink));
            ParamList.Add(new SqlParameter("@CssStyle", CssStyle));

            return query;
        }

        public static void GetSocialName(this DropDownList cmb)
        {
            Dictionary<int, string> cmbDictCls = new Dictionary<int, string>();
            if (cmb.Items.Count > 0)
            {
                cmb.DataSource = null;
            }
            cmbDictCls.Add(-1, "--Select--");
            string query = "Select  Id, SocialMediaName  FROM   SocialDetails order by slNo";
            DataTable dt = SqlHelper.GetInstance().ExcuteNonQuery(query);
            if (dt.IsValidDataTable())
            {
                foreach (DataRow item in dt.Rows)
                {
                    cmbDictCls.Add(int.Parse(item["Id"].ToString()), item["SocialMediaName"].ToString());
                }
                cmb.DataSource = cmbDictCls;
                cmb.DataTextField = "Value";
                cmb.DataValueField = "Key";
                cmb.DataBind();
            }
        }
    }
}