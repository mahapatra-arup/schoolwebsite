﻿using SchoolWebLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary
{
    public static class ThemeDesignTools
    {
        public static void ViewColor(this Page page, string pageName, ref Panel pnl, List<Models.ThemeDesign> _ThemeDesign, bool isRemovePreviousClass, bool isRemovePreviousStyle)
        {
            //Remove Spacial Character
            pageName = Regex.Replace(pageName, @"[^0-9a-zA-Z]+", "_");

            //Remove
            if (isRemovePreviousClass)
            {
                pnl.Attributes.Remove("class");
            }
            if (isRemovePreviousStyle)
            {
                pnl.Attributes.Remove("style");
            }

            //Create
            StringBuilder str = new StringBuilder();
            if (_ThemeDesign.IsValidList())
            {
                string _sectionName = string.Empty;
                foreach (var item in _ThemeDesign)
                {
                    _sectionName = item.SectionName;
                    str.Append(item.AttributeKey + ":" + item.AttributeValue + ";" + Environment.NewLine);
                }

                //fill
                string cssClassName = _sectionName + "_" + pageName;
                page.Header.Controls.Add(
                new LiteralControl(
                @"<style type='text/css'>
                                    ." + cssClassName +
                                    "{" + Environment.NewLine
                                  + str.ToString() +
                                    "}" + Environment.NewLine +
                                    "</style>" + Environment.NewLine

                                         ));
                pnl.CssClass += " " + cssClassName;
            }


        }

        public static List<Models.ThemeDesign> GetDetails(string _SectionName)
        {
            return SqlHelper.DbContext().ThemeDesigns.Where(a => a.SectionName == _SectionName).ToList();
        }

        public static IEnumerable<Models.ThemeDesign> GetDetails_All()
        {
            return SqlHelper.DbContext().ThemeDesigns;
        }
        /// <summary>
        /// Delete Use Section and Attribute Key
        /// </summary>
        /// <param name="Delete_lst"></param>
        /// <param name="Create_lst"></param>
        /// <returns></returns>
        public static bool DeleteAndCreate(List<Models.ThemeDesign> Delete_lst, List<Models.ThemeDesign> Create_lst)
        {

            try
            {
                if (Delete_lst.IsValidList())
                {
                    foreach (var dcDel in Delete_lst)
                    {
                        using (var dc = SqlHelper.DbContext())

                        {
                            var del_lst = dc.ThemeDesigns.Where(del => del.SectionName == dcDel.SectionName).Where(del => del.AttributeKey == dcDel.AttributeKey);

                            if (del_lst.ISValidObject())
                            {
                                dc.ThemeDesigns.RemoveRange(del_lst);

                                dc.SaveChanges();
                            }
                        }
                    }
                }

                //Ctreate
                var context = SqlHelper.DbContext();
                context.ThemeDesigns.AddRange(Create_lst);
                context.SaveChanges();

            }
            catch (DbEntityValidationException ex)
            {
                throw ExceptionUtils.DBEntityValidation(ex);

            }
            return true;
        }

        public static bool Delete(List<Models.ThemeDesign> Delete_lst)
        {

            try
            {
                if (Delete_lst.IsValidList())
                {
                    foreach (var dcDel in Delete_lst)
                    {
                        using (var dc = SqlHelper.DbContext())

                        {
                            var del_lst = dc.ThemeDesigns.Where(del => del.SectionName == dcDel.SectionName).Where(del => del.AttributeKey == dcDel.AttributeKey);

                            if (del_lst.ISValidObject())
                            {
                                dc.ThemeDesigns.RemoveRange(del_lst);
                                dc.SaveChanges();
                            }
                        }
                    }
                }

            }
            catch (DbEntityValidationException ex)
            {
                throw ExceptionUtils.DBEntityValidation(ex);

            }
            return true;
        }

        public static bool IsExistGroup(string AttributeKey)
        {
            var data = SqlHelper.DbContext().ThemeDesigns.Where(s => s.AttributeKey == AttributeKey).FirstOrDefault();

            if (data.ISValidObject())
            {
                return true;
            }
            return false;
        }
        public static bool InsertThemeDesign(ThemeDesign _ThemeDesign)
        {
            if (_ThemeDesign.ISValidObject())
            {
                var db = SqlHelper.DbContext();
                var _Table = db.ThemeDesigns;
                if (_Table.ISValidObject())
                {
                    _Table.Add(_ThemeDesign);
                    db.SaveChanges();
                    return true;
                }
            }
            return false;
        }

    }
}