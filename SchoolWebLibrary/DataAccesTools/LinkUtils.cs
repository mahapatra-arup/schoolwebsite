﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace SchoolWebLibrary
{
    public static class LinkUtils
    {
        // private static List<string> lstQuery = new List<string>();
        private static string QryLink(string link_url, string link_name, byte[] link_image, string link_target,
           string link_description, string link_visible, string link_owner, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO Sc_Links( link_url, link_name, " +
                  "link_image, link_target, link_description,link_visible) " +
                  "VALUES(@link_url, @link_name, " +
                  "@link_image, @link_target, @link_description,@link_visible, @link_owner)";

            ParamList.Add(new SqlParameter("@link_url", link_url));
            ParamList.Add(new SqlParameter("@link_name", link_name));
            ParamList.Add(new SqlParameter("@link_image", link_image));
            ParamList.Add(new SqlParameter("@link_target", link_description));
            ParamList.Add(new SqlParameter("@link_description", link_description));
            ParamList.Add(new SqlParameter("@link_visible", link_visible));
            ParamList.Add(new SqlParameter("@link_owner", link_owner));
            return query;
        }
    }
}