﻿using SchoolWebLibrary.Models;
using System.Linq;

namespace SchoolWebLibrary.DataAccesTools
{
    public class SchoolProfileSettingTool
    {
        public static SchoolProfileSetting GetSchoolProfileSetting()
        {
            return SqlHelper.DbContext().SchoolProfileSettings.FirstOrDefault();
        }
    }
}
