﻿using SchoolWebLibrary.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary
{
    public static class StaffGroupsUtils
    {

        public static string QryInsertGroupUtils(string GroupName, string slNo, bool Activity, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO Staff_Groups(GroupName,slNo,Activity) " +
              "VALUES(@GroupName,@slNo,@Activity)";
            ParamList.Add(new SqlParameter("@GroupName", GroupName));
            ParamList.Add(new SqlParameter("@slNo", slNo));
            ParamList.Add(new SqlParameter("@Activity", Activity));

            return query;
        }

        public static bool UpdateGroups(int groupId, bool Activity, string GroupName)
        {
            if (groupId.ISValidObject())
            {
                try
                {
                    var db = SqlHelper.DbContext();
                    //Get Single course which need to update
                    var objCourse = db.Staff_Groups.Single(u => u.Id == groupId);
                    //Field which will be update
                    objCourse.Activity = Activity;
                    objCourse.GroupName = GroupName;
                    // executes the appropriate commands to implement the changes to the database
                    db.SaveChanges();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException)
                {

                }

            }
            return false;
        }
        public static bool UpdateGroupSlNo(int groupId, int SlnO)
        {
            if (groupId.ISValidObject())
            {
                try
                {
                    var db = SqlHelper.DbContext();
                    //Get Single course which need to update
                    var objCourse = db.Staff_Groups.Single(u => u.Id == groupId);
                    //Field which will be update
                    objCourse.slNo = SlnO;
                    // executes the appropriate commands to implement the changes to the database
                    db.SaveChanges();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException)
                {

                }

            }
            return false;
        }
        /// <summary>
        /// With Out Activity
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Staff_Groups> GetGroupDetailsAll()
        {
            return SqlHelper.DbContext().Staff_Groups.OrderBy(s => s.slNo);
        }
        /// <summary>
        /// only get  Activity equals to True
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Staff_Groups> GetGroupDetails()
        {
            return SqlHelper.DbContext().Staff_Groups.Where(s => s.Activity == true).OrderBy(s => s.slNo);
        }
        public static int GetMaxMenuSlno()
        {
            var data = SqlHelper.DbContext().Staff_Groups;
            if (data.IsValidIEnumerable())
            {
                return data.Max(s => s.slNo).ConvertObjectToInt();
            }
            return 0;
        }
        public static void AddStaffGroup(this DropDownList cmb)
        {
            Dictionary<int, string> cmbDictCls = new Dictionary<int, string>();
            if (cmb.Items.Count > 0)
            {
                cmb.DataSource = null;
            }
            var data = GetGroupDetails();
            if (data.IsValidIEnumerable())
            {
                foreach (var item in data)
                {
                    cmbDictCls.Add(item.Id, item.GroupName);
                }
                cmb.DataSource = cmbDictCls;
                cmb.DataTextField = "Value";
                cmb.DataValueField = "Key";
                cmb.DataBind();
            }
        }

        public static bool IsExistGroup(string groupName)
        {
            var data = SqlHelper.DbContext().Staff_Groups.Where(s => s.GroupName == groupName).FirstOrDefault();

            if (data.ISValidObject())
            {
                return true;
            }
            return false;
        }
    }
}