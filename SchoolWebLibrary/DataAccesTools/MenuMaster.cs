﻿using SchoolWebLibrary.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace SchoolWebLibrary
{
    public static class MenuMaster
    {
        //Defullt parent Properties
        public static string DefultmenuName { get => DefultParents().ISValidObject() ? DefultParents().Title : ""; }
        public static string DefultmenuLink { get => DefultParents().ISValidObject() ? DefultParents().link_url : ""; }

        //Method

        /// <summary>
        /// Defult Parents
        /// </summary>
        /// <returns></returns>
        private static MenuView DefultParents()
        {
            return GetParentMenus().FirstOrDefault();
        }

        public static void AddPagesTitle(this DropDownList cmb)
        {
            Dictionary<string, string> cmbDictCls = new Dictionary<string, string>();
            if (cmb.Items.Count > 0)
            {
                cmb.DataSource = null;
                cmb.Items.Clear();
            }

            var Sc_menuMaster = SqlHelper.DbContext().SC_MenuMaster.ToList();
            if (Sc_menuMaster.IsValidList())
            {
                foreach (var item in Sc_menuMaster)
                {
                    cmbDictCls.Add(item.Module_Id, item.Title);
                }
                cmb.DataSource = cmbDictCls;
                cmb.DataTextField = "Value";
                cmb.DataValueField = "Key";
                cmb.DataBind();
            }
        }

        public static void CmbParentPages(this DropDownList cmb)
        {
            Dictionary<string, string> cmbDictCls = new Dictionary<string, string>();
            if (cmb.Items.Count > 0)
            {
                cmb.DataSource = null;
                cmb.Items.Clear();
            }

            var db = SqlHelper.DbContext().SC_MenuMaster.ToList();
            var Sc_menuMaster = (from MenuView in db
                                 let value = MenuView.Parent_Module_Id
                                 where MenuView.Activity == true
                                 && (value == null || string.IsNullOrEmpty(value.Trim()))
                                 select MenuView).OrderBy(s => s.SlNo).ToList();

            if (Sc_menuMaster.IsValidList())
            {
                foreach (var item in Sc_menuMaster)
                {
                    cmbDictCls.Add(item.Module_Id, item.Title);
                }
                cmb.DataSource = cmbDictCls;
                cmb.DataTextField = "Value";
                cmb.DataValueField = "Key";
                cmb.DataBind();
            }
        }
        public static List<MenuView> GetParentMenus()
        {
            List<MenuView> lstView = new List<MenuView>();
            var db = SqlHelper.DbContext().MenuViews;

            if (db.IsValidIEnumerable())
            {
                lstView = (from MenuView in db
                           let value = MenuView.Parent_Module_Id
                           where MenuView.Activity == true
                           && (value == null || value.Trim() == string.Empty)
                           select MenuView).OrderBy(s => s.SlNo).ToList();

            }

            return lstView;
        }

        public static List<MenuView> GetSubMenus(string _ParentmenuId)
        {
            List<MenuView> lstView = new List<MenuView>();
            var db = SqlHelper.DbContext().MenuViews.ToList();

            if (db.IsValidList())
            {
                lstView = (from MenuView in db
                           let value = MenuView.Parent_Module_Id
                           where MenuView.Activity == true
                           && value == _ParentmenuId
                           select MenuView).OrderBy(s => s.SlNo).ToList();

            }
            return lstView;
        }

        public static string GetModuleIdByVirtualPath(string path)
        {
            MenuView lstView = new MenuView();
            var db = SqlHelper.DbContext().MenuViews;

            if (db.ISValidObject())
            {
                lstView = (from MenuViews in db
                           where MenuViews.link_url == path
                           select MenuViews).FirstOrDefault();

            }
            if (lstView.ISValidObject())
            {
                return lstView.Module_Id;
            }
            return "";

        }

        public static MenuView GetMenuByVirtualPath(string path)
        {
            var db = SqlHelper.DbContext().MenuViews;

            return (from MenuViews in db
                    where MenuViews.link_url == path
                    select MenuViews).FirstOrDefault();


        }
        public static bool IsExistMenu(string PageName)
        {
            SC_MenuMaster sC_MenuMaster = new SC_MenuMaster();
            var db = SqlHelper.DbContext();

            if (db.ISValidObject())
            {
                sC_MenuMaster = (from sd in db.SC_MenuMaster
                                 where sd.Title == PageName
                                 select sd).FirstOrDefault();

            }

            if (sC_MenuMaster.ISValidObject())
            {
                return true;
            }
            return false;
        }

        public static bool IsExistMenu(string PageName, string UpdateModuleId)
        {
            SC_MenuMaster sC_MenuMaster = new SC_MenuMaster();
            var db = SqlHelper.DbContext();

            if (db.ISValidObject())
            {
                sC_MenuMaster = (from sd in db.SC_MenuMaster
                                 where sd.Title == PageName && sd.Module_Id != UpdateModuleId
                                 select sd).FirstOrDefault();
            }

            if (sC_MenuMaster.ISValidObject())
            {
                return true;
            }
            return false;
        }
    }
}
