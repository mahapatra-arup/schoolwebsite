﻿using SchoolWebLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SchoolWebLibrary
{
    public static class NoticeUtils
    {
        #region <-----------Notice--------->
        public static string InsertNoticeQry(string NoticeFileUrl, string Subjects, string Contents, string Footer, string PublishDate, string Publisher, string Slno, bool Activity, object Link_Id, string DateFrom, string DateTo, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "INSERT INTO  Notice(NoticeFileUrl, Subjects, Contents, Footer, PublishDate, Publisher, Slno, Activity,Link_Id,DateFrom,DateTo) " +
            "VALUES(@NoticeFileUrl,@Subjects,@Contents,@Footer,@PublishDate,@Publisher,@Slno,@Activity,@Link_Id,@DateFrom,@DateTo)";

            ParamList.Add(new SqlParameter("@NoticeFileUrl", NoticeFileUrl));
            ParamList.Add(new SqlParameter("@Subjects", Subjects));
            ParamList.Add(new SqlParameter("@Contents", Contents));
            ParamList.Add(new SqlParameter("@Footer", Footer));
            ParamList.Add(new SqlParameter("@PublishDate", PublishDate));
            ParamList.Add(new SqlParameter("@Publisher", Publisher));
            ParamList.Add(new SqlParameter("@Slno", Slno));
            ParamList.Add(new SqlParameter("@Activity", Activity));
            ParamList.Add(new SqlParameter("@Link_Id", Link_Id));
            ParamList.Add(new SqlParameter("@DateFrom", DateFrom));
            ParamList.Add(new SqlParameter("@DateTo", DateTo));
            return query;
        }

        public static string UpdateNoticeQry(long Id, string Subjects, string Contents, string Footer, string PublishDate, string Publisher, bool Activity, object Link_Id, string DateFrom, string DateTo, out List<SqlParameter> ParamList)
        {
            ParamList = new List<SqlParameter>();

            string query = "Update  Notice set Subjects=@Subjects, Contents=@Contents, Footer=@Footer, PublishDate=@PublishDate, Publisher=@Publisher, Activity=@Activity,Link_Id=@Link_Id,DateFrom=@DateFrom,DateTo=@DateTo " +
            " where Id=@Id";
            ParamList.Add(new SqlParameter("@Subjects", Subjects));
            ParamList.Add(new SqlParameter("@Contents", Contents));
            ParamList.Add(new SqlParameter("@Footer", Footer));
            ParamList.Add(new SqlParameter("@PublishDate", PublishDate));
            ParamList.Add(new SqlParameter("@Publisher", Publisher));
            ParamList.Add(new SqlParameter("@Id", Id));
            ParamList.Add(new SqlParameter("@Activity", Activity));
            ParamList.Add(new SqlParameter("@Link_Id", Link_Id));
            ParamList.Add(new SqlParameter("@DateFrom", DateFrom));
            ParamList.Add(new SqlParameter("@DateTo", DateTo));
            return query;
        }


        public static bool UpdateNoticeFilePath(long id, string NoticeFileUrl)
        {
            SqlHelper.MParameterList = new List<SqlParameter>();
            string Query = "Update Notice set NoticeFileUrl=@NoticeFileUrl  where  id=@id";
            SqlHelper.MParameterList.Add(new SqlParameter("@id", id));
            SqlHelper.MParameterList.Add(new SqlParameter("@NoticeFileUrl", NoticeFileUrl));

            return SqlHelper.GetInstance().ExcuteQuery(Query);
        }

        public static bool DeleteNotice(string Id)
        {
            string Query = "Delete FROM  Notice where  Id=@Id";
            SqlHelper.MParameterList.Add(new SqlParameter("@Id", Id));
            bool obj = SqlHelper.GetInstance().ExcuteQuery(Query);
            if (obj)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region <------------Get----------->
        public static int GetMaxNoticeSlno()
        {
            var db = SqlHelper.DbContext().Notices;
            int slno = 0;
            if (db.ISValidObject())
            {
                slno = ((from notice in db
                         select notice).Max(s => s.Slno)).ConvertObjectToInt();
            }
            return slno;
        }

        /// <summary>
        /// ///get notice between date
        /// </summary>

        public static List<Notice> GetNoticeBetweenDate()
        {
            var db = SqlHelper.DbContext().Notices;
            List<Notice> lstntc = new List<Notice>();
            if (db.ISValidObject())
            {
                lstntc = (from notice in db
                          where (notice.DateFrom <= DateTime.Today.Date) && (notice.DateTo >= DateTime.Today.Date)
                          select notice).OrderByDescending(p => p.PublishDate).ThenBy(q => q.Slno).ToList();
            }

            return lstntc;
        }

        public static List<Notice> GetAllNotice()
        {
            var db = SqlHelper.DbContext().Notices;
            List<Notice> lstntc = new List<Notice>();
            if (db.ISValidObject())
            {
                lstntc = (from notice in db
                          select notice).OrderByDescending(p => p.PublishDate).ThenBy(q => q.Slno).ToList();
            }

            return lstntc;
        }
        public static Notice GetNotice(long id)
        {
            var db = SqlHelper.DbContext().Notices;
            Notice lstntc = new Notice();
            if (db.ISValidObject())
            {
                lstntc = (from notice in db
                          where notice.Id == id
                          select notice).FirstOrDefault();
            }

            return lstntc;
        }

        public static string GetNoticeFilePath(long Id)
        {
            var notc = GetNotice(Id);
            if (notc.ISValidObject())
            {
                return notc.NoticeFileUrl;
            }
            return string.Empty;
        }

        #endregion
    }
}