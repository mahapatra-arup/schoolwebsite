﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web/Site1.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="SchoolWebApplication.Web.ContactUs" %>
<%@ Register Src="~/Web/UserControl/UC_Form_Header_Label.ascx" TagPrefix="uc1" TagName="UC_Form_Header_Label" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
  
    <uc1:UC_Form_Header_Label runat="server" id="UC_Form_Header_Label" />
 
  
    <asp:Panel ID="pnlContect" runat="server" class="container">
        <div class="row">
                <%-- Body --%>
                <div class="panel panel-success">
                    <div class="panel-heading" >Contect Details</div>
                    <div class="panel-body">
                        
                            <%-- Map Details --%>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <asp:PlaceHolder ID="PlaceHolderMap" runat="server"></asp:PlaceHolder>
                                </div>
                            </div>
                            <%-- //Map Details --%>

                            <%-- Contect Details --%>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <%-- Save Messege --%>
                                <asp:Literal ID="ltMessege" runat="server"></asp:Literal>
                                <div class="form-group">
                                    <%-- Name --%>

                                    <label for="txtName">Name :</label>
                                    <asp:TextBox ID="txtName" class="form-control" placeholder="Enter name.." runat="server"></asp:TextBox>

                                    <br />
                                    <%-- Email --%>
                                    <label for="txtEmail">Email Address : </label>
                                    <asp:TextBox ID="txtEmail" class="form-control" placeholder="Enter Email Address.." runat="server"></asp:TextBox>

                                    <%-- Validator --%>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorMail" runat="server" ErrorMessage="Invalid Email" ControlToValidate="txtEmail" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="ContectSend"></asp:RegularExpressionValidator>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorGroup" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Email is require" ForeColor="Red" ValidationGroup="ContectSend" SetFocusOnError="True"></asp:RequiredFieldValidator>

                                    <br />
                                    <%-- Subjects --%>
                                    <label for="pwd">Subject : </label>
                                    <asp:TextBox ID="txtSubject" class="form-control" placeholder="Enter Subject.." runat="server"></asp:TextBox>
                                    <br />
                                    <%-- Message --%>

                                    <label for="txtMessage">Message : </label>
                                    <asp:TextBox ID="txtMessage" class="form-control" placeholder="Enter Message.." Rows="5" runat="server" TextMode="MultiLine"></asp:TextBox>

                                    <%--// Button--%>
                                    <div class="text-right w3-padding">
                                        <asp:Button ID="btnSend" class="w3-button w3-red w3-animate-right" runat="server" Text="Send" OnClick="btnSend_Click"
                                            ValidationGroup="ContectSend"></asp:Button>

                                    </div>

                                </div>
                            </div>
                            <%-- //Contect Details --%>
                      
                    </div>
                </div>
                <%-- End of Body --%>

        </div>
    </asp:Panel>
</asp:Content>
