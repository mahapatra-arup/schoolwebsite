<%@ Page Title="" Language="C#" MasterPageFile="~/Web/Site1.Master" AutoEventWireup="true" CodeBehind="EmpDetails.aspx.cs" Inherits="SchoolWebApplication.Web.EmpDetails" %>

<%@ Import Namespace="SchoolWebLibrary" %>
<%@ Register Src="~/Web/UserControl/UC_Form_Header_Label.ascx" TagPrefix="uc1" TagName="UC_Form_Header_Label" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <!--Page css -->
    <asp:PlaceHolder runat="server">
        <%: Styles.Render("~/Content/css/Staff") %>
    </asp:PlaceHolder>
    <!-- css -->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">


    <!-- ==Headere Label=== -->
    <uc1:UC_Form_Header_Label runat="server" ID="UC_Form_Header_Label" />
    <!-- ==Headere Label=== -->

    <asp:Panel ID="pnl_EmpDetails" runat="server">

        <div class="container">
            <div class="row">

                <!-- Module Part -->
                <div class="col-sm-3 col-md-3">
                    <!-- -------Modules------- -->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-th"></span>&nbsp;Modules</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">

                            <div class="panel-body">

                                <!-- ==Colupsable==-->
                                <div id="accordion_staff">

                                    <% 
                                        //===Group====
                                        var _StaffGroup = StaffGroupsUtils.GetGroupDetails(); if (_StaffGroup.IsValidIEnumerable())
                                        {
                                            foreach (var _StaffGroupitem in _StaffGroup)
                                            {
                                                int i = 0;
                                    %>

                                    <h6><%=_StaffGroupitem.GroupName %></h6>
                                    <div style="padding: 10px !important; text-align: center !important">
                                        <!-- Tab Button -->
                                        <ul class="nav nav-pills">
                                            <%
                                                var _mSubGroup = StaffSubGroupsUtil.GetSubGroupDetails(_StaffGroupitem.Id); if (_mSubGroup.IsValidIEnumerable())
                                                {
                                                    foreach (var _mSubGroupitem in _mSubGroup)
                                                    {
                                            %>
                                            <li class='<%=i==0?"active":"" %>'><a data-toggle="pill" class="small" href='#<%=_mSubGroupitem.SubGroupName.ReplaceStrSpecialChar("_") %>'><%=_mSubGroupitem.SubGroupName %></a></li>
                                            <%
                                                        i++;
                                                    }
                                                }
                                            %>
                                        </ul>
                                    </div>

                                    <%
                                            }
                                        }
                                    %>
                                </div>
                                <!--==//End Colupsable== -->
                            </div>

                        </div>
                    </div>
                    <!-- -------Modules------- -->
                </div>



                <!--------- View Part----- -->
                <div class="col-sm-9 col-md-9">

                    <div class="tab-content">
                        <!--------//Start SubGroup--------->
                        <% 
                            var _StaffSubGroup = StaffSubGroupsUtil.GetSubGroupDetails(); if (_StaffSubGroup.IsValidIEnumerable())
                            {
                                int j = 0;
                                foreach (var _StaffSubGroupitem in _StaffSubGroup)
                                {
                        %>
                        <div id='<%=_StaffSubGroupitem.SubGroupName.ReplaceStrSpecialChar("_") %>' class='<%=j==0?"tab-pane fade in active":"tab-pane fade" %>'>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <span class="glyphicon glyphicon-user text-primary"></span>
                                    <%=_StaffSubGroupitem.SubGroupName %>
                                </div>

                                <div class="panel-body">
                                    <% 
                                        var _StaffDetails = StaffUtils.GetStaffDetailsBySubGroup(_StaffSubGroupitem.Id); if (_StaffDetails.IsValidList())
                                        {
                                            var _notaAvailableImage = "../web/images/NotAvailable_Img.png";
                                            foreach (var _StaffItem in _StaffDetails)
                                            {
                                                var image_path = (!_StaffItem.ImagePath.ISNullOrWhiteSpace()) ? _StaffItem.ImagePath : _notaAvailableImage;
                                    %>
                                    <!-- Team member -->
                                    <div class="col-md-4 w3-margin-bottom">

                                        <div class="flip-card">
                                            <div class="flip-card-inner">

                                                <div class="flip-card-front">

                                                    <%--  Color Sesign --%>
                                                    <p class="bg-danger" style="width: 100%; height: 10px; background: linear-gradient(#bbed69,#5895ea)">.</p>

                                                    <%-- Body --%>
                                                    <div class=" text-center" style="background: linear-gradient(#f7f0f0 95%,#ee9a16 20%,#ffd800)">
                                                        <img class="w3-card img-responsive img-rounded" src='<%=image_path %>' alt="Staff image" style="max-height: 80px; max-width: 80px; display: block; margin-left: auto; margin-right: auto;">
                                                        <var>
                                                            <h6 class="">
                                                                <%= _StaffItem.EmpCode %>
                                                            </h6>
                                                            <h6>
                                                                <%=_StaffItem.EmpName %>
                                                            </h6>
                                                            <h6>
                                                                <%=!_StaffItem.designation.ISNullOrWhiteSpace()?_StaffItem.designation:"-" %>
                                                            </h6>
                                                        </var>
                                                        <br />
                                                    </div>

                                                </div>

                                                <div class="flip-card-back">

                                                    <%-- top Color Sesign --%>
                                                    <div>
                                                        <p class="bg-danger" style="vertical-align: top; top: 0; width: 100%; height: 5px; background: linear-gradient(#808080,#ffd800)">.</p>
                                                        <h6><%=_StaffItem.EmpName %></h6>
                                                        <%--  Color Sesign --%>
                                                        <p class="bg-danger" style="width: 100%; height: 5px; background: linear-gradient(#ffd800,#007b5e)">.</p>
                                                    </div>


                                                    <%-- Body --%>
                                                    <div class="text-left small w3-padding-small pre-scrollable">
                                                        <p>
                                                            <var>
                                                                Subjects : <%=_StaffItem.Subjects %>
                                                                <br />
                                                                Educational Qualification : <%=_StaffItem.EducationalQualification %>
                                                                <br />
                                                                Address :  <%=_StaffItem.Address %> %>
                                                                <br />
                                                                Description : <%=_StaffItem.Description %>
                                                            </var>
                                                        </p>


                                                        <div>
                                                            <%-- Contect Details --%>
                                                            <ul class="list-inline">
                                                                <%-- phone no --%>
                                                                <li class="list-inline-item">
                                                                    <a class="social-icon text-xs-center" href="#">
                                                                        <i class="fa fa-mail-reply">&nbsp; <%=_StaffItem.Email %></i></a></li>
                                                                <%-- ph no --%>
                                                                <li class="list-inline-item">
                                                                    <a class="social-icon text-xs-center" href="#">
                                                                        <i class="fa fa-phone">&nbsp; <%=_StaffItem.PhNo %></i></a></li>

                                                            </ul>

                                                        </div>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                    <!-- ./Team member -->

                                    <%}
                                        } %>
                                </div>
                            </div>

                        </div>
                        <% 
                                    j++;
                                }
                            } %>
                        <!--------//End SubGroup--------->
                    </div>

                </div>
                <!-- View Part -->
            </div>
        </div>


    </asp:Panel>

    <!-- Script -->
    <script>
        $(function () {
            $("#accordion_staff").accordion({
                heightStyle: "content"
            });
        });
    </script>
</asp:Content>
