﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Footer.ascx.cs" Inherits="SchoolWebApplication.Web.UserControl.UC_Footer" %>

 <!--Page css -->
    <asp:PlaceHolder runat="server">
        <%: Styles.Render("~/Content/css/UC_Footer") %>
    </asp:PlaceHolder>
    <!-- css -->

<!--========================= footer=================== -->
<asp:Panel runat="server" ID="pnlFooterSection" class="footer">
    <div class="w3-container">
        <div class="w3-row agileinfo_footer_grids">

            <!--== SocialDetails with Visitors ===-->
            <div class="w3-col m4  agileinfo_footer_grid">

                <div class="agileinfo-social-grids">
                    <h3>Social Details</h3>
                    <asp:PlaceHolder ID="phSocialDetails" runat="server"></asp:PlaceHolder>

                </div>

                <%-- ===Online suer /Visitor/Total User=== --%>
                <div class="">
                    <asp:Literal ID="ltrNoOfVisitor" runat="server"></asp:Literal>
                </div>
                <%-- ===Online suer /Visitor/Total User=== --%>
            </div>
            <!--== SocialDetails ===-->

            <!--== Address Details== -->
            <div class="w3-col m4 agileinfo_footer_grid">
                <asp:HiddenField ID="hfAddress" runat="server" Visible="False" />
                <asp:HiddenField ID="hfEmail" runat="server" Visible="False" />
                <asp:HiddenField ID="hfPhNo" runat="server" Visible="False" />
                <h3>Contact Info</h3>
                <ul class="agileinfo_footer_grid_list ">
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i><%:hfAddress.Value %></li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:<%:hfEmail.Value %>"><%:hfEmail.Value %></a></li>
                    <li> <i class="glyphicon glyphicon-earphone" aria-hidden="true"></i> <%:hfPhNo.Value %></li>
                     <li><i class="fa fa-user" aria-hidden="true"></i><a href="../sp-admin" target="_blank">Log in</a></li>
                </ul>
            </div>
            <!--== Address Details== -->

            <!-- ======Useful link===== -->
            <div class="w3-col m4">
                <div class="agileinfo_footer_grid">
                    <h3>Useful Links</h3>
                </div>
                <ul class="list-unstyled">
                    <%-- 1 --%>
                    <li class="w3-padding-small"><a href="http://www.wbsed.gov.in/wbsed/welcome.html" target="_blank" class="sm-link sm-link_text sm-link8 " data-sm-link-text="🖎School Education Department">
                        <p class="sm-link__label w3-hover-text-blue">🖎&nbsp;School Education Department</p>
                    </a></li>

                    <%-- 2 --%>
                    <li class="w3-padding-small"><a href="http://www.wbbse.org/" target="_blank" class="sm-link sm-link_text sm-link8" data-sm-link-text="🖎West Bengal Board of Secondary Education">
                        <p class="sm-link__label w3-hover-text-blue">🖎&nbsp;West Bengal Board of Secondary Education</p>
                    </a></li>

                    <%-- 3 --%>
                    <li class="w3-padding-small"><a href="http://wbchse.nic.in/html/index.html" target="_blank" class="sm-link sm-link_text sm-link8" data-sm-link-text="🖎West Bengal Council of Higher Secondary">
                        <p class="sm-link__label w3-hover-text-blue">🖎&nbsp;West Bengal Council of Higher Secondary</p>
                    </a></li>

                    <%-- 4 --%>
                    <li class="w3-padding-small"><a href="http://www.wbkanyashree.gov.in/" target="_blank" class="sm-link sm-link_text sm-link8" data-sm-link-text="🖎Kanyashree">
                        <p class="sm-link__label  w3-hover-text-blue">🖎&nbsp;Kanyashree</p>
                    </a></li>

                    <%-- 5 --%>
                    <li class="w3-padding-small"><a href="http://www.oasis.gov.in/" target="_blank" class="sm-link sm-link_text sm-link8" data-sm-link-text="🖎Online Oasis Scholarship">
                        <p class="sm-link__label w3-hover-text-blue">🖎&nbsp;Online Oasis Scholarship</p>
                    </a></li>
                </ul>

            </div>
            <!-- ======Useful link===== -->

        </div>
    </div>
</asp:Panel>

<!-- ===Devloper Signature==== -->
<div class="w3-container w3-card w3-animate-bottom w3-black border border-primary">
    <div class="col-md-12 text-right">
        <%--== Devloper Signature== --%>
        <asp:Label ID="lblFooter" runat="server" Text=""></asp:Label>
    </div>
</div>
<!--========================= //footer================== -->



