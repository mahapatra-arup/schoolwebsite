﻿<%@ Control Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="UC_Form_BrackingNews.ascx.cs" Inherits="SchoolWebApplication.Web.UserControl.UC_Form_BrackingNews" %>

 <!--Page css -->
    <asp:PlaceHolder runat="server">
        <%: Styles.Render("~/Content/css/UC_Form_BrackingNews") %>
    </asp:PlaceHolder>
    <!-- css -->
 
<asp:Panel runat="server" ID="pnlLatestNews" class="row" style="height: 35px; background:linear-gradient(#ffffff,#a5edf2);">

                    <%-- Lateest News Font Design--%>
                    <div class="col-md-2" >
                        <div class="graybox bold">
                            <div class="latest-news">
                                Latest News <span class="left-arrow">&nbsp;</span>
                            </div>
                        </div>

                    </div>
                   

                    <div class="col-md-10" style="height: 35px; background:linear-gradient(#ffffff,#a5edf2);">

                        <%-- News Viw fade in and Out and Change Script--%>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                jQuery('.news li:gt(0)').hide();
                                setInterval(function () { jQuery('.news li:first-child').fadeOut().next('li').fadeIn().end().appendTo('.news'); }, 3000);
                            });
                        </script>
                        <%-- News Viw fade in and Out Change Script--%>

                        <%-- News --%>
                        <ul class="news">
                            <asp:Repeater ID="Repeater_News" runat="server" OnItemCommand="NewsRepeater_ItemCommand">
                                <ItemTemplate>
                                    <li style="overflow: hidden; display: list-item;">
                                        <span class="w3-text-green">
                                            <asp:Button ID="newsPath" CommandName="newsId" CommandArgument='<%#Eval("Id") %>' runat="server" OnClick="newsPath_Click" Text='<%#Eval("post_title") %>' CssClass="btn-link"></asp:Button>
                                        </span></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>

                        <%-- News --%>
                    </div>

                </asp:Panel>

  <%-- =================Modal View================= --%>
    <asp:Panel ID="PanelNewsView002" runat="server" CssClass="w3-container">
        <%-- =================Event and Post Modal================= --%>
        <div id="id01Event" class="w3-modal">
            <br />
            <div class="w3-modal-content w3-animate-opacity w3-card-4">

                <%-- == Header=== --%>
                <header class="w3-container w3-teal">

                    <span onclick="document.getElementById('id01Event').style.display='none'"
                        class="btn w3-display-topright">&times;</span>

                    <%-- Post Title --%>
                    <p>
                        <asp:Label ID="lblposttitle" runat="server" Text=""></asp:Label>
                    </p>
                </header>
                <%-- ==//Header=== --%>

                <%-- ==Body== --%>
                <div class="w3-container">
                    <asp:Label ID="lblPublishDate" CssClass="pull-right w3-text-blue" runat="server" Text=""></asp:Label>

                    <div class="row">
                        <%-- Image --%>
                        <asp:Panel ID="Pnlimgview" runat="server" CssClass="col-md-3">
                            <asp:Image ID="imgImage" CssClass="img-thumbnail img-responsive" runat="server" />
                        </asp:Panel>

                        <%-- Text  Body content--%>
                        <asp:Panel ID="Pnlcontetview" runat="server" CssClass="col-md-9">
                            <p>
                                <asp:Label ID="lblPostContent" runat="server" CssClass="pull-left" Text=""></asp:Label>
                            </p>
                        </asp:Panel>
                    </div>

                    <%-- Post Author --%>
                    <p>
                        <asp:Label ID="lblpostAuthor" CssClass="pull-right w3-text-blue-gray" runat="server" Text=""></asp:Label>
                    </p>

                </div>
                <%-- ==Body== --%>

                <%-- ===Footer ==--%>
                <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                    <button onclick="document.getElementById('id01Event').style.display='none'" type="button" class="w3-button w3-red">Cancel</button>

                    <%--File Download Link --%>
                    <asp:Literal ID="lblDownloadLink" runat="server"></asp:Literal>
                </div>
                <%-- ===//Footer ==--%>
            </div>
        </div>
        <%-- =================Event and Post Modal================= --%>
    </asp:Panel>
    <%-- =================Modal View================= --%>