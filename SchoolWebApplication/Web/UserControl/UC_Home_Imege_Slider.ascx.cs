﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;

namespace SchoolWebApplication.Web.UserControl
{
    public partial class UC_Home_Imege_Slider : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FillRepeater_Carousel();
        }

        private void FillRepeater_Carousel()
        {
            var dt = GalleryUtils.GetGalleryByGroup("navcarouselImage");
            if (dt.IsValidList())
            {
                Repeater_Home_Slider.DataSource = dt;
                Repeater_Home_Slider.DataBind();
            }
        }
    }
}