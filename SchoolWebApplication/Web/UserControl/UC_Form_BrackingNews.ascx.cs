﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SchoolWebApplication.Web.UserControl
{
    public partial class UC_Form_BrackingNews : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillBrackingNews();
            }
        }

        #region <---------------News--------------->
        /// <summary>
        ///   News means Post
        /// </summary>
        private void FillBrackingNews()
        {
            DataTable dt = PostUtils.GetTop10PostDetails(PostUtils._PostType.Post);
            if (dt.IsValidDataTable())
            {
                Repeater_News.DataSource = dt;
                Repeater_News.DataBind();
            }
            else
            {
                //Response.Write("Events are Not Available");
            }
        }

        protected void NewsRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "newsId")
                {
                    lblDownloadLink.Text = "";
                    string vlu = Convert.ToString(e.CommandArgument);
                    DataTable dt = PostUtils.GetPostDetailsById(vlu);
                    if (dt.IsValidDataTable())
                    {
                        lblposttitle.Text = Convert.ToString(dt.Rows[0]["post_title"]);
                        lblPostContent.Text = Convert.ToString(dt.Rows[0]["post_content"]);
                        lblpostAuthor.Text = Convert.ToString(dt.Rows[0]["post_author"]);
                        lblPublishDate.Text = Convert.ToDateTime(dt.Rows[0]["Publish_Date"]).ToString("dd-MMM-yyyy");
                        imgImage.ImageUrl = Convert.ToString(dt.Rows[0]["Post_ImgPath"]);
                        imgImage.ImageAlign = ImageAlign.Left;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        protected void newsPath_Click(object sender, EventArgs e)
        {
            //call Modal
            ScriptManager.RegisterStartupScript(this, GetType(), "myeventModal", "document.getElementById('id01Event').style.display = 'block';", true);
        }
        #endregion

    }
}