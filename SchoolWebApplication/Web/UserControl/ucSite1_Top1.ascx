﻿<%@ Control Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="ucSite1_Top1.ascx.cs" Inherits="SchoolWebApplication.Web.UserControl.ucSite1_Top1" %>
<!--Page css -->
<asp:PlaceHolder runat="server">
    <%: Styles.Render("~/Content/css/ucSite1_Top1") %>
</asp:PlaceHolder>
<!-- css -->

<%-- ----Top Section---- --%>
<asp:Panel ID="pnlTopSection" CssClass="text-center " runat="server" Style="background: linear-gradient(#093453,#306262); margin: 0px; padding: 0px; border: 0px">
    <asp:PlaceHolder ID="phSchoolDetails" runat="server"></asp:PlaceHolder>
</asp:Panel>
<%-- ----Top Section---- --%>