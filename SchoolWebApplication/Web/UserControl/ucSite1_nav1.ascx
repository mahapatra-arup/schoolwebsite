﻿<%@ Control Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="ucSite1_nav1.ascx.cs" Inherits="SchoolWebApplication.Web.UserControl.ucSite1_nav1" %>


<script type="text/javascript">
    //window.onscroll = function () { myFunction() };

    //var navbar = document.getElementById("navbar");
    //var sticky = navbar.offsetTop;

    //function myFunction() {
    //    if (window.pageYOffset >= sticky) {
    //        navbar.classList.add("sticky")
    //    } else {
    //        navbar.classList.remove("sticky");
    //    }
    //}
</script>

<!--Page css -->
<%-- Stiky Generate --%>
<style type="text/css">
    .sticky {
        z-index: 9999;
        position: fixed;
        top: 0;
        width: 100%;
    }
        .sticky + .content {
            padding-top: 60px;
        }
</style>
<asp:PlaceHolder runat="server">
    <%: Styles.Render("~/Content/css/ucSite1_nav1") %>
</asp:PlaceHolder>
<!-- css -->


<%-- ----Navigation Ber---- --%>
<div class="header" id="navbar">
    <asp:Panel runat="server" class="container-fluid row" ID="pnlNavigationSection">
        <nav class="navbar-inverse">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!--== Collect the nav links, forms, and other content for toggling ==-->
            <div class="collapse navbar-collapse text-center" id="bs-example-navbar-collapse-1">


                <%--ref: https://www.aspsnippets.com/Articles/Database-driven-ASPNet-Menu-control-Populating-Menu-items-from-the-Database-in-ASPNet-using-C-and-VBNet.aspx --%>
                <asp:Menu ID="HomeNavBer" runat="server" Orientation="Horizontal" RenderingMode="List"
                    StaticMenuStyle-CssClass="nav navbar-nav navbar-right" DynamicMenuStyle-CssClass="dropdown-menu"
                    IncludeStyleBlock="false">
                    
                </asp:Menu>
                
            </div>
        </nav>
    </asp:Panel>
</div>
<%-- ----Navigation Ber---- --%>


<!-- Page js================ -->
<asp:PlaceHolder runat="server">
    <%: Scripts.Render("~/bundles/ucSite1_nav1") %>
</asp:PlaceHolder>
<!-- Page js================ -->