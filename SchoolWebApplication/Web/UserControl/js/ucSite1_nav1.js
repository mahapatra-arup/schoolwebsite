﻿//*================sticky===================*/
window.onscroll = function () { mystickynav(); };

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function mystickynav() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
    } else {
        navbar.classList.remove("sticky");
    }
}
//*=====================sticky=================



//<% -- ==============//Selected click navigation Button========== --%>
//ref:https://www.aspsnippets.com/Articles/Database-driven-ASPNet-Menu-control-Populating-Menu-items-from-the-Database-in-ASPNet-using-C-and-VBNet.aspx
//Disable the default MouseOver functionality of ASP.Net Menu control.
// only click then show sub menu /hover time not show sub menu
//Sys.WebForms.Menu._elementObjectMapper.getMappedObject = function () {
//    return false;
//};

$(function () {
    //*vvi*
    //====Remove the style attributes. because it is show bydefult==== 
    $("#HomeNavBer,#HomeNavBer li, #HomeNavBer a, #HomeNavBer ul").removeAttr("style");
    //====Remove the style attributes. because it generate By defult====

    //=================Add button Hover Style===================
    $("#HomeNavBer a").addClass("hvr-sweep-to-bottom");

    //====Apply the Bootstrap class to the SubMenu.====
    $(".dropdown-menu").closest("#HomeNavBer li").removeClass().addClass("dropdown-toggle");

    //===Apply the Bootstrap properties to the SubMenu.===ADD ARROW on Menu
    //Ref:https://stackoverflow.com/questions/5995628/adding-attribute-in-jquery#5995650
    $("#HomeNavBer .dropdown-toggle").find("a.popout").attr("data-toggle", "dropdown").attr("aria-haspopup", "true").attr("aria-expanded", "false").attr("role", "button").append("<span class='caret'></span>");
    //---------------------------------------------------------------------------

    //====Apply the Bootstrap "active" class to the selected Menu item.====
    $("a.selected").closest("#HomeNavBer li").addClass("active");
    $("a.selected").closest("#HomeNavBer .dropdown-toggle").addClass("active");

});
//<% -- ==============//Selected click navigation Button========== --%>
