﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace SchoolWebApplication.Web.UserControl
{
    public partial class UC_Footer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Fill
            FillFooterAndSchoolDetailsWithColor();
            FillSocialDetails();
        }

        private void FillColor()
        {

            //--==Body==--
            var lst = ThemeDesignTools.GetDetails(_SectionEnum._EnumOfSection.FOOTER.GetEnumName());
            if (lst.IsValidList())
            {
                ThemeDesignTools.ViewColor(Page, this.GetCurrentPageName(), ref pnlFooterSection, lst, false, true);
            }
        }

        #region <===========    Fill   ============>
        private void FillFooterAndSchoolDetailsWithColor()
        {


            //Color
            FillColor();

            SchoolProfile _SchoolDetails = SchoolUtils.GetSchoolDetails();
            //Details
            if (_SchoolDetails.ISValidObject())
            {
                hfAddress.Value = _SchoolDetails.At + ",\n" + _SchoolDetails.PO + ",\n" + _SchoolDetails.PIN;
                hfEmail.Value = Server.HtmlDecode(_SchoolDetails.Email);
                hfPhNo.Value = Server.HtmlDecode(_SchoolDetails.Ph);
            }


            DevloperSignature _DevloperSignature = SqlHelper.DbContext().DevloperSignatures.FirstOrDefault();
            if (_DevloperSignature.ISValidObject())
            {
                //Devloper Signture
                lblFooter.Text = "<p>" + _DevloperSignature.Copyright + "" + _DevloperSignature.KeyWords +
                  "<a href=" + _DevloperSignature.link + " target=_blank> " + _DevloperSignature.Name + " </a> </p>";

            }

            //Online user
            ltrNoOfVisitor.Text = WebsiteAnalyticsTools.GetWebsiteAnalyticsTrackingId();
        }

        private void FillSocialDetails()
        {
            DataTable dt = SocialDetails.GetSocialDetails();
            StringBuilder sb = new StringBuilder();
            if (dt.IsValidDataTable())
            {
                sb.Append("<ul> \r\n");
                foreach (DataRow item in dt.Rows)
                {
                    //Image Ico Class
                    string iClass = item["IClass"].ConvertObjectToString();
                    string aClass = item["AClass"].ConvertObjectToString();

                    string name = item["SocialMediaName"].ConvertObjectToString();
                    string links = item["SocialLink"].ConvertObjectToString();
                    string ToolsTrip = item["Tooltip"].ConvertObjectToString();

                    sb.Append("<li title=\"" + ToolsTrip + "\"><a href =\"" + links + "\" ><i class=\"" + iClass + "\" aria-hidden=\"true\"></i>&nbsp;</a></li>");

                }
                sb.Append("</ul>");
            }

            phSocialDetails.Controls.Add(new LiteralControl(sb.ToString()));
        }

        #endregion
    }
}