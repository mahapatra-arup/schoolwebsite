﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Web.UI;

namespace SchoolWebApplication.Web.UserControl
{
    public partial class UC_Form_Header_Label : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Fill();
            }
        }

        private void Fill()
        {
            string contanerpage = Page.AppRelativeVirtualPath.ToString().Replace(".aspx", "");
            var menuDetails = MenuMaster.GetMenuByVirtualPath(contanerpage);
            if (menuDetails.ISValidObject())
            {
                Heading_title_div.InnerText = menuDetails.Title.Substring(0, 1);
                lblheadertitle0025.InnerText = menuDetails.Title.Substring(1);
            }
        }
    }
}