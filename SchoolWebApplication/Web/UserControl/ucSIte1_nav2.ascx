﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucSIte1_nav2.ascx.cs" Inherits="SchoolWebApplication.Web.UserControl.ucSIte1" %>
<%@ Import Namespace="SchoolWebLibrary" %>

<!-- Start css3menu.com HEAD section -->
<link href="../Web/UserControl/css/ucSite1_nav2.css" rel="stylesheet" />
<%--/*====Line Hight and Height Set====*/--%>
<style type="text/css">
    .li_nav2_height_LineHight {
        height: 35px;
        line-height: 19px;
         cursor: pointer;
    }
</style>
<%--/*====Line Hight and Height Set====*/--%>


<!-- End css3menu.com HEAD section -->
<asp:Panel runat="server" ID="pnlNavigationSection001" CssClass="nav2_Panel_Dir" Direction="RightToLeft"><%--//Direction sety--%>
    <!-- Start css3menu.com BODY section -->
    <input type="checkbox" id="css3menu-switcher" class="c3m-switch-input">
    <ul id="css3menu100" class="topmenu">

        <%-- ==Mobile Version 3 Roll Line== --%>
        <li class="switch" style="height: 40px">
            <label onclick="" for="css3menu-switcher"></label>
        </li>


        <% foreach (var _Parentdt in MenuMaster.GetParentMenus())
            {
                Image img001 = System_Web_UI_WebControls_Imager.byteArrayToWebImage(_Parentdt.Image); %>


        <%--================== Child Or not check======= --%>
        <% if (_Parentdt.IsChild.ConvertObjectToBool() == true)
            { %>

         <%--===== Parent Module with  child===== --%>
        <li class="topmenu"><a href='<%=_Parentdt.link_url %>' target="_self" title='<%=_Parentdt.ToolsTrip %>' class="li_nav2_height_LineHight"><span><%=_Parentdt.Title %></span></a>
            <ul>

                <%-- Child Module loop --%>
                <% foreach (var _SubMenus in MenuMaster.GetSubMenus(_Parentdt.Module_Id))
                    {
                        Image img002 = System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SubMenus.Image);%>
                <li class="subfirst"><a href="<%=_SubMenus.link_url.Replace("~","..") %>" target="_self" title='<%=_SubMenus.ToolsTrip %>'>
                     <img src='<%=img002.ISValidObject()?img002.ImageUrl:"" %>' alt="" /><span><%=_SubMenus.Title %></span></a>
                    <%-- <ul>
                        <li class="subfirst"><a href="#">Administration 1 0</a></li>
                    </ul>--%>
                </li>
                <% } %>
            </ul>
        </li>
         <%--===== Parent Module with  child===== --%>

        <% }
            else
            { %>

        <%--===== Parent Module with out child===== --%>
        <li class="topfirst"><a class="pressed li_nav2_height_LineHight" href='<%=_Parentdt.link_url.Replace("~","..") %>' target="_self" title='<%=_Parentdt.ToolsTrip %>'>
            <img src='<%=img001.ISValidObject()?img001.ImageUrl:"" %>' alt="" /><%=_Parentdt.Title %></a>
        </li>
         <%--===== Parent Module with out child===== --%>

        <% } %>
        <%--================== //Child Or not check======= --%>

        <% } %>
    </ul>
    <!-- End css3menu.com BODY section -->
</asp:Panel>
