﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;

namespace SchoolWebApplication.Web.UserControl
{
    public partial class ucSIte1 : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { }
            FillColor();

        }

        private void FillColor()
        {
            //--==Nav==--
            var lst = ThemeDesignTools.GetDetails(_SectionEnum._EnumOfSection.NAV.GetEnumName());
            if (lst.IsValidList())
            {
                ThemeDesignTools.ViewColor(Page, this.GetCurrentPageName(), ref pnlNavigationSection001, lst, false, true);
            }
        }
    }
}