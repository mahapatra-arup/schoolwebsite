﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Home_Imege_Slider.ascx.cs" ClientIDMode="Static" Inherits="SchoolWebApplication.Web.UserControl.UC_Home_Imege_Slider" %>


<!--Page css -->
<asp:PlaceHolder runat="server">
    <%: Styles.Render("~/Content/css/Carousel_Slider") %>
</asp:PlaceHolder>
<!-- css -->


<asp:Panel runat="server" Style="width: 100%;">
    <div class="ism-slider" data-transition_type="zoom" data-play_type="loop" data-image_fx="zoomrotate" id="my-slider">
        <ol>
            <asp:Repeater ID="Repeater_Home_Slider" runat="server">
                <ItemTemplate>

                    <li>
                        <img src='<%#Eval("ImgPath") %>' alt='<%#Eval("AlternateText") %>' style="width: 100%; background-size: cover;">
                        <%-- 1 --%>
                        <div class="ism-caption ism-caption-0">
                            <h6><%#Eval("Subjects") %></h6>
                        </div>

                        <%-- 2 --%>
                        <div class="ism-caption ism-caption-1" data-delay='100'>
                            <h6><%#Eval("Contents") %></h6>
                        </div>

                        <%-- 3 --%>
                        <%--<div class="ism-caption ism-caption-2">My slide caption text</div>--%>
                       
                    </li>

                </ItemTemplate>
            </asp:Repeater>
        </ol>
    </div>
</asp:Panel>

<!-- Page js================ -->
<asp:PlaceHolder runat="server">
    <%: Scripts.Render("~/bundles/ism_2_2") %>
</asp:PlaceHolder>
<!-- Page js================ -->
