﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.DataAccesTools;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SchoolWebApplication.Web.UserControl
{
    public partial class ucSite1_Top1 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //==========Catching store ==========
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(60));
            Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
            Response.Cache.SetValidUntilExpires(true);
            //==========Catching==========


            //if (!IsPostBack)

            //{
            //Color Fill
            FillColor();

            FillSchoolLogoDetails();
            //}
        }

        private void FillColor()
        {
            //--==Top Section==--
            var lst = ThemeDesignTools.GetDetails(_SectionEnum._EnumOfSection.HEADER.GetEnumName());
            if (lst.IsValidList())
            {
                ThemeDesignTools.ViewColor(Page, this.GetCurrentPageName(), ref pnlTopSection, lst, false, true);
            }

        }

        private void FillSchoolLogoDetails()
        {
            StringBuilder sb = new StringBuilder();

            //Clear
            phSchoolDetails.Controls.Clear();

            //setting Details
            var schoolProfileSettingTool = SchoolProfileSettingTool.GetSchoolProfileSetting();

            //School details
            SchoolProfile _SchoolDetails = SchoolUtils.GetSchoolDetails();
            if (!_SchoolDetails.ISValidObject())
            {
                return;
            }

            #region -=School Information=-
            string sb1 = string.Empty;
            if (schoolProfileSettingTool.IsShowSchoolDetails)
            {
                sb1 = "<div class=\"w3layouts-logo\">" +
                                "<h1 class=\"w3layouts-logo-text1\">" + _SchoolDetails.SchoolName + "</h1>" +
                                //"<h2 class=\"w3layouts-logo-text2\"> " + _SchoolDetails.ESTD + "</h2>" +
                                "<h2 class=\"w3layouts-logo-text2\"> " + _SchoolDetails.At + " :: " + _SchoolDetails.PS + " :: " + _SchoolDetails.DIST + " :: " + _SchoolDetails.PIN + "</h2>" +
                              //" <h2 class=\"w3layouts-logo-text2\"> " + _SchoolDetails.Ph + ". " + _SchoolDetails.Email + ". " + _SchoolDetails.Website + "</h2> " +
                              "</div>";
            }
            #endregion

            #region -=Logo (For Left Side)=-
            if (schoolProfileSettingTool.IsShowLogo)
            {
                double height = 0d; double width = 0d;
                System_Drawing_Imager.GetImgHeightWidth(_SchoolDetails.Logo, out height, out width);

                ImageButton imgSchoolLogo = new ImageButton();
                imgSchoolLogo.CommandName = "imgSchoolLogo";
                imgSchoolLogo.CssClass = "img-responsive";

                #region ---Image size  in  X --
                if (_SchoolDetails.Logo != null && _SchoolDetails.Logo.Length > 0)
                {
                    //imgSchoolLogo.Visible = true;
                    Image img = System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo);
                    imgSchoolLogo.Width = img.Width;
                    imgSchoolLogo.Height = img.Height;
                    imgSchoolLogo.ImageUrl = img.ImageUrl;
                    imgSchoolLogo.AlternateText = _SchoolDetails.SchoolName;
                    imgSchoolLogo.BorderStyle = BorderStyle.None;



                    //Add For  image and Text bothare Show
                    imgSchoolLogo.Style.Add("height", "18%");
                    imgSchoolLogo.Style.Add("height", "18%");
                    imgSchoolLogo.Style.Add("Z-INDEX", "999");
                    //imgSchoolLogo.Style.Add("float", "left");

                    //small Then Hide
                    //Bootstrap
                    imgSchoolLogo.CssClass = "img-responsive w3-hide-small  w3-display-topleft  w3-padding";
                    phSchoolDetails.Controls.Add(imgSchoolLogo);
                }
                #endregion


            }
            #endregion

            #region -=Logo (For Right Side)=-
            if (schoolProfileSettingTool.IsShowLogo1)
            {
                double height = 0d; double width = 0d;
                System_Drawing_Imager.GetImgHeightWidth(_SchoolDetails.Logo1, out height, out width);

                ImageButton imgSchoolLogo1 = new ImageButton();
                imgSchoolLogo1.CommandName = "imgSchoolLogo";
                imgSchoolLogo1.CssClass = "img-responsive";

                #region ---Image size  in  X --

                if (_SchoolDetails.Logo1 != null && _SchoolDetails.Logo1.Length > 0)
                {
                    //imgSchoolLogo.Visible = true;
                    Image img = System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo1);
                    imgSchoolLogo1.Width = img.Width;
                    imgSchoolLogo1.Height = img.Height;
                    imgSchoolLogo1.ImageUrl = img.ImageUrl;
                    imgSchoolLogo1.AlternateText = _SchoolDetails.SchoolName;
                    imgSchoolLogo1.BorderStyle = BorderStyle.None;



                    //Add For  image and Text bothare Show
                    imgSchoolLogo1.Style.Add("height", "18%");
                    imgSchoolLogo1.Style.Add("height", "18%");
                    imgSchoolLogo1.Style.Add("Z-INDEX", "999");
                    //imgSchoolLogo.Style.Add("float", "left");

                    //small Then Hide
                    //Bootstrap
                    imgSchoolLogo1.CssClass = "img-responsive w3-hide-small  w3-display-topright  w3-padding";
                    phSchoolDetails.Controls.Add(imgSchoolLogo1);
                }


            }
            #endregion
            #endregion

            #region --=Banner=--
            if (schoolProfileSettingTool.IsShowLogo2)
            {
                double height = 0d; double width = 0d;
                System_Drawing_Imager.GetImgHeightWidth(_SchoolDetails.Logo2, out height, out width);

                ImageButton imgSchoolLogo = new ImageButton();
                imgSchoolLogo.CommandName = "imgSchoolLogo";
                imgSchoolLogo.CssClass = "img-responsive";

                #region ---Image size  in  X --

                if (_SchoolDetails.Logo2 != null && _SchoolDetails.Logo2.Length > 0)
                {
                    //----Image size 2560 X 1440-------
                    if (_SchoolDetails.Logo2 != null && _SchoolDetails.Logo2.Length > 0)
                    {
                        Image img = System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo2);
                        imgSchoolLogo.Width = img.Width;
                        imgSchoolLogo.Height = img.Height;
                        imgSchoolLogo.ImageUrl = img.ImageUrl;
                        imgSchoolLogo.AlternateText = _SchoolDetails.SchoolName;

                        //Style
                        imgSchoolLogo.CssClass = "img-responsive";
                        phSchoolDetails.Controls.Add(imgSchoolLogo);
                    }
                }

            }


            #endregion
            #endregion

            //--Text Show---
            phSchoolDetails.Controls.Add(new LiteralControl(sb1.ToString()));
        }
    }
}