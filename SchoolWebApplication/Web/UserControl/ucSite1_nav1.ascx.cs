﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;

namespace SchoolWebApplication.Web.UserControl
{
    public partial class ucSite1_nav1 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateMenu(MenuMaster.GetParentMenus());
            }
            FillColor();
        }

#pragma warning disable CS0246 // The type or namespace name 'MenuView' could not be found (are you missing a using directive or an assembly reference?)
        private void PopulateMenu(List<MenuView> lstView)
#pragma warning restore CS0246 // The type or namespace name 'MenuView' could not be found (are you missing a using directive or an assembly reference?)
        {
            string currentPage = Path.GetFileName(Request.Url.AbsolutePath);
            if (lstView.IsValidList())
            {
                foreach (var data in lstView)
                {
                    MenuItem menuItem = new MenuItem();
                    menuItem.Value = data.Module_Id;
                    menuItem.Text = data.IsTitleShow == true ? data.Title : "";
                    menuItem.NavigateUrl = data.link_url;
                    menuItem.Selected = data.link_url.ConvertObjectToString().EndsWith(currentPage, StringComparison.CurrentCultureIgnoreCase);
                    menuItem.ToolTip = data.ToolsTrip;
                    menuItem.ImageUrl = data.IsImageShow == true ? data.Image.ISValidByte() ? System_Web_UI_WebControls_Imager.byteArrayToWebImage(data.Image).ImageUrl : "" : "";


                    //==Get Sub Menu==
                    var dtChild = MenuMaster.GetSubMenus(menuItem.Value);

                    //Child
                    if (dtChild.IsValidList())
                    {
                        foreach (var childitem in dtChild)
                        {
                            MenuItem ChildItem = new MenuItem();
                            ChildItem.Value = childitem.Module_Id.ConvertObjectToString();
                            ChildItem.Text = childitem.IsTitleShow == true ? childitem.Title.ConvertObjectToString() : "";
                            ChildItem.NavigateUrl = childitem.link_url.ConvertObjectToString();
                            ChildItem.Selected = childitem.link_url.ConvertObjectToString().EndsWith(currentPage, StringComparison.CurrentCultureIgnoreCase);
                            ChildItem.ToolTip = childitem.ToolsTrip.ConvertObjectToString();
                            ChildItem.ImageUrl = childitem.IsImageShow == true ? childitem.Image.ISValidByte() ? System_Web_UI_WebControls_Imager.byteArrayToWebImage(childitem.Image).ImageUrl : "" : "";



                            menuItem.ChildItems.Add(ChildItem);
                        }
                    }

                    //==Add  Menu==
                    HomeNavBer.Items.Add(menuItem);
                }
            }
        }

        private void FillColor()
        {
            //--==Nav==--
            var lst = ThemeDesignTools.GetDetails(_SectionEnum._EnumOfSection.NAV.GetEnumName());
            if (lst.IsValidList())
            {
                ThemeDesignTools.ViewColor(Page, this.GetCurrentPageName(), ref pnlNavigationSection, lst, false, true);
            }

        }

    }
}