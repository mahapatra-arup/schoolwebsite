﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SchoolWebApplication.Web
{
    public partial class Home : System.Web.UI.Page
    {

        protected void Page_PreInit(object sender, EventArgs e)
        {

            if (!DefultThemeMasterFile.MasterfileName.ISNullOrWhiteSpace())
            {
                this.MasterPageFile = DefultThemeMasterFile.MasterfileName;
            }
            else
            {
                //database Connection Problem or others problem
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                FillMarqueEvent();
                FillMarquenews();
                FillMarqueNotice();
                FillTeachersOpinion();
            }
        }


        #region -------------------Notice--------------------
        /// <summary>
        /// Event
        /// </summary>
        private void FillMarqueNotice()
        {
            var dt = NoticeUtils.GetNoticeBetweenDate();
            if (dt.IsValidList())
            {
                Repeater_Notice.DataSource = dt;
                Repeater_Notice.DataBind();
            }
            else
            {
                //Response.Write("Events are Not Available");
            }
        }


        protected void noticePath_Click(object sender, EventArgs e)
        {

        }

        protected void Repeater_Notice_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            //Logic
            try
            {

                if (e.CommandName.ToUpper() == "NOTICEID")
                {
                    lblDownloadLink.Text = "";
                    string vlu = Convert.ToString(e.CommandArgument);
                    var dt = NoticeUtils.GetNotice(vlu.ConvertObjectToLong());
                    if (dt.ISValidObject())
                    {
                        lblposttitle.Text = dt.Subjects;
                        lblPostContent.Text = dt.Contents;



                        lblPublishDate.Text = dt.PublishDate.ConvertObjectDateTimeToDateTime().ToString("dd-MMM-yyyy")
                            + "<br/> <p>From " + dt.DateFrom.ConvertObjectDateTimeToDateTime().ToString("dd-MMM-yyyy") +
                           " To " + dt.DateTo.ConvertObjectDateTimeToDateTime().ToString("dd-MMM-yyyy") + "</p>";
                        imgImage.ImageUrl = "";
                        imgImage.ImageAlign = ImageAlign.Left;


                        string fter = dt.Footer;
                        string publsher = dt.Publisher;
                        lblpostAuthor.Text = fter.ISNullOrWhiteSpace() ? publsher : fter + "<br/>" + publsher;

                        //Note :Do not support Phycal full path like use  Server.MapPath()
                        string strnoticepath = dt.NoticeFileUrl;

                        if (!strnoticepath.ISNullOrWhiteSpace())
                        {
                            lblDownloadLink.Text = "<a href=\"#\" onClick=\"window.open('" + strnoticepath + "', '_blank')\"><span class=\"fa fa-download\" title=\"Download File\" ></span>Download</a>";
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            //call Modal)
            ScriptManager.RegisterStartupScript(this, GetType(), "myeventModal", "document.getElementById('id0_Home_1Event').style.display = 'block';", true);

        }
        #endregion

        #region -------------------Event---------------------
        /// <summary>
        /// Event
        /// </summary>
        private void FillMarqueEvent()
        {
            DataTable dt = PostUtils.GetTop10PostDetails(PostUtils._PostType.Event);
            if (dt.IsValidDataTable())
            {
                EventRepeater.DataSource = dt;
                EventRepeater.DataBind();
            }
            else
            {
                //Response.Write("Events are Not Available");
            }
        }
        protected void eventpath_ServerClick(object sender, EventArgs e)
        {
            //call Modal
            ScriptManager.RegisterStartupScript(this, GetType(), "myeventModal", "document.getElementById('id0_Home_1Event').style.display = 'block';", true);
        }
        protected void EventRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.ToUpper() == "EVENTID")
                {
                    lblDownloadLink.Text = "";
                    string vlu = Convert.ToString(e.CommandArgument);
                    DataTable dt = PostUtils.GetPostDetailsById(vlu);
                    if (dt.IsValidDataTable())
                    {
                        lblposttitle.Text = Convert.ToString(dt.Rows[0]["post_title"]);
                        lblPostContent.Text = Convert.ToString(dt.Rows[0]["post_content"]);
                        lblpostAuthor.Text = Convert.ToString(dt.Rows[0]["post_author"]);
                        lblPublishDate.Text = Convert.ToDateTime(dt.Rows[0]["Publish_Date"]).ToString("dd-MMM-yyyy");
                        imgImage.ImageUrl = Convert.ToString(dt.Rows[0]["Post_ImgPath"]);
                        imgImage.ImageAlign = ImageAlign.Left;
                    }
                }

            }
            catch (Exception)
            {

            }
        }

        #endregion

        #region ------------------News-----------------------
        /// <summary>
        ///   News means Post
        /// </summary>
        private void FillMarquenews()
        {
            DataTable dt = PostUtils.GetTop10PostDetails(PostUtils._PostType.Post);
            if (dt.IsValidDataTable())
            {
                NewsRepeater.DataSource = dt;
                NewsRepeater.DataBind();
            }
            else
            {
                //Response.Write("Events are Not Available");
            }
        }

        protected void NewsRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "newsId")
                {
                    lblDownloadLink.Text = "";
                    string vlu = Convert.ToString(e.CommandArgument);
                    DataTable dt = PostUtils.GetPostDetailsById(vlu);
                    if (dt.IsValidDataTable())
                    {
                        lblposttitle.Text = Convert.ToString(dt.Rows[0]["post_title"]);
                        lblPostContent.Text = Convert.ToString(dt.Rows[0]["post_content"]);
                        lblpostAuthor.Text = Convert.ToString(dt.Rows[0]["post_author"]);
                        lblPublishDate.Text = Convert.ToDateTime(dt.Rows[0]["Publish_Date"]).ToString("dd-MMM-yyyy");
                        imgImage.ImageUrl = Convert.ToString(dt.Rows[0]["Post_ImgPath"]);
                        imgImage.ImageAlign = ImageAlign.Left;
                    }
                }

            }
            catch (Exception)
            {

            }
        }

        protected void newsPath_Click(object sender, EventArgs e)
        {
            //call Modal
            ScriptManager.RegisterStartupScript(this, GetType(), "myeventModal", "document.getElementById('id0_Home_1Event').style.display = 'block';", true);
        }
        #endregion

        #region ---------------Teacher Opinion---------------
        private void FillTeachersOpinion()
        {
            var lst = StaffUtils.OpinionPresentStaffDetails();
            if (lst.IsValidIEnumerable())
            {
                Repeater_TeacherDesk.DataSource = lst.ToList();
                Repeater_TeacherDesk.DataBind();
            }
            else
            {
                //Response.Write("Opinion are Not Present");
            }
        }
        #endregion

    }
}