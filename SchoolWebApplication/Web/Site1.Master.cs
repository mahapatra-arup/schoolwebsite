﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SchoolWebApplication.Web
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

            }
            SiteMetaFill();
            FillColor();
        }


        /// <summary>
        /// Site Meta Details Fill
        /// </summary>
        private void SiteMetaFill()
        {
            //Variable
            byte[] ico_img = null;
            string _Title = "";
            string _KeyWords = "";
            string _Description = "";

            #region SiteMeta
            //Known Page name of contaner of body
            //SiteMeta columns are (Id, Title, Title_Ico,KeyWords,Page)
            string contanerpage = Page.AppRelativeVirtualPath.ToString().Replace(".aspx", "");

            var moduleId = MenuMaster.GetModuleIdByVirtualPath(contanerpage);
            var dt = SiteMeta.GetSiteMetaDetails(moduleId);

            if (dt.ISValidObject())
            {

                //get a value
                _Title = dt.Title;
                _KeyWords = dt.KeyWords;
                _Description = dt.Description;


                //image
                //Photo
                try
                {
                    if (dt.Title_Ico.ISValidObject())
                    {
                        Byte[] imageData = (byte[])dt.Title_Ico;
                        if (imageData != null)
                        {
                            ico_img = imageData;
                        }

                        if (ico_img.Length > 0 && ico_img != null)
                        {
                            Image img = System_Web_UI_WebControls_Imager.byteArrayToWebImage(ico_img);
                            titleico2.Href = img.ImageUrl;
                            titleico1.Href = img.ImageUrl;
                        }
                    }
                }
                catch (Exception)
                {
                }

            }

            //Fill
            Page.Title = _Title;
            Page.MetaKeywords = _KeyWords;
            Page.MetaDescription = _Description;
            #endregion

        }

        private void FillColor()
        {

            //--==Body==--
            var lst = ThemeDesignTools.GetDetails(_SectionEnum._EnumOfSection.BODY.GetEnumName());
            if (lst.IsValidList())
            {
                ThemeDesignTools.ViewColor(Page, this.GetCurrentPageName(), ref pnlBodySection, lst, false, true);
            }
        }

    }
}