﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web/Site1.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="SchoolWebApplication.Web.Gallery" %>

<%@ Register Src="~/Web/UserControl/UC_Form_Header_Label.ascx" TagPrefix="uc1" TagName="UC_Form_Header_Label" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">

     <!-- Page ================ -->
    <asp:PlaceHolder runat="server">
        <%--//Jssor Slider Begin--%>
        <%: Scripts.Render("~/bundles/jssor_slider") %>
        <%--//Gallery--%>
         <%: Styles.Render("~/Content/css/Gallery") %>
    </asp:PlaceHolder>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <uc1:UC_Form_Header_Label runat="server" id="UC_Form_Header_Label" />

    <asp:Panel ID="pnl1" runat="server">

        <div class="container">
            <div class="row">

                <%--==Thumbnail Repeater== --%>
                <asp:Repeater OnItemCommand="Repeater_thumnail_ItemCommand" ID="Repeater_thumnail" runat="server">
                    <ItemTemplate>

                        <div class="col-lg-3 col-md-3 col-sm-3" style="margin-bottom: 10px; text-align: center">

                            <div class="container-thumb">
                                <img src='<%#Eval("ImagePath") %>' alt="Image" class="image-thumb img-thumbnail w3-card" />

                                <div class="overlay-thumb">

                                    <%-- Text --%>
                                    <div class="text-thumb">
                                        <%#Eval("GroupName")%><br />
                                       
                                        <%#Eval("Subjects") %><br />
                                       
                                        <%-- Click Button --%>
                                        <asp:ImageButton ID="btnlinkbutton" ToolTip="Show Modal" AlternateText="Show Modal" Text="Show"
                                            CssClass="w3-opacity-off img-circle  w3-orange " Width="50%" CommandArgument='<%#Eval("GroupName")%>' ForeColor="white" CommandName="showmodalview"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Web/images/button-round-play-icon.png"></asp:ImageButton>
                                        <%-- // Button --%>
                                    </div>
                                    <%-- //Text --%>
                                </div>

                            </div>

                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <%--==//Thumbnail Repeater== --%>
            </div>
        </div>

    </asp:Panel>


    <%-- --------========modal popup========----------%>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>

            <asp:Panel ID="Modal_Panel" runat="server">
                <div id="id01Modal" class="w3-modal">

                    <!-- Modal content-->
                    <div class="w3-modal-content w3-card-4 w3-animate-zoom w3-orange" style="padding-right:30px">

                        <%--===========Modal Header===========  --%>
                        <header class="w3-container" >
                            <span onclick="document.getElementById('id01Modal').style.display='none'"
                                class="w3-button w3-display-topright w3-red">&times;</span>
                            <h2>Gallery</h2>
                        </header>
                        <%--===========//Modal Header===========  --%>

                        <!--------============= Body=============------>
                        <div class="w3-container">
                            <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 960px; height: 480px; overflow: hidden; visibility: hidden; background-color: #24262e;">

                                <!-- ========Loading Screen=========== -->
                                <div data-u="loading" class="jssorl-009-spin" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; text-align: center; background-color: rgba(0,0,0,0.7);">
                                    <img style="margin-top: -19px; position: relative; top: 50%; width: 38px; height: 38px;" src="../GlobalTools/image/Loading/spin.svg" />
                                </div>
                                <!-- //========Loading Screen=========== -->

                                <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 240px; width: 720px; height: 480px; overflow: hidden;">

                                    <!----------- Repeater Set ------------>
                                    <asp:Repeater ID="Repeater_gallery" runat="server">
                                        <ItemTemplate>
                                            <div data-p="150.00">
                                                <!-- Image -->
                                                <img data-u="image" class="img-responsive" src='<%#Eval("ImgPath") %>' />
                                                <!-- ThumbNail -->
                                                <img data-u="thumb" class="img-responsive" src='<%#Eval("ImgPath") %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                </div>

                                <!-- Thumbnail Navigator -->
                                <div data-u="thumbnavigator" class="jssort101" style="position: absolute; left: 0px; top: 0px; width: 240px; height: 480px; background-color: #000;" data-autocenter="2" data-scale-left="0.75">
                                    <div data-u="slides">
                                        <div data-u="prototype" class="p" style="width: 99px; height: 66px;">
                                            <div data-u="thumbnailtemplate" class="t"></div>
                                            <svg viewbox="0 0 16000 16000" class="cv">
                                                <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                                                <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                                                <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                                            </svg>
                                        </div>
                                    </div>
                                </div>

                                <!-- Arrow Navigator -->
                                <div data-u="arrowleft" class="jssora093" style="width: 50px; height: 50px; top: 0px; left: 270px;" data-autocenter="2">
                                    <svg viewbox="0 0 16000 16000" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                        <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                                        <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
                                        <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
                                    </svg>
                                </div>

                                <div data-u="arrowright" class="jssora093" style="width: 50px; height: 50px; top: 0px; right: 30px;" data-autocenter="2">
                                    <svg viewbox="0 0 16000 16000" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
                                        <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                                        <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
                                        <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
                                    </svg>
                                </div>

                            </div>
                            <!-- #endregion Jssor Slider End -->


                        </div>
                        <%-- ==============//Body==================== --%>

                        <!-- =============Modal footer===================-->
                        <footer class="w3-container w3-border-top w3-padding-24">
                            <span onclick="document.getElementById('id01Modal').style.display='none'"
                                class="w3-button w3-display-bottomright w3-red">Cancel</span>
                        </footer>
                        <!-- =============//Modal footer=================-->

                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- --------========modal popup========----------%>

     <!-- Page js================ -->
    <asp:PlaceHolder runat="server">
        <%--//Gallery js--%>
        <%: Scripts.Render("~/bundles/Gallery") %>
    </asp:PlaceHolder>
     <%--==== jssor slider Call===== --%>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <%--==== jssor slider Call===== --%>
</asp:Content>
