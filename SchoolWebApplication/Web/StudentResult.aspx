﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web/Site1.Master" AutoEventWireup="true" CodeBehind="StudentResult.aspx.cs" Inherits="SchoolWebApplication.Web.StudentResult1" %>

<%@ Register Src="~/UCView/UC_Loading01_View.ascx" TagPrefix="uc1" TagName="UC_Loading01_View" %>
<%@ Register Src="~/UCView/UC_SnackBar_View.ascx" TagPrefix="uc1" TagName="UC_SnackBar_View" %>
<%@ Register Src="~/UCView/UC_Loading002_View.ascx" TagPrefix="uc1" TagName="UC_Loading002_View" %>
<%@ Register Src="~/Web/UserControl/ucSite1_Top1.ascx" TagPrefix="uc1" TagName="ucSite1_Top1" %>
<%@ Register Src="~/Web/UserControl/UC_Form_Header_Label.ascx" TagPrefix="uc1" TagName="UC_Form_Header_Label" %>





<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
   <!-- Print Page(Bootstrap Support)-->
    <script type="text/javascript">
        function PrintMarksheet() {
            //Bootstrap
            var panel = document.getElementById("<%=printMaeksheet.ClientID %>");
            var printWindow = window.open('', '', 'height=500,width=800');
            printWindow.document.write('<html><head><title>MarkSheet</title>');
            printWindow.document.write('<link href="../GlobalTools/W3.css" rel="stylesheet" type="text/css" media="all" runat="server" />');
            printWindow.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">');
            printWindow.document.write('<link href="UserControl/css/ucSite1_Top1.css" rel="stylesheet" type="text/css" media="all" runat="server" />');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <asp:Panel runat="server" ID="pnlResultDetilsEntry" class="container-fluid w3-padding-64">

        <!-- Header -->
        <uc1:UC_Form_Header_Label runat="server" ID="UC_Form_Header_Label" />

        <!-- == SnackBar== -->
        <uc1:UC_SnackBar_View runat="server" ID="UC_SnackBar_View" />
        <!-- == SnackBar== -->



        <div class="row">
            <div class="col-md-4"></div>

            <!-- ============Student Submit Details============ -->
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">Fill up Required Details </div>


                    <%-- Body --%>
                    <div class="panel-body">
                        <div class="form-group-sm">
                            <asp:UpdatePanel ID="UpdatePanegetResult001" runat="server">
                                <ContentTemplate>
                                    <%-- ==Messege=== --%>
                                    <asp:Literal ID="ltrMessege" runat="server"></asp:Literal>
                                    <%-- ==Messege=== --%>

                                    <%-- Class --%>
                                    <label for="cmbClass001" class="label label-danger"><span>*</span>Class : </label>
                                    <asp:DropDownList runat="server" ID="cmbClass001" AutoPostBack="true" EnableViewState="true" OnSelectedIndexChanged="cmbClass001_SelectedIndexChanged" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Class Is Required" ValidationGroup="ResultSubmit" InitialValue="--Select--" ControlToValidate="cmbClass001" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <br />
                                    <%-- Sec --%>
                                    <label for="cmbSec" class="label label-info"><span>*</span>Section : </label>
                                    <asp:DropDownList ID="cmbSec" class="form-control" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="ResultSubmit" InitialValue="--Select--" runat="server" ControlToValidate="cmbSec" Display="Dynamic" ErrorMessage="Section is Required Like(A,B..D)" ForeColor="Red" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <br />

                                    <%-- Rollno --%>
                                    <label for="txtRollNo" class="label label-info"><span>*</span>RollNo : </label>
                                    <asp:TextBox ID="txtRollNo" class="form-control" TextMode="Number" PlaceHolder="**Enter Your Roll No" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvRollNo" ValidationGroup="ResultSubmit" runat="server" ControlToValidate="txtRollNo" Display="Dynamic" ErrorMessage="Roll No is Required" ForeColor="Red" SetFocusOnError="True"></asp:RequiredFieldValidator>

                                    <br />
                                    <%-- Dob --%>
                                    <label for="txtDob" class="label label-info"><span>*</span>DOB : </label>
                                    <asp:TextBox ID="txtDob" class="form-control" TextMode="Date" PlaceHolder="**Enter Your DOB" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="ResultSubmit" runat="server" ControlToValidate="txtDob" Display="Dynamic" ErrorMessage="Dob is Required" ForeColor="Red" SetFocusOnError="True"></asp:RequiredFieldValidator>


                                    <hr />
                                    <%-- result Button--%>
                                    <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" UseSubmitBehavior="true" Style="float: right;" CssClass="ui-button w3-round-small" Text="Submit" Width="175px" ValidationGroup="ResultSubmit" OnClientClick="snakberfunction()" />
                                    <asp:LinkButton ID="lnkShowAnotherResult" OnClick="lnkShowAnotherResult_Click" runat="server" Font-Underline="True" CssClass="btn-link" OnClientClick="snakberfunction()">Try another</asp:LinkButton>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <hr />

                        </div>

                    </div>
                </div>
            </div>
            <!-- ============Student Submit Details============ -->

            <div class="col-md-4"></div>

        </div>

        <!-- =============Instruction============= -->
        <br />
        <asp:Panel runat="server" CssClass="row">
            <div class="W3-col  w3-card-4 bg-danger text-center w3-margin-left w3-margin-right w3-animate-fading w3-round-medium">
                <div class="w3-panel w3-display-container w3-round-medium">
                    <%-- Close Button --%>
                    <span onclick="this.parentElement.style.display='none'" class="w3-button w3-display-topright w3-hover-red">X</span>
                    <%-- Close Button --%>

                    <p>N.B. : Fill Student details.All details must be correct otherwise show invalid result</p>
                    <p>N.B. : If Student Marksheet is Available then show a "Download" Button</p>
                </div>
            </div>
        </asp:Panel>
        <!-- =============Instruction============= -->
    </asp:Panel>


    <!-- ============Result View Modal ============ -->
    <div id="StudentResultDialog" title="Result">

        <asp:UpdatePanel ID="uppnlResult" runat="server">
            <ContentTemplate>

                <asp:Panel runat="server" ID="printMaeksheet">

                    <%-- Design --%>
                    <uc1:ucSite1_Top1 runat="server" ID="ucSite1_Top1" />

                    <!-- ==Student Details== -->
                    <asp:Panel ID="pnlStudentDetails" runat="server">
                        <table class="w3-table-all w3-card-4"> 
                            <tbody>
                                <tr>
                                    <td>
                                            <%-- Name --%>
                                            <div class="">
                                                <label for="lblName">Student Name:</label>
                                                <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
                                            </div>

                                            <%--Father Name --%>
                                            <div class="">
                                                <label for="lblFatherName">Father Name  :</label>
                                                <asp:Label ID="lblFatherName" runat="server" Text="Label"></asp:Label>
                                            </div>

                                            <%--mother Name --%>
                                            <div class="">
                                                <label for="lblMotherName">Mother Name  :</label>
                                                <asp:Label ID="lblMotherName" runat="server" Text="Label"></asp:Label>
                                            </div>

                                       
                                    </td>
                                    <td>

                                            <div class="">
                                                <label for="lblClass">Class :</label>
                                                <asp:Label ID="lblClass" runat="server" Text="Label"></asp:Label>
                                            </div>

                                            <%--Sec--%>
                                            <div class="">
                                                <label for="lblSec">Sec :</label>
                                                <asp:Label ID="lblSec" runat="server" Text="Label"></asp:Label>
                                            </div>

                                            <%--Rollno--%>
                                            <div class="">
                                                <label for="lblRollno">Roll No :</label>
                                                <asp:Label ID="lblRollno" runat="server" Text="Label"></asp:Label>
                                            </div>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </asp:Panel>
                    <!-- ==//Student Details== -->

                    <!-- =====Student Marks Details=== -->
                    <div class="table-responsive">
                        <asp:GridView ID="gvStudentResult" HeaderStyle-CssClass="bg-success" ShowHeaderWhenEmpty="false" runat="server" AutoGenerateColumns="false" CssClass="w3-table w3-border-blue">
                            <EmptyDataTemplate>
                            </EmptyDataTemplate>

                        </asp:GridView>
                    </div>
                    <!-- =====//Student Marks Details=== -->

                    <!-- ==Total Details== -->
                    <asp:Panel ID="pnlMarksDetails" runat="server" CssClass="text-right w3-border w3-border-blue w3-padding-small bg-success" Style="font-size: 18px">

                        <div class="row">

                            <%-- Total --%>
                            <div class="col-sm-4">
                                <label for="lblName"><strong>Total :</strong></label>
                                <asp:Label ID="lblGrandTotal" CssClass="" runat="server" Text="Label"></asp:Label>
                            </div>

                            <%--Result --%>

                            <div class="col-sm-4">
                                <label for="lblName"><strong>Result :</strong></label>
                                <asp:Label ID="lblResult" CssClass="" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>

                    </asp:Panel>
                    <!-- ==//Total Details== -->

                    <span>Ref :- <%:Request.Url.Host %></span>
                </asp:Panel>

                <hr />

                <!-- ==Download link== -->
                <div class="text-right">

                    <button id="btnprint" class="ui-button ui-widget ui-corner-al" onclick="return PrintMarksheet();">

                        <span class="ui-icon ui-icon-print"></span>Print
                    </button>
                    <button runat="server" id="btnDownload" class="ui-button ui-widget ui-corner-al" onserverclick="btnDownload_ServerClick">
                        <span class="ui-icon ui-icon-document"></span>Download
                    </button>
                </div>
                <!-- ==//Download link== -->

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDownload" />
            </Triggers>
        </asp:UpdatePanel>

    </div>
    <!-- ============Result View Modal============ -->

    <!-- Script================= -->
    <script type="text/javascript">

        function openResultDialog() {
            $(function () {
                $("#StudentResultDialog").dialog("open");
            });
        }
    </script>

    <script type="text/javascript">
        $().ready(function () {

            $("#StudentResultDialog").dialog(
                {
                    autoOpen: false,
                    modal: true,
                    bgiframe: true,
                    resizable: false,
                    height: 'auto',

                    show: {
                        effect: "blind",
                        duration: 1000
                    },
                    hide: {
                        effect: "explode",
                        duration: 1000
                    },

                    width: 720,

                    //===Hide (X)Close Button on top right corner===
                    open: function (event, ui) { jQuery('.ui-dialog-titlebar-close').hide(); },

                    //BUtton
                    buttons: {

                        //Cancel
                        'Cancel': function () { $(this).dialog('close'); }
                    }
                })
        });

    </script>
    <!-- Script================= -->
</asp:Content>
