﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web/Site1.Master" AutoEventWireup="true" CodeBehind="PostAndEventView.aspx.cs" Inherits="SchoolWebApplication.Web.PostAndEventView" %>

<%@ Register Src="~/Web/UserControl/UC_Form_Header_Label.ascx" TagPrefix="uc1" TagName="UC_Form_Header_Label" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <uc1:UC_Form_Header_Label runat="server" id="UC_Form_Header_Label" />

    <asp:Panel ID="pnlPostandEvent001" runat="server">

        <div id="PostEvents_tabs" class="w3-container w3-margin-left w3-margin-right">
            <ul>
                <li><a href="#tabs-1"><span class="fa fa-newspaper-o"></span>&nbsp;News</a></li>
                <li><a href="#tabs-2"><span class="fa fa-info-circle"></span>&nbsp;Event</a></li>
            </ul>

            <!-- Post========== -->
            <div id="tabs-1">
                <br />
                <table id="tblPost" class="stripe">
                    <thead>
                        <tr>
                            <th></th> 
                        </tr>
                    </thead>

                    <tbody>
                        <asp:Repeater ID="Repeater_post" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <div class="w3-panel w3-white w3-card w3-display-container w3-padding-small">

                                            <%-- Close Button --%>
                                            <span class="w3-display-topright w3-padding w3-hover-red" onclick="this.parentElement.style.display='none'">X</span>

                                            <%-- Header subject --%>
                                            <p class="w3-text-blue">
                                                <b>
                                                    <asp:Label ID="lblSubject" CssClass="pull-lef" runat="server" Text='<%#Eval("post_title") %>'></asp:Label>
                                                </b>
                                                <br />
                                                <hr />

                                                <asp:Label ID="lblPublisherDate" CssClass="pull-right w3-text-blue" runat="server" Text='<%# "Publish Date : "+
                                                               (Eval("Publish_Date")!=DBNull.Value? System.Convert.ToDateTime(Eval(" Publish_Date")).ToString("dd-MMM-yyyy") :"-" ) %>'></asp:Label>

                                            </p>

                                            <%-- Content --%>
                                            <div class="row">
                                                <%-- Image --%>
                                                <div class="col-md-4">
                                                    <asp:Image ID="imgPost" runat="server" ImageAlign="Left" Visible='<%#Convert.ToString(Eval("Post_ImgPath"))!=string.Empty?true:false %>' ImageUrl='<%#Eval("Post_ImgPath") %>' />
                                                </div>

                                                <%-- Content Text --%>
                                                <div class="col-md-8">
                                                    <p>
                                                        <asp:Label ID="lblContent" runat="server" Text='<%#Eval("post_content") %>'></asp:Label>
                                                    </p>
                                                </div>

                                            </div>

                                            <%-- Footer --%>
                                            <p class="w3-text-blue">
                                                <asp:Label ID="lblFooter" CssClass="pull-right" runat="server" Text='<%#Eval("post_author") %>'></asp:Label>
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- //Post========== -->

            <!-- Event========== -->
            <div id="tabs-2">
                <asp:Repeater ID="Repeater_Event" runat="server">
                    <ItemTemplate>
                        <div class="w3-panel w3-white w3-card w3-display-container w3-padding-small">

                            <%-- Close Button --%>
                            <span class="w3-display-topright w3-padding w3-hover-red" onclick="this.parentElement.style.display='none'">X</span>

                            <%-- Header subject --%>
                            <p class="w3-text-blue">
                                <b>
                                    <asp:Label ID="lblSubject" CssClass="pull-lef" runat="server" Text='<%#Eval("post_title") %>'></asp:Label>
                                </b>
                                <br />
                                <hr />
                                <asp:Label ID="lblPublisherDate" CssClass="pull-right w3-text-blue" runat="server" Text='<%# "Publish Date : "+
                                                               (Eval("Publish_Date")!=DBNull.Value? System.Convert.ToDateTime(Eval(" Publish_Date")).ToString("dd-MMM-yyyy") :"-" ) %>'></asp:Label>
                            </p>

                            <%-- Content --%>
                            <div class="row">
                                <%-- Image --%>
                                <div class="col-md-4">
                                    <asp:Image ID="imgPost" runat="server" ImageAlign="Left" Visible='<%#Convert.ToString(Eval("Post_ImgPath"))!=string.Empty?true:false %>' ImageUrl='<%#Eval("Post_ImgPath") %>' />
                                </div>

                                <%-- Content Text --%>
                                <div class="col-md-8">
                                    <p>
                                        <asp:Label ID="lblContent" runat="server" Text='<%#Eval("post_content") %>'></asp:Label>
                                    </p>
                                </div>

                            </div>

                            <%-- Footer --%>
                            <p class="w3-text-blue">
                                <asp:Label ID="lblFooter" CssClass="pull-right" runat="server" Text='<%#Eval("post_author") %>'></asp:Label>
                            </p>

                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <!-- //Post========== -->

        </div>
    </asp:Panel>

    <%--Datatable======--%>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">

        //Data table
        $(document).ready(function () {
            $('#tblPost').DataTable({
                "ordering": false,
                 "scrollX": true
            });
        });

        //tab
        $(function () {
            $("#PostEvents_tabs").tabs();
        });
    </script>

</asp:Content>
