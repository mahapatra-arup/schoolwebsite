﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Data;

namespace SchoolWebApplication.Web
{
    public partial class AboutUs : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (!DefultThemeMasterFile.MasterfileName.ISNullOrWhiteSpace())
            {
                this.MasterPageFile = DefultThemeMasterFile.MasterfileName;
            }
            else
            {
                //database Connection Problem or others problem
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            FillAboutDetails();

        }


        /// <summary>
        ///Get and Fill About Details
        /// </summary>
        private void FillAboutDetails()
        {
            DataTable dtgroup = AboutUsTools.GetAboutUs();
            if (dtgroup.IsValidDataTable())
            {
                aboutsRepeater.DataSource = dtgroup;
                aboutsRepeater.DataBind();
            }
        }

    }
}