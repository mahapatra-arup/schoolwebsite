﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Data;


namespace SchoolWebApplication.Web
{
    public partial class PostAndEventView : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (!DefultThemeMasterFile.MasterfileName.ISNullOrWhiteSpace())
            {
                this.MasterPageFile = DefultThemeMasterFile.MasterfileName;
            }
            else
            {
                //database Connection Problem or others problem
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillNews();
                FillEvent();
            }
        }

        private void FillNews()
        {
            DataTable dt = PostUtils.GetTop10PostDetails(PostUtils._PostType.Post);

            if (dt != null)
            {
                Repeater_post.DataSource = dt;
                Repeater_post.DataBind();
            }
            else
            {
                //Response.Write("Events are Not Available");
            }
        }

        private void FillEvent()
        {
            DataTable dt = PostUtils.GetTop10PostDetails(PostUtils._PostType.Event);

            if (dt != null)
            {
                Repeater_Event.DataSource = dt;
                Repeater_Event.DataBind();
            }
            else
            {
                //Response.Write("Events are Not Available");
            }
        }
    }
}