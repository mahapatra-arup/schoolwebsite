﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SchoolWebApplication.Web
{
    public partial class Gallery : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (!DefultThemeMasterFile.MasterfileName.ISNullOrWhiteSpace())
            {
                this.MasterPageFile = DefultThemeMasterFile.MasterfileName;
            }
            else
            {
                //database Connection Problem or others problem
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillGalleryThumb();
            }
        }

        private void FillGalleryThumb()
        {
            Repeater_thumnail.DataSource = null;
            DataTable dt = GalleryUtils.GetThumbnail();
            if (dt.IsValidDataTable())
            {

                Repeater_thumnail.DataSource = dt;
                Repeater_thumnail.DataBind();
            }
            else
            {
                //Response.Write("Events are Not Available");
            }
        }


        protected void Repeater_thumnail_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            Repeater_gallery.DataSource = null;
            if (e.CommandName == "showmodalview")
            {
                try
                {
                    string vlu = Convert.ToString(e.CommandArgument);
                    var dt = GalleryUtils.GetGalleryByGroup(vlu);
                    if (dt.IsValidList())
                    {
                        Repeater_gallery.DataSource = dt;
                        Repeater_gallery.DataBind();

                        //Call Modal
                        ScriptManager.RegisterStartupScript(this, GetType(), "myGalleryModal", "document.getElementById('id01Modal').style.display = 'block';", true);

                    }
                    else
                    {
                        Response.Write("<script>alert('Image are not available');</script>");
                    }

                }
                catch (Exception)
                {

                }
                finally
                {
                }

            }
        }
    }
}
