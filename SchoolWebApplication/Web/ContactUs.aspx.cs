﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Linq;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Web
{
    public partial class ContactUs : System.Web.UI.Page
    {

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (!DefultThemeMasterFile.MasterfileName.ISNullOrWhiteSpace())
            {
                this.MasterPageFile = DefultThemeMasterFile.MasterfileName;
            }
            else
            {
                //database Connection Problem or others problem
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SchoolWebLibrary.Models.SchoolProfile _SchoolDetails = SqlHelper.DbContext().SchoolProfiles.FirstOrDefault();
            //if (!IsPostBack)
            //{
            Literal LT = new Literal();
            LT.Text = _SchoolDetails.GoogleMapUrl;
            PlaceHolderMap.Controls.Add(LT);
            //}
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (!SendMailTools.SendMail(txtName.Text, txtEmail.Text, txtSubject.Text, txtMessage.Text))
            {
                Show("Getting an error Some problem has Occurred. try again later.", "Error!", _messegeType.danger, _alartTopIndex.Simple, ref ltMessege);
            }
            else
            {
                Show("Send Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltMessege);
            }
        }
    }
}
