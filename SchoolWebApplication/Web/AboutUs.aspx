﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web/Site1.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="SchoolWebApplication.Web.AboutUs" %>

<%@ Register Src="~/Web/UserControl/UC_Form_Header_Label.ascx" TagPrefix="uc1" TagName="UC_Form_Header_Label" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <%-- Form Header---------- --%>
    <uc1:UC_Form_Header_Label runat="server" ID="UC_Form_Header_Label" />
    <%-- Form Header---- --%>
   
    <div class="panel panel-primary  w3-card-4">
        <div class="panel-body">
              <div class="align-middle w3-padding-large" style="width: 100%">

                <%-- Next and Previous Button --%>
                <div class="w3-left w3-hover-text-blue w3-text-red" onclick="plusDivs(-1)"><i class="fa fa-arrow-circle-o-left fa-4x"></i></div>
                <div class="w3-right w3-hover-text-blue w3-text-red" onclick="plusDivs(1)"><i class="fa fa-arrow-circle-o-right fa-4x"></i></div>
            </div>
            <asp:Repeater ID="aboutsRepeater" runat="server">
                <ItemTemplate>

                    <div class="myAboutSlides" >

                       <%-- =Title /subject= --%>
                        <div class="row">
                            <p class="text-justify text-left">
                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("Title") %>'></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Subjects") %>'></asp:Label>
                            </p>
                        </div>

                        <div class="row">

                            <%-- ==Image== --%>
                            <div class="col-lg-3">
                                <asp:Image ID="Image1" CssClass="img-thumbnail img-responsive  w3-padding text-center w3-card" runat="server" alt="Image" Style="width: 100%;" data-toggle="tooltip" data-placement="top" Visible='<%#Convert.ToString(Eval("ImgPath"))!=string.Empty?true:false %>' title='<%#Eval("ToolsTrip") %>' src='<%#Eval("ImgPath") %>' />
                            </div>
                            <%-- ==//Image== --%>

                            <div class="col-lg-9">
                                <%-- >>Less or >>more  --%>
                                <div class="w3-serif w3-medium" id="less_more">

                                    <%-- ==Content== --%>
                                    <p class="text-justify">
                                        <asp:Label ID="lbl_Content" runat="server" Text='<%#Eval("Contents") %>'></asp:Label>
                                    </p>
                                    <%-- ==//Content== --%>
                                </div>

                                <%-- ==Footer== --%>
                                <div class="panel-footer">
                                    <asp:Label ID="lbl_Footer" runat="server" CssClass="w3-display-bottomright text-justify" Text='<%#Eval("Footer") %>'></asp:Label>
                                </div>
                            </div>

                        </div>

                    </div>
                </ItemTemplate>
            </asp:Repeater>
          
        </div>
    </div>

    <!-- Page js================ -->
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/AboutUs") %>
    </asp:PlaceHolder>
    <!-- Page js================ -->
</asp:Content>
