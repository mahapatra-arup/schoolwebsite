﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
namespace SchoolWebApplication.Web
{
    public partial class StudentResult1 : System.Web.UI.Page
    {
        //variable
        #region <====Var===>
        List<string> _clsMp = StudentClassTools.ClassMp;
        List<string> _clsHs = StudentClassTools.ClassHs;
        static string _MarksheetPath { get; set; }
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (!DefultThemeMasterFile.MasterfileName.ISNullOrWhiteSpace())
            {
                this.MasterPageFile = DefultThemeMasterFile.MasterfileName;
            }
            else
            {
                //database Connection Problem or others problem
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cmbClass001.GetMpHsClasses();
                pnlMarksDetails.Visible = false;
                pnlStudentDetails.Visible = false;
                btnDownload.Visible = false;
            }
        }

        private void AddColumn()
        {
            gvStudentResult.Columns.Clear();
            if (_clsMp.Contains(cmbClass001.Text))
            {
                #region --------------mp-----------


                BoundField SubjectsNamea8 = new BoundField();
                SubjectsNamea8.HeaderText = "Subjects Name";
                SubjectsNamea8.DataField = "SubjectsName";
                gvStudentResult.Columns.Add(SubjectsNamea8);

                BoundField Unit1_Marksa9 = new BoundField();
                Unit1_Marksa9.HeaderText = "Unit-I";
                Unit1_Marksa9.DataField = "Unit1_Marks";
                gvStudentResult.Columns.Add(Unit1_Marksa9);

                BoundField Unit2_Marksa10 = new BoundField();
                Unit2_Marksa10.HeaderText = "Unit-II";
                Unit2_Marksa10.DataField = "Unit2_Marks";
                gvStudentResult.Columns.Add(Unit2_Marksa10);

                BoundField Unit3_Marksa11 = new BoundField();
                Unit3_Marksa11.HeaderText = "Unit-III";
                Unit3_Marksa11.DataField = "Unit3_Marks";
                gvStudentResult.Columns.Add(Unit3_Marksa11);

                BoundField Totala12 = new BoundField();
                Totala12.HeaderText = "Total";
                Totala12.DataField = "Total";
                gvStudentResult.Columns.Add(Totala12);

                BoundField AvgAndGradea13 = new BoundField();
                AvgAndGradea13.HeaderText = "Avg And Grade";
                AvgAndGradea13.DataField = "AvgAndGrade";
                gvStudentResult.Columns.Add(AvgAndGradea13);


                SubjectsNamea8.HtmlEncode = false;
                Unit1_Marksa9.HtmlEncode = false;
                Unit2_Marksa10.HtmlEncode = false;
                Unit3_Marksa11.HtmlEncode = false;

                Totala12.HtmlEncode = false;
                AvgAndGradea13.HtmlEncode = false;

                #endregion
            }
            else
            {
                #region -------------hs--------
                BoundField SubjectsName8 = new BoundField();
                SubjectsName8.HeaderText = "Subjects Name";
                SubjectsName8.DataField = "SubjectsName";
                gvStudentResult.Columns.Add(SubjectsName8);

                //BoundField Marks9 = new BoundField();
                //Marks9.HeaderText = "Marks";
                //Marks9.DataField = "Marks";
                //gvStudentResult.Columns.Add(Marks9);

                //BoundField Total10 = new BoundField();
                //Total10.HeaderText = "Total";
                //Total10.DataField = "Total";
                //gvStudentResult.Columns.Add(Total10);

                BoundField PercentageandGrade11 = new BoundField();
                PercentageandGrade11.HeaderText = "Per and Grade";
                PercentageandGrade11.DataField = "PercentageandGrade";
                gvStudentResult.Columns.Add(PercentageandGrade11);

                //=====Support Html Encode like<Pre> </Pre>(if it not use the html tag are not support .the tag formate is change like : &lt;pre&gt; )=-======
                //Ref:https://msdn.microsoft.com/en-us/library/system.web.ui.webcontrols.boundfield.htmlencode(v=vs.110).aspx
                SubjectsName8.HtmlEncode = false;
                //Marks9.HtmlEncode = false;
                //Total10.HtmlEncode = false;
                PercentageandGrade11.HtmlEncode = false;
                #endregion
            }
        }
        private bool FillResult()
        {
            //Variable
            bool isMarksAvailable = false;
            string clas = cmbClass001.Text;
            string sec = cmbSec.Text;
            string rollno = txtRollNo.Text;
            string dob = txtDob.Text;
            DataTable dt = new DataTable();

            //Ligic
            AddColumn();

            if (_clsMp.Contains(clas))
            {
                dt = StudentResultImportTools.GetMpresultDetails(clas, sec, rollno, dob);
            }
            else
            {
                dt = StudentResultImportTools.GetHSresultDetails(clas, sec, rollno, dob);
            }

            if (dt.IsValidDataTable())
            {
                isMarksAvailable = true;
                pnlMarksDetails.Visible = true;
                pnlStudentDetails.Visible = true;

                lblName.Text = Convert.ToString(dt.Rows[0]["Name"]);
                lblFatherName.Text = Convert.ToString(dt.Rows[0]["FathersName"]);
                lblMotherName.Text = Convert.ToString(dt.Rows[0]["MothersName"]);
                lblGrandTotal.Text = Convert.ToString(dt.Rows[0]["GrandTotal"]);
                lblResult.Text = Convert.ToString(dt.Rows[0]["Result"]);

                lblClass.Text = cmbClass001.Text;
                lblSec.Text = cmbSec.Text;
                lblRollno.Text = txtRollNo.Text;
            }
            else
            {
                isMarksAvailable = false;
                pnlMarksDetails.Visible = false;
                pnlStudentDetails.Visible = false;
                lblName.Text = "XXX";
                lblFatherName.Text = "XXX";
                lblMotherName.Text = "XXX";
                lblGrandTotal.Text = "XXX";
                lblResult.Text = "XXX";
            }

            //Source
            gvStudentResult.DataSource = dt;
            gvStudentResult.DataBind();
            return isMarksAvailable;
        }
        private void GetMarksheet()
        {
            //Variable;
            btnDownload.Visible = false;
            _MarksheetPath = string.Empty;
            string clas = cmbClass001.Text;
            string sec = cmbSec.Text;
            string rollno = txtRollNo.Text;
            string dob = txtDob.Text;
            //Replace add contact funtionality in the empty data template as user doesn't have rights
            TemplateBuilder tmpEmptyDataTemplate = new TemplateBuilder();


            DataTable dt1 = StudentResultImportTools.GetRStudentsMarksheetReports(clas, sec, rollno, dob);
            if (dt1.IsValidDataTable())
            {
                _MarksheetPath = Convert.ToString(dt1.Rows[0]["MarksheetPath"]);
                btnDownload.Visible = true;
                tmpEmptyDataTemplate.AppendLiteralString("<div style =\"text-align:center\"><p>Click on the link below to download the Marks Report</p> " +
                    " <img src=\"images/Down_Arrow.gif\" class=\"\"  style=\"float:right\" /></div>");
            }
            else
            {
                tmpEmptyDataTemplate.AppendLiteralString("<div style=\"text-align:center\"><p>!Records is not available</p></div>");
            }

            //Empty datatext bind
            gvStudentResult.EmptyDataTemplate = tmpEmptyDataTemplate;
            gvStudentResult.DataBind();
        }

        #region <====--Eveny--=====>
        protected void btnDownload_ServerClick(object sender, EventArgs e)
        {
            if (!_MarksheetPath.ISNullOrWhiteSpace())
            {
                //Process 1 Cient Side
                string filePathn = _MarksheetPath;//Do not use Server.map path()
                string script = "<script type='text/javascript'>window.open('" + filePathn + "','_blank')</script>";
                this.ClientScript.RegisterStartupScript(this.GetType(), "script", script);

                //Process 2 Server Side

                //string filePathn = Server.MapPath(_MarksheetPath);
                //FileInfo fileInfo = new FileInfo(filePathn);
                //if (fileInfo.Exists)
                //{
                //    Response.Clear();
                //    Response.AddHeader("Content-Disposition", "inline;attachment; filename=" + fileInfo.Name);
                //    Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                //    Response.ContentType = SSMUtils.ReturnExtension(fileInfo.Extension.ToLower());
                //    Response.Flush();
                //    Response.WriteFile(fileInfo.FullName);
                //    Response.End();
                //}
            }
        }

        protected void cmbClass001_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbClass001.SelectedIndex >= 0)
            {
                cmbSec.GetMpHsSecByClass(cmbClass001.Text);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                bool isMarksAvailable = FillResult();
                GetMarksheet();
                if (!isMarksAvailable && btnDownload.Visible == false)
                {
                    Show("Record not found", "Invalid!", _messegeType.warning, _alartTopIndex.Topindex, ref ltrMessege);

                    ////Java script
                    //Response.Write("<script>alert('Record not found');</script>");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scr", "openResultDialog();", true);
                }
            }
        }

        protected void lnkShowAnotherResult_Click(object sender, EventArgs e)
        {
            cmbClass001.SelectedIndex = -1;
            cmbSec.SelectedIndex = -1;
            txtRollNo.Text = "";
            FillResult();
            GetMarksheet();
        }
        #endregion
    }
}