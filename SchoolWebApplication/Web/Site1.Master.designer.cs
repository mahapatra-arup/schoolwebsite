﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolWebApplication.Web {
    
    
    public partial class Site1 {
        
        /// <summary>
        /// titleico1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlLink titleico1;
        
        /// <summary>
        /// titleico2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlLink titleico2;
        
        /// <summary>
        /// ContentPlaceHolderHead control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder ContentPlaceHolderHead;
        
        /// <summary>
        /// site2002 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm site2002;
        
        /// <summary>
        /// ScriptManagerforweb control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.ScriptManager ScriptManagerforweb;
        
        /// <summary>
        /// UC_Loading01_View control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SchoolWebApplication.UCView.UC_Loading01_View UC_Loading01_View;
        
        /// <summary>
        /// UC_Loading002_View control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SchoolWebApplication.UCView.UC_Loading002_View UC_Loading002_View;
        
        /// <summary>
        /// site_panel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel site_panel;
        
        /// <summary>
        /// ucSite1_Top1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SchoolWebApplication.Web.UserControl.ucSite1_Top1 ucSite1_Top1;
        
        /// <summary>
        /// ucSite1_nav1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SchoolWebApplication.Web.UserControl.ucSite1_nav1 ucSite1_nav1;
        
        /// <summary>
        /// ucSIte1_nav2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SchoolWebApplication.Web.UserControl.ucSIte1 ucSIte1_nav2;
        
        /// <summary>
        /// UC_Form_BrackingNews control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SchoolWebApplication.Web.UserControl.UC_Form_BrackingNews UC_Form_BrackingNews;
        
        /// <summary>
        /// pnlBodySection control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlBodySection;
        
        /// <summary>
        /// ContentPlaceHolderBody control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder ContentPlaceHolderBody;
        
        /// <summary>
        /// UC_Footer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::SchoolWebApplication.Web.UserControl.UC_Footer UC_Footer;
    }
}
