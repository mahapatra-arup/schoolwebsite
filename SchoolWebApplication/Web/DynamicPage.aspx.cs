﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.DataAccesTools;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SchoolWebApplication.Web
{
    public partial class DynamicPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PopulatePage();
            //if (!this.IsPostBack)
            //{

            //}
        }

        private void PopulatePage()
        {
            try
            {
                //-----------
                //****use This 'PageName' means routes.MapPageRoute("DynamicPage", "Pages/{PageName}", "~/Web/DynamicPage.aspx");
                //-----------
                string srrpageName = this.Page.RouteData.Values["PageName"].ToString();
                //Modify PageName
                string pageName = srrpageName.Replace("_", " ");

                //Get Data
                var data = DynamicPageUtils.GetDynamicPage(pageName);
                if (data.ISValidObject())
                {
                    //Tittle---
                    Heading_title_div.InnerText = data.PageTitle.Substring(0, 1);
                    lblheadertitle0025.InnerText = data.PageTitle.Substring(1);

                    //Page Content----
                    lblPageContent.Text = data.PageContent;
                    AddCss(data.css);

                    //Table View----
                    GetDataDateBetween(data.Id);
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void GetDataDateBetween(Guid _dynamicpageId)
        {
            pnlDynamicDataList.Visible = true;

            var dt = DynamicPageSubUtils.GetData_BetweenDate(_dynamicpageId).ToList();
            if (dt.IsValidList())
            {
                Repeater_Page.DataSource = dt;
                Repeater_Page.DataBind();
            }
            else
            {
                Repeater_Page.DataSource = null;
                Repeater_Page.DataBind();
                pnlDynamicDataList.Visible = false;
            }
        }

        private void GetData(long id)
        {
            var dt = DynamicPageSubUtils.GetData(id);
            if (dt.ISValidObject())
            {
                pSubject.InnerText = dt.Subjects;
                pContents.InnerText = dt.Contents;
                pFooter.InnerText = dt.Footer;
                pPublishDate.InnerText = dt.PublishDate.ConvertObjectDateTimeToDateTime().ToString("dd-MMM-yyyy");
                pPublisher.InnerText = dt.Publisher;
            }
        }

        protected void Repeater_Data_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "pdf_download")
            {
                try
                {
                    string filePathn = e.CommandArgument.ToString();
                    //Do not use Server.map path()
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scr01", "window.open('" + filePathn + "','_blank')", true);
                }
                catch (Exception)
                {

                }
            }
            else if (e.CommandName == "image_download")
            {
                try
                {
                    string filePathn = e.CommandArgument.ToString();
                    //Do not use Server.map path()

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scr", "window.open('" + filePathn + "','_blank')", true);
                }
                catch (Exception)
                {

                }
            }
            else if (e.CommandName == "content_model_Data")
            {
                try
                {
                    string vlu = e.CommandArgument.ToString();

                    GetData(vlu.ConvertObjectToLong());

                    //Call Dialog

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scr", "document.getElementById('idmodal015').style.display='block'", true);
                }
                catch (Exception)
                {

                }
            }
        }

        private void AddCss(string css)
        {
            Page.Header.Controls.Add(
               new LiteralControl(
               @"<style type='text/css'>" +
               css +
                                   "</style>" + Environment.NewLine

                                        ));
        }
    }
}