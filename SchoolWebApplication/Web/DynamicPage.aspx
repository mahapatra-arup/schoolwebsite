﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web/Site1.Master" AutoEventWireup="true" CodeBehind="DynamicPage.aspx.cs" Inherits="SchoolWebApplication.Web.DynamicPage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
        <!--Datatable Cdn--------------------------->
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">

    <div class="container-fluid w3-margin">

        <!-- Page Title------------------------------->
        <div class="title-div">
            <h3 class="tittle">
                <span id="Heading_title_div" runat="server"></span><label id="lblheadertitle0025" runat="server"></label>
            </h3>
            <div class="tittle-style">
            </div>
        </div>

        <!-- Content View------------------------------->
        <div class="panel panel-primary">
            <div class="panel-body">
                <%-- Page Data --%>
                <asp:Label ID="lblPageContent" runat="server" Text="Label"></asp:Label>

                <!--Table View------------------------------->
                <asp:Panel ID="pnlDynamicDataList" runat="server" class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="panel-title"><span class="fa fa-table">&nbsp;Data</span></div>
                    </div>

                    <div class="panel-body">
                        <table id="tblView" class="w3-table-all" style="width: 100%">

                            <thead>
                                <tr>
                                    <th>SlNo</th>
                                    <th>Subject</th>
                                    <th>Publish Date</th>
                                    <th>Publisher Name</th>
                                    <th>Action</th>

                                </tr>
                            </thead>

                            <tbody>
                                <asp:Repeater ID="Repeater_Page" OnItemCommand="Repeater_Data_ItemCommand" runat="server">
                                    <ItemTemplate>

                                        <tr>
                                            <%-- slNo ::[Row_001] - use foe row value get--%>
                                            <td class="Row_001"><%# ((RepeaterItem)Container).ItemIndex + 1%></td>

                                            <%-- subject --%>
                                            <td class="Row_002"><%# Eval("Subjects")%></td>

                                            <%-- publisherdate --%>
                                            <td class="Row_003"><%# Eval("PublishDate","{0:d}")%></td>

                                            <td class="Row_004"><%# Eval("Publisher")%></td>


                                            <%-- Action Button --%>
                                            <td class="Row_005">
                                                <table>
                                                    <tr>
                                                        <%-- Content View --%>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="imgOpenDialog" Visible='<%#Convert.ToString(Eval("Contents"))!=string.Empty?true:false %>' ToolTip="Content View"
                                                                        AlternateText="Content View" CommandArgument='<%#Eval("id")%>' ForeColor="White"
                                                                        CommandName="content_model_Data" ImageUrl="../GlobalTools/image/Document.png" Height="40" Width="40" runat="server" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                <ContentTemplate>
                                                                    <%-- pdf Download--%>
                                                                    <asp:ImageButton ID="imgPdfDownload" Visible='<%#Convert.ToString(Eval("PdfFileUrl"))!=string.Empty?true:false %>' ToolTip="Download Pdf File"
                                                                        AlternateText="Download" CommandArgument='<%#Eval("PdfFileUrl")%>' ForeColor="White"
                                                                        CommandName="pdf_download" ImageUrl="../GlobalTools/image/pdf.png" Height="40" Width="40" runat="server" />

                                                                    <%-- Image Download--%>
                                                                    <asp:ImageButton ID="imgImageDownload" Visible='<%#Convert.ToString(Eval("ImageFileUrl"))!=string.Empty?true:false %>' ToolTip="Download Image File"
                                                                        AlternateText="Download" CommandArgument='<%#Eval("ImageFileUrl")%>' ForeColor="Red"
                                                                        CommandName="image_download" CssClass="fa fa-download" Height="40" Width="40" runat="server" />

                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>


                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>

                        </table>

                    </div>
                </asp:Panel>
                <!--//Table View------------------------------->

            </div>
        </div>

        <br />


    </div>


    <!--Table View Modal-------------------->
    <div id="idmodal015" class="w3-modal">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom">
            <div class="w3-center">
                <br />
                <span onclick="document.getElementById('idmodal012').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
            </div>

            <%-- Body --%>
            <div class="w3-container">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="w3-section">
                            <%-- Date --%>
                            <p runat="server" id="pSubject" style="direction: rtl; color: forestgreen"><%# Eval("Subjects")%></p>

                            <%-- Date --%>
                            <p runat="server" id="pPublishDate" style="direction: rtl; color: forestgreen"><%# Eval("PublishDate","{0:d}")%></p>

                            <%-- Publisher --%>
                            <p runat="server" id="pPublisher" class="text-info"><%# Eval("Publisher")%></p>
                            <hr />

                            <%-- Content --%>
                            <p runat="server" id="pContents" class="w3-agileits-blog-text"><%# Eval("Contents")%></p>

                            <hr />
                            <%-- Footer --%>
                            <p runat="server" id="pFooter" style="direction: rtl;" class=""><%# Eval("Footer")%></p>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <%-- footer --%>
            <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                <button onclick="document.getElementById('idmodal012').style.display='none'" type="button" class="w3-button w3-red">Cancel</button>

            </div>

        </div>
    </div>
    <!--Modal-------------------->
   
    <!--//Datatable Cdn--------------------------->
     <!--Datatable-->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js "></script>

    <script type="text/javascript">
        //======================Data table========
        $(document).ready(function () {
            var dat = document.getElementById("tblView");
            if (dat != null) {


                var table = $('#tblView').DataTable({
                    "scrollX": true
                });
            }
        });
        //======================//Data table========
    </script>
    
</asp:Content>
