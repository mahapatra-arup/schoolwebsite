﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Web.UI;

namespace SchoolWebApplication.Web
{

    public partial class NoticeBoardView : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (!DefultThemeMasterFile.MasterfileName.ISNullOrWhiteSpace())
            {
                this.MasterPageFile = DefultThemeMasterFile.MasterfileName;
            }
            else
            {
                //database Connection Problem or others problem
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetNotice();
            }
        }

        private void GetNotice()
        {
            var dt = NoticeUtils.GetNoticeBetweenDate();
            if (dt.IsValidList())
            {
                Repeater_Notice.DataSource = dt;
                Repeater_Notice.DataBind();
            }
        }

        private void GetNotice(long id)
        {
            var dt = NoticeUtils.GetNotice(id);
            if (dt.ISValidObject())
            {
                pSubject.InnerText = dt.Subjects;
                pContents.InnerText = dt.Contents;
                pFooter.InnerText = dt.Footer;
                pPublishDate.InnerText = dt.PublishDate.ConvertObjectDateTimeToDateTime().ToString("dd-MMM-yyyy");
                pPublisher.InnerText = dt.Publisher;
            }
        }
        protected void Repeater_Notice_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "pdfdownload")
            {
                try
                {
                    string filePathn = e.CommandArgument.ToString();
                    //Do not use Server.map path()

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scr", "window.open('" + filePathn + "','_blank')", true);
                }
                catch (Exception)
                {

                }
            }
            else if (e.CommandName == "contentView")
            {
                try
                {
                    string vlu = e.CommandArgument.ToString();

                    GetNotice(vlu.ConvertObjectToLong());

                    //Call Dialog

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scr", "document.getElementById('idmodal012').style.display='block'", true);
                }
                catch (Exception)
                {

                }
            }
        }
    }
}