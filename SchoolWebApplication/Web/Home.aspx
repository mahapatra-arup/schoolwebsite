﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web/Site1.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="SchoolWebApplication.Web.Home" %>

<%@ Register Src="~/Web/UserControl/UC_Home_Imege_Slider.ascx" TagPrefix="uc1" TagName="UC_Home_Imege_Slider" %>
<%@ Register Src="~/Web/UserControl/UC_National_Anthem.ascx" TagPrefix="uc1" TagName="UC_National_Anthem" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <asp:PlaceHolder runat="server">
        <!--NoticeBoard/Braking News--------------->
        <%: Styles.Render("~/Content/css/Home") %>
        <%: Styles.Render("~/Content/css/NoticeBoard") %>
    </asp:PlaceHolder>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <br />

    <asp:Panel ID="Panel1" runat="server" class="container-fluid">

        <div class="row">

            <!--Bannere/Teachers Opinion----------------------->
            <div class="col-sm-8 col-md-8 col-lg-8">

                <%-- ===============Bannere/============= --%>
                <uc1:UC_Home_Imege_Slider runat="server" ID="UC_Home_Imege_Slider" />
                <%-- ===============Bannere/============= --%>

                <%-- ===============Teacher Opinion============= --%>
                <div class="w3-panel w3-card bg-success">

                    <div class="w3-content w3-display-container">
                        <%-- Repeater rep[eate image slide --%>
                        <asp:Repeater ID="Repeater_TeacherDesk" runat="server">
                            <ItemTemplate>

                                <div class="myTeacherSlides" style="width: 100%">

                                    <%------- Header Image--------- --%>
                                    <div class="w3-display-container">
                                        <img src="../GlobalTools/image/Header001.png" class="img-responsive center-block w3-grayscale" style="width: 40%; min-width: 300px" />
                                        <div class="w3-display-middle"><span class="w3-text-white"><%#Eval("GroupName")+" " %></span></div>
                                    </div>
                                    <%------- //Header Image--------- --%>

                                    <div class="w3-row">
                                        <div class="col-lg-3 w3-border w3-border-deep-orange w3-padding text-center w3-card">
                                            <img class="img-circle" src="<%#Eval("ImagePath") %>" style="width: 100px; height: 100px;">

                                            <i class="w3-serif w3-medium text-primary glyphicon glyphicon-user"><%# " "+Eval("EmpName"+"") %></i>

                                            <%--  <% if (Eval("EducationalQualification")!= null)
                                                    { %>
                                                <i class="w3-serif w3-medium fa fa-book"><%# " "+Eval("EducationalQualification") %></i>
                                                <%}%>--%>

                                            <i class="w3-serif w3-medium glyphicon glyphicon-desktop"><%# " "+Eval("designation") %></i>
                                        </div>
                                        <div class="col-lg-9">
                                            <i class="fa fa-quote-right w3-xlarge w3-margin-right w3-text-red"></i>

                                            <blockquote class="blockquote-reverse">
                                                <%-- >>Less or >>more  --%>

                                                <div class="comment w3-serif w3-medium" id="less_more">

                                                    <%#Eval("Opinion") %>
                                                </div>

                                                <footer><%#Eval("EmpName") %></footer>
                                            </blockquote>
                                        </div>
                                    </div>

                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style="width: 100%">

                            <%-- Next and Previous Button --%>
                            <div class="w3-left w3-hover-text-blue w3-text-red" onclick="plusDivs(-1)">&#10094;</div>
                            <div class="w3-right w3-hover-text-blue w3-text-red" onclick="plusDivs(1)">&#10095;</div>
                        </div>
                    </div>
                </div>
                <%-- ===============Bannere============= --%>

                <%-- ===============Audio/============= --%>
                <uc1:UC_National_Anthem runat="server" ID="UC_National_Anthem" />
                <%-- ===============Audio/============= --%>
            </div>
            <!--//Bannere/Teachers Opinion----------------------->


            <!---Event/Notice/News--------------------------------->
            <div class="col-sm-4 col-md-4 col-lg-4">

                <%-- ===============Notice=============== --%>
                <div class="panel panel-default noticeboard w3-card">
                    <div class="buttom">&nbsp;</div>
                    <div class="">
                        <div class="LeftCornar"></div>
                        <div class="RightCornar"></div>
                        <div class="Title">
                            <i class="glyphicon glyphicon-blackboard"></i><span id="lblNoticeBoard">Notice Board</span>
                        </div>
                        <div class="panel-body">

                            <marquee direction="up" onmouseover="this.stop()" onmouseout="this.start()" scrollamount="3" style="height: 100%; width: 100%; min-height: 200px; max-height: 210px" loop="INFINITE">
                                    <ul style="list-style-type: none;">
                                    <asp:Repeater ID="Repeater_Notice" runat="server" OnItemCommand="Repeater_Notice_ItemCommand">
                                    <ItemTemplate>
                                    <li style="border-bottom: 2px solid red;border-bottom-style:dotted;">
                                    <h6 style="text-align:justify">
                                    <span class="fa fa-paperclip text-danger"></span><asp:Button   ID="noticePath"  CommandName="NOTICEID"  CommandArgument='<%#Eval("id") %>' runat="server" 
                                        OnClick="noticePath_Click" Text='<%#String.Format("{0} | {1}", Eval("Subjects"),String.Format( "{0:d}",Eval("PublishDate"))) %>' CssClass="btn-link"
                                        style="white-space:normal;font-size:13px"></asp:Button>
                                    </h6>
                                  
                                    </li>
                                    </ItemTemplate>
                                    </asp:Repeater>
                                    </ul>
                                    </marquee>

                        </div>

                    </div>
                </div>
                <%-- //==============Notice============== --%>


                <%-- ===============News/Event=============== --%>
                <div class="w3-container noticeboard w3-card" style="max-height: 800px; min-height: 530px">
                    <div id="home_tabs">
                        <ul>
                            <li><a href="#tabs-1">Event</a></li>
                            <li><a href="#tabs-2">News</a></li>
                        </ul>

                        <div id="tabs-1" class="table-responsive">
                            <%-- //================Event============ --%>
                            <%------- Header Image--------- --%>
                            <div class="w3-display-container">
                                <img src="../GlobalTools/image/Header001.png" class="img-responsive center-block" style="width: 50%;" />
                                <div class="w3-display-middle"><span class="w3-text-white">Event</span></div>
                            </div>
                            <%------- //Header Image--------- --%>

                            <%-- =================Event================== --%>
                            <marquee direction="up" onmouseover="this.stop()" onmouseout="this.start()" scrollamount="3" style="height: 100%; width: 100%; max-height: 380px; min-height: 380px" loop="INFINITE">
                                      
                                        <table class="table table-striped" >
                                      <tbody>
                                          <asp:Repeater ID="EventRepeater" runat="server" OnItemCommand="EventRepeater_ItemCommand">
                                    <ItemTemplate>
                                   
                                        <tr class="text-left" >
                                        <td>
                                            <img src="../GlobalTools/image/New-icon.gif" style="min-width:13px" class="" />
                                        </td>
                                           <td>
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel001">
                                                   <ContentTemplate>
                                                 <asp:Button  ID="eventpath"  CommandName="eventId"  CommandArgument='<%#Eval("Id") %>' runat="server" 
                                                        OnClick="eventpath_ServerClick" Text='<%#Eval("post_title") %>' style="font-size:13px;white-space:normal;" CssClass="btn-link" ></asp:Button>
                                                     </ContentTemplate>
                                                </asp:UpdatePanel>
                                      </td>
                                          </tr>

                                        <tr  class="text-center">
                                            <td colspan="2" >
                                           <img src="../GlobalTools/image/Draw_line.png" style="max-height:20px;" class="" />
                                                </td>
                                        </tr>
                                    </ItemTemplate>
                                    </asp:Repeater>
                                    </tbody>
                                  </table>
                                    </marquee>
                            <%-- //=====================Event================= --%>

                            <%-- //================Event============ --%>
                        </div>

                        <div id="tabs-2" class="table-responsive">
                            <%-- ==================News================== --%>
                            <%------- Header Image--------- --%>
                            <div class="w3-display-container">
                                <img src="../GlobalTools/image/Header001.png" class="img-responsive center-block" style="width: 50%;" />
                                <div class="w3-display-middle"><span class="w3-text-white">News</span></div>
                            </div>
                            <%------- //Header Image--------- --%>

                            <marquee direction="up" onmouseover="this.stop()" onmouseout="this.start()" scrollamount="3" style="height: 100%; width: 100%; max-height: 380px; min-height: 380px" loop="INFINITE">
                                  <table class="table table-striped">
                                      <tbody >
                                        <asp:Repeater ID="NewsRepeater" runat="server" OnItemCommand="NewsRepeater_ItemCommand">
                                    <ItemTemplate>
                                    <tr  class="text-left">
                                        <td>
                                            <img src="../GlobalTools/image/New-icon.gif" style="min-width:13px" class="rounded mx-auto d-block" />
                                        </td>
                                           <td> 
                                               <asp:UpdatePanel runat="server" ID="UpdatePanel001">
                                                   <ContentTemplate>
                                 
                                     <asp:Button   ID="newsPath" CommandName="newsId"  CommandArgument='<%#Eval("Id") %>'
                                         runat="server"  OnClick="newsPath_Click" Text='<%#Eval("post_title") %>' style="font-size:13px;white-space:normal" CssClass="btn-link" ></asp:Button>
                                         </ContentTemplate>
                                                </asp:UpdatePanel>
                                
                                               </td>
                                          </tr>

                                        <tr  class="text-center">
                                            <td colspan="2" >
                                           <img src="../GlobalTools/image/Draw_line.png" style="max-height:20px;" class="rounded mx-auto d-block" />
                                                </td>
                                        </tr>

                                    </ItemTemplate>
                                    </asp:Repeater>

                                                                                           </tbody>
                                  </table>
                                    </marquee>

                            <%-- //================News============ --%>
                        </div>

                    </div>

                </div>
                <%-- ===============News/Event=============== --%>
            </div>
            <!---Event/Notice/News--------------------------------->
        </div>


        <!---Modal Post Event------------------------------------->
        <div id="id0_Home_1Event" class="w3-modal">
            <br />

            <div class="w3-modal-content w3-animate-opacity w3-card-4">
                <asp:UpdatePanel runat="server" ID="upmodalhm">
                    <ContentTemplate>
                        <%-- == Header=== --%>
                        <header class="w3-container" style="background: linear-gradient(#ff6a00,#b6ff00)">

                            <span onclick="document.getElementById('id0_Home_1Event').style.display='none'"
                                class="btn w3-display-topright">&times;</span>

                            <%-- Post Title --%>
                            <p>
                                <asp:Label ID="lblposttitle" runat="server" Text=""></asp:Label>
                            </p>
                        </header>
                        <%-- ==//Header=== --%>

                        <%-- ==Body== --%>
                        <div class="w3-container">
                            <asp:Label ID="lblPublishDate" CssClass="pull-right w3-text-blue" runat="server" Text=""></asp:Label>

                            <div class="row">
                                <%-- Image --%>
                                <asp:Panel ID="Pnlimgview" runat="server" CssClass="col-md-3">
                                    <asp:Image ID="imgImage" CssClass="img-thumbnail img-responsive" runat="server" />
                                </asp:Panel>

                                <%-- Text  Body content--%>
                                <asp:Panel ID="Pnlcontetview" runat="server" CssClass="col-md-9">
                                    <p>
                                        <asp:Label ID="lblPostContent" runat="server" CssClass="pull-left" Text=""></asp:Label>
                                    </p>
                                </asp:Panel>
                            </div>

                            <%-- Post Author --%>
                            <p>
                                <asp:Label ID="lblpostAuthor" CssClass="pull-right w3-text-blue-gray" runat="server" Text=""></asp:Label>
                            </p>

                        </div>
                        <%-- ==Body== --%>

                        <%-- ===Footer ==--%>
                        <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                            <button onclick="document.getElementById('id0_Home_1Event').style.display='none'" type="button" class="w3-button w3-red">Cancel</button>

                            <%--File Download Link --%>
                            <asp:Literal ID="lblDownloadLink" runat="server"></asp:Literal>
                        </div>
                        <%-- ===//Footer ==--%>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>

        </div>
        <!---Modal Post Event------------------------------------->

    </asp:Panel>

    <!-- Page js----->
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/Home") %>
        <%: Scripts.Render("~/bundles/jquery_shorten") %>
    </asp:PlaceHolder>
    <%--Link Shorten (Ref:https://viralpatel.net/blogs/dynamically-shortened-text-show-more-link-jquery/)--%>
    <script type="text/javascript">
        //Link Shorten js------>
        $(document).ready(function () {

            $(".comment").shorten({
                "showChars": 300,
                "moreText": "See More >>",
                "lessText": "<< Less",
            });

        });

        //Home Tab------>
        $(function () {
            $("#home_tabs").tabs();
        });
    </script>

</asp:Content>
