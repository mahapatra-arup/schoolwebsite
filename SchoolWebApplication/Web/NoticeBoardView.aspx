﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web/Site1.Master" AutoEventWireup="true"  CodeBehind="NoticeBoardView.aspx.cs" Inherits="SchoolWebApplication.Web.NoticeBoardView" %>

<%@ Register Src="~/Web/UserControl/UC_Form_Header_Label.ascx" TagPrefix="uc1" TagName="UC_Form_Header_Label" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">

    <!--Datatable --------------------------->
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" />
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <!--Header Label------------------------->
    <uc1:UC_Form_Header_Label runat="server" id="UC_Form_Header_Label" />
     <!--//Header Label------------------------->

    <asp:Panel ID="pnlNoticeList" runat="server" class="panel panel-primary w3-margin">

            <div class="panel-heading" style="background-image: url('../Web/images/Textture_Colorty3.jpg'); background-repeat: repeat;">NoticeBoard</div>

            <div class="panel-body">
                <a href="https://get.adobe.com/reader/" class="text-danger">* Download Adobe Acrobat Reader >> </a>
                <br />
                <br />

                <table id="tblNoticBoard" class="row-border display hover" style="width: 100%">

                    <thead>
                        <tr>
                            <th>SlNo</th>
                            <th>Subject</th>
                            <th>Publish Date</th>
                            <th>Publisher Name</th>
                            <th>Action</th>

                        </tr>
                    </thead>

                    <tbody>
                        <asp:Repeater ID="Repeater_Notice" OnItemCommand="Repeater_Notice_ItemCommand" runat="server">
                            <ItemTemplate>

                                <tr>
                                    <%-- slNo ::[Row_001] - use foe row value get--%>
                                    <td class="Row_001"><%# ((RepeaterItem)Container).ItemIndex + 1%></td>

                                    <%-- subject --%>
                                    <td class="Row_002"><%# Eval("Subjects")%></td>

                                    <%-- publisherdate --%>
                                    <td class="Row_003"><%# Eval("PublishDate","{0:d}")%></td>

                                    <td class="Row_004"><%# Eval("Publisher")%></td>


                                    <%-- Action Button --%>
                                    <td class="Row_005">
                                        <table>
                                            <tr>
                                                <%-- Content View --%>
                                                <td>
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <asp:ImageButton ID="imgOpenDialog" Visible='<%#Convert.ToString(Eval("Contents"))!=string.Empty?true:false %>' ToolTip="Content View"
                                                                AlternateText="Content View" CommandArgument='<%#Eval("id")%>' ForeColor="White"
                                                                CommandName="contentView" ImageUrl="../GlobalTools/image/Document.png" Height="40" Width="40" runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <%-- Notice Board pdf--%>
                                                            <asp:ImageButton ID="imgbtnDownload" Visible='<%#Convert.ToString(Eval("NoticeFileUrl"))!=string.Empty?true:false %>' ToolTip="Download File"
                                                                AlternateText="Download" CommandArgument='<%#Eval("NoticeFileUrl")%>' ForeColor="White"
                                                                CommandName="pdfdownload" ImageUrl="../GlobalTools/image/pdf.png" Height="40" Width="40" runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>


                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                   
                </table>
            </div>
        
    </asp:Panel>

     <!--Modal-------------------->
    <div id="idmodal012" class="w3-modal">
        <div class="w3-modal-content w3-card-4 w3-animate-zoom" >
            <div class="w3-center">
                <br />
                <span onclick="document.getElementById('idmodal012').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
            </div>

           <%-- Body --%>
            <div class="w3-container">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="w3-section">
                                <%-- Date --%>
                                <p runat="server" id="pSubject" style="direction: rtl; color: forestgreen"><%# Eval("Subjects")%></p>

                                <%-- Date --%>
                                <p runat="server" id="pPublishDate" style="direction: rtl; color: forestgreen"><%# Eval("PublishDate","{0:d}")%></p>

                                <%-- Publisher --%>
                                <p runat="server" id="pPublisher" class="text-info"><%# Eval("Publisher")%></p>
                                <hr />

                                <%-- Content --%>
                                <p runat="server" id="pContents" class="w3-agileits-blog-text"><%# Eval("Contents")%></p>

                                <hr />
                                <%-- Footer --%>
                                <p runat="server" id="pFooter" style="direction: rtl;" class=""><%# Eval("Footer")%></p>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </div>

            <%-- footer --%>
            <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                <button onclick="document.getElementById('idmodal012').style.display='none'" type="button" class="w3-button w3-red">Cancel</button>

            </div>

        </div>
    </div>
   <!--Modal-------------------->

     <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#tblNoticBoard').DataTable({
                "scrollX": true
            });

            //column Content
            table.column(5).visible(false);

            //column footer
            table.column(6).visible(false);

        });
    </script>
</asp:Content>


