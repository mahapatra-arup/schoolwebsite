﻿using Microsoft.AspNet.FriendlyUrls;
using System.Web.Mvc;
using System.Web.Routing;

namespace SchoolWebApplication
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //1
            //DynamicPage Control Pages Go to-->DynamicPage.aspx
            routes.MapPageRoute("DynamicPage", "Pages/{PageName}", "~/Web/DynamicPage.aspx");

            //or Use This For All Page
            //routes.MapPageRoute("DynamicPage", "Web/{PageName}", "~/Web/{PageName}.aspx");
            // ref :https://stackoverflow.com/questions/4441222/mvc-mappageroute-and-actionlink


            //2
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);

            //3
            routes.MapRoute(
                    name: "Default",
                    url: "{controller}/{action}/{id}",
                    defaults: new { action = "Index", id = UrlParameter.Optional }
                );
        }
    }
}
