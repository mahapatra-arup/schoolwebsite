﻿using System.Web.Optimization;

namespace SchoolWebApplication
{
    public class BundleConfig
    {
        // For more information on Bundling, visit https://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            #region Global============>
            #region js----------------------->
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/GlobalTools/jQuery/jquery.min.js"));
            //
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/GlobalTools/bootstrap/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/SmoothScroll").Include(
                    "~/GlobalTools/bootstrap/js/SmoothScroll.min.js"));
            //
            bundles.Add(new ScriptBundle("~/bundles/move_top").Include(
                    "~/GlobalTools/bootstrap/js/move-top.js"));
            //
            bundles.Add(new ScriptBundle("~/bundles/easing").Include(
                    "~/GlobalTools/bootstrap/js/easing.js"));

            //jquery shorten
            bundles.Add(new ScriptBundle("~/bundles/jquery_shorten").Include(
                     "~/GlobalTools/jQuery/jquery.shorten.1.0.js"));


            #region use admin Section
            bundles.Add(new ScriptBundle("~/bundles/jquery_validate").Include(
                  "~/GlobalTools/jQuery/jquery.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery_validate_unobtrusive").Include(
                "~/GlobalTools/jQuery/jquery.validate.unobtrusive.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/GlobalTools/jQuery/modernizr.js"));

            //-----------tinymce-------------------
            //bundles.Add(new ScriptBundle("~/bundles/tinymce").Include(
            //          "~/GlobalTools/tinymce/tinymce.min.js"));//#!!Because after bundle plugin are not source
            bundles.Add(new ScriptBundle("~/bundles/init-tinymce").Include(
                      "~/GlobalTools/tinymce/init-tinymce.js"));
            #endregion

            #region --use Web Section--
            //ism for Carousel_Slider ref:https://imageslidermaker.com/v2
            bundles.Add(new ScriptBundle("~/bundles/ism_2_2").Include(
                "~/GlobalTools/Imageslider/ism-2.2.min.js"));
            #endregion

            #endregion

            #region CSS----------------------->
            bundles.Add(new StyleBundle("~/Content/css/bootstrap").Include(
                      "~/GlobalTools/bootstrap/css/bootstrap.min.css"));


            bundles.Add(new StyleBundle("~/Content/css/gStyle").Include(
                  "~/GlobalTools/gStyle.css"));

            bundles.Add(new StyleBundle("~/Content/css/W3").Include(
           "~/GlobalTools/W3.css"));


            bundles.Add(new StyleBundle("~/Content/css/LinkStyle").Include(
        "~/GlobalTools/Link/LinkStyle.css"));


            bundles.Add(new StyleBundle("~/Content/css/ScrollBars").Include(
     "~/GlobalTools/ScrollBar/ScrollBars.css"));

            //Remove Jquery Ui Close Button(Login Window)
            bundles.Add(new StyleBundle("~/Content/css/JqueryUI_CloseButton_Remove").Include(
                      "~/GlobalTools/JqueryUI_CloseButton_Remove"));



            #region Web Section css
            //Carousel_Slider ref:https://imageslidermaker.com/v2
            bundles.Add(new StyleBundle("~/Content/css/Carousel_Slider").Include(
                        "~/GlobalTools/Imageslider/Carousel_Slider.css"));
            #endregion
            #endregion

            #endregion


            #region Admin=========>




            //--------------------------------
            //Css--------------------------------------------------------------------------->
            //------------------------------
            bundles.Add(new StyleBundle("~/Content/css/style").Include(
                  "~/Admin/css/Customcss/style.css"));

            #endregion


            #region Web===========>

            #region js
            //Site Master-1
            bundles.Add(new ScriptBundle("~/bundles/Site1Master").Include(
                       "~/Web/js/Site1Master.js"));
            //Home
            bundles.Add(new ScriptBundle("~/bundles/Home").Include(
                      "~/Web/js/Home.js"));

            //about--
            bundles.Add(new ScriptBundle("~/bundles/AboutUs").Include(
                      "~/Web/js/AboutUs.js"));
            //Gallary
            bundles.Add(new ScriptBundle("~/bundles/Gallery").Include(
                      "~/Web/js/Gallery.js"));


            //jssor.slider for use gallery
            bundles.Add(new ScriptBundle("~/bundles/jssor_slider").Include(
          "~/Web/js/jssor.slider-27.1.0.min.js"));



            #region -User-Control(js)
            //jssor.slider for use gallery
            bundles.Add(new ScriptBundle("~/bundles/ucSite1_nav1").Include(
          "~/Web/UserControl/js/ucSite1_nav1.js"));
            #endregion
            #endregion



            #region css

            bundles.Add(new StyleBundle("~/Content/css/Site1master").Include(
                       "~/Web/css/Site1master.css"));
            //Home
            bundles.Add(new StyleBundle("~/Content/css/Home").Include(
                     "~/Web/css/Home.css"));

            //NoticeBoard
            bundles.Add(new StyleBundle("~/Content/css/NoticeBoard").Include(
                    "~/Web/css/NoticeBoard.css"));


            //Staff
            bundles.Add(new StyleBundle("~/Content/css/Staff").Include(
                    "~/Web/css/Staff.css"));

            //Gallery
            bundles.Add(new StyleBundle("~/Content/css/Gallery").Include(
                    "~/Web/css/Gallery.css"));



            #region -User Control (css)-
            //UC_Footer
            bundles.Add(new StyleBundle("~/Content/css/UC_Footer").Include(
                    "~/Web/UserControl/css/UC_Footer.css"));

            //UC_Form_BrackingNews
            bundles.Add(new StyleBundle("~/Content/css/UC_Form_BrackingNews").Include(
                    "~/Web/UserControl/css/UC_Form_BrackingNews.css"));

            //ucSite1_nav1
            bundles.Add(new StyleBundle("~/Content/css/ucSite1_nav1").Include(
                    "~/Web/UserControl/css/ucSite1_nav1.css"));


            //ucSite1_Top1
            bundles.Add(new StyleBundle("~/Content/css/ucSite1_Top1").Include(
                    "~/Web/UserControl/css/ucSite1_Top1.css"));
            #endregion
            #endregion

            #endregion
        }

    }
}