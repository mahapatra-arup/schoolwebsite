﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admission/AdmissionMaster.master" AutoEventWireup="true" CodeBehind="Sp-StudentAdmission.aspx.cs" Inherits="SchoolWebApplication.Admission.Sp_StudentAdmission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--css links-->
    <style>
        #wizHeader li .prevStep {
            background-color: #669966;
        }

            #wizHeader li .prevStep:after {
                border-left-color: #669966 !important;
            }

        #wizHeader li .currentStep {
            background-color: #C36615;
        }

            #wizHeader li .currentStep:after {
                border-left-color: #C36615 !important;
            }

        #wizHeader li .nextStep {
            background-color: #C2C2C2;
        }

            #wizHeader li .nextStep:after {
                border-left-color: #C2C2C2 !important;
            }

        #wizHeader {
            list-style: none;
            overflow: hidden;
            font: 18px Helvetica, Arial, Sans-Serif;
            margin: 0px;
            padding: 0px;
        }

            #wizHeader li {
                float: left;
            }

                #wizHeader li a {
                    color: white;
                    text-decoration: none;
                    padding: 10px 0 10px 55px;
                    background: brown; /* fallback color */
                    background: hsla(34,85%,35%,1);
                    position: relative;
                    display: block;
                    float: left;
                }

                    #wizHeader li a:after {
                        content: " ";
                        display: block;
                        width: 0;
                        height: 0;
                        border-top: 50px solid transparent; /* Go big on the size, and let overflow hide */
                        border-bottom: 50px solid transparent;
                        border-left: 30px solid hsla(34,85%,35%,1);
                        position: absolute;
                        top: 50%;
                        margin-top: -50px;
                        left: 100%;
                        z-index: 2;
                    }

                    #wizHeader li a:before {
                        content: " ";
                        display: block;
                        width: 0;
                        height: 0;
                        border-top: 50px solid transparent;
                        border-bottom: 50px solid transparent;
                        border-left: 30px solid white;
                        position: absolute;
                        top: 50%;
                        margin-top: -50px;
                        margin-left: 1px;
                        left: 100%;
                        z-index: 1;
                    }

                #wizHeader li:first-child a {
                    padding-left: 10px;
                }

                #wizHeader li:last-child {
                    padding-right: 50px;
                }

                #wizHeader li a:hover {
                    background: #FE9400;
                }

                    #wizHeader li a:hover:after {
                        border-left-color: #FE9400 !important;
                    }

        .content {
            /*height: 150px;
    padding-top: 75px;
    text-align: center;
    background-color: #F9F9F9;
    font-size: 48px;*/
        }
    </style>


    <div class="panel panel-primary">
        <div class="panel-heading   text-center">Student Registration Form</div>
        <div class="panel-body">

            <asp:Wizard ID="Wizard1" runat="server" CssClass="container-fluid" DisplaySideBar="False" Width="100%">

                <%-- Header Design --%>

                <HeaderTemplate>
                    <ul id="wizHeader" class="fa-list-ul">
                        <asp:Repeater ID="SideBarList" runat="server">
                            <ItemTemplate>
                                <li><a class="<%# GetClassForWizardStep(Container.DataItem) %>" title="<%#Eval("Name")%>">
                                    <%# Eval("Name")%></a> </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </HeaderTemplate>

                <%-- First  Nex Button --%>
                <StartNavigationTemplate>
                    <asp:Button ID="StartNextButton" runat="server" ValidationGroup="SDetailsGroup1" CssClass="btn btn-danger" CommandName="MoveNext" Text="  Next  " />
                </StartNavigationTemplate>

                <%-- Previous and Next Button --%>
                <StepNavigationTemplate>
                    <asp:Button ID="StepPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious" CssClass="btn btn-info" Text="Previous" />
                    <asp:Button ID="StepNextButton" runat="server" ValidationGroup="SMpMarksGroup1" CommandName="MoveNext" CssClass="btn btn-danger" Text="  Next  " />
                </StepNavigationTemplate>


                <%-- Finish --%>
                <FinishNavigationTemplate>
                    <asp:Button ID="FinishPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious" CssClass="btn btn-info" Text="Previous" />
                    <asp:Button ID="FinishButton" runat="server" CommandName="MoveComplete" CssClass="btn btn-danger" OnClientClick="if (!UserConfirmationMessage()) return false;" Text="Finish" />
                </FinishNavigationTemplate>


                <WizardSteps>

                    <%-- HS marks  form --%>
                    <asp:WizardStep ID="WizardStep1" runat="server" Title="Student Details">
                        <hr />

                        <div class="container-fluid">

                            <%-- First row --%>

                            <div class="row">

                                <%-- ++++++++++++++Left Side+++++++++ --%>

                                <div class="col-sm-8">
                                    <div class="form-group">


                                        <%-- Student Name --%>


                                        <label class="label label-warning" for="txtStudentName">Student Name : </label>
                                        <asp:TextBox ID="txtStudentName" class="form-control" runat="server" Placeholder="**Enter StudentName (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorName" runat="server" ControlToValidate="txtStudentName" Display="Dynamic" ErrorMessage="Student Name is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>
                                        <%-- Father Name --%>
                                        <br />
                                        <label class="label label-warning" for="txtFatherName">Father Name : </label>
                                        <asp:TextBox ID="txtFatherName" class="form-control" runat="server" Placeholder="**Enter StudentName (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFatherName" Display="Dynamic" ErrorMessage="Father Name is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>
                                        <%-- mother Name --%>
                                        <br />
                                        <label class="label label-info" for="txtMotherName">Mother Name : </label>
                                        <asp:TextBox ID="txtMotherName" class="form-control" runat="server" Placeholder="**Enter StudentName (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMotherName" Display="Dynamic" ErrorMessage="Father Name is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>

                                    </div>
                                </div>

                                <%-- ----------Right SIde---------- --%>
                                <div class="col-sm-4 fa-border" style="background-color: white; text-align: center">
                                    <%-- --------Photo-------- --%>
                                    <label for="FileuploadOfStudent">Student Image : </label>
                                    <div>


                                        <asp:Image ID="ImageOfStudent" runat="server" class="img-responsive img-thumbnail" Height="180px" Width="180px" GenerateEmptyAlternateText="True" ViewStateMode="Enabled" />

                                        <asp:FileUpload ID="FileuploadOfStudent" onchange="FileOnchangeforSiteMeta(this);" class="form-control btn-file" runat="server" ViewStateMode="Enabled" />
                                        <%-- Reular expression --%>
                                        <asp:RegularExpressionValidator ID="rev_FileuploadOfStudent" runat="server" Display="Dynamic"
                                            ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
                                            ControlToValidate="FileuploadOfStudent" ValidationGroup="SDetailsGroup" SetFocusOnError="True" ForeColor="Red"></asp:RegularExpressionValidator>
                                        <%-- File Size Check --%>
                                        <asp:CustomValidator ID="cv_FileuploadOfStudent" ValidationGroup="SDetailsGroup" runat="server" ErrorMessage="File size cannot be greater than 200 KB " OnServerValidate="cv_FileuploadOfStudent_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileuploadOfStudent" ValidateRequestMode="Enabled"></asp:CustomValidator>
                                    </div>
                                </div>

                            </div>

                            <hr />
                            <%-- 2nd row --%>
                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">

                                        <%-- Dob --%>

                                        <%-- // --%>
                                        <label for="txtDob">DOB :</label>
                                        <asp:TextBox ID="txtDob" runat="server" class="form-control" TextMode="Date"></asp:TextBox>

                                        <%--//Gender --%>

                                        <br />
                                        <label for="rbtnGender">Gender : </label>
                                        <asp:RadioButtonList ID="rbtnGender" class="form-control" runat="server" RepeatDirection="Horizontal" Style="text-align: center; vertical-align: central">
                                            <asp:ListItem Selected="True">Male</asp:ListItem>
                                            <asp:ListItem>Female</asp:ListItem>
                                            <asp:ListItem>Others</asp:ListItem>
                                        </asp:RadioButtonList>


                                        <%-- Caste --%>

                                        <br />
                                        <label for="cmbCaste">Caste : </label>
                                        <asp:DropDownList ID="cmbCaste" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem>GENERAL</asp:ListItem>
                                            <asp:ListItem>SC</asp:ListItem>
                                            <asp:ListItem>ST</asp:ListItem>
                                            <asp:ListItem>OBC</asp:ListItem>
                                            <asp:ListItem>Others</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCaste" runat="server" ControlToValidate="cmbCaste" Display="Dynamic" InitialValue="0" ErrorMessage="Caste is required" ForeColor="Red" ValidationGroup="SDetailsGroup" SetFocusOnError="True"></asp:RequiredFieldValidator>

                                        <%-- Religion --%>

                                        <br />
                                        <label for="cmbReligion">Religion :</label>
                                        <asp:DropDownList ID="cmbReligion" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem>HINDUISM</asp:ListItem>
                                            <asp:ListItem>ISLAM</asp:ListItem>
                                            <asp:ListItem>CHRISTIANITY</asp:ListItem>
                                            <asp:ListItem>SIKHISM</asp:ListItem>
                                            <asp:ListItem>BUDDHISM</asp:ListItem>
                                            <asp:ListItem>JAINISM</asp:ListItem>
                                            <asp:ListItem>ZOROASTRIANISM</asp:ListItem>
                                            <asp:ListItem>JUDAISM</asp:ListItem>

                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorReligion" runat="server" ControlToValidate="cmbReligion" Display="Dynamic" InitialValue="0" ErrorMessage="Religion is required" ForeColor="Red" ValidationGroup="SDetailsGroup" SetFocusOnError="True"></asp:RequiredFieldValidator>

                                        <%-- AadhaarNo --%>
                                        <br />
                                        <label for="txtAadhaarNo">Aadhaar No :</label>
                                        <asp:TextBox ID="txtAadhaarNo" TextMode="Number" class="form-control" runat="server" Placeholder="**Enter Aadhaar No (limit of 12 characters)" MaxLength="12"></asp:TextBox>
                                        <%-- Expression --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid Aadhaar No" ControlToValidate="txtPostalCode" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                                            ValidationExpression="^\d{4}\d{4}\d{4}$" ValidationGroup="SDetailsGroup"></asp:RegularExpressionValidator>


                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">

                                        <%-- Vill --%>

                                        <label for="txtVill">Village No :</label>
                                        <asp:TextBox ID="txtVill" class="form-control" runat="server" Placeholder="**Enter Village (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtVill" runat="server" ControlToValidate="txtVill" Display="Dynamic" ErrorMessage="Vill  is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>
                                        <%-- po --%>
                                        <br />
                                        <label for="txtpo">P.O.:</label>
                                        <asp:TextBox ID="txtpo" class="form-control" runat="server" Placeholder="**Enter P.O. (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtpo" runat="server" ControlToValidate="txtpo" Display="Dynamic" ErrorMessage="PO  is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>
                                        <%-- ps --%>
                                        <br />
                                        <label for="txtps">P.S.:</label>
                                        <asp:TextBox ID="txtps" class="form-control" runat="server" Placeholder="**Enter P.S. (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtps" runat="server" ControlToValidate="txtps" Display="Dynamic" ErrorMessage="PS  is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>
                                        <%-- Block --%>
                                        <br />
                                        <label for="txtBlock">Block:</label>
                                        <asp:TextBox ID="txtBlock" class="form-control" runat="server" Placeholder="**Enter Block (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtBlock" runat="server" ControlToValidate="txtBlock" Display="Dynamic" ErrorMessage="Block  is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>

                                        <%-- Subdivision --%>

                                        <br />
                                        <label for="txtSubdivision">Subdivision :</label>
                                        <asp:TextBox ID="txtSubdivision" class="form-control" runat="server" Placeholder="**Enter SubDivision (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtSubdivision" runat="server" ControlToValidate="txtSubdivision" Display="Dynamic" ErrorMessage="Subdivision  is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>



                                    </div>
                                </div>



                                <div class="col-sm-4">
                                    <div class="form-group">

                                        <%-- Dist --%>

                                        <label for="cmbDist">Dist :</label>
                                        <asp:DropDownList ID="cmbDist" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem>Bankura</asp:ListItem>
                                            <asp:ListItem>Bardhaman</asp:ListItem>
                                            <asp:ListItem>Birbhum</asp:ListItem>
                                            <asp:ListItem>Purba Medinipur</asp:ListItem>
                                            <asp:ListItem>Hooghly</asp:ListItem>
                                            <asp:ListItem>Purulia</asp:ListItem>
                                            <asp:ListItem>Paschim Medinipur</asp:ListItem>
                                            <asp:ListItem>Cooch Behar</asp:ListItem>
                                            <asp:ListItem>Darjeeling</asp:ListItem>
                                            <asp:ListItem>Jalpaiguri</asp:ListItem>
                                            <asp:ListItem>Malda</asp:ListItem>
                                            <asp:ListItem>North Dinajpur</asp:ListItem>
                                            <asp:ListItem>South Dinajpur</asp:ListItem>
                                            <asp:ListItem>Howrah</asp:ListItem>
                                            <asp:ListItem>Kolkata</asp:ListItem>
                                            <asp:ListItem>Murshidabad</asp:ListItem>
                                            <asp:ListItem>Nadia</asp:ListItem>
                                            <asp:ListItem>North 24 Parganas</asp:ListItem>
                                            <asp:ListItem>South 24 Parganas</asp:ListItem>
                                            <asp:ListItem>East Singbhum</asp:ListItem>
                                            <asp:ListItem>West Singbhum</asp:ListItem>


                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cmbDist" Display="Dynamic" InitialValue="0" ErrorMessage="Dist is required" ForeColor="Red" ValidationGroup="SDetailsGroup" SetFocusOnError="True"></asp:RequiredFieldValidator>


                                        <%-- State --%>

                                        <br />
                                        <label for="cmbState">State :</label>
                                        <asp:DropDownList ID="cmbState" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem>ANDHRA PRADESH</asp:ListItem>
                                            <asp:ListItem>ARUNACHAL PRADESH</asp:ListItem>
                                            <asp:ListItem>ASSAM</asp:ListItem>
                                            <asp:ListItem>BHIAR</asp:ListItem>
                                            <asp:ListItem>CHANDIGARH</asp:ListItem>
                                            <asp:ListItem>CHHATTISGARH</asp:ListItem>
                                            <asp:ListItem>DELHI</asp:ListItem>
                                            <asp:ListItem>GOA</asp:ListItem>
                                            <asp:ListItem>GUJRAT</asp:ListItem>
                                            <asp:ListItem>HARYANA</asp:ListItem>
                                            <asp:ListItem>HIMACHAL PRADESH</asp:ListItem>
                                            <asp:ListItem>JAMMU AND KASHMIR</asp:ListItem>
                                            <asp:ListItem>Jharkhand</asp:ListItem>
                                            <asp:ListItem>KARNATAKA</asp:ListItem>
                                            <asp:ListItem>KERALA</asp:ListItem>
                                            <asp:ListItem>MADHYA PRADESH</asp:ListItem>
                                            <asp:ListItem>MAHARASTRA</asp:ListItem>
                                            <asp:ListItem>MANIPUR</asp:ListItem>
                                            <asp:ListItem>MEGHALAYA</asp:ListItem>
                                            <asp:ListItem>MIZORAM</asp:ListItem>
                                            <asp:ListItem>NAGALAND</asp:ListItem>
                                            <asp:ListItem>ODISHA</asp:ListItem>
                                            <asp:ListItem>PUNJAB</asp:ListItem>
                                            <asp:ListItem>RAJASTAN</asp:ListItem>
                                            <asp:ListItem>SIKKIM</asp:ListItem>
                                            <asp:ListItem>TAMIL NADU</asp:ListItem>
                                            <asp:ListItem>TELANGANA</asp:ListItem>
                                            <asp:ListItem>TRIPURA</asp:ListItem>
                                            <asp:ListItem>UTTAR PRADESH</asp:ListItem>
                                            <asp:ListItem>UTTARAKHAND</asp:ListItem>
                                            <asp:ListItem>West Bengal</asp:ListItem>


                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbState" Display="Dynamic" InitialValue="0" ErrorMessage="State is required" ForeColor="Red" ValidationGroup="SDetailsGroup" SetFocusOnError="True"></asp:RequiredFieldValidator>

                                        <%-- pin --%>

                                        <br />
                                        <label for="txtPostalCode">Postal Code:</label>
                                        <asp:TextBox ID="txtPostalCode" class="form-control" TextMode="Number" runat="server" Placeholder="**Enter Postal Code (limit of 6 characters)" MaxLength="6"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPostalCode" Display="Dynamic" ErrorMessage="Postal Code  is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>
                                        <%-- Expression --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorPostalCode" runat="server" ErrorMessage="Invalid Postal Code" ControlToValidate="txtPostalCode" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                                            ValidationExpression="^[1-9][0-9]{5}$" ValidationGroup="SDetailsGroup"></asp:RegularExpressionValidator>


                                        <%-- PhNo --%>

                                        <br />
                                        <label for="txtPhNo">Ph. No. :</label>
                                        <asp:TextBox ID="txtPhNo" class="form-control" AutoCompleteType="HomePhone" runat="server" TextMode="SingleLine" Placeholder="Enter Student Phone Number" MaxLength="12"></asp:TextBox>

                                        <%-- Expression --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhNo" runat="server" ErrorMessage="Invalid PhNo" ControlToValidate="txtPhNo" ForeColor="#FF6600" SetFocusOnError="True"
                                            ValidationExpression="^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}|(\d[ -]?){10}\d$" ValidationGroup="SDetailsGroup" Display="Dynamic"></asp:RegularExpressionValidator>

                                        <%-- Same School --%>
                                        <br />
                                        <label for="chkXStudent">Same School :</label>
                                        <asp:CheckBox ID="chkXStudent" class="form-control" runat="server" />

                                        <br />
                                    </div>
                                </div>

                            </div>

                            <hr />
                            <%-- 3rd row for Security Purpose --%>
                            <div class="row">

                                <%--1> column --%>

                                <div class="col-sm-6">

                                    <%-- UserName --%>
                                    <asp:UpdatePanel ID="UpdatePaneleUser" runat="server">
                                        <ContentTemplate>
                                            <label class="label label-warning" for="txtUserName">User Name (ex:info@example.com) : </label>
                                            <asp:TextBox ID="txtUserName" class="form-control" runat="server" Placeholder="**Enter Email Address (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                            <%-- required filed --%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUserName" Display="Dynamic" ErrorMessage="User Name is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>
                                            <%-- already exost check --%>
                                            <asp:CustomValidator ID="cvExistCheck" OnServerValidate="cvExistCheck_ServerValidate" runat="server" ErrorMessage="User Name is not available. please try another one" ControlToValidate="txtUserName" ValidationGroup="SDetailsGroup" ForeColor="Red" Display="Dynamic" BorderStyle="Ridge" SetFocusOnError="True"></asp:CustomValidator>
                                            <%-- expression --%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" ControlToValidate="txtUserName" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SDetailsGroup"></asp:RegularExpressionValidator>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <%-- Password --%>

                                    <br />
                                    <label for="txtpsw">Password :</label>
                                    <asp:TextBox ID="txtpsw" class="form-control" runat="server" Placeholder="**Enter Password (limit of 10 characters)" MaxLength="10" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtPassword" runat="server" ControlToValidate="txtpsw" Display="Dynamic" ErrorMessage="Password  is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>

                                    <%--Confirm Password --%>
                                    <br />
                                    <label for="txtConfirmPsw">Confirm Password :</label>
                                    <asp:TextBox ID="txtConfirmPsw" class="form-control" runat="server" Placeholder="**Enter Confirm Password (limit of 10 characters)" MaxLength="10" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtConfirmPsw" Display="Dynamic" ErrorMessage="Password  is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtConfirmPsw" ControlToCompare="txtpsw" Display="Dynamic" ErrorMessage="Password does not match the confirm password" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:CompareValidator>

                                </div>

                                <%--2> column--%>
                                <div class="col-sm-6">
                                    <%-- Security Question --%>
                                    <%-- Security Answer --%>

                                    <%--// Button--%>
                                    <br />
                                    <label for="cmbSecurityQuestion">Security Question :</label>
                                    <asp:DropDownList ID="cmbSecurityQuestion" runat="server" class="form-control" Width="100%">
                                        <asp:ListItem> is the first and last name of your first boyfriend or girlfriend?</asp:ListItem>
                                        <asp:ListItem>Which phone number do you remember most from your childhood?</asp:ListItem>
                                        <asp:ListItem>What was your favorite place to visit as a child?</asp:ListItem>
                                        <asp:ListItem>Who is your favorite actor, musician, or artist?</asp:ListItem>
                                        <asp:ListItem>What is the name of your favorite pet?</asp:ListItem>
                                        <asp:ListItem>In what city were you born?</asp:ListItem>
                                        <asp:ListItem>What high school did you attend?</asp:ListItem>
                                        <asp:ListItem>What is the name of your first school?</asp:ListItem>
                                        <asp:ListItem>What is your favorite movie?</asp:ListItem>
                                        <asp:ListItem>What is your mother's maiden name?</asp:ListItem>
                                        <asp:ListItem>What street did you grow up on?</asp:ListItem>
                                        <asp:ListItem>What was the make of your first car?</asp:ListItem>
                                        <asp:ListItem>When is your anniversary?</asp:ListItem>
                                        <asp:ListItem>What is your favorite color?</asp:ListItem>
                                        <asp:ListItem>What is your father's middle name?</asp:ListItem>
                                        <asp:ListItem>What is the name of your first grade teacher?</asp:ListItem>
                                        <asp:ListItem>What was your high school mascot?</asp:ListItem>
                                        <asp:ListItem>Which is your favorite web browser?</asp:ListItem>
                                        <asp:ListItem>what is your favorite website?</asp:ListItem>
                                        <asp:ListItem>what is your favorite forum?</asp:ListItem>
                                        <asp:ListItem>what is your favorite online platform?</asp:ListItem>
                                        <asp:ListItem>what is your favorite social media website?</asp:ListItem>


                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="cmbSecurityQuestion" Display="Dynamic" InitialValue="0" ErrorMessage="Security Question is required" ForeColor="Red" ValidationGroup="SDetailsGroup" SetFocusOnError="True"></asp:RequiredFieldValidator>


                                    <br />
                                    <label for="txtSecurityAnswer">Security Answer :</label>
                                    <asp:TextBox ID="txtSecurityAnswer" class="form-control" runat="server" Placeholder="**Enter Security Answer (limit of 25 characters)" MaxLength="25"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtpsw" Display="Dynamic" ErrorMessage="Security answer  is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SDetailsGroup"></asp:RequiredFieldValidator>

                                    <hr />
                                </div>

                            </div>

                        </div>
                    </asp:WizardStep>



                    <asp:WizardStep ID="WizardStep2" runat="server" Title="M.P. Details">
                        <div class="container-fluid">
                            <div class="form-group">
                                <hr />

                                <div class="row" style="background: linear-gradient(#4fd0d8,#5cd174)">

                                    <div class="col-sm-4">
                                        <%-- Passing Year --%>
                                        <label for="cmbPassingYear">Passing Year :</label>
                                        <asp:DropDownList ID="cmbPassingYear" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem>2011-2012</asp:ListItem>
                                            <asp:ListItem>2012-2013</asp:ListItem>
                                            <asp:ListItem>2013-2014</asp:ListItem>
                                            <asp:ListItem>2014-2015</asp:ListItem>
                                            <asp:ListItem>2015-2016</asp:ListItem>
                                            <asp:ListItem>2016-2017</asp:ListItem>
                                            <asp:ListItem>2017-2018</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="cmbSecurityQuestion" Display="Dynamic" InitialValue="0" ErrorMessage="Passing Year is require" ForeColor="Red" ValidationGroup="SMpMarksGroup" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="col-sm-4">
                                        <%-- Roll --%>
                                        <label for="txtRoll">Roll :</label>
                                        <asp:TextBox ID="txtRoll" class="form-control" runat="server" Placeholder="**Enter Roll (limit of 25 characters)" MaxLength="25"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtRoll" Display="Dynamic" ErrorMessage="MP Roll  is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SMpMarksGroup"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="col-sm-4">

                                        <%-- No --%>
                                        <label for="txtNo">No :</label>
                                        <asp:TextBox ID="txtNo" class="form-control" runat="server" Placeholder="**Enter No (limit of 25 characters)" MaxLength="25"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtNo" Display="Dynamic" ErrorMessage="MP No  is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SMpMarksGroup"></asp:RequiredFieldValidator>
                                    </div>


                                </div>

                                <hr />
                                <div class="row">

                                    <div class="col-sm-3">

                                        <%-- ======================1> Start==================== --%>
                                        <%-- Language 1 --%>
                                        <label for="cmblang1Sub">1st Language :</label>
                                        <asp:DropDownList ID="cmblang1Sub" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem Selected="True">Bengali</asp:ListItem>
                                            <asp:ListItem>English</asp:ListItem>
                                            <asp:ListItem>Hindi</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-sm-3">

                                        <%-- lag1 --%>
                                        <label for="txtlang1">Marks :</label>
                                        <asp:TextBox ID="txtlang1" class="form-control" runat="server" Placeholder="**Enter Mark of Subject"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtlang1" Display="Dynamic" ErrorMessage="Subject Mark  is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SMpMarksGroup"></asp:RequiredFieldValidator>
                                        <%-- ++++++++++++++++++++++++End++++++++++++++++++++++++ --%>
                                    </div>

                                    <div class="col-sm-3">
                                        <%-- ==========================2>start=================== --%>
                                        <%-- Language 2 --%>
                                        <label for="cmblang2Sub">2nd Language :</label>
                                        <asp:DropDownList ID="cmblang2Sub" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem>Bengali</asp:ListItem>
                                            <asp:ListItem Selected="True">English</asp:ListItem>
                                            <asp:ListItem>Hindi</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-sm-3">
                                        <%-- lag2 --%>
                                        <label for="txtlang2">Marks :</label>
                                        <asp:TextBox ID="txtlang2" class="form-control" runat="server" Placeholder="**Enter Mark of Subject"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtlang2" Display="Dynamic" ErrorMessage="Subject Mark  is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SMpMarksGroup"></asp:RequiredFieldValidator>
                                        <%-- ==========================End=================== --%>
                                    </div>

                                </div>

                                <hr />
                                <div class="row">
                                    <div class="col-sm-3">
                                        <%-- ------------------3>start----------------- --%>
                                        <%-- sub 1 --%>
                                        <label for="cmbsub1">Subject-I :</label>
                                        <asp:DropDownList ID="cmbsub1" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem Selected="True">Geography</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-sm-3">
                                        <%-- sub 1 marks --%>
                                        <label for="txtSub1">Marks :</label>
                                        <asp:TextBox ID="txtSub1" class="form-control" runat="server" Placeholder="**Enter Mark of Subject"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtSub1" Display="Dynamic" ErrorMessage="Subject Mark  is requir" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SMpMarksGroup"></asp:RequiredFieldValidator>

                                        <%--//end ============================================================= --%>
                                    </div>


                                    <div class="col-sm-3">
                                        <%-- ------------------4>start----------------- --%>
                                        <%-- sub 2--%>
                                        <label for="cmbSub2">Subject-II :</label>
                                        <asp:DropDownList ID="cmbSub2" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem Selected="True">History</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-sm-3">
                                        <%-- sub 2 marks --%>
                                        <label for="txtSub2">Marks :</label>
                                        <asp:TextBox ID="txtSub2" class="form-control" runat="server" Placeholder="**Enter Mark of Subject"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtSub2" Display="Dynamic" ErrorMessage="Subject Mark  is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SMpMarksGroup"></asp:RequiredFieldValidator>

                                        <%--//end ============================================================= --%>
                                    </div>
                                </div>

                                <hr />
                                <div class="row">
                                    <div class="col-sm-3">
                                        <%-- ------------------5> start----------------- --%>
                                        <%-- sub 3--%>
                                        <label for="cmbSub3">Subject-III :</label>
                                        <asp:DropDownList ID="cmbSub3" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem Selected="True">Physical Science</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-sm-3">

                                        <%-- sub 2 marks --%>
                                        <label for="txtSub3">Marks :</label>
                                        <asp:TextBox ID="txtSub3" class="form-control" runat="server" Placeholder="**Enter Mark of Subject"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtSub3" Display="Dynamic" ErrorMessage="Subject Mark  is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SMpMarksGroup"></asp:RequiredFieldValidator>

                                        <%--//end ============================================================= --%>
                                    </div>

                                    <div class="col-sm-3">
                                        <%-- ------------------6>start----------------- --%>
                                        <%-- sub 4--%>
                                        <label for="cmbSub4">Subject-IV :</label>
                                        <asp:DropDownList ID="cmbSub4" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem Selected="True">Mathematics</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-sm-3">

                                        <%-- sub 4 marks --%>
                                        <label for="txtSub4">Marks :</label>
                                        <asp:TextBox ID="txtSub4" class="form-control" runat="server" Placeholder="**Enter Mark of Subject"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtSub4" Display="Dynamic" ErrorMessage="Subject Mark  is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SMpMarksGroup"></asp:RequiredFieldValidator>

                                        <%--//end ============================================================= --%>
                                    </div>
                                </div>

                                <hr />
                                <div class="row">
                                    <div class="col-sm-3">
                                        <%-- ------------------7//>start----------------- --%>
                                        <%-- sub 5--%>
                                        <label for="cmbSub5">Subject-V :</label>
                                        <asp:DropDownList ID="cmbSub5" runat="server" class="form-control" Width="100%">
                                            <asp:ListItem Selected="True">Life Science</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <div class="col-sm-3">
                                        <%-- sub 5 marks --%>
                                        <label for="txtSub5">Marks :</label>
                                        <asp:TextBox ID="txtSub5" class="form-control" runat="server" Placeholder="**Enter Mark of Subject"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtSub5" Display="Dynamic" ErrorMessage="Subject Mark  is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="SMpMarksGroup"></asp:RequiredFieldValidator>

                                        <%--//end ============================================================= --%>
                                    </div>

                                    <div class="col-sm-3"></div>

                                    <div class="col-sm-3 bg-warning">
                                        <%-- ============Total marks ============--%>
                                        <label for="txtTotal">Total :</label>
                                        <asp:TextBox ID="txtTotal" class="form-control" runat="server" Placeholder="**Enter Mark of Subject"></asp:TextBox>

                                    </div>

                                </div>

                                <hr />
                                <div class="row">
                                    <div class="col-sm-3">
                                        <%-- ===================Admit Image================ --%>
                                        <label for="FileUploadAdmitImage">Admit Image :</label>
                                        <asp:FileUpload ID="FileUploadAdmitImage" class="form-control" runat="server" />

                                        <%-- Regular expreession --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorFileUpload" runat="server" Display="Dynamic"
                                            ErrorMessage="Only .png and jpg and jpeg Files are allow" ControlToValidate="FileUploadAdmitImage" SetFocusOnError="True" ForeColor="Red"
                                            ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                                        <%-- File Size Check --%>
                                        <asp:CustomValidator ID="vc_FileUploadAdmitImage" ValidationGroup="SMpMarksGroup" runat="server" ErrorMessage="" OnServerValidate="vc_FileUploadAdmitImage_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUploadAdmitImage"></asp:CustomValidator>
                                    </div>

                                    <div class="col-sm-3">

                                        <%-- ===================Marksheet Image================ --%>
                                        <label for="FileUploadMarksheetImage">Marksheet:</label>
                                        <asp:FileUpload ID="FileUploadMarksheetImage" class="form-control" runat="server" />

                                        <%-- Regular expreession --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                            ErrorMessage="Only .png and jpg and jpeg Files are allow" ControlToValidate="FileUploadMarksheetImage" SetFocusOnError="True" ForeColor="Red"
                                            ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ValidationGroup="SMpMarksGroup"></asp:RegularExpressionValidator>

                                        <%-- File Size Check --%>
                                        <asp:CustomValidator ID="cv_MarksheetCheck" ValidationGroup="SMpMarksGroup" runat="server" ErrorMessage="" OnServerValidate="cv_MarksheetCheck_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUploadAdmitImage"></asp:CustomValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:WizardStep>



                    <asp:WizardStep ID="WizardStep3" runat="server" Title="H.S. Details">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>

                                <div class="container-fluid">

                                    <div class="form-group">

                                        <div class="row fa-border">

                                            <%-- ============Stream ============--%>
                                            <div class="col-sm-4">
                                                <label for="cmbStream">Stream :</label>
                                                <asp:DropDownList ID="cmbStream" OnSelectedIndexChanged="cmbStream_SelectedIndexChanged" runat="server" class="form-control" Width="100%" AutoPostBack="True">
                                                </asp:DropDownList>

                                            </div>

                                            <%-- ============1 st Language ============--%>
                                            <div class="col-sm-4">
                                                <label for="cmbFirstLanguage">1st Language :</label>
                                                <asp:DropDownList ID="cmbFirstLanguage" runat="server" class="form-control" Width="100%">
                                                </asp:DropDownList>

                                            </div>

                                            <%-- ============2nd language  ============--%>
                                            <div class="col-sm-4">
                                                <label for="cmbSecondLanguage">2nd Language :</label>
                                                <asp:DropDownList ID="cmbSecondLanguage" runat="server" class="form-control" Width="100%">
                                                    <asp:ListItem Selected="True" Value="-1">--Select--</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>

                                        <hr />
                                        <%-- =========SUbject List============ --%>
                                        <div class="row fa-border">
                                            <div class="col-sm-8">
                                                <asp:PlaceHolder ID="PlaceHolder_SubjectGroup" runat="server"></asp:PlaceHolder>
                                            </div>
                                            <%-- ===Selected Subject list==== --%>
                                             <div class="col-sm-4">
                                                 <asp:ListBox ID="ListBox_SelectedSubject" CssClass="form-control" runat="server"></asp:ListBox>
                                        </div>

                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:WizardStep>

                </WizardSteps>

            </asp:Wizard>


        </div>
    </div>


    <%------------Script For file upload ------------%>
    <%------------Script For file upload ------------%>
    <script>
        function FileOnchangeforSiteMeta(input) {
            var imgv = document.getElementById("<%=ImageOfStudent.ClientID %>");
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(imgv)
                        .attr('src', e.target.result)
                        .width(180)
                        .height(180);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        //confermmation Message box
        function UserConfirmationMessage() {
            return confirm("Are you sure you want to delete this user?");
        }
    </script>
</asp:Content>
