﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace SchoolWebApplication.Admission
{
    public partial class Sp_StudentAdmission : System.Web.UI.Page
    {
        #region ==================Global Variable with Assign=================
        string _AdmitfileName = string.Empty;
        string _MarkSheertfileName = string.Empty;
        string _StudentfileName = string.Empty;

        double _lAdmit_MarkSheet_MaxFileSize = 512000;//1mb
        double _lStudentImageFileSize = 204800;//200kb 
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            Wizard1.PreRender += new EventHandler(Wizard1_PreRender);
            if (!IsPostBack)
            {
                cmbStream.GetStream();
                cmbStream.SelectedIndex = -1;
            }
        }

        #region =====================Wizerd Control=======================
        protected void Wizard1_PreRender(object sender, EventArgs e)
        {
            Repeater SideBarList = Wizard1.FindControl("HeaderContainer").FindControl("SideBarList") as Repeater;
            SideBarList.DataSource = Wizard1.WizardSteps;
            SideBarList.DataBind();

        }

        protected string GetClassForWizardStep(object wizardStep)
        {
            WizardStep step = wizardStep as WizardStep;

            if (step == null)
            {
                return "";
            }
            int stepIndex = Wizard1.WizardSteps.IndexOf(step);

            if (stepIndex < Wizard1.ActiveStepIndex)
            {
                return "prevStep";
            }
            else if (stepIndex > Wizard1.ActiveStepIndex)
            {
                return "nextStep";
            }
            else
            {
                return "currentStep";
            }
        }

        #endregion

        #region ===================Studentdetails=====================

        protected void cvExistCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string data = args.Value;
            args.IsValid = false;
            if (Student_Details_Tools.DuplicateUserName(txtUserName.Text))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }


        protected void cv_FileuploadOfStudent_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileuploadOfStudent.FileContent.Length;
            if (FileuploadOfStudent.HasFile)
            {
                if (filesize > _lStudentImageFileSize)
                {
                    args.IsValid = false;
                    cv_FileuploadOfStudent.ErrorMessage = "File size cannot be greater than 200kb";
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                args.IsValid = false;
                cv_FileuploadOfStudent.ErrorMessage = "Student Image is required";
            }
        }
        private void StudentImageFileCheck()
        {
            if (FileuploadOfStudent.HasFile)
            {
                // Get the file extension
                string fileExtension = System.IO.Path.GetExtension(FileuploadOfStudent.FileName);

                if (fileExtension.ToLower() != ".jpg" && fileExtension.ToLower() != ".jpeg" && fileExtension.ToLower() != ".png"
                    && fileExtension.ToUpper() != ".JPG" && fileExtension.ToUpper() != ".JPEG" && fileExtension.ToUpper() != ".PNG")
                {
                    Response.Write("<script>alert('Only files with .jpg and .jpeg and png extension are allowed');</script>");
                }
                else
                {
                    // Get the file size
                    int fileSize = FileuploadOfStudent.PostedFile.ContentLength;
                    // If file size is greater than 2 MB
                    if (fileSize > _lStudentImageFileSize)//byte formate
                    {
                        Response.Write("<script>alert('File size cannot be greater than 200 KB');</script>");

                        //Ref:http://converter.elliotbeken.com/
                    }
                    else
                    {
                        // Specify the path to save the uploaded file to.
                        string savePath = "../FilesAndFolder/StudentImage/";

                        // Get the name of the file to upload.
                        _StudentfileName = txtStudentName.Text + "_" + (txtDob.Text).Replace('-', '_') + fileExtension;
                        // Create the path and file name to check for duplicates.
                        string pathToCheck = savePath + _StudentfileName;

                        //Alrady exist Check
                        #region ......AlreadyExist Check..........
                        // Create a temporary file name to use for checking duplicates.
                        string tempfileName = "";

                        // Check to see if a file already exists with the
                        // same name as the file to upload.
                        if (System.IO.File.Exists(pathToCheck))
                        {
                            int counter = 2;
                            while (System.IO.File.Exists(pathToCheck))
                            {
                                // if a file with this name already exists,
                                // prefix the filename with a number.
                                tempfileName = counter.ToString() + _StudentfileName;
                                pathToCheck = savePath + tempfileName;
                                counter++;
                            }

                            _StudentfileName = savePath + tempfileName;

                        }
                        else
                        {
                            _StudentfileName = pathToCheck;
                        }
                        #endregion
                    }
                }
            }

        }
        private void ThumbNailFileUpload(string path)
        {
            if (!path.ISNullOrWhiteSpace())
            {
                // Notify the user that the file name was changed.
                FileuploadOfStudent.SaveAs(Server.MapPath(path));
                //Image Show
            }
        }
        #endregion

        #region ====================MP Details======================
        protected void vc_FileUploadAdmitImage_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileUploadAdmitImage.FileContent.Length;
            if (FileUploadAdmitImage.HasFile)
            {
                if (filesize > _lAdmit_MarkSheet_MaxFileSize)
                {
                    args.IsValid = false;
                    cv_FileuploadOfStudent.ErrorMessage = "File size cannot be greater than 1mb";
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                args.IsValid = false;
                cv_FileuploadOfStudent.ErrorMessage = "Admit Image is required";
            }
        }

        protected void cv_MarksheetCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileUploadMarksheetImage.FileContent.Length;
            if (FileUploadMarksheetImage.HasFile)
            {
                if (filesize > _lAdmit_MarkSheet_MaxFileSize)
                {
                    args.IsValid = false;
                    cv_MarksheetCheck.ErrorMessage = "File size cannot be greater than 1mb";
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                args.IsValid = false;
                cv_MarksheetCheck.ErrorMessage = "Admit Image is required";
            }
        }
        #endregion

        #region ======================Hs Details==================
        private void FillSubject(string streamid)
        {
            int i = 0;
            DataTable dt = Subjects_StreamUtils.GetStreamGroup(streamid);
            if (dt.IsValidDataTable())
            {
                foreach (DataRow item in dt.Rows)
                {
                    string groupId = Convert.ToString(dt.Rows[0]["Id"]);

                    DataTable dt1 = Subjects_StreamUtils.GetSubject(groupId);
                    if (dt1.IsValidDataTable())
                    {
                        //dynamic Checkbox list
                        CheckBoxList chklst = new CheckBoxList();
                        chklst.ID = "chkSub" + (++i);
                        chklst.RepeatDirection = RepeatDirection.Horizontal;

                        chklst.CssClass = "form-control";

                        //AddedControl Check Box Item
                        foreach (DataRow item_sub in dt1.Rows)
                        {
                            ListItem lsd = new ListItem();
                            lsd.Attributes.Add(Convert.ToString(item_sub["id"]), Convert.ToString(item_sub["Subject_Name"]));
                            lsd.Text = Convert.ToString(item_sub["Subject_Name"]);
                            chklst.Items.Add(lsd);
                        }
                        chklst.SelectedIndexChanged += Chklst_SelectedIndexChanged;
                        chklst.AutoPostBack = true;
                        //AddedControl check box in place holder
                        PlaceHolder_SubjectGroup.Controls.Add(chklst);
                    }
                }
            }

        }

        private void Chklst_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmbStream_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbStream.SelectedIndex >= 0)
            {
                string _sreamid = cmbStream.SelectedValue;
                string _sreamName = cmbStream.SelectedItem.Text;
                FillSubject(_sreamid);
            }

        }
        #endregion
    }
}