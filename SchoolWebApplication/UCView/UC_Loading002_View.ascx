﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Loading002_View.ascx.cs" Inherits="SchoolWebApplication.UCView.UC_Loading002_View" %>


<style>
    #ucdvLoading02 {
        background: url('../GlobalTools/image/Loading/Loading1.gif') no-repeat center center;
        height: 20%;
        width: 20%;
        position: fixed;
        left: 50%;
        top: 50%;
        margin: -25px 0 0 -25px;
        z-index: 1000;
    }
</style>

<script>
    $(window).on("load",function () {
        $('#ucdvLoading02').fadeOut(2000);
    });
</script>

<%-- ----For Page Load Time Loading------ --%>
<div id="ucdvLoading02"></div>


<%-- Donot use update panel if use update panel the continue load show when change update panel --%>