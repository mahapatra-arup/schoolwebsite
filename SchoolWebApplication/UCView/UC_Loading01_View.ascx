﻿<%@ Control Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="UC_Loading01_View.ascx.cs" Inherits="SchoolWebApplication.UCView.UC_Loading01_View" %>
<style>
    #ucdivLoading01 {
height: 15%;
width: 15%;
position: fixed;
left: 50%;
top: 50%;
margin: -25px 0 0 -25px;
z-index: 1000;
}
</style>


<script type="text/javascript">
    // Get the instance of PageRequestManager.
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    // Add initializeRequest and endRequest
    prm.add_initializeRequest(prm_InitializeRequest);
    prm.add_endRequest(prm_EndRequest);

    // Called when async postback begins
    function prm_InitializeRequest(sender, args) {
        // get the divImage and set it to visible
        var panelProg = $get('ucdivLoading01');
        panelProg.style.display = '';
        // reset label text
        var lbl = $get('<%= this.lblText.ClientID %>');
        lbl.innerHTML = '';

        // Disable button that caused a postback
        $get(args._postBackElement.id).disabled = true;
    }

    // Called when async postback ends
    function prm_EndRequest(sender, args) {
        // get the divImage and hide it again
        var panelProg = $get('ucdivLoading01');
        panelProg.style.display = 'none';

        // Enable button that caused a postback
        $get(sender._postBackSettings.sourceElement.id).disabled = false;
    }
</script>

<%-- ----------For Update panel load time Loading------------- --%>
<asp:UpdatePanel ID="UpdatePanelLoading01" runat="server">
    <ContentTemplate>
        <asp:Label ID="lblText" runat="server" Text=""></asp:Label>
        <div id="ucdivLoading01" style="display: none">
            <asp:Image ID="img1" runat="server" ImageUrl="../GlobalTools/image/Loading/SpinnerProgress.svg" />
            Processing...
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
