﻿<%@ Control Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="UC_SnackBar_View.ascx.cs" Inherits="SchoolWebApplication.Web.UserControl.UC_SnakBer_View" %>

<link href="../GlobalTools/SnackBar/snackber.css" rel="stylesheet" type="text/css" />

<!--====================== The actual snackbar=========================== -->
<asp:UpdatePanel ID="UpdatePanel_Snackber" runat="server">
    <ContentTemplate>
        <div id="snackbar"><i class="fa fa-spinner fa-spin" style="font-size: 35px; color: white"></i></div>
    </ContentTemplate>
</asp:UpdatePanel>
<!-- =======================//The actual snackbar===================== -->

<script src="../GlobalTools/SnackBar/snackber.js"></script>

<%-- Call like  <asp:Button ID="btnSubmit" runat="server"  OnClientClick="snakberfunction()" /> --%>
