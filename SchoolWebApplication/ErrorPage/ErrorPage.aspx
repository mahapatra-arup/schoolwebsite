﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="SchoolWebApplication.ErrorPage" %>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Oops 404 Not Found</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href='//fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister' rel='stylesheet' type='text/css' />
    <style type="text/css">
        body {
            font-family: 'Love Ya Like A Sister', cursive;
        }

        body {
            background: #eaeaea;
        }

        .wrap {
            margin: 0 auto;
            width: 100%;
        }

      
        .footer {
            color: black;
            position: absolute;
            right: 10px;
            bottom: 10px;
        }

            .footer a {
                color: rgb(114, 173, 38);
            }
    </style>
</head>


<body>
  
    <div class="wrap">
          <div>
            <h2>Error:</h2>
            <p></p>
            <asp:Label ID="FriendlyErrorMsg" runat="server" Text="Label" Font-Size="Large" Style="color: red"></asp:Label>

            <asp:Panel ID="DetailedErrorPanel" runat="server" Visible="false">
                <p>&nbsp;</p>
                <h4>Detailed Error:</h4>
                <p>
                    <asp:Label ID="ErrorDetailedMsg" runat="server" Font-Size="Small" /><br />
                </p>

                

                <h4>Detailed Error Message:</h4>
                <p>
                    <asp:Label ID="InnerMessage" runat="server" Font-Size="Small" /><br />
                </p>
                <p>
                    <asp:Label ID="InnerTrace" runat="server" />
                </p>
            </asp:Panel>

        </div>
 <p><a runat="server" id="lnkPreviousLink">Back To Previous </a> &nbsp;&nbsp; <a href="../Default.aspx">Back To Home </a></p>
        <br />
    </div>


    <div class="footer">  
        &copy 2019 Oops 404 . All Rights Reserved | Design by <span>Drycode Technology LLP.</span>
    </div>

</body>
</html>
