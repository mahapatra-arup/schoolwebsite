﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SchoolWebApplication.Admin
{
    public partial class Sp_Admin : System.Web.UI.MasterPage
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                //which Form Request the user
                //  string RequestPage = Page.AppRelativeVirtualPath;
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                FillSchoolDetails();
                FillUserDetails();
            }
        }

        protected void lblLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            try
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Buffer = true;
                Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                Response.Expires = -1000;
                Response.CacheControl = "no-cache";
                //Response.Redirect("login.aspx", true);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            //Redirect
            Response.Redirect("Login.aspx");
        }

        private void FillSchoolDetails()
        {
            //get
            var _SchoolDetails = SqlHelper.DbContext().SchoolProfiles.FirstOrDefault();
            var _DevloperSignature = SqlHelper.DbContext().DevloperSignatures.FirstOrDefault();

            //Site Title Ico fixed in admin panel
            if (_SchoolDetails.Logo != null && _SchoolDetails.Logo.Length > 0)
            {
                Image img = System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo);
                titleico2.Href = img.ImageUrl;
                titleico1.Href = img.ImageUrl;

                lblHeaderName.Text = "<img src='" + img.ImageUrl + "' style='height: 25px!important' class='img-thumbnail img-circle' /> &nbsp";
            }


            Page.Title = _SchoolDetails.SchoolName;
            Page.MetaKeywords = _SchoolDetails.SchoolName + ", " + _SchoolDetails.At;
            footer.InnerHtml = "<p>" + _DevloperSignature.Copyright + "" + _DevloperSignature.KeyWords +
                "<a href=" + _DevloperSignature.link + " target=_blank> " + _DevloperSignature.Name + " </a> </p>";

            lblHeaderName.Text += _SchoolDetails.SchoolName;
        }

        private void FillUserDetails()
        {
            string username = HttpContext.Current.User.Identity.Name;
            var db = UserUtils.GetUserDetails(username);
            if (db.ISValidObject())
            {
                lblUserName.Text = db.display_name;

                if (db.User_Image != null && db.User_Image.Length > 0)
                {
                    //Recomended Height and width set 100*110
                    //imgSchoolLogo.Visible = true;
                    Image img = System_Web_UI_WebControls_Imager.byteArrayToWebImage(db.User_Image);
                    imgUserImage.ImageUrl = img.ImageUrl;
                }
            }
        }
        protected void lblUpdaterSetting_Click(object sender, EventArgs e)
        {
            DBUpdateUtils.Execute();
            Response.Write("<script>alert('Update Successfull');</script>");
        }
    }
}