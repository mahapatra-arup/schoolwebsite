﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin
{
    public partial class ThemeMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region ===============Method=============
        private string GetSectionName()
        {
            string _sectionName = radio_1.Checked ? "HEADER" :
              radio_2.Checked ? "NAV" : radio_3.Checked ? "BODY" : "FOOTER";
            return _sectionName;
        }
        private void FillColor()
        {
            try
            {
                var lst = ThemeDesignTools.GetDetails(GetSectionName());
                if (lst.IsValidList())
                {
                    //View Fill
                    ThemeDesignTools.ViewColor(Page, GetCurrentPageName(), ref pnlBackColorView, lst, false, false);

                    //Get Color
                    foreach (var item in lst)
                    {
                        //Text Box Fill
                        if (item.AttributeKey.ToLower() == "color")
                        {
                            txtForeColorCode.Text = item.AttributeValue;
                        }
                        else if (item.AttributeKey.ToLower() == "background")
                        {
                            var arrVal = item.AttributeValue.Split(',');
                            if (arrVal.Length > 0)
                            {
                                txtFirstBackColor.Text = (arrVal[1].Split(' ')[0]).ConvertObjectToString();
                                txtSecondBackColor.Text = (arrVal[2].Split(' ')[0]).ConvertObjectToString();
                                txtThirdBackColor.Text = (arrVal[3].Split(' ')[0]).ConvertObjectToString();

                                //Percentage
                                txtFirstBackColor_Range.Text = (arrVal[1].Split(' ')[1]).ConvertObjectToString();
                                txtSecondBackColor_Range.Text = (arrVal[2].Split(' ')[1]).ConvertObjectToString();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }
        protected void ForeGroundColor_Refresh()
        {
            txtForeColorCode.Text = hfforecolor.Value;
            lblSectionForeColor.Style.Add(HtmlTextWriterStyle.Color, txtForeColorCode.Text);
        }
        private void DeleteAndAddData()
        {
            btnBackcolorRefresh_ServerClick(null, null);

            string _sectionName = GetSectionName();
            string key = string.Empty;
            string value = string.Empty;

            #region =================For Delete================
            //Fill Data
            key = "background";
            List<ThemeDesign> Delete_ThemeDesign = new List<ThemeDesign>();

            //Back Color
            Delete_ThemeDesign.Add(new ThemeDesign()
            {
                SectionName = _sectionName,
                AttributeKey = key,
            });


            //Fore Color
            key = "color";
            Delete_ThemeDesign.Add(new ThemeDesign()
            {
                SectionName = _sectionName,
                AttributeKey = key,
            });

            //ThemeDesignTools.Delete(Delete_ThemeDesign);
            #endregion

            #region =================For Create================
            //Fill Data
            key = "background";
            value = "linear-gradient(" + cmbBackColorDirestion.Text + "," + txtFirstBackColor.Text + " " + txtFirstBackColor_Range.Text
                + "%," + txtSecondBackColor.Text + " " + txtSecondBackColor_Range.Text + "%," + txtThirdBackColor.Text + ")";
            List<ThemeDesign> Create_ThemeDesign = new List<ThemeDesign>();

            //Back Color
            Create_ThemeDesign.Add(new ThemeDesign()
            {
                SectionName = _sectionName,
                AttributeKey = key,
                AttributeValue = value,
                MasterPageId = DefultThemeMasterFile.Id,
                PageId = null
            });


            //Fore Color
            key = "color";
            value = txtForeColorCode.Text;
            Create_ThemeDesign.Add(new ThemeDesign()
            {
                SectionName = _sectionName,
                AttributeKey = key,
                AttributeValue = value,
                PageId = null,
                MasterPageId = DefultThemeMasterFile.Id
            });


            #endregion

            if (ThemeDesignTools.DeleteAndCreate(Delete_ThemeDesign, Create_ThemeDesign))
            {
                MessageBoxAlart.Show("Data Save Successfull", "Success!", MessageBoxAlart._messegeType.info, MessageBoxAlart._alartTopIndex.Simple, ref ltrMessege);
            }
        }
        public string GetCurrentPageName()
        {
            string sPath = Request.Url.AbsolutePath;
            System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
            string sRet = oInfo.Name;
            return sRet;
        }
        #endregion

        #region =============Event================
        protected void btnTemp_Click(object sender, EventArgs e)
        {
            string _sectionName = GetSectionName();
            BindGrid(_sectionName);
            FillColor();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!txtFirstBackColor_Range.Text.ISNullOrWhiteSpace() && !txtSecondBackColor_Range.Text.ISNullOrWhiteSpace())
                {
                    DeleteAndAddData();
                }
                else
                {
                    MessageBoxAlart.Show("Invalid Color Range", "Stop!", MessageBoxAlart._messegeType.warning, MessageBoxAlart._alartTopIndex.Topindex, ref ltrMessege);
                    txtSecondBackColor_Range.Focus();
                }
            }
        }

        protected void btnBackcolorRefresh_ServerClick(object sender, EventArgs e)
        {
            ForeGroundColor_Refresh();

            if (!txtFirstBackColor_Range.Text.ISNullOrWhiteSpace() && !txtSecondBackColor_Range.Text.ISNullOrWhiteSpace())
            {
                List<SchoolWebLibrary.Models.ThemeDesign> _ThemeDesign = new List<ThemeDesign>();

                //===Back Ground===
                string key = "background";
                string value = "linear-gradient(" + cmbBackColorDirestion.Text + "," + txtFirstBackColor.Text + " " + txtFirstBackColor_Range.Text
                    + "%," + txtSecondBackColor.Text + " " + txtSecondBackColor_Range.Text + "%," + txtThirdBackColor.Text + ")";

                _ThemeDesign.Add(new ThemeDesign()
                {
                    AttributeKey = key,
                    AttributeValue = value,
                    PageId = null,
                    MasterPageId = DefultThemeMasterFile.Id
                }
                    );

                //===Fore Ground===
                key = "color";
                value = hfforecolor.Value;

                _ThemeDesign.Add(new SchoolWebLibrary.Models.ThemeDesign()
                {
                    AttributeKey = key,
                    AttributeValue = value,
                    PageId = null,
                    MasterPageId = DefultThemeMasterFile.Id
                }
                     );

                ThemeDesignTools.ViewColor(Page, GetCurrentPageName(), ref pnlBackColorView, _ThemeDesign, false, false);

            }
            else
            {
                MessageBoxAlart.Show("Invalid Color Range", "Stop!", MessageBoxAlart._messegeType.warning, MessageBoxAlart._alartTopIndex.Topindex, ref ltrMessege);
                txtSecondBackColor_Range.Focus();
            }
        }


        #endregion

        #region <--------External CSS------------>
        private void SaveExternalCss()
        {
            string _sectionName = GetSectionName();
            if (Page.IsValid)
            {
                lblAlreadyExist.Visible = false;
                if (!ThemeDesignTools.IsExistGroup(txtExternalAttributeKey.Text))
                {
                    if (Page.IsValid)
                    {
                        var _tThemeDesign = new ThemeDesign
                        {
                            PageId = null,
                            MasterPageId = DefultThemeMasterFile.Id,
                            SectionName = _sectionName,
                            AttributeKey = txtExternalAttributeKey.Text,
                            AttributeValue = txtExternalAttributevalue.Text
                        };
                        var issuccess = ThemeDesignTools.InsertThemeDesign(_tThemeDesign);
                        if (issuccess)
                        {
                            Show("Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrMessege);
                            this.BindGrid(_sectionName);
                        }
                    }
                }
                else
                {
                    lblAlreadyExist.Visible = true;
                }
            }
        }
        private void BindGrid(string sectionname)
        {
            var data = ThemeDesignTools.GetDetails(sectionname);
            if (data.IsValidIEnumerable())
            {
                gvExternalCssView.DataSource = data.ToList();
                gvExternalCssView.DataBind();
                gvExternalCssView.HeaderRow.TableSection = TableRowSection.TableHeader;

            }
            else
            {
                gvExternalCssView.DataSource = null;
                gvExternalCssView.DataBind();
            }
        }
        #region <---=Eventy=-->
        protected void gvExternalCssView_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            string _sectionName = GetSectionName();
            List<ThemeDesign> Delete_ThemeDesign = new List<ThemeDesign>();
            List<ThemeDesign> Create_ThemeDesign = new List<ThemeDesign>();
            //if (Page.IsValid)
            //{
            //Finding the controls from Gridview for the row which is going to update   
            Label lbl_ExternalcssKey = gvExternalCssView.Rows[e.RowIndex].FindControl("lbl_ExternalAttcssKey") as Label;
            TextBox New_txt_ExternalcssKey = gvExternalCssView.Rows[e.RowIndex].FindControl("txt_ExternalcssKey") as TextBox;
            TextBox New_txt_ExternalcssValue = gvExternalCssView.Rows[e.RowIndex].FindControl("txt_ExternalcssValue") as TextBox;

            if (!New_txt_ExternalcssKey.Text.ISNullOrWhiteSpace() && !New_txt_ExternalcssValue.Text.ISNullOrWhiteSpace())
            {
                var old_lbl_ExternalcssKey = lbl_ExternalcssKey.Text;
                //Delete
                Delete_ThemeDesign.Add(new ThemeDesign()
                {
                    SectionName = _sectionName,
                    AttributeKey = old_lbl_ExternalcssKey,
                });

                //Create
                Create_ThemeDesign.Add(new ThemeDesign()
                {
                    SectionName = _sectionName,
                    AttributeKey = New_txt_ExternalcssKey.Text.ConvertObjectToString(),
                    AttributeValue = New_txt_ExternalcssValue.Text.ConvertObjectToString(),
                    MasterPageId = DefultThemeMasterFile.Id,
                    PageId = null
                });

                ThemeDesignTools.DeleteAndCreate(Delete_ThemeDesign, Create_ThemeDesign);
            }
            else
            {
                if (ThemeDesignTools.Delete(Delete_ThemeDesign))
                {
                    Show("Delete Successfull", "Delete!", _messegeType.info, _alartTopIndex.Simple, ref ltrMessege);
                }
            }
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview   
            gvExternalCssView.EditIndex = -1;
            //Call ShowData method for displaying updated data   
            this.BindGrid(_sectionName);
            //}
        }

        protected void gvExternalCssView_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            //NewEditIndex property used to determine the index of the row being edited.   
            gvExternalCssView.EditIndex = e.NewEditIndex;
            string _sectionName = GetSectionName();
            BindGrid(_sectionName);
        }

        protected void gvExternalCssView_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview   
            gvExternalCssView.EditIndex = -1;
            string _sectionName = GetSectionName();
            BindGrid(_sectionName);
        }

        protected void btnExternalCssSubmit_Click(object sender, EventArgs e)
        {
            SaveExternalCss();
        }
        #endregion
        #endregion
    }
}