﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="Setting.aspx.cs" Inherits="SchoolWebApplication.Admin.Setting" ValidateRequest="false" %>

<%@ Register Src="~/Admin/UCView/Setting/UC_SettingOfWebSiteAnalytics.ascx" TagPrefix="uc1" TagName="UC_SettingOfWebSiteAnalytics" %>
<%@ Register Src="~/Admin/UCView/Setting/UC_EmailSetting.ascx" TagPrefix="uc1" TagName="UC_EmailSetting" %>
<%@ Register Src="~/Admin/UCView/Setting/UC_SiteMetaSettings.ascx" TagPrefix="uc1" TagName="UC_SiteMetaSettings" %>
<%@ Register Src="~/Admin/UCView/Setting/UC_SocialDetailsSettings.ascx" TagPrefix="uc1" TagName="UC_SocialDetailsSettings" %>
<%@ Register Src="~/Admin/UCView/Setting/UC_ThumbnailSettings.ascx" TagPrefix="uc1" TagName="UC_ThumbnailSettings" %>
<%@ Register Src="~/Admin/UCView/Setting/UC_StaffSubGroupSettings.ascx" TagPrefix="uc1" TagName="UC_StaffSubGroupSettings" %>
<%@ Register Src="~/Admin/UCView/Setting/UC_StaffGroupSettings.ascx" TagPrefix="uc1" TagName="UC_StaffGroupSettings" %>









<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <%-- css --%>
    <link href="css/Customcss/Settings.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="panel-group" id="setting_accordion">

                    <%-- -------Modules------- --%>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#setting_accordion" href="#collapseOne"><span class="glyphicon glyphicon-th"></span>Modules</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <table class="table nav nav-pills">
                                    <tr>
                                        <td class="active">
                                            <span class="glyphicon glyphicon-pencil text-primary"></span>
                                            <a data-toggle="pill" href="#GroupSettings">Staff-Groups</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-pencil text-primary"></span>
                                            <a data-toggle="pill" href="#WebSiteAnalytics">WebSite Analytics</a></>
                                    </td>
                                    </tr>



                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-pencil text-primary"></span>
                                            <a data-toggle="pill" href="#SiteMetaSettings">SiteMeta</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-pencil text-primary"></span>
                                            <a data-toggle="pill" href="#ThumbnailSettings">ThumbNail</a>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </div>
                    <%-- -------Modules------- --%>

                    <%-- -------Social------- --%>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#setting_accordion" href="#collapseTwo"><span class="fa fa-envelope-open-o"></span>&nbsp;Social</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">

                                    <tr>
                                        <td>
                                            <span class="fa fa-envelope text-primary"></span>
                                            <a data-toggle="pill" href="#EmailSetting">&nbsp; Email</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <span class="fa fa-comment-o text-primary"></span>
                                            <a data-toggle="pill" href="#SocialDetailsSettings">&nbsp; Social Details</a>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </div>
                    <%-- -------Social------- --%>
                </div>
            </div>

            <%--------- View Part----- --%>
            <div class="col-sm-7 col-md-7">

                <div class="tab-content well">

                    <div id="GroupSettings" class="tab-pane fade in active">
                        <div class="">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Sub-Group</a></li>
                                <li><a data-toggle="tab" href="#menu1">Group</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                     <uc1:UC_StaffSubGroupSettings runat="server" id="UC_StaffSubGroupSettings" />
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                    <uc1:UC_StaffGroupSettings runat="server" id="UC_StaffGroupSettings" />
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div id="WebSiteAnalytics" class="tab-pane fade">
                        <uc1:UC_SettingOfWebSiteAnalytics runat="server" id="UC_SettingOfWebSiteAnalytics" />
                    </div>


                    <div id="SiteMetaSettings" class="tab-pane fade">
                        <uc1:UC_SiteMetaSettings runat="server" id="UC_SiteMetaSettings" />
                    </div>

                    <div id="ThumbnailSettings" class="tab-pane fade">
                        <uc1:UC_ThumbnailSettings runat="server" id="UC_ThumbnailSettings" />
                    </div>


                    <div id="EmailSetting" class="tab-pane fade">
                        <uc1:UC_EmailSetting runat="server" id="UC_EmailSetting" />
                    </div>

                    <div id="SocialDetailsSettings" class="tab-pane fade">
                        <uc1:UC_SocialDetailsSettings runat="server" id="UC_SocialDetailsSettings" />

                    </div>

                </div>

            </div>
            <%-- View Part --%>
        </div>
    </div>


</asp:Content>
