﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="ImportStudentResult.aspx.cs" Inherits="SchoolWebApplication.Admin.ImportStudentResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="Panel1" runat="server" CssClass="container-fluid">

        <%--== Save Messege== --%>
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
        <%-- ==//Save Messege ==--%>


        <!--=============== View Body ====================-->
        <div class="panel panel-primary w3-card-4">

            <%--== Header== --%>
            <div class="panel-heading">Student Result Import</div>
            <%--==// Header== --%>

            <%-- ==Body== --%>
            <div class="panel-body">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <%-- ============Input Part============ --%>
                        <div class="row w3-padding-small">

                            <!-- Class ComboBox -->
                            <div class="col-md-5">
                                <asp:DropDownList ID="ddlClass" CssClass="form-control" Style="width: 100%" runat="server">
                                    <asp:ListItem Selected="True">--Select--</asp:ListItem>
                                    <asp:ListItem>I</asp:ListItem>
                                    <asp:ListItem>II</asp:ListItem>
                                    <asp:ListItem>III</asp:ListItem>
                                    <asp:ListItem>IV</asp:ListItem>
                                    <asp:ListItem>V</asp:ListItem>
                                    <asp:ListItem>VI</asp:ListItem>
                                    <asp:ListItem>VII</asp:ListItem>
                                    <asp:ListItem>VIII</asp:ListItem>

                                    <asp:ListItem>IX</asp:ListItem>
                                    <asp:ListItem>X</asp:ListItem>

                                    <asp:ListItem>XI</asp:ListItem>
                                    <asp:ListItem>XII</asp:ListItem>

                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="listView" ControlToValidate="ddlClass" InitialValue="--Select--" runat="server" ErrorMessage="Class is Required" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>

                            <!-- Refresh -->
                            <div class="col-md-2">
                                <button type="button" runat="server" id="btnRefresh" onserverclick="btnRefresh_Click" validationgroup="listView" class="w3-btn  w3-border w3-border-green w3-round"><i class="fa fa-refresh"></i>Refresh</button>
                            </div>

                            <!-- Trigger the modal of import excel -->
                            <div class="col-md-1">
                                <button runat="server" type="button" style="float: right" class="w3-btn  w3-border w3-border-red w3-round-small" data-toggle="modal" data-target="#StudentResultImportModal" title="Import Excel /Marksheet Report"><i class="fa fa-plus-circle"></i>Import</button>
                            </div>

                            <!-- Trigger the modal of marksheet view -->
                            <div class="col-md-2">
                                <button id="btnshowviewofMarksheet" type="button" validationgroup="listView" style="float: right" class="w3-btn  w3-border w3-border-red w3-round-small" data-toggle="modal" data-target="#StudentResultmarksheetViewModal" title="View"><i class="fa fa-ban"></i>View</button>
                            </div>

                            <!-- Delete Details -->
                            <div class="col-md-2">
                                <asp:Button ID="btnDeleteMarksReport" OnClientClick="ConfirmDeleteClassDetails()"  CssClass="btn btn-primary" OnClick="btnDeleteMarksReport_Click" runat="server" Text="Delete" validationgroup="listView" />
                            </div>
                        </div>
                        <%-- =============Input Part============ --%>

                        <%-- =============Gried View============ --%>
                        <div class="table-responsive">
                            <asp:GridView ID="gvStudentResult" runat="server" CssClass="w3-table-all w3-card-4" AutoGenerateColumns="false">

                                <%-- If Empty  --%>
                                <EmptyDataTemplate>
                                    <div class="text-center">!No record found </div>
                                </EmptyDataTemplate>

                            </asp:GridView>
                            <%-- =============Gried View============ --%>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
            <%-- ==//Body== --%>
        </div>
        <!--=============== View Body ====================-->


        <!--=============== Modal Import ====================-->
        <div class="modal fade" id="StudentResultImportModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">

                    <%--===== Modal Header==== --%>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Import Result</h4>
                    </div>
                    <%--//===== Modal Header==== --%>

                    <%-- ======Modal Body =====--%>
                    <div class="modal-body">

                        <%-- ====Tab==== --%>
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#excel">Excel</a></li>
                            <li><a data-toggle="tab" href="#report">Report</a></li>
                        </ul>
                        <%-- ====//Tab==== --%>


                        <div class="tab-content">
                            <br />

                            <%-- =======Excel Import========= --%>
                            <div id="excel" class="tab-pane fade in active">
                                <asp:UpdatePanel ID="UpdatePanel_ResultModal" runat="server">
                                    <ContentTemplate>

                                        <div class="form-group">
                                            <!-- Class ComboBox -->
                                            <asp:DropDownList ID="ddlClassForExcel" CssClass="form-control" runat="server">
                                                <asp:ListItem Selected="True">--Select--</asp:ListItem>

                                                <asp:ListItem>I</asp:ListItem>
                                                <asp:ListItem>II</asp:ListItem>
                                                <asp:ListItem>III</asp:ListItem>
                                                <asp:ListItem>IV</asp:ListItem>
                                                <asp:ListItem>V</asp:ListItem>
                                                <asp:ListItem>VI</asp:ListItem>
                                                <asp:ListItem>VII</asp:ListItem>
                                                <asp:ListItem>VIII</asp:ListItem>

                                                <asp:ListItem>IX</asp:ListItem>
                                                <asp:ListItem>X</asp:ListItem>

                                                <asp:ListItem>XI</asp:ListItem>
                                                <asp:ListItem>XII</asp:ListItem>

                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Importexcel" ControlToValidate="ddlClassForExcel" InitialValue="--Select--" runat="server" ErrorMessage="Class is Required" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>



                                            <%-- fILE UPLOAD --%>
                                            <label>Choose excel file</label>

                                            <asp:FileUpload ID="FileUploadForExcel" CssClass="custom-file-input" runat="server" />

                                            <%-- Reular expression --%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorFileUpload" runat="server" Display="Dynamic"
                                                ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.XLS|.xls)$" ErrorMessage="!!Only .xls Files are allowed"
                                                ControlToValidate="FileUploadForExcel" SetFocusOnError="True" ForeColor="Red" ValidationGroup="Importexcel"></asp:RegularExpressionValidator>

                                            <%-- File Size Check --%>
                                            <asp:CustomValidator ID="validatorCheckFileSize" ValidationGroup="Importexcel" runat="server" ErrorMessage="Excel size cannot be greater than 500KB" OnServerValidate="validatorCheckFileSize_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUploadForExcel" ValidateEmptyText="True"></asp:CustomValidator>
                                            <div class="text-right ">
                                                <asp:Button ID="btnExcelUpload" runat="server" CssClass="ui-button ui-widget ui-corner-all" Text="Upload" ValidationGroup="Importexcel" OnClick="btnExcelUpload_Click" OnClientClick="return confirm('Are you sure You want Delete Old Data Of This Class?');" />
                                            </div>
                                        </div>

                                    </ContentTemplate>

                                    <%-- if button is not trigger inner of update panel then <fileupload.hasFile-- Null> --%>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnExcelUpload" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </div>
                            <%-- =======//Excel Import========= --%>

                            <%-- =======Report Import========= --%>
                            <div id="report" class="tab-pane fade">
                                <asp:UpdatePanel ID="UpdatePanel_ResulrMarksheet" runat="server">
                                    <ContentTemplate>

                                        <div class="form-group">

                                            <!-- Class ComboBox -->
                                            <asp:DropDownList ID="ddlclassForMarksheet" CssClass="form-control" runat="server">
                                                <asp:ListItem Selected="True">--Select--</asp:ListItem>
                                                <asp:ListItem>I</asp:ListItem>
                                                <asp:ListItem>II</asp:ListItem>
                                                <asp:ListItem>III</asp:ListItem>
                                                <asp:ListItem>IV</asp:ListItem>
                                                <asp:ListItem>V</asp:ListItem>
                                                <asp:ListItem>VI</asp:ListItem>
                                                <asp:ListItem>VII</asp:ListItem>
                                                <asp:ListItem>VIII</asp:ListItem>

                                                <asp:ListItem>IX</asp:ListItem>
                                                <asp:ListItem>X</asp:ListItem>

                                                <asp:ListItem>XI</asp:ListItem>
                                                <asp:ListItem>XII</asp:ListItem>

                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="ImportReport" ControlToValidate="ddlclassForMarksheet" InitialValue="--Select--" runat="server" ErrorMessage="Class is Required" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>

                                            <%-- fILE UPLOAD --%>
                                            <label>Choose Pdf file</label>

                                            <asp:FileUpload ID="FileUploadReport" CssClass="custom-file-input" AllowMultiple="true" runat="server" />

                                            <%-- File Size Check --%>
                                            <asp:CustomValidator ID="cv_ReportFilecheck" ValidationGroup="ImportReport" runat="server" ErrorMessage="" OnServerValidate="cv_ReportFilecheck_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUploadReport" ValidateEmptyText="True"></asp:CustomValidator>
                                            <div class="text-right ">
                                                <asp:Button ID="btnReportUpload" runat="server" CssClass="ui-button ui-widget ui-corner-all" Text="Upload" ValidationGroup="ImportReport" OnClick="btnReportUpload_Click" OnClientClick="return confirm('Are you sure You want Delete Old Data Of This Class?');" />
                                            </div>
                                        </div>

                                    </ContentTemplate>

                                    <%-- if button is not trigger inner of update panel then <fileupload.hasFile-- Null> --%>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnReportUpload" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </div>
                            <%-- =======//Report Import========= --%>
                        </div>

                        <%-- Messege --%>
                        <asp:Label ID="lblMessage" ForeColor="Red" runat="server" Text=""></asp:Label>

                    </div>
                    <%-- ======//Modal Body===== --%>

                    <%-- ====Modal Footer==== --%>
                    <div class="modal-footer text-right">
                        <button type="button" class="ui-button ui-widget ui-corner-all" data-dismiss="modal">Close</button>
                    </div>
                    <%-- ====//Modal Footer=== --%>
                </div>
            </div>
        </div>
        <!--=============== //Modal Import ====================-->

        <!--=============== Modal marksheet view====================-->
        <div class="modal fade" id="StudentResultmarksheetViewModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <%--===== Modal Header==== --%>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">View Of Marksheet</h4>
                    </div>
                    <%--//===== Modal Header==== --%>

                    <%-- ======Modal Body =====--%>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="UpdatePanelviewmarksheet" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="container-fluid text-center">

                                    <%-- Marksheet --%>
                                    <asp:PlaceHolder ID="PlaceHoldermarksheet" runat="server"></asp:PlaceHolder>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <%-- ======//Modal Body===== --%>

                    <%-- ====Modal Footer==== --%>
                    <div class="modal-footer text-right col-sm-12">
                        <button type="button" class="ui-button ui-widget ui-corner-all" data-dismiss="modal">Close</button>
                    </div>
                    <br />
                    <%-- ====//Modal Footer=== --%>
                </div>
            </div>
        </div>
        <!--=============== //Modal marksheet view ====================-->

    </asp:Panel>

    <%-- Delete Class Details Confarmation --%>
     <script type = "text/javascript">
        function ConfirmDeleteClassDetails() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirmClassDelete_value";
            if (confirm("Do you want to Delete data?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>

    <%-- For Datatable --%>
    <script type="text/javascript">  
        //Call
        $(function () {
            CreateDataTable();
            UpdatePanelRefresh();
        });

        //--For Datatable-- %>
        function CreateDataTable() {
            $(function () {
                $('#<%=gvStudentResult.ClientID%>').DataTable({ "scrollX": true });
            });
        }

        ////On UpdatePanel Refresh.
        function UpdatePanelRefresh() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        CreateDataTable();
                    }
                });
            };
        }
    </script>
</asp:Content>
