﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="SchoolWebApplication.Admin.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
   <style>
        .myDashboardCard{
            font-size:30px!important;
            padding:0!important;
            margin:5px!important;

        }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="panel panel-primary">
        <div class="panel-heading"><span class="fa fa-leaf"></span></div>

        <div class="panel-body">

            <!-- Staff -->
           <div class="col-md-3  panel panel-primary   text-center myDashboardCard">
                <div class="panel-heading" ><span class="fa fa-user-circle"></span>&nbsp;Staff</div>
                <div class="panel-body bg-light   ">
                   <asp:Label ID="lblTotalStaff" runat="server" Text="0"></asp:Label>
                </div>
            </div>
            <!-- //Staff -->

             <!-- Total Notic  -->
            <div class="col-md-2 panel panel-success text-center myDashboardCard" >
                <div class="panel-heading" ><span class="fa fa-pagelines"></span>&nbsp;Notice</div>
                <div class="panel-body bg-success ">
                     <asp:Label ID="lblTotalNotic" runat="server" Text="0"></asp:Label>
                </div>
            </div>
            <!-- // Notice -->

             <!-- Total post and Event  -->
            <div class="col-md-3  panel panel-warning text-center myDashboardCard" >
                <div class="panel-heading" ><span class="fa fa-eject"></span>&nbsp;Post/Event</div>
                <div class="panel-body bg-warning ">
                   <asp:Label ID="lblTotalPost" runat="server" Text="0"></asp:Label>
                </div>
            </div>
            <!-- //Total post and Event -->

              <!-- Total post and Event  -->
            <div class="col-md-3  panel panel-info text-center myDashboardCard" >
                <div class="panel-heading"><span class="fa fa-history"></span>&nbsp;Result</div>
                <div class="panel-body bg-info" style="font-size:medium !important">
                   <asp:Label ID="lblTotalStudentResult" runat="server" Text="0"></asp:Label>
                </div>
            </div>
            <!-- //Total post and Event -->
        </div>
    </div>

    <asp:Panel ID="pnlNoticeList" runat="server" class="panel panel-primary">
        <div class="panel-heading"><span class="fa fa-users"></span>Visitors Details</div>

        <div class="panel-body">
            <%--ref:https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Other_embedding_technologies--%>
            <div style="height: 100%; width: 100%;">
                <div class="embed-responsive embed-responsive-16by9" style="min-height: 500px">
                    <iframe id="iframDashboard" runat="server" class="embed-responsive-item" allowfullscreen sandbox="allow-scripts"></iframe>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
