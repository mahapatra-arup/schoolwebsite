﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.DataAccesTools;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin
{
    public partial class AddPages : System.Web.UI.Page
    {
        #region <------Var------>
        static Guid _gUpdate_Id;
        #endregion

        #region Prevent Page Refresh in C#

        //For refresh
        private bool _refreshState;
        private bool _isRefresh;
        protected override void LoadViewState(object savedState)
        {
            try
            {
                object[] AllStates = (object[])savedState;
                base.LoadViewState(AllStates[0]);
                _refreshState = bool.Parse(AllStates[1].ToString());
                _isRefresh = _refreshState == bool.Parse(Session["__ISREFRESH"].ToString());
            }
            catch (Exception)
            {
            }
        }

        protected override object SaveViewState()
        {
            Session["__ISREFRESH"] = _refreshState;
            object[] AllStates = new object[2];
            AllStates[0] = base.SaveViewState();
            AllStates[1] = !(_refreshState);
            return AllStates;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.BindGrid();
                ltSaveMessege.Text = "";
            }
        }

        private void BindGrid()
        {
            var data = SqlHelper.DbContext().DynamicPages.OrderBy(s => s.PageName);
            if (data.IsValidIEnumerable())
            {
                gvDetails.DataSource = data.ToList();
                gvDetails.DataBind();
                gvDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void UpdateData()
        {
            if (Page.IsValid && _gUpdate_Id != default(Guid))
            {
                //add
                var isSuccess = DynamicPageUtils.UpdateDynamicpage(_gUpdate_Id, txtContent.Text, txtTitle.Text, txtCss.Text);
                if (isSuccess)
                {
                    //Update  in gried Data
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);

                    Clear();
                    //update grid
                    BindGrid();
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Select Valid Navigation", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }

        private void GetDetails()
        {
            var db = SqlHelper.DbContext();
            var dt = db.DynamicPages.Where(a => a.Id == _gUpdate_Id).FirstOrDefault();
            if (dt.ISValidObject())
            {
                try
                {
                    txtTitle.Text = dt.PageTitle;
                    txtContent.Text = dt.PageContent;
                    txtCss.Text = dt.css;
                }
                catch (Exception)
                {
                }
                //call Modal)
                ScriptManager.RegisterStartupScript(this, GetType(), "myeventModal", "document.getElementById('modelAddPage').style.display = 'block';", true);
            }
        }

        private void Clear()
        {
            //DynamicDesign-------------------------------
            txtTitle.Text = string.Empty;
            txtContent.Text = string.Empty;
            txtCss.Text = string.Empty;
            //id
            _gUpdate_Id = default(Guid);
        }

        #region <=====Event=====>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!_isRefresh) // check that page is not refreshed by browser.
            {
                btnSave.Enabled = false;
                btnSave.Text = "Please Wait.......";
                UpdateData();
                btnSave.Enabled = true;
                btnSave.Text = "Save";
            }

        }

        protected void gvDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRow")
            {
                string vlu = e.CommandArgument.ToString();
                _gUpdate_Id = vlu.ConvertToGuid();
                if (!vlu.ISNullOrWhiteSpace())
                {
                    GetDetails();
                }
                else
                {
                    Show("Select Valid Row", "Warning!", _messegeType.warning, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
        }
        #endregion
    }
}