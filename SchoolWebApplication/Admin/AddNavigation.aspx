﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="AddNavigation.aspx.cs" Inherits="SchoolWebApplication.Admin.AddNavigation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <style>
        [id*=gvDetails] tr:hover, tr.selected {
            background-color: #FFCF8B
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="pnlNavEntry" runat="server" class="container-fluid">
        <!-- Save Messege -->
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
        <div class="row">

            <!--Left Side-->
            <div class="col-md-7">
                <!--insert Mode=========== -->
                <button class="w3-button w3-xlarge w3-circle w3-red w3-card pull-right" runat="server" id="btnAddMode" onserverclick="btnAddMode_ServerClick" title="Active Insert Mode">+</button>


                <div class="panel panel-primary">
                    <div class="panel-heading text-center"><span class="fa fa-key">&nbsp;>Entry (** Bootstrap v3.3.7 Class Supported)</span></div>

                    <div class="panel-body">

                        <!-- Instruction -->
                        <span class="fa fa-info-circle alert alert-info">&nbsp; After add Menu, Automatically Create Default Page !!</span>
                        <!-- //Instruction -->

                        <asp:UpdatePanel runat="server" ID="updtEntryform">
                            <ContentTemplate>

                                <%--Master Page--%>
                                <label class="label label-success" for="ddlPages">Master Pages : </label>
                                <asp:DropDownList ID="ddlPages" runat="server" class="form-control">
                                </asp:DropDownList>


                                <%--page--%>
                                <div class="form-group">

                                    <%-- EmppId --%>
                                    <asp:UpdatePanel ID="UpdatePaneleMpid" runat="server">
                                        <ContentTemplate>
                                            <label class="label label-info" for="txtTitle">Page Name :* </label>
                                            <asp:TextBox ID="txtPageName" class="form-control" PlaceHolder="Enter Your Page Name (limit of 50 characters)..." runat="server" MaxLength="50" AutoPostBack="true" OnTextChanged="txtPageName_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfPage" ValidationGroup="InsertSave" runat="server" ControlToValidate="txtPageName" Display="Dynamic" ErrorMessage="Page Name is Require" ForeColor="Red" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationGroup="InsertSave" ControlToValidate="txtPageName" ErrorMessage="Enter only a-z A-Z 0-9 - _" ValidationExpression="^[a-zA-Z0-9-_ ]*$" ForeColor="Red" SetFocusOnError="true" Display="Dynamic"></asp:RegularExpressionValidator>
                                            <asp:Label ID="lblEmpIdExistMessege" runat="server" Text="" ForeColor="Red"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>

                                <%-- ----Photo---- --%>
                                <div class="form-group">

                                    <label class="label label-success" for="SiteIco_FileUpload">Menu Icon : </label>

                                    <asp:Image ID="ImageOfMenuIcon" runat="server" class="img-thumbnail img-responsive" Height="80px" Width="80px" />

                                    <asp:FileUpload ID="FileUpload_Menuico" onchange="FileOnchangeforSiteMeta(this);" class="btn-file" runat="server" />

                                    <%-- Reular expression --%>
                                    <asp:RegularExpressionValidator ID="rev_menuIco_FileUpload" runat="server" Display="Dynamic"
                                        ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
                                        ControlToValidate="FileUpload_Menuico" ValidationGroup="InsertSave" SetFocusOnError="True" ForeColor="Red"></asp:RegularExpressionValidator>
                                    <%-- File Size Check --%>
                                    <asp:CustomValidator ID="cv_MenuIco_FileUpload" ValidationGroup="InsertSave" runat="server" ErrorMessage="File size cannot be greater than 3-KB " OnServerValidate="cv_MenuIco_FileUpload_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUpload_Menuico" ValidateRequestMode="Enabled"></asp:CustomValidator>

                                </div>

                                <asp:CheckBox ID="chkVisibility" CssClass="form-control" runat="server" Text="&nbsp;Is Active" Checked="True" />

                                <%-- Checkbox --%>
                                <div>
                                    <asp:CheckBox ID="chkimg_Show" runat="server" Text="&nbsp;Is Menu Image Show ?" Checked="True" />
                                    &nbsp;
                                   <span class="fa fa-info-circle alert alert-info">&nbsp; Must be Tile / Image Check</span>
                                </div>
                                <asp:CheckBox ID="chkShow_Title" runat="server" Text="&nbsp;Is Menu Title Show ?" Checked="True" />
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>

                    <div class="panel-footer text-right">
                        <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Save" ValidationGroup="InsertSave" OnClick="btnSave_Click" />
                    </div>
                </div>

            </div>
            <!--//Left sIDE------------------>


            <!--Right sIDE------------------->
            <div class="col-md-5">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center"><span class="fa fa-info">&nbsp;Details</span></div>

                    <div class="panel-body">

                        <asp:UpdatePanel ID="UpdatePanel_nav" runat="server">
                            <ContentTemplate>

                                <%-- Save Messege --%>
                                <asp:Literal ID="ltrGriedMessage" runat="server"></asp:Literal>

                                <div class="table-responsive">
                                    <%--Gried View--%>

                                    <asp:GridView ID="gvDetails" runat="server" CssClass="w3-table-all" AutoGenerateColumns="False"
                                        DataKeyNames="Module_Id" EmptyDataText="There are no data records to display." OnRowCommand="gvDetails_RowCommand">

                                        <%-- Columns --%>
                                        <Columns>

                                            <%-- Column  Id --%>
                                            <asp:TemplateField HeaderText="">

                                                <%-- Simple Template --%>
                                                <ItemTemplate>
                                                    <input type="hidden" name="MenuMasterId" id="MenuMasterId" value='<%# Eval("Module_Id") %>' />
                                                </ItemTemplate>

                                                <%-- Edit Template --%>
                                                <EditItemTemplate>
                                                </EditItemTemplate>

                                                <%-- Footer --%>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <%-- Column  Slno --%>
                                            <asp:TemplateField HeaderText="Sl. No">

                                                <%-- Simple Template --%>
                                                <ItemTemplate>
                                                    <%-- SlNo --%>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>

                                                <%-- Edit Template --%>
                                                <EditItemTemplate>
                                                </EditItemTemplate>

                                                <%-- Footer --%>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <%-- Column  Page Name --%>
                                            <asp:TemplateField HeaderText="Page Name">

                                                <%-- Simple Template --%>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTitle" Text='<%#Convert.ToString(Eval("Title")) %>' runat="server"></asp:Label>
                                                </ItemTemplate>

                                                <%-- Edit Template --%>
                                                <EditItemTemplate>
                                                </EditItemTemplate>


                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <%-- Column Is Dynamic --%>
                                            <asp:TemplateField HeaderText="Dynamic Page">

                                                <%-- Simple Template --%>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblIsDynamicPage" Text='<%#Convert.ToString(Eval("IsDynamicPage")) %>' runat="server"></asp:Label>
                                                </ItemTemplate>

                                                <%-- Edit Template --%>
                                                <EditItemTemplate>
                                                </EditItemTemplate>


                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <%-- Column Edit and Delete --%>
                                            <asp:TemplateField HeaderText="">

                                                <%-- Simple Template --%>
                                                <ItemTemplate>
                                                    <asp:ImageButton ImageUrl="~/Admin/images/Alter.png" ID="imgAlter" CommandName="EditRow" ToolTip="Edit" CommandArgument='<%# Eval("Module_Id") %>' runat="server"></asp:ImageButton>
                                                </ItemTemplate>

                                                <%-- Edit Template --%>
                                                <EditItemTemplate>
                                                </EditItemTemplate>

                                                <%-- Footer --%>
                                                <FooterTemplate>
                                                </FooterTemplate>

                                            </asp:TemplateField>
                                        </Columns>

                                    </asp:GridView>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <div class="panel-footer text-right">
                        <asp:Button Text="Update SlNo" class="btn btn-primary" ID="btnUpdateMenuslNo" runat="server" OnClick="btnUpdateMenuslNo_Click" />
                    </div>

                </div>
            </div>
            <!--//Right sIDE------------------>

        </div>
    </asp:Panel>


    
    <script type="text/javascript">
        //Table Data slno Update time use------->
        $(function () {
            $("[id*=gvDetails]").sortable({
                items: 'tr',
                cursor: 'pointer',
                axis: 'y',
                dropOnEmpty: false,
                start: function (e, ui) {
                    ui.item.addClass("selected");
                },
                stop: function (e, ui) {
                    ui.item.removeClass("selected");
                },
                receive: function (e, ui) {
                    $(this).find("tbody").append(ui.item);
                }
            });
        });

        //Image Source Image Field--->
        function FileOnchangeforSiteMeta(input) {
            var imgv = document.getElementById("<%=ImageOfMenuIcon.ClientID %>");
             if (input.files && input.files[0]) {
                 var reader = new FileReader();

                 reader.onload = function (e) {
                     $(imgv)
                         .attr('src', e.target.result)
                         .width(80)
                         .height(80);
                 };

                 reader.readAsDataURL(input.files[0]);
             }
         }
    </script>

    
</asp:Content>

