﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.DataAccesTools;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin
{
    public partial class AddPageSub : System.Web.UI.Page
    {
        #region <-------------Var------------->
        string _SaveImagePath = DefultPaths.DynamicPages_ImageFile_Path;
        string _SavePdfPath = DefultPaths.DynamicPages_PdfFile_Path;
        //
        double _lMaxFileSize = 2097152;
        static bool mUpdateTime = false;
        private static long _gUpdateId = 0;
        #endregion

        #region Prevent Page Refresh in C#

        //For refresh
        private bool _refreshState;
        private bool _isRefresh;
        protected override void LoadViewState(object savedState)
        {
            try
            {
                object[] AllStates = (object[])savedState;
                base.LoadViewState(AllStates[0]);
                _refreshState = bool.Parse(AllStates[1].ToString());
                _isRefresh = _refreshState == bool.Parse(Session["__ISREFRESH"].ToString());
            }
            catch (Exception)
            {
            }
        }

        protected override object SaveViewState()
        {
            Session["__ISREFRESH"] = _refreshState;
            object[] AllStates = new object[2];
            AllStates[0] = base.SaveViewState();
            AllStates[1] = !(_refreshState);
            return AllStates;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Defaultset();

                //Design_Sub------------
                txtSubDateFrom.SetCurrentDateInTextBox();
                txtSubDateTo.SetCurrentDateInTextBox();
                txtSubDate.SetCurrentDateInTextBox();

                //add
                ddlPagesName.cmbDynamicPages();
                ddlPagesName.Items.Insert(0, "--Select--");
                //find
                ddlPageForFind.cmbDynamicPages();
                ddlPageForFind.Items.Insert(0, "--Select--");

            }
            ltSaveMessege.Text = "";
        }

        #region Post Request===============>
        private void InsertData()
        {
            if (Page.IsValid && !ddlPagesName.SelectedValue.ISNullOrWhiteSpace())
            {
                #region <--------Var------->
                DynamicPages_Sub dynamicPages_Sub = new DynamicPages_Sub();
                dynamicPages_Sub.DynamicPageId = ddlPagesName.SelectedValue.ConvertToGuid();

                int Slno = (NoticeUtils.GetMaxNoticeSlno() + 1);

                //File
                //File(If you check then Update Path othrtwise Avoid path update)(if you checked when the fileUpload empty then Update Empty)
                if (chkFileUploadSubImage.Checked)
                {
                    string _Path_Image = FileUploadSubImage.GenerateFileUploadPath(_SaveImagePath);
                    dynamicPages_Sub.ImageFileUrl = _Path_Image.ISNullOrWhiteSpace() ? string.Empty : _Path_Image;
                }
                if (chkFileUploadSubPdf.Checked)
                {
                    string _Path_Pdf = FileUploadSubPdf.GenerateFileUploadPath(_SavePdfPath);
                    dynamicPages_Sub.PdfFileUrl = _Path_Pdf.ISNullOrWhiteSpace() ? string.Empty : _Path_Pdf;
                }

                //Text
                dynamicPages_Sub.Publisher = txtSubPublisherName.Text;
                dynamicPages_Sub.PublishDate = txtSubDate.Text.ConvertObjectDateTimeToDateTime();
                dynamicPages_Sub.DateFrom = txtSubDateFrom.Text.ConvertObjectDateTimeToDateTime();
                dynamicPages_Sub.DateTo = txtSubDateTo.Text.ConvertObjectDateTimeToDateTime();
                dynamicPages_Sub.Subjects = txtSubSubjects.Text;
                dynamicPages_Sub.Contents = txtSubContents.Text;
                dynamicPages_Sub.Footer = txtSubFooter.Text;
                dynamicPages_Sub.Activity = true;
                dynamicPages_Sub.Slno = (DynamicPageSubUtils.GetMaxSlno(dynamicPages_Sub.DynamicPageId.ConvertToGuid()) + 1);


                #endregion

                if (DynamicPageSubUtils.InsertData(dynamicPages_Sub))
                {
                    //File upload
                    FileUploadImage(dynamicPages_Sub.ImageFileUrl);
                    FileUploadPdf(dynamicPages_Sub.PdfFileUrl);

                    Defaultset();

                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    //Java script
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }
        private void FileUploadImage(string path)
        {
            if (!path.ISNullOrWhiteSpace())
            {
                // Notify the user that the file name was changed.
                FileUploadSubImage.SaveAs(Server.MapPath(path));
                //Image Show
            }
        }

        private void FileUploadPdf(string path)
        {
            if (!path.ISNullOrWhiteSpace())
            {
                // Notify the user that the file name was changed.
                FileUploadSubPdf.SaveAs(Server.MapPath(path));
                //Image Show
            }
        }
        private void UpdateData()
        {
            if (Page.IsValid)
            {
                //Prev Data
                DynamicPages_Sub dbdata = DynamicPageSubUtils.GetData(_gUpdateId);
                //New Data instent
                DynamicPages_Sub dynamicPages_Sub = new DynamicPages_Sub();

                #region <----------Var---------->

                //File(If you check then Update Path othrtwise Avoid path update)(if you checked when the fileUpload empty then Update Empty)
                if (chkFileUploadSubImage.Checked)
                {
                    string _Path_Image = FileUploadSubImage.GenerateFileUploadPath(_SaveImagePath);
                    dynamicPages_Sub.ImageFileUrl = _Path_Image.ISNullOrWhiteSpace() ? string.Empty : _Path_Image;
                }
                if (chkFileUploadSubPdf.Checked)
                {
                    string _Path_Pdf = FileUploadSubPdf.GenerateFileUploadPath(_SavePdfPath);
                    dynamicPages_Sub.PdfFileUrl = _Path_Pdf.ISNullOrWhiteSpace() ? string.Empty : _Path_Pdf;
                }


                //Text
                dynamicPages_Sub.Id = _gUpdateId;
                dynamicPages_Sub.Publisher = txtSubPublisherName.Text;
                dynamicPages_Sub.PublishDate = txtSubDate.Text.ConvertObjectDateTimeToDateTime();
                dynamicPages_Sub.DateFrom = txtSubDateFrom.Text.ConvertObjectDateTimeToDateTime();
                dynamicPages_Sub.DateTo = txtSubDateTo.Text.ConvertObjectDateTimeToDateTime();
                dynamicPages_Sub.Subjects = txtSubSubjects.Text;
                dynamicPages_Sub.Contents = txtSubContents.Text;
                dynamicPages_Sub.Footer = txtSubFooter.Text;
                dynamicPages_Sub.Activity = true;

                dynamicPages_Sub.DynamicPageId = ddlPagesName.SelectedValue.ConvertObjectToGuid();
                #endregion

                var issucc = DynamicPageSubUtils.UpdateData(dynamicPages_Sub, chkFileUploadSubImage.Checked, chkFileUploadSubPdf.Checked);
                if (issucc)
                {
                    #region --Delete File old----
                    if (chkFileUploadSubImage.Checked)
                    {
                        try
                        {
                            //Delete old File
                            if (!dbdata.ImageFileUrl.ISNullOrWhiteSpace())
                            {
                                System.IO.File.Delete(Server.MapPath(dbdata.ImageFileUrl));
                            }
                        }
                        catch (Exception)
                        {
                        }

                        //File upload
                        FileUploadImage(dynamicPages_Sub.ImageFileUrl);

                    }
                    if (chkFileUploadSubPdf.Checked)
                    {
                        try
                        {
                            //Delete old File
                            if (!dbdata.PdfFileUrl.ISNullOrWhiteSpace())
                            {
                                System.IO.File.Delete(Server.MapPath(dbdata.PdfFileUrl));

                                //File upload
                                FileUploadPdf(dynamicPages_Sub.PdfFileUrl);
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    #endregion

                    //  Default Set
                    Defaultset();

                    //Messege
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Navfn0006", "$('.nav-pills a[href=\"#SubDynamicPageEntry\"]').tab('show')", true);
                    Show("Data Update Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    Response.Write("<script>alert('Data Update SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not Update", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not Update because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }

        private bool DeleteData(long id)
        {
            var data = DynamicPageSubUtils.GetData(id);
            if (data.ISValidObject())
            {
                if (DynamicPageSubUtils.DeleteData(id))
                {
                    //pdf delete
                    if (!data.PdfFileUrl.ISNullOrWhiteSpace())
                    {
                        System.IO.File.Delete(Server.MapPath(data.PdfFileUrl));
                    }

                    //image delete
                    if (!data.ImageFileUrl.ISNullOrWhiteSpace())
                    {
                        System.IO.File.Delete(Server.MapPath(data.ImageFileUrl));
                    }

                    Defaultset();
                    Show("Delete Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    return true;
                }

            }
            Show("Some errors occurred while processing the requested tasks", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            return false;
        }
        #endregion

        #region Get Request===========>
        private void FillGried()
        {
            Guid dynamicpageId = default(Guid);
            if (!ddlPageForFind.SelectedValue.ISNullOrWhiteSpace())
            {
                dynamicpageId = ddlPageForFind.SelectedValue.ConvertToGuid();
            }
            var dt = DynamicPageSubUtils.GetAllData(dynamicpageId).OrderByDescending(s => s.Slno).ToList();
            gvView.DataSource = dt;
            gvView.DataBind();
            if (dt.IsValidList())
            {
                gvView.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void GetDetails(long id)
        {
            var dt = DynamicPageSubUtils.GetData(id);
            if (dt.ISValidObject())
            {
                //uncheck Default Save
                chkFileUploadSubImage.Checked = false;
                chkFileUploadSubPdf.Checked = false;
                try
                {
                    _gUpdateId = id;
                    txtSubSubjects.Text = dt.Subjects;
                    txtSubContents.Text = dt.Contents;
                    txtSubFooter.Text = dt.Footer;
                    txtSubPublisherName.Text = dt.Publisher;

                    string _dynamicPageId = dt.DynamicPageId.ConvertObjectToString();
                    ddlPagesName.ClearSelection(); //making sure the previous selection has been cleared
                    ddlPagesName.SelectedIndex = ddlPagesName.Items.IndexOf(ddlPagesName.Items.FindByValue(_dynamicPageId));


                    DateTime dttime = DateTime.Now.Date;

                    //Date Time must be set formate of yyyy-MM-dd
                    //Publisher date
                    dttime = dt.PublishDate.ConvertObjectDateTimeToDateTime();
                    txtSubDate.Text = dttime.Date.ToString("yyyy-MM-dd");

                    //Date from
                    dttime = dt.DateFrom.ConvertObjectDateTimeToDateTime();
                    txtSubDateFrom.Text = dttime.Date.ToString("yyyy-MM-dd");

                    //Date To
                    dttime = dt.DateTo.ConvertObjectDateTimeToDateTime();
                    txtSubDateTo.Text = dttime.Date.ToString("yyyy-MM-dd");


                }
                catch (Exception)
                {

                }
            }
        }

        private void Clear()
        {
            //DynamicDesign_Sub-----------------------
            txtSubSubjects.Text = string.Empty;
            txtSubPublisherName.Text = string.Empty;
            txtSubContents.Text = string.Empty;
            txtSubFooter.Text = string.Empty;
            ddlPagesName.ClearSelection();

            //Update
            mUpdateTime = false;
            _gUpdateId = 0;
        }

        private void Defaultset()
        {
            Clear();
            //Refresh
            FillGried();
        }
        #endregion

        #region Event================>


        protected void cvSubImageSizeCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileUploadSubImage.FileContent.Length;
            if (filesize > _lMaxFileSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void cvSubPdfSizeCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileUploadSubPdf.FileContent.Length;
            if (filesize > _lMaxFileSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void btnAddMode_ServerClick(object sender, EventArgs e)
        {
            Defaultset();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Navfn055", "$('.nav-pills a[href=\"#SubDynamicPageEntry\"]').tab('show')", true);
        }

        protected void gvView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "DeleteRow")
            {
                var vlu = e.CommandArgument.ConvertObjectToLong();
                DeleteData(vlu);
            }

            else if (e.CommandName == "EditRow")
            {
                var vlu = e.CommandArgument.ConvertObjectToLong();
                if (vlu.ISValidObject())
                {
                    GetDetails(vlu);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Navkey008", "$('.nav-pills a[href=\"#SubDynamicPageEntry\"]').tab('show')", true);
                    //set
                    mUpdateTime = true;
                }
                else
                {
                    Show("Select Valid Row", "Warning!", _messegeType.warning, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
        }


        protected void ddlPageForFind_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillGried();
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!_isRefresh) // check that page is not refreshed by browser.
            {
                if (!mUpdateTime)
                {
                    InsertData();
                }
                else
                {
                    UpdateData();
                }
            }
        }
        #endregion
    }
}