<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="AddPages.aspx.cs" Inherits="SchoolWebApplication.Admin.AddPages"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <style>
        [id*=gvDetails] tr:hover, tr.selected {
            background-color: #FFCF8B
        }

    </style>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="pnlAboutEntry" runat="server" class="container-fluid">
        <!-- Save Messege -->
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>

        <div class="row">
            <div class="panel panel-primary">
                <div class="panel-heading text-center"><span class="fa fa-info">&nbsp;Details</span></div>

                <div class="panel-body">

                    <%-- Save Messege --%>
                    <asp:Literal ID="ltrGriedMessage" runat="server"></asp:Literal>

                    <div class="table-responsive">
                        <%--Gried View--%>

                        <asp:GridView ID="gvDetails" runat="server" CssClass="w3-table-all" AutoGenerateColumns="False"
                            DataKeyNames="Id" EmptyDataText="There are no data records to display." OnRowCommand="gvDetails_RowCommand">

                            <%-- Columns --%>
                            <Columns>

                                <%-- Column  Slno --%>
                                <asp:TemplateField HeaderText="Sl. No">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <%-- SlNo --%>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                    </EditItemTemplate>

                                    <%-- Footer --%>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateField>


                                <%-- Column  Page Name --%>
                                <asp:TemplateField HeaderText="Page Name">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTitle" Text='<%#Convert.ToString(Eval("PageName")) %>' runat="server"></asp:Label>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                    </EditItemTemplate>


                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateField>


                                <%-- Column Edit  --%>
                                <asp:TemplateField HeaderText="">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgAlter" CommandName="EditRow" ImageUrl="~/Admin/images/Alter.png" ToolTip="Update Page" CommandArgument='<%# Eval("Id") %>' runat="server"></asp:ImageButton>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                    </EditItemTemplate>

                                    <%-- Footer --%>
                                    <FooterTemplate>
                                    </FooterTemplate>

                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>

                    </div>
                </div>

            </div>
        </div>


        <!--Right sIDE------------------->
        <div class="w3-modal" id="modelAddPage">
            <div class="w3-modal-content w3-card-4 w3-animate-zoom panel panel-primary" style="max-width: 800px">
                <div class="panel-heading text-center">
                    <span class="fa fa-pencil">&nbsp;Entry </span>
                    <%-- close Admin Topnavber --%>
                    &nbsp;&nbsp;<span onclick="document.getElementById('nvTopNavBer').style.display='none'" class="btn btn-warning" title="Hide Top NavBer">&times;&nbsp;Hide Top Menu</span>
                    &nbsp;&nbsp;<span onclick="document.getElementById('nvTopNavBer').style.display='block'" class="btn btn-info" title="Show Top NavBer">&times;&nbsp;Show Top Menu</span>
                    
                    <%-- Close model --%>
                    <span onclick="document.getElementById('modelAddPage').style.display='none'" class="w3-button w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
                    </div>

                <div class="panel-body">
                    <%-- Notification!! --%>
                     <span class="fa fa-info-circle alert alert-info">&nbsp;Support : Bootstrap v3.3.7| W3.css V4 !!</span>
                               
                    <asp:UpdatePanel runat="server" ID="updtEntryform">
                        <ContentTemplate>
                            <%--Title--%>
                            <div class="form-group">
                                <label class="label label-info" for="txtTitle">Page Title : *</label>
                                <asp:TextBox ID="txtTitle" class="form-control" PlaceHolder="Enter Your Page Title (limit of 100 characters)..." runat="server" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="InsertSave" runat="server" ControlToValidate="txtTitle" Display="Dynamic" ErrorMessage="Page Name is Require" ForeColor="Red" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="InsertSave" ControlToValidate="txtTitle" ErrorMessage="Enter only (a-z A-Z 0-9 - _)" ValidationExpression="^[a-zA-Z0-9-_ ]*$" ForeColor="Red" SetFocusOnError="true" Display="Dynamic"></asp:RegularExpressionValidator>
                            </div>
                            
                            <%--Content--%>
                            <div class="form-group" >
                                <label class="label label-info" for="txtContent">Content :</label>
                                <asp:TextBox ID="txtContent"  class="tinymce form-control" runat="server" placeholder="Enter Content (limit of 4000 characters)" Rows="12" aria-valuemax="4000" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                            </div>

                            <%--css--%>
                            <div class="form-group">
                                <label class="label label-info" for="txtTitle">Css : </label>
                                <asp:TextBox ID="txtCss" class="form-control" PlaceHolder="Enter Your StyleSheet." runat="server" TextMode="MultiLine" Height="100px"></asp:TextBox>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>

                <div class="panel-footer text-right">
                    <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Submit" ValidationGroup="InsertSave" OnClick="btnSave_Click" />
                </div>
            </div>

        </div>
        <!--//Right sIDE------------------>


    </asp:Panel>

          <asp:PlaceHolder runat="server">
         <%: Scripts.Render("~/bundles/init-tinymce") %>
    </asp:PlaceHolder>
   
</asp:Content>

