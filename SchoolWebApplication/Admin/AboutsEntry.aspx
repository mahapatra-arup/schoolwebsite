﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="AboutsEntry.aspx.cs" Inherits="SchoolWebApplication.AboutsEntry"
    EnableEventValidation="False" ValidateRequest="false" %>

<%@ Import Namespace="SchoolWebApplication" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ValidateRequestMode="Inherit">
    <asp:Panel ID="pnlAboutEntry" runat="server" class="container-fluid">

        <%-- Save Messege --%>
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
        <%-- Save Messege --%>

        <div class="row">

            <!--Left Side-->
            <div class="col-sm-7">
                <!--insert Mode======================== -->
                <button class="w3-button w3-xlarge w3-circle w3-red w3-card pull-right" runat="server" id="btnAddMode" onserverclick="btnAddMode_ServerClick" title="Active Insert Mode">+</button>


                <div class="panel panel-primary">
                    <div class="panel-heading text-center"><span class="fa fa-pencil-square-o">&nbsp;Entry</span></div>
                    <div class="panel-body">

                       <%-- <asp:UpdatePanel runat="server" ID="updtEntryform">
                            <ContentTemplate>--%>

                                <%--Title--%>
                                <div class="form-group">
                                    <label class="label label-info" for="txtTitle">Title : </label>
                                    <asp:TextBox ID="txtTitle" class="form-control" PlaceHolder="Enter Your Subjects (limit of 100 characters)..." runat="server" MaxLength="100"></asp:TextBox>
                                </div>

                                <%--Subjects--%>
                                <div class="form-group">
                                    <label class="label label-info" for="txtSubjects"><span>*</span>Subjects : </label>
                                    <asp:TextBox ID="txtSubjects" class="form-control" runat="server" placeholder="**Enter Your Subjects (limit of 100 characters)..." MaxLength="100" Text="#1" ></asp:TextBox>

                                    <%-- Required Field --%>
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfvSubjects" ValidationGroup="InsertSave" runat="server" ControlToValidate="txtSubjects" Display="Dynamic" ErrorMessage="Subject is Require" ForeColor="Red" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </div>


                                <%--Content--%>
                                <div class="form-group">
                                    <label class="label label-info" for="txtEditor">Content:</label>
                                    <asp:TextBox ID="txtEditor" class="tinymce form-control" runat="server" placeholder="Enter Content (limit of 4000 characters)" Rows="6" aria-valuemax="4000" TextMode="MultiLine"></asp:TextBox>


                                </div>
                                <%--Footer--%>
                                <div class="form-group">
                                    <label class="label label-info" for="txtFooter">Footer:</label>
                                    <asp:TextBox ID="txtFooter" class="form-control" runat="server" Placeholder="Enter Content (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                </div>

                                <%--Image--%>
                                <div class="form-group">
                                    <label class="label label-warning" for="FileUploadImage">Image:</label>
                                    <asp:FileUpload ID="FileUploadImage" class="form-control1" runat="server" />

                                    <%-- Regular expreession --%>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorFileUpload" runat="server" Display="Dynamic"
                                        ErrorMessage="&quot;Only .png and jpg and jpeg Files are allowed&quot;" ControlToValidate="FileUploadImage" SetFocusOnError="True" ForeColor="Red"
                                        ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                                    <%-- File Size Check --%>
                                    <asp:CustomValidator ID="validatorCheckFileSize" ValidationGroup="InsertSave" runat="server" ErrorMessage="File size cannot be greater than 500 KB " OnServerValidate="validatorCheckFileSize_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUploadImage"></asp:CustomValidator>
                                </div>
                          <%--  </ContentTemplate>

                        </asp:UpdatePanel>--%>

                    </div>

                </div>
                <div class="text-right col-sm-12">
                    <asp:Button ID="btnSave" class="w3-button w3-white w3-border w3-border-red w3-round-large" runat="server" Text="Save" ValidationGroup="InsertSave" OnClick="btnSave_Click" />
                </div>
            </div>

            <!--//Left sIDE============-->


            <!--Right sIDE==============-->
            <div class="col-sm-5">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center"><span class="fa fa-info-circle">&nbsp;Details</span></div>
                    <div class="panel-body">

                      <%--  <asp:UpdatePanel ID="UpdatePanelAbouts" runat="server">
                            <ContentTemplate>--%>

                                <%-- Save Messege --%>
                                <asp:Literal ID="ltrGriedMessage" runat="server"></asp:Literal>

                                <div class="table-responsive">
                                    <%--Gried View--%>

                                    <asp:GridView ID="gvDetails" runat="server" BackColor="White"
                                        BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                                        CellPadding="3" ForeColor="Black" GridLines="Vertical"
                                        CssClass="w3-table-all"
                                        AutoGenerateColumns="False" DataKeyNames="Id"
                                        EmptyDataText="There are no data records to display." OnPageIndexChanging="gvDetails_PageIndexChanging" AllowPaging="True" AllowSorting="True" OnRowCommand="gvDetails_RowCommand">


                                        <%-- Columns --%>
                                        <Columns>

                                            <%-- Column  Id --%>
                                            <asp:TemplateField HeaderText="Id" Visible="False">

                                                <%-- Simple Template --%>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" Text='<%#Eval("Id") %>' runat="server"></asp:Label>
                                                </ItemTemplate>

                                                <%-- Edit Template --%>
                                                <EditItemTemplate>
                                                </EditItemTemplate>

                                                <%-- Footer --%>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <%-- Column  Subjects --%>
                                            <asp:TemplateField HeaderText="Subjects">

                                                <%-- Simple Template --%>
                                                <ItemTemplate>
                                                  
                                                    <asp:Label ID="lblSubjects" Text='<%#Convert.ToString(Eval("Subjects")) %>' runat="server"></asp:Label>
                                                </ItemTemplate>

                                                <%-- Edit Template --%>
                                                <EditItemTemplate>
                                                </EditItemTemplate>

                                                <%-- Footer --%>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:TemplateField>

                                            <%-- Column Edit and Delete --%>
                                            <asp:TemplateField HeaderText="">

                                                <%-- Simple Template --%>
                                                <ItemTemplate>
                                                    <asp:ImageButton ImageUrl="~/Admin/images/Alter.png" ID="imgAlter" CommandName="EditRow" ToolTip="Edit" CommandArgument='<%# Eval("Id") %>' runat="server"></asp:ImageButton>
                                                    <asp:ImageButton ImageUrl="~/Admin/images/Delete.png" ID="imgDelete" CommandName="DeleteRow" ToolTip="Delete" CommandArgument='<%# Eval("Id") %>' runat="server"
                                                        OnClientClick="return confirm('Are you sure you want to delete this event?');"></asp:ImageButton>
                                                </ItemTemplate>

                                                <%-- Edit Template --%>
                                                <EditItemTemplate>
                                                </EditItemTemplate>

                                                <%-- Footer --%>
                                                <FooterTemplate>
                                                </FooterTemplate>

                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>

                           <%-- </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                </div>
            </div>
            <!--//Right sIDE============-->


        </div>
    </asp:Panel>

    <%--tinymce Initial  --%>
     <asp:PlaceHolder runat="server">
         <%: Scripts.Render("~/bundles/init-tinymce") %>
    </asp:PlaceHolder>
</asp:Content>
