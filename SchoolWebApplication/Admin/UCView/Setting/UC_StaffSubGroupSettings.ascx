﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_StaffSubGroupSettings.ascx.cs" Inherits="SchoolWebApplication.Admin.UCView.Setting.UC_StaffSubGroupSettings" %>

<style>
    [id*=gvSubGroupView] tr:hover, tr.selected {
        background-color: #FFCF8B
    }
</style>

<script type="text/javascript">
    $(function () {
        $("[id*=gvSubGroupView]").sortable({
            items: 'tr',
            cursor: 'pointer',
            axis: 'y',
            dropOnEmpty: false,
            start: function (e, ui) {
                ui.item.addClass("selected");
            },
            stop: function (e, ui) {
                ui.item.removeClass("selected");
            },
            receive: function (e, ui) {
                $(this).find("tbody").append(ui.item);
            }
        });
    });


</script>


<%--==== Save Messege==== --%>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
    </ContentTemplate>
</asp:UpdatePanel>
<%--==== //Save Messege==== --%>


<div class="panel panel-primary">
    <%-- Header --%>
    <div class="panel-heading">
        <span style="color: white">Staff Sub-Group</span>
    </div>


    <%-- Body --%>
    <div class="panel-body w3-margin">

        <div class="col-md-12 text-left   w3-border w3-border-orange">

            <label class="label label-success" for="ddlGroups">Group : </label>
            <asp:DropDownList ID="ddlGroups" runat="server" class="form-control" ></asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvPages" Display="Dynamic" runat="server" ErrorMessage="GroupName is Require" ForeColor="Red" ControlToValidate="ddlGroups" InitialValue="--Select--" SetFocusOnError="True" ValidationGroup="InsertSubGroupValidate"></asp:RequiredFieldValidator>
            <br />

            <label class="label label-success" for="txtName">Sub Group : </label>
            <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorGroup" runat="server" ControlToValidate="txtName" ErrorMessage="group name is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertSubGroupValidate"></asp:RequiredFieldValidator>
            <asp:Label ID="lblAlreadyExist" runat="server" ForeColor="Red" Text="Already Exist" Visible="False"></asp:Label>
        

              <%-- Save Button --%>
           
            <asp:Button ID="btnGroupSubmit" runat="server" Text="ADD" OnClick="btnGroupSubmit_Click"
                ValidationGroup="InsertSubGroupValidate" class="btn btn-primary w3-margin pull-right" />
            
        </div>

       


        <div class="col-md-12">
             <br />
            <span class="text-primary">>> If Want to Rearrange ordering then Drag and Drop The row</span>
            <br />
            <label class="label label-warning" for="ddlPages">Groups Name : </label>
            <asp:DropDownList ID="ddlFindGroups" runat="server" class="form-control"  AutoPostBack="True" OnSelectedIndexChanged="ddlFindGroups_SelectedIndexChanged"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ForeColor="Red" ErrorMessage="Group Name is Require" InitialValue="--Select--" ControlToValidate="ddlFindGroups" SetFocusOnError="True" ValidationGroup="UpdateSlNoValidation"></asp:RequiredFieldValidator>

            <%-- =-===View==== --%>
            <asp:GridView ID="gvSubGroupView" runat="server" AutoGenerateColumns="false" OnRowUpdating="gvGroupView_RowUpdating" OnRowEditing="gvGroupView_RowEditing" OnRowCancelingEdit="gvGroupView_RowCancelingEdit" CssClass="w3-table-all">
                <Columns>

                    <%-- Id --%>
                    <asp:TemplateField HeaderText="Id">
                        <ItemTemplate>
                            <input type="hidden" name="SubGroupId" id="SubGroupId" value='<%# Eval("Id") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- SL No --%>
                    <asp:BoundField DataField="slNo" HeaderText="SL.No." />

                    <%-- Group Name --%>
                    <asp:TemplateField HeaderText="Group Name">
                        <ItemTemplate>
                            <asp:Label ID="lbl_GroupName" runat="server" Text=' <%# Eval("SubGroupName") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtgrup_Name" runat="server" class="form-control" Text='<%#Eval("SubGroupName") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>



                    <%-- ACtivity --%>
                    <asp:TemplateField HeaderText="Activity">
                        <ItemTemplate>
                            <asp:Label ID="hfGroupActivity" runat="server" Text='<%# Eval("Activity") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlActivity" runat="server" CssClass="form-control">
                                <asp:ListItem Text="True"></asp:ListItem>
                                <asp:ListItem Text="False"></asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <%-- Button --%>
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:Button ID="btn_Edit" runat="server" CssClass="btn btn-primary" Text="Edit" CommandName="Edit" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btn_Update" runat="server" CssClass="btn btn-primary" Text="Update" CommandName="Update" />
                            <asp:Button ID="btn_Cancel" runat="server" CssClass="btn btn-primary" Text="Cancel" CommandName="Cancel" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </div>
    </div>
    <div class="panel-footer text-right">
        <asp:Button Text="Update SlNo" class="btn btn-primary" ValidationGroup="UpdateSlNoValidation" ID="btnUpdateRecord" runat="server" OnClick="btnUpdateRecord_Click" />
    </div>

</div>
