﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin.UCView.Setting
{
    public partial class UC_SiteMetaSettings : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Get pages title
                ddlPages.AddPagesTitle();
            }
        }
        private void UpdateSiteMeta()
        {
            if (Page.IsValid)
            {
                string title = txtSiteTitle.Text;
                string KeyWords = txtSiteKeyword.Text;
                string Page = ddlPages.SelectedValue;
                string description = txtDescription.Text;

                byte[] img = null;


                //read from File upload control
                if (SiteIco_FileUpload.HasFile)
                {
                    using (Stream fs = SiteIco_FileUpload.PostedFile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            img = br.ReadBytes((Int32)fs.Length);
                        }
                    }
                }
                else
                {
                    //read from image control
                    if (ImageOfSitemeta.ImageUrl != null)
                    {
                        img = System_Web_UI_WebControls_Imager.ImageControlToByte(ref ImageOfSitemeta);
                    }
                }

                List<SqlParameter> lstParem = new List<SqlParameter>();

                //Query
                string qry = SiteMeta.UpdateQrySitemeta(title, img, KeyWords, Page, description, out lstParem);
                SqlHelper.MParameterList = lstParem;

                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    Show("Data Update Successfull", " Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    //Java script
                    Response.Write("<script>alert('Data Update SuccessFull');</script>");
                    SiteIco_FileUpload_clear();
                    //Fill Box After Update or insert
                    GetSiteMetaDetails(ddlPages.SelectedValue);
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }
        private void InsertSiteMeta()
        {
            if (Page.IsValid)
            {
                string title = txtSiteTitle.Text;
                string KeyWords = txtSiteKeyword.Text;
                string Page = ddlPages.SelectedValue;
                string description = txtDescription.Text;

                byte[] img = null;


                //read from File upload control
                if (SiteIco_FileUpload.HasFile)
                {
                    using (Stream fs = SiteIco_FileUpload.PostedFile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            img = br.ReadBytes((Int32)fs.Length);
                        }
                    }
                }
                else
                {
                    //read from image control
                    if (ImageOfSitemeta.ImageUrl != null)
                    {
                        img = System_Web_UI_WebControls_Imager.ImageControlToByte(ref ImageOfSitemeta);
                    }
                }

                List<SqlParameter> lstParem = new List<SqlParameter>();

                //Query
                string qry = SiteMeta.InsertSitemeta(title, img, KeyWords, Page, description, out lstParem);
                SqlHelper.MParameterList = lstParem;

                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    //Java script
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");
                    SiteIco_FileUpload_clear();
                    //Fill Box After Update or insert
                    GetSiteMetaDetails(ddlPages.SelectedValue);
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }
        private void SiteIco_FileUpload_clear()
        {
            txtSiteTitle.Text = string.Empty;
            txtSiteKeyword.Text = string.Empty;
            txtDescription.Text = string.Empty;
            ImageOfSitemeta.ImageUrl = "";
        }
        private void GetSiteMetaDetails(string pagename)
        {
            // Clear details Details
            txtSiteTitle.Text = string.Empty;
            txtSiteKeyword.Text = string.Empty;
            txtDescription.Text = string.Empty;
            ImageOfSitemeta.ImageUrl = "";

            var dt = SiteMeta.GetSiteMetaDetails(pagename);
            if (dt.ISValidObject())
            {
                txtSiteTitle.Text = dt.Title;
                txtSiteKeyword.Text = dt.KeyWords;
                txtDescription.Text = dt.Description;
                byte[] ico_img = null;

                //===================image==========
                //Photo
                try
                {
                    if (dt.Title_Ico.ISValidObject())
                    {
                        Byte[] imageData = (byte[])dt.Title_Ico;
                        if (imageData != null)
                        {
                            ico_img = imageData;
                        }
                        if (ico_img.Length > 0 && ico_img != null)
                        {
                            Image img = System_Web_UI_WebControls_Imager.byteArrayToWebImage(ico_img);
                            ImageOfSitemeta.ImageUrl = img.ImageUrl;
                        }
                    }
                }
                catch (Exception)
                {
                    ico_img = null;
                }
            }

            UpdatePanel_SiteMeta.Update();//for condition
        }

        #region -----------------Event------------------
        protected void cv_SiteIco_FileUpload_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;

            double filesize = SiteIco_FileUpload.PostedFile.ContentLength;
            if (filesize > 204800)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void btnSubmitSiteMeta_Click(object sender, EventArgs e)
        {
            string value = ddlPages.SelectedValue;

            if (!SiteMeta.IsExist(value))
            {
                InsertSiteMeta();
            }
            else
            {
                UpdateSiteMeta();
            }

        }

        protected void ddlPages_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (!ddlPages.SelectedValue.ISNullOrWhiteSpace())
            {
                var value = ddlPages.SelectedValue;

                GetSiteMetaDetails(value);
            }
        }
        #endregion

    }
}