﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_ThumbnailSettings.ascx.cs" Inherits="SchoolWebApplication.Admin.UCView.Setting.UC_ThumbnailSettings" %>
<%--==== Save Messege==== --%>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
    </ContentTemplate>
</asp:UpdatePanel>
<%--==== //Save Messege==== --%>

<div class="panel panel-danger">

    <%-- Header --%>
    <div class="panel-heading">
        <span style="color: white">Thumbnail</span>
    </div>


    <%-- Body --%>
    <div class="panel-body w3-margin">
        <%-- group name --%>
        <label class="label label-success" for="txtGroupName">Group Name : </label>
        <asp:TextBox ID="txtGroupName" class="form-control" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvGroupName" ControlToValidate="txtGroupName" ValidationGroup="InsertThumNailGroup" runat="server" ErrorMessage="Group name Require" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:CustomValidator ID="cvExistCheck" OnServerValidate="cvExistCheck_ServerValidate" runat="server" ErrorMessage="Already Exist" ControlToValidate="txtGroupName" ValidationGroup="InsertThumNailGroup" ForeColor="Red" Display="Dynamic" BorderStyle="Solid"></asp:CustomValidator>
      
        <br />
        <%--  Subject--%>
        <label class="label label-success" for="txtSubject">Subject : </label>
        <asp:TextBox ID="txtSubject" class="form-control" runat="server"></asp:TextBox>
         
        <br />
        <%-- Content --%>
        <label class="label label-success" for="txtContent">Content : </label>
        <asp:TextBox ID="txtContent" class="form-control" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox>

         <br />
        <%-- --------Photo-------- --%>
        <label class="label label-success" for="ThumbNail_FileUpload">Thumbnail : </label>
        <asp:FileUpload ID="ThumbNail_FileUpload" class="btn-file" runat="server" />
        <%-- Requireed --%>
        <asp:CustomValidator ID="CustomValidatorEmptyCheck" runat="server" ErrorMessage="Image Is Require" Display="Dynamic" Font-Bold="True" SetFocusOnError="True" OnServerValidate="CustomValidatorEmptyCheck_ServerValidate" ValidationGroup="InsertThumNailGroup" ControlToValidate="ThumbNail_FileUpload" ForeColor="Red" ValidateEmptyText="True"></asp:CustomValidator>

        <%-- Reular expression --%>
        <asp:RegularExpressionValidator ID="RegularExpressionValidatorFileUpload" runat="server" Display="Dynamic"
            ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
            ControlToValidate="ThumbNail_FileUpload" ValidationGroup="InsertThumNailGroup" SetFocusOnError="True" ForeColor="Red"></asp:RegularExpressionValidator>
        <%-- File Size Check --%>
        <asp:CustomValidator ID="validatorCheckFileSize" ValidationGroup="InsertThumNailGroup" runat="server" ErrorMessage="File size cannot be greater than 200 KB " OnServerValidate="validatorCheckFileSize_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="ThumbNail_FileUpload"></asp:CustomValidator>

    </div>

    <%-- Footer --%>
    <div class="panel-footer text-right">

        <%-- Save Button --%>
        <asp:Button ID="btnThumbnailSave" ValidationGroup="InsertThumNailGroup" runat="server"
            Text="Submit" OnClick="btnThumbnailSave_Click" class="btn btn-primary" />

    </div>
</div>
