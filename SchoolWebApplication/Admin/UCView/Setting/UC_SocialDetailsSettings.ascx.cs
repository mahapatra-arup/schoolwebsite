﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin.UCView.Setting
{
    public partial class UC_SocialDetailsSettings : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Get Social Media Name
                cmbSocialMediaName.GetSocialName();
            }
        }


        private void UpdateSocialDetails()
        {
            if (Page.IsValid)
            {
                string SocialMediaName = cmbSocialMediaName.SelectedItem.Text;
                bool Activity = chkSocialACtivity.Checked;
                string IClass = txtIClass.Text;
                string AClass = txtAClass.Text;
                string Tooltip = txtSocialTooltip.Text;
                string SocialLink = txtSocialLink.Text;
                object CssStyle = DBNull.Value;

                //Query
                List<SqlParameter> ParamList = new List<SqlParameter>();
                string qry = SocialDetails.UpdateSocialDetailsQry(SocialMediaName, Activity, IClass, AClass, Tooltip, SocialLink, CssStyle, out ParamList);
                SqlHelper.MParameterList = ParamList;

                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    Show("Data Update Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    //Java script
                    Response.Write("<script>alert('Data Update SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not Update", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }

            }
        }

        private void GetSocialDetails(string name)
        {
            DataTable dt = SocialDetails.GetSocialDetails(name);
            if (dt.IsValidDataTable())
            {
                txtIClass.Text = Convert.ToString(dt.Rows[0]["IClass"]);
                txtAClass.Text = Convert.ToString(dt.Rows[0]["AClass"]);
                txtSocialTooltip.Text = Convert.ToString(dt.Rows[0]["Tooltip"]);
                txtSocialLink.Text = Convert.ToString(dt.Rows[0]["SocialLink"]);
                chkSocialACtivity.Checked = dt.Rows[0]["Activity"].ISValidObject() ? Convert.ToBoolean(dt.Rows[0]["Activity"]) : false;
            }
        }

        #region ---------------Event-------------
        protected void btnSocialDetailsSave_Click(object sender, EventArgs e)
        {
            UpdateSocialDetails();
        }

        protected void cmbSocialMediaName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSocialMediaName.SelectedIndex >= 0)
            {
                GetSocialDetails(cmbSocialMediaName.SelectedItem.Text);
            }
        }
        #endregion
    }
}