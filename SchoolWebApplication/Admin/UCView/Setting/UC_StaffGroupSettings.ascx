﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_StaffGroupSettings.ascx.cs" Inherits="SchoolWebApplication.Admin.UCView.Setting.UC_StaffGroupSettings" %>

<style>
    [id*=gvGroupView] tr:hover, tr.selected {
        background-color: #FFCF8B
    }
</style>

<script type="text/javascript">
    $(function () {
        $("[id*=gvGroupView]").sortable({
            items: 'tr',
            cursor: 'pointer',
            axis: 'y',
            dropOnEmpty: false,
            start: function (e, ui) {
                ui.item.addClass("selected");
            },
            stop: function (e, ui) {
                ui.item.removeClass("selected");
            },
            receive: function (e, ui) {
                $(this).find("tbody").append(ui.item);
            }
        });
    });


</script>


<%--==== Save Messege==== --%>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
    </ContentTemplate>
</asp:UpdatePanel>
<%--==== //Save Messege==== --%>


<div class="panel panel-primary">
    <%-- Header --%>
    <div class="panel-heading">
        <span style="color: white">Staff Group</span>
    </div>


    <%-- Body --%>
    <div class="panel-body w3-margin">

        <div class="col-md-9 text-left">
            <label class="label label-success" for="txtName">Group Name : </label>
            <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorGroup" runat="server" ControlToValidate="txtName" ErrorMessage="group name is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertGroupValidate"></asp:RequiredFieldValidator>
            <asp:Label ID="lblAlreadyExist" runat="server" ForeColor="Red" Text="Already Exist" Visible="False"></asp:Label>
        </div>

        <div class="col-md-3">
            <%-- Save Button --%>
            <br />
            <asp:Button ID="btnGroupSubmit" runat="server" Text="ADD" OnClick="btnGroupSubmit_Click"
                ValidationGroup="InsertGroupValidate" class="btn btn-primary" />
        </div>

        <div class="col-md-12">
            <span>>> If Want to Rearrange ordering then Drag and Drop The row</span>
            <br />
            <%-- =-===View==== --%>
            <asp:GridView ID="gvGroupView" runat="server" AutoGenerateColumns="false" OnRowUpdating="gvGroupView_RowUpdating" OnRowEditing="gvGroupView_RowEditing" OnRowCancelingEdit="gvGroupView_RowCancelingEdit" CssClass="w3-table-all">
                <Columns>

                    <%-- Id --%>
                    <asp:TemplateField HeaderText="Id">
                        <ItemTemplate>
                            <input type="hidden" name="GroupId" id="GroupId" value='<%# Eval("Id") %>' />
                            <asp:Label ID="lbl_ID" runat="server" Text=' <%# Eval("Id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%-- Group Name --%>
                    <asp:TemplateField HeaderText="GroupName">
                        <ItemTemplate>
                            <asp:Label ID="lbl_GroupName" runat="server" Text=' <%# Eval("GroupName") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtgrup_Name" runat="server" class="form-control" Text='<%#Eval("GroupName") %>' ></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <%-- SL No --%>
                    <asp:BoundField DataField="slNo" HeaderText="SL.No." />

                    <%-- ACtivity --%>
                    <asp:TemplateField HeaderText="Activity">
                        <ItemTemplate>
                            <asp:Label ID="hfGroupActivity" runat="server" Text='<%# Eval("Activity") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlActivity" runat="server" CssClass="form-control">
                                <asp:ListItem Text="True"></asp:ListItem>
                                <asp:ListItem Text="False"></asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <%-- Button --%>
                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>
                            <asp:Button ID="btn_Edit" runat="server" CssClass="btn btn-primary" Text="Edit" CommandName="Edit" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Button ID="btn_Update" runat="server" CssClass="btn btn-primary" Text="Update" CommandName="Update" />
                            <asp:Button ID="btn_Cancel" runat="server" CssClass="btn btn-primary" Text="Cancel" CommandName="Cancel" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </div>
    </div>
    <div class="panel-footer text-right">
        <asp:Button Text="Update SlNo" class="btn btn-primary" ID="btnUpdateRecord" runat="server" OnClick="btnUpdateRecord_Click" />
    </div>

</div>
