﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin.UCView.Setting
{
    public partial class UC_ThumbnailSettings : System.Web.UI.UserControl
    {
        double _lThumnailMaxFileSize = 204800;
        string _ThumnailfileName = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected bool ThumnailIsvalid()
        {
            if (GalleryUtils.IsExistGroup(txtGroupName.Text))
            {
                return false;
            }

            return true;
        }
        private void Thumnailclear()
        {
            txtGroupName.Text = string.Empty;
            txtSubject.Text = string.Empty;

            txtContent.Text = string.Empty;
            txtContent.Text = string.Empty;
        }
        private void ThumnailImageFileCheck()
        {
            if (ThumbNail_FileUpload.HasFile)
            {
                // Get the file extension
                string fileExtension = System.IO.Path.GetExtension(ThumbNail_FileUpload.FileName);

                if (fileExtension.ToLower() != ".jpg" && fileExtension.ToLower() != ".jpeg" && fileExtension.ToLower() != ".png"
                    && fileExtension.ToUpper() != ".JPG" && fileExtension.ToUpper() != ".JPEG" && fileExtension.ToUpper() != ".PNG")
                {
                    Response.Write("<script>alert('Only files with .jpg and .jpeg and png extension are allowed');</script>");
                }
                else
                {
                    // Get the file size
                    int fileSize = ThumbNail_FileUpload.PostedFile.ContentLength;
                    // If file size is greater than 2 MB
                    if (fileSize > _lThumnailMaxFileSize)//byte formate
                    {
                        Response.Write("<script>alert('File size cannot be greater than 200 KB');</script>");

                        //Ref:http://converter.elliotbeken.com/
                    }
                    else
                    {
                        // Specify the path to save the uploaded file to.
                        string savePath = "../FilesAndFolder/GalleryFile/";
                        // Get the name of the file to upload.
                        _ThumnailfileName = txtGroupName.Text + fileExtension;
                        // Create the path and file name to check for duplicates.
                        string pathToCheck = savePath + _ThumnailfileName;

                        //Alrady exist Check
                        #region ......AlreadyExist Check..........
                        // Create a temporary file name to use for checking duplicates.
                        string tempfileName = "";

                        // Check to see if a file already exists with the
                        // same name as the file to upload.
                        if (System.IO.File.Exists(pathToCheck))
                        {
                            int counter = 2;
                            while (System.IO.File.Exists(pathToCheck))
                            {
                                // if a file with this name already exists,
                                // prefix the filename with a number.
                                tempfileName = "am" + counter.ToString() + _ThumnailfileName;
                                pathToCheck = savePath + tempfileName;
                                counter++;
                            }

                            _ThumnailfileName = savePath + tempfileName;

                        }
                        else
                        {
                            _ThumnailfileName = pathToCheck;
                        }
                        #endregion
                    }
                }
            }

        }
        private void ThumbNailFileUpload(string path)
        {
            if (!path.ISNullOrWhiteSpace())
            {
                // Notify the user that the file name was changed.
                ThumbNail_FileUpload.SaveAs(Server.MapPath(path));
                //Image Show
            }
        }

        #region .....................Event.....................
        protected void validatorCheckFileSize_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = ThumbNail_FileUpload.FileContent.Length;
            if (filesize > _lThumnailMaxFileSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
        protected void cvExistCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string data = args.Value;
            args.IsValid = false;
            if (!ThumnailIsvalid())
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
        protected void CustomValidatorEmptyCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string data = args.Value;
            args.IsValid = false;
            if (ThumbNail_FileUpload.HasFile)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }
        protected void btnThumbnailSave_Click(object sender, EventArgs e)
        {
            //Fire All Validator
            // Page.Validate();

            ThumnailImageFileCheck();
            if (Page.IsValid && ThumnailIsvalid())
            {
                //Variable
                string GroupName = txtGroupName.Text;
                string Subjects = txtSubject.Text;
                string contents = txtContent.Text;
                string ImagePath = _ThumnailfileName.ISNullOrWhiteSpace() ? string.Empty : _ThumnailfileName;

                string SlNo = (GalleryUtils.GetMaxThumbnailSlno() + 1).ToString();
                bool ACtivity = true;

                //Query
                List<SqlParameter> lstParem = new List<SqlParameter>();
                string qry = GalleryUtils.InsertThumbnailQry(ImagePath, Subjects, Subjects, contents,
                    GroupName, ACtivity, SlNo, DBNull.Value, out lstParem);

                SqlHelper.MParameterList = lstParem;
                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    ThumbNailFileUpload(ImagePath);
                    Thumnailclear();

                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }
        #endregion

    }
}