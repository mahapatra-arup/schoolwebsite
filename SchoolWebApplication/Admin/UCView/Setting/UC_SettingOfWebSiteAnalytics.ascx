﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_SettingOfWebSiteAnalytics.ascx.cs" Inherits="SchoolWebApplication.Admin.UCView.Setting.UC_SettingOfWebSiteAnalytics" ValidateRequestMode="Disabled" %>

<%---------//Message ---------%>
<asp:Literal ID="ltrSaveMessege" runat="server"></asp:Literal>
<%---------//Message ---------%>

<div class="panel panel-primary">
    <div class="panel-heading">
        WebSite Analytics
    </div>
    <div class="panel-body w3-margin">

        <label class="label label-success"   for="txtTrackingId">Counter Code : </label>
        <asp:TextBox ID="txtTrackingId" class="form-control" runat="server" Placeholder="**Enter Counter code (limit of 500 characters)" TextMode="MultiLine" Rows="6" MaxLength="500"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvtxtTrackingId" runat="server" ErrorMessage="Counter code  is require" ControlToValidate="txtTrackingId" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="AnalyticsUpdate"></asp:RequiredFieldValidator>

        <br />

        <label class="label label-success"  for="txtAnalytics">Analytics Path : </label>
        <asp:TextBox ID="txtAnalytics" class="form-control" runat="server" Placeholder="**Enter Analytics Path (limit of 1000 characters)" TextMode="MultiLine" Rows="6" MaxLength="1000"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Analytics Path is require" ControlToValidate="txtAnalytics" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="AnalyticsUpdate"></asp:RequiredFieldValidator>

        <br />

        <div class="col-sm-12 text-right">
            <%--// Button--%>
            <asp:Button ID="btnSave" align="right" CssClass="w3-button  w3-border w3-border-red w3-round-large" runat="server" Text="Save" ValidationGroup="AnalyticsUpdate" OnClick="btnSave_Click"></asp:Button>
        </div>
    </div>
</div>
