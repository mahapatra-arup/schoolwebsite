﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin.UCView.Setting
{
    public partial class UC_EmailSetting : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Get Mail Details And fill Box
                GetMailDetails();
            }
        }



        private void UpdateEmailSetting()
        {
            if (Page.IsValid)
            {
                string _mailusername = txtMailUserName.Text;
                string _mail = txtMailAddress.Text;
                string _smtp = txtMailsmtp.Text;
                string _passwd = txtPassword.Text;
                string _port = txtport.Text;
                bool isSsl = chkSSL.Checked;

                //Query
                List<SqlParameter> ParamList = new List<SqlParameter>();
                string qry = MailUtils.UpdateMailQry(_mail, _passwd, _mailusername, _smtp, _port, isSsl, out ParamList);
                SqlHelper.MParameterList = ParamList;

                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    GetMailDetails();
                    Show("Data Update Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    //Java script
                    Response.Write("<script>alert('Data Update SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not Update", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
        }
        private void GetMailDetails()
        {
            DataTable dt = MailUtils.GetMailDetails();
            if (dt.IsValidDataTable())
            {
                txtMailUserName.Text = Convert.ToString(dt.Rows[0]["Name"]);
                txtMailAddress.Text = Convert.ToString(dt.Rows[0]["EmailId"]);
                txtMailsmtp.Text = Convert.ToString(dt.Rows[0]["SmtpServer"]);
                txtPassword.Text = Convert.ToString(dt.Rows[0]["Password"]);
                txtport.Text = Convert.ToString(dt.Rows[0]["Port"]);
                chkSSL.Checked = dt.Rows[0]["SSL"].ISValidObject() ? Convert.ToBoolean(dt.Rows[0]["SSL"]) : false;
            }
        }

        #region <------------Event----------->
        protected void btnMailSettingSave_Click(object sender, EventArgs e)
        {
            UpdateEmailSetting();
        }
        #endregion


    }
}