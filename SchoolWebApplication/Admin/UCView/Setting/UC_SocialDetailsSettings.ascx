﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_SocialDetailsSettings.ascx.cs" Inherits="SchoolWebApplication.Admin.UCView.Setting.UC_SocialDetailsSettings" %>
    <%--==== Save Messege==== --%>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==== //Save Messege==== --%>

<div class="panel panel-primary">
                    <%-- Header --%>
                    <div class="panel-heading" >
                        <span style="color: black">Social Setting</span>
                    </div>

                        <%-- Body --%>
                        <div class="panel-body w3-margin" >
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <%-- ===SocialMediaName== --%>
                                    <label class="label label-success" for="SocialMediaName">Social Media Name: </label>
                                    <asp:DropDownList ID="cmbSocialMediaName" CssClass="form-control" runat="server" OnSelectedIndexChanged="cmbSocialMediaName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cmbSocialMediaName" ErrorMessage="Social Media Name is required" InitialValue="--Select--" ForeColor="Red" SetFocusOnError="True" ValidationGroup="GroupofSocial"></asp:RequiredFieldValidator>

                                    <br />
                                    <%-- ====  SocialLink=== --%>
                                    <label class="label label-success" for="txtSocialLink">Social Link : </label>
                                    <asp:TextBox ID="txtSocialLink" runat="server" class="form-control" Placeholder="Enter Social Link (limit of 500 characters)" MaxLength="500" TextMode="MultiLine"></asp:TextBox>

                                      <br />
                                    <%-- ====IClass === --%>
                                    <label class="label label-success" for="txtIClass">(Bootstrap)Icon (e.g: fa fa-facebook) & hover Class:(e.g: w3-hover-red,w3-hover-sand,w3-hover-pale-green) </label>
                                    <asp:TextBox ID="txtIClass" runat="server" class="form-control" Placeholder="Enter IClass (limit of 100 characters)" MaxLength="100"></asp:TextBox>

                                      <br />
                                    <%-- ====  AClass=== --%>
                                    <label class="label label-success" for="txtAClass">A tag Class(e.g: w3_agile_facebook): </label>
                                    <asp:TextBox ID="txtAClass" runat="server" class="form-control" Placeholder="Enter AClass (limit of 100 characters)" MaxLength="100"></asp:TextBox>

                                      <br />
                                    <%-- ====Tooltip and Activity=== --%>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="label label-success" for="txtSocialTooltip">Tooltip : </label>
                                            <asp:TextBox ID="txtSocialTooltip" runat="server" class="form-control" Placeholder="Enter ToolTip (limit of 100 characters)" MaxLength="50"></asp:TextBox>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="label label-success" for="chkSocialACtivity">Activity : </label>
                                            <asp:CheckBox ID="chkSocialACtivity" CssClass="form-control" runat="server" />
                                        </div>
                                    </div>
                                    <%-- ====//Tooltip and Activity=== --%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <%-- Footer --%>
                        <div class="panel-footer text-right" >
                            <%-- Save Button --%>
                            <asp:Button ID="btnSocialDetailsSave" runat="server" Text="Submit" OnClick="btnSocialDetailsSave_Click"
                                ValidationGroup="GroupofSocial" class="btn btn-primary" />
                        </div>

                    </div>
