﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin.UCView.Setting
{
    public partial class UC_StaffSubGroupSettings : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
                ddlGroups.AddStaffGroup();
                ddlGroups.Items.Insert(0, "--Select--");

                //Find
                ddlFindGroups.AddStaffGroup();
                ddlFindGroups.Items.Insert(0, "--Select--");
            }
        }
        private void Save()
        {
            lblAlreadyExist.Visible = false;
            if (!StaffSubGroupsUtil.IsExistSubGroup(txtName.Text))
            {
                if (Page.IsValid)
                {
                    //Variable
                    string Subgroupname = txtName.Text;
                    string slno = (StaffSubGroupsUtil.GetMaxMenuSlno() + 1).ToString();
                    bool groupactivitye = true;
                    int GroupId = ddlGroups.SelectedValue.ConvertObjectToInt();

                    //Query
                    List<SqlParameter> lstParem = new List<SqlParameter>();
                    string qry = StaffSubGroupsUtil.QryInsertSubGroupUtils(Subgroupname, GroupId, slno, groupactivitye, out lstParem);
                    SqlHelper.MParameterList = lstParem;
                    if (SqlHelper.GetInstance().ExcuteQuery(qry))
                    {
                        Show("Group Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                        this.BindGrid();
                        txtName.Text = "";
                    }
                    else
                    {
                        Show("Group  was not Save ", "Error!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
                    }
                }
            }
            else
            {
                lblAlreadyExist.Visible = true;
            }
        }

        private void BindGrid()
        {
            if (!ddlFindGroups.SelectedValue.ISNullOrWhiteSpace())
            {
                var _groupId = ddlFindGroups.SelectedValue.ConvertObjectToInt();


                var data = StaffSubGroupsUtil.GetSubGroupDetails(_groupId);
                if (data.IsValidIEnumerable())
                {
                    gvSubGroupView.DataSource = data.ToList();
                    gvSubGroupView.DataBind();
                    gvSubGroupView.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void UpdatePreference()
        {
            if (Page.IsValid)
            {
                //GroupIds
                var v = Request.Form["SubGroupId"];
                int[] Ids = (from Q in v.Split(',')
                             select int.Parse(Q)).ToArray();

                int SlNo = 1;
                foreach (int Id in Ids)
                {
                    StaffSubGroupsUtil.UpdateSubGroupSlNo(Id, SlNo);
                    SlNo += 1;
                }

                //Refresh Time Save Problem Solve
                Response.Redirect(Request.Url.AbsoluteUri);
            }
        }



        #region <...............Event..................>

        protected void btnGroupSubmit_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void btnUpdateRecord_Click(object sender, EventArgs e)
        {
            UpdatePreference();
        }

        protected void gvGroupView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview   
            gvSubGroupView.EditIndex = -1;
            BindGrid();
        }

        protected void gvGroupView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //NewEditIndex property used to determine the index of the row being edited.   
            gvSubGroupView.EditIndex = e.NewEditIndex;
            BindGrid();
        }

        protected void gvGroupView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (Page.IsValid)
            {
                //Finding the controls from Gridview for the row which is going to update   
                Label id = gvSubGroupView.Rows[e.RowIndex].FindControl("lbl_ID") as Label;
                TextBox name = gvSubGroupView.Rows[e.RowIndex].FindControl("txtgrup_Name") as TextBox;
                DropDownList ddlActivity = gvSubGroupView.Rows[e.RowIndex].FindControl("ddlActivity") as DropDownList;

                if (!name.Text.ISNullOrWhiteSpace())
                {
                    int _id = id.Text.ConvertObjectToInt();
                    //Check Exist Group Name
                    var data = SqlHelper.DbContext().Staff_SubGroup.Where(s => s.SubGroupName == name.Text).Where(s => s.Id != _id).FirstOrDefault();
                    if (!data.ISValidObject())
                    {
                        StaffSubGroupsUtil.UpdateSubGroups(id.Text.ConvertObjectToInt(), ddlActivity.Text.ConvertObjectToBool(), name.Text);
                    }
                    else
                    {
                        Show("Group Name Is Require", "Invalid!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
                    }
                }
                else
                {
                    Show("Group Name Is Require", "Invalid!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
                }
                //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview   
                gvSubGroupView.EditIndex = -1;
                //Call ShowData method for displaying updated data   
                this.BindGrid();
            }
        }

        protected void ddlFindGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindGrid();
        }
        #endregion
    }
}