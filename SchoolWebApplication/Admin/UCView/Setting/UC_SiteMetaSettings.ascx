﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_SiteMetaSettings.ascx.cs" Inherits="SchoolWebApplication.Admin.UCView.Setting.UC_SiteMetaSettings" %>
<%--==== Save Messege==== --%>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
    </ContentTemplate>
</asp:UpdatePanel>
<%--==== //Save Messege==== --%>

<div class="panel panel-primary">
    <%-- Header --%>
    <div class="panel-heading">
        <span style="color: white">Meta Description</span>
      
    </div>

 
        <%-- Body --%>
        <div class="panel-body w3-margin">

            <asp:UpdatePanel ID="UpdatePanel_SiteMeta" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <div class="row">
                        <%-- 1 st Column --%>
                        <div class="col-sm-6">

                            <%-- Page --%>
                            <label class="label label-success" for="ddlPages">Pages : </label>
                            <asp:DropDownList ID="ddlPages" runat="server" class="form-control" OnSelectedIndexChanged="ddlPages_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvPages" Display="Dynamic" runat="server" ErrorMessage="Page is Require" ControlToValidate="ddlPages" SetFocusOnError="True" ValidationGroup="InsertSiteMeta"></asp:RequiredFieldValidator>

                              <br />
                            <%-- Title --%>
                            <label class="label label-success" for="txtSiteTitle">Title : </label>
                            <asp:TextBox ID="txtSiteTitle" runat="server" class="form-control"></asp:TextBox>

                        </div>

                        <%-- 2nd  Col --%>
                        <div class="col-sm-6" style="text-align: center">
                            <%-- --------Photo-------- --%>
                            <label class="label label-success" for="SiteIco_FileUpload">Website Icon : </label>
                            <div style="background-color: #ececf5; text-align: center">

                                <asp:Image ID="ImageOfSitemeta" runat="server" class="img-thumbnail img-responsive" Height="80px" Width="80px" />

                                <asp:FileUpload ID="SiteIco_FileUpload" onchange="FileOnchangeforSiteMeta(this);" class="btn-file" runat="server" />

                                <%-- Reular expression --%>
                                <asp:RegularExpressionValidator ID="rev_SiteIco_FileUpload" runat="server" Display="Dynamic"
                                    ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
                                    ControlToValidate="SiteIco_FileUpload" ValidationGroup="InsertSiteMeta" SetFocusOnError="True" ForeColor="Red"></asp:RegularExpressionValidator>
                                <%-- File Size Check --%>
                                <asp:CustomValidator ID="cv_SiteIco_FileUpload" ValidationGroup="InsertSiteMeta" runat="server" ErrorMessage="File size cannot be greater than 200 KB " OnServerValidate="cv_SiteIco_FileUpload_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="SiteIco_FileUpload" ValidateRequestMode="Enabled"></asp:CustomValidator>
                            </div>
                        </div>
                    </div>

                    <br />
                    <%-- Keyword --%>
                    <label class="label label-success" for="txtSiteKeyword">KeyWords : </label>
                    <asp:TextBox ID="txtSiteKeyword" class="form-control" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>

                     <br />
                    <%-- Description --%>
                    <label class="label label-success" for="txtDescription">Description : </label>
                    <asp:TextBox ID="txtDescription" class="form-control" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>

                </ContentTemplate>
            </asp:UpdatePanel>

        </div>

        <%-- Footer --%>
        <div class="panel-footer text-right" >
            <%-- Save Button --%>

            <asp:Button ID="btnSubmitSiteMeta" runat="server" Text="Submit" OnClick="btnSubmitSiteMeta_Click"
                ValidationGroup="InsertSiteMeta" class="btn btn-primary" />
        </div>
    </div>


 <%--==================Script For file upload (SiteMeta)=================--%>
    <script>
        function FileOnchangeforSiteMeta(input) {
            var imgv = document.getElementById("<%=ImageOfSitemeta.ClientID %>");
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(imgv)
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <%--==================//Script For file upload (SiteMeta)=================--%>
