﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_EmailSetting.ascx.cs" Inherits="SchoolWebApplication.Admin.UCView.Setting.UC_EmailSetting" %>

<%--==== Save Messege==== --%>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
    </ContentTemplate>
</asp:UpdatePanel>
<%--==== //Save Messege==== --%>


<div class="panel panel-primary">

    <%-- Header --%>
    <div class="panel-heading">
        <span style="color: black">Mail Setting</span>
       
    </div>

        <%-- Body --%>
        <div class="panel-body w3-margin" >
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <%-- Name --%>
                    <label class="label label-success" for="txtMailUserName">Full Name : </label>
                    <asp:TextBox ID="txtMailUserName" runat="server" class="form-control" Placeholder="Enter Name (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                    
                    <br />
                    <%-- Email --%>
                    <label class="label label-success" for="txtMailAddress">Mail Address : </label>
                    <asp:TextBox ID="txtMailAddress" runat="server" class="form-control" Placeholder="**Enter Mail Id (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMailAddress" ErrorMessage="Mail Address is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="groupofMailSetting"></asp:RequiredFieldValidator>

                      <br />
                    <%-- Password --%>
                    <label class="label label-success" for="txtPassword">Mail Password : </label>
                    <asp:TextBox ID="txtPassword" runat="server" class="form-control" Placeholder="**Enter Password (limit of 20 characters)" MaxLength="20"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" ErrorMessage="Mail Password is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="groupofMailSetting"></asp:RequiredFieldValidator>

                      <br />
                    <%-- smtp server --%>
                    <label class="label label-success" for="txtMailsmtp">SMTP Server : </label>
                    <asp:TextBox ID="txtMailsmtp" runat="server" class="form-control" Placeholder="**Enter SMTP server (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtMailsmtp" ErrorMessage="SMTP server is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="groupofMailSetting"></asp:RequiredFieldValidator>

                    <%-- ====port and ssl=== --%>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="label label-success" for="txtport">Port : </label>
                            <asp:TextBox ID="txtport" runat="server" TextMode="Number" class="form-control" Text="25" MaxLength="5"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtport" ErrorMessage="Port is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="groupofMailSetting"></asp:RequiredFieldValidator>
                        </div>

                        <div class="col-md-6">
                            <label class="label label-success" for="chkSSL">SSL : </label>
                            <asp:CheckBox ID="chkSSL" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <%-- ====//port and ssl=== --%>
                </ContentTemplate>
            </asp:UpdatePanel>
       
 </div>
        <%-- Footer --%>
        <div class="panel-footer text-right" >
            <%-- Save Button --%>

            <asp:Button ID="btnMailSettingSave" runat="server" Text="Submit" OnClick="btnMailSettingSave_Click"
                ValidationGroup="groupofMailSetting" class="btn btn-primary fa-save" />

        </div>


</div>
