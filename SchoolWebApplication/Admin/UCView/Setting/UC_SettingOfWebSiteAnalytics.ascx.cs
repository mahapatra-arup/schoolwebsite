﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Linq;
using System.Web.UI;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin.UCView.Setting
{
    public partial class UC_SettingOfWebSiteAnalytics : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowDetails();
            }
        }
        private void ShowDetails()
        {
            var _WebsiteAnalytics = WebsiteAnalyticsTools.GetWebsiteAnalytics().FirstOrDefault();
            if (_WebsiteAnalytics.ISValidObject())
            {
                txtAnalytics.Text = _WebsiteAnalytics.AnalyticsPath;
                txtTrackingId.Text = _WebsiteAnalytics.TrackingId;
            }
        }
        private void UpdateUserData()
        {
            if (Page.IsValid)
            {
                WebsiteAnalytic data = new WebsiteAnalytic();

                // data fill
                data.AnalyticsPath = txtAnalytics.Text;
                data.TrackingId = txtTrackingId.Text;

                //Execute
                if (WebsiteAnalyticsTools.UpdateWebsiteAnalytics(data))
                {
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrSaveMessege);

                    //Java script
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltrSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltrSaveMessege);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            UpdateUserData();
        }
    }
}