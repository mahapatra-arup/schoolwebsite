﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication
{
    public partial class AboutsEntry : System.Web.UI.Page
    {
        #region <--------Var-------->
        string _SavePath = DefultPaths.AboutUs_Folder_Path;
        double _lMaxFileSize = 512000;
        static bool mUpdateTime = false;
        static string _aboutId = string.Empty;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ltSaveMessege.Text = "";

            if (!IsPostBack)
            {
                //FillGried view
                FillGried();
                Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
            }
        }

        private void InsertData()
        {
            //Check
            if (Page.IsValid)
            {
                string _Path = FileUploadImage.GenerateFileUploadPath(_SavePath);

                #region <----Var Value---->
                About_Details _About_Details = new About_Details();
                _About_Details.Title = txtTitle.Text;
                _About_Details.Subjects = txtSubjects.Text;
                _About_Details.Contents = txtEditor.Text;
                _About_Details.Footer = txtFooter.Text;
                _About_Details.Activity = true;
                _About_Details.ImgPath = _Path.ISNullOrWhiteSpace() ? string.Empty : _Path;
                _About_Details.slno = (AboutUsTools.GetMaxAboutSlno() + 1);
                #endregion

                List<SqlParameter> lstParem = new List<SqlParameter>();

                //Query
                bool IsSuccess = AboutUsTools.InsertAboutUs(_About_Details);

                if (IsSuccess)
                {
                    //File Upload
                    FileUpload(_About_Details.ImgPath);

                    //Clear
                    Clear();

                    //Save Sussess
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);

                    //Java script
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");

                    //Refresh Gried
                    FillGried();
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }
        private void Clear()
        {
            txtTitle.Text = string.Empty;
            txtSubjects.Text = string.Empty;

            txtFooter.Text = string.Empty;
            txtEditor.Text = string.Empty;
            //must be
            mUpdateTime = false;
        }

        private void FileUpload(string path)
        {
            if (!path.ISNullOrWhiteSpace())
            {
                // Notify the user that the file name was changed.
                FileUploadImage.SaveAs(Server.MapPath(path));
                //Image Show
            }
        }
        private void UpdatetData(string id)
        {
            //Valid Check
            if (Page.IsValid)
            {
                #region <-----Var---->
                string _Title = txtTitle.Text;
                string Subjects = txtSubjects.Text;

                string Contents = txtEditor.Text;
                string Footer = txtFooter.Text;

                //new Image path
                string _Path = FileUploadImage.GenerateFileUploadPath(_SavePath);
                string ImgPath = _Path.ISNullOrWhiteSpace() ? string.Empty : _Path;

                List<SqlParameter> lstParem = new List<SqlParameter>();

                //.......Old path for Image Delete......
                string Oldpath = AboutUsTools.GetImagePath(id);

                #endregion

                //Query
                string qry = AboutUsTools.UpdateAboutUsQry(_Title, Subjects, Contents, Footer,
                 DBNull.Value, Subjects, id, out lstParem);

                SqlHelper.MParameterList = lstParem;

                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    //If Image path is available then
                    if (!ImgPath.ISNullOrWhiteSpace())
                    {
                        try
                        {
                            if (!Oldpath.ISNullOrWhiteSpace())
                            {
                                System.IO.File.Delete(Server.MapPath(Oldpath));
                            }
                        }
                        catch (Exception)
                        {
                        }

                        //.....Update.......
                        FileUpload(ImgPath);
                        AboutUsTools.UpdateAboutsImgPath(id, ImgPath);
                    }
                    else
                    {
                    }

                    //Clear
                    Clear();

                    //Success
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);

                    //Java script
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");

                    //Refresh Gried
                    FillGried();
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }


        #region <---------View-------->
        private void FillGried()
        {
            DataTable dt = AboutUsTools.GetAboutUs();
            if (dt.IsValidDataTable())
            {
                if (gvDetails.DataSource != null)
                {
                    gvDetails.DataSource = null;
                }
                gvDetails.DataSource = dt;
                gvDetails.DataBind();
            }
            else
            {
                dt.Rows.Add(dt.NewRow());
                gvDetails.DataSource = dt;
                gvDetails.DataBind();
                gvDetails.Rows[0].Cells.Clear();
                gvDetails.Rows[0].Cells.Add(new TableCell());
                gvDetails.Rows[0].Cells[0].ColumnSpan = dt.Columns.Count;
                gvDetails.Rows[0].Cells[0].Text = "No data Found";
                gvDetails.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }
        }

        private void GetDetails(string Id)
        {
            DataTable dt = AboutUsTools.GetAboutUs(Id);
            if (dt.IsValidDataTable())
            {
                try
                {
                    txtTitle.Text = dt.Rows[0]["Title"].ConvertObjectToString();
                    txtSubjects.Text = dt.Rows[0]["Subjects"].ConvertObjectToString();
                    txtEditor.Text = dt.Rows[0]["Contents"].ConvertObjectToString();
                    txtFooter.Text = dt.Rows[0]["Footer"].ConvertObjectToString();
                }
                catch (Exception)
                {

                }
            }

        }

        protected void gvDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDetails.PageIndex = e.NewPageIndex;
            this.FillGried();
        }

        #endregion

        #region <---------Event------->
        protected void gvDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                string vlu = e.CommandArgument.ToString();
                _aboutId = vlu;

                //old Image Path
                string path = AboutUsTools.GetImagePath(vlu);
                if (AboutUsTools.DeleteAbouts(vlu))
                {

                    if (!path.ISNullOrWhiteSpace())
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }

                    Show("Delete Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    //Refresh Gried
                    FillGried();
                }
            }
            else if (e.CommandName == "EditRow")
            {
                string vlu = e.CommandArgument.ToString();
                _aboutId = vlu;
                if (!vlu.ISNullOrWhiteSpace())
                {
                    GetDetails(vlu);
                    mUpdateTime = true;
                }
                else
                {
                    Show("Select Valid Row", "Warning!", _messegeType.warning, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
        }
        protected void validatorCheckFileSize_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileUploadImage.FileContent.Length;
            if (filesize > _lMaxFileSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
            {
                //CODE HEAR
                if (!mUpdateTime)
                {
                    InsertData();
                }
                else
                {
                    UpdatetData(_aboutId);
                }

                //UPDATE REFRESH PAGEE DATA
                Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
            }
            else
            {
                // "Page Refreshed";
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ViewState["CheckRefresh"] = Session["CheckRefresh"];
        }

        protected void btnAddMode_ServerClick(object sender, EventArgs e)
        {
            Clear();
        }
        #endregion

    }
}