﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="AddPageSub.aspx.cs" Inherits="SchoolWebApplication.Admin.AddPageSub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!---Save Messege------------ -->
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Save Messege-------------- -->


    <div class="panel panel-warning">
        <!--insert Mode--------------- -->
        <button class="w3-button w3-xlarge w3-circle w3-teal w3-card pull-right" runat="server" id="btnAddMode" onserverclick="btnAddMode_ServerClick" title="Active Insert Mode">+</button>
       
        <ul class="nav nav-pills panel-heading ">
            <li class="active"><a data-toggle="pill" href="#SubDynamicPageEntry">Create</a></li>
            <li><a data-toggle="pill" href="#SubDynamicPageViews">View</a></li>
        </ul>

        <div class="tab-content panel-body">

            <!-- Entry----------------------------------------------- -->
            <section id="SubDynamicPageEntry" class="tab-pane fade in active">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="col-md-12">
                            <label class="label label-info" for="ddlPagesName">Pages Name : </label>
                            <asp:DropDownList ID="ddlPagesName" runat="server" class="form-control"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvPages" InitialValue="--Select--" Display="Dynamic" runat="server" ForeColor="Red" ErrorMessage="Page is Require" ControlToValidate="ddlPagesName" SetFocusOnError="True" ValidationGroup="InsertDataGroup"></asp:RequiredFieldValidator>
                        </div>

                        <br />

                        <div class="col-md-12">
                            <label class="label label-info" for="txtSubPublisherName">Publisher/Auther : </label>
                            <asp:TextBox ID="txtSubPublisherName" class="form-control" runat="server" Placeholder="**Enter Publisher Auther (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                        </div>

                        <br />

                        <div class="col-md-7">
                            <label class="label label-info" for="txtSubSubjects">Subjects:</label>
                            <asp:TextBox ID="txtSubSubjects" class="form-control" runat="server" Placeholder="**Enter Subjects (limit of 200 characters)" MaxLength="200"></asp:TextBox>
                        </div>

                        <div class="col-md-5">
                            <label class="label label-info" for="txtSubDate">Publish Date : </label>
                            <asp:TextBox ID="txtSubDate" type="date" runat="server" class="form-control"></asp:TextBox>
                        </div>


                        <br />

                        <div class="col-md-12">
                            <label class="label label-info" for="txtSubContents">Content:</label>
                            <asp:TextBox ID="txtSubContents" class="form-control" Rows="5" runat="server" TextMode="MultiLine" Placeholder="Enter Content (limit of 500 characters)" MaxLength="500"></asp:TextBox>
                        </div>

                        <br />

                        <%--// Footer--%>
                        <div class="col-md-12">
                            <label class="label label-info" for="txtSubFooter">Footer : </label>
                            <asp:TextBox ID="txtSubFooter" class="form-control" runat="server" Placeholder="Enter Footer (limit of 200 characters)" MaxLength="200"></asp:TextBox>
                        </div>

                        <br />

                        <%--// Date From----------------%>
                        <div class="col-md-6">
                            <label class="label label-info" for="txtSubDateFrom">Date-From : </label>
                            <asp:TextBox ID="txtSubDateFrom" type="date" runat="server" class="form-control"></asp:TextBox>
                        </div>

                        <%--// Date To--------------------%>
                        <div class="col-md-6">
                            <label class="label label-info" for="txtSubDateTo">Date-To : </label>
                            <asp:TextBox ID="txtSubDateTo" type="date" runat="server" class="form-control"></asp:TextBox>
                        </div>

                        <br />

                        <%-- FileUpload--------------%>
                        <div class="col-md-12">

                            <%-- Pdf-------------------------------------------------------------------- --%>
                            <label class="label label-info" for="FileUploadSubPdf">PDF File : </label>
                            <asp:FileUpload ID="FileUploadSubPdf" runat="server" class="form-control" />
                            <asp:RegularExpressionValidator ID="rvfuFileCheck" runat="server" Display="Dynamic"
                                ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.pdf|.PDF)$" ErrorMessage="Only .pdf Files are allowed;" ControlToValidate="FileUploadSubPdf" SetFocusOnError="True" ForeColor="Red" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="cvSubPdfSizeCheck" ValidationGroup="InsertDataGroup" runat="server" ErrorMessage="File size cannot be greater than 200 KB " Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUploadSubPdf" OnServerValidate="cvSubPdfSizeCheck_ServerValidate"></asp:CustomValidator>

                            <!--Check Box -->
                            <asp:CheckBox ID="chkFileUploadSubPdf" runat="server" CssClass="w3-text-green" Text="&nbsp;Do you Save This ?" Font-Italic="True" Checked="True" />
                        </div>

                        <div class="col-md-12">
                            <%-- Image-------------------------------------------------------------------- --%>
                            <label class="label label-info" for="FileUploadSubImage">Image File : </label>
                            <asp:FileUpload ID="FileUploadSubImage" runat="server" class="form-control" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="Only .pdf Files are allowed;" ControlToValidate="FileUploadSubImage" SetFocusOnError="True" ForeColor="Red" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>
                            <asp:CustomValidator ID="cvSubImageSizeCheck" ValidationGroup="InsertDataGroup" runat="server" ErrorMessage="File size cannot be greater than 200 KB " Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUploadSubImage" OnServerValidate="cvSubImageSizeCheck_ServerValidate"></asp:CustomValidator>
                            <!--Check Box -->
                            <asp:CheckBox ID="chkFileUploadSubImage" runat="server" CssClass="w3-text-green" Text="&nbsp;Do you Save This ?" Checked="True" Font-Italic="True" BorderStyle="NotSet" />
                        </div>
                        <br />
                    </ContentTemplate>
                </asp:UpdatePanel>

                <span class="fa fa-info-circle alert alert-info">&nbsp;&nbsp;If you Update File Path then Check othrtwise not</span>
                <div class="text-right">
                    <%-- Save Button --%>
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" ValidationGroup="InsertDataGroup" CssClass="btn btn-primary" />
                </div>


            </section>



            <!-- View----------------------------------------------- -->
            <section id="SubDynamicPageViews" class="tab-panel fade">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <div>
                            <label class="label label-info" for="ddlPagesName">Pages Name : </label>
                            <asp:DropDownList ID="ddlPageForFind" runat="server" class="form-control" OnSelectedIndexChanged="ddlPageForFind_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>

                        </div>


                        <asp:GridView ID="gvView" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="table table-striped table-bordered" OnRowCommand="gvView_RowCommand">

                            <%-- ==Columns== --%>
                            <Columns>
                                <%-- slNo --%>
                                <asp:TemplateField HeaderText="SlNo">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- ====== Id=== --%>
                                <asp:TemplateField HeaderText="Id" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("Id") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <%-- ====== Subjects=== --%>
                                <asp:TemplateField HeaderText="Subjects">
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("Subjects") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- ====== Contents=== --%>
                                <asp:TemplateField HeaderText="Contents">
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("Contents") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- ====== Footer=== --%>
                                <asp:TemplateField HeaderText="Footer">
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("Footer") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- ====== Date=== --%>
                                <asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                        <asp:Label Text='<%#  "Publish Date : " 
                                            + (Eval("DateFrom")!=DBNull.Value? System.Convert.ToDateTime(Eval("PublishDate")).ToString("dd-MMM-yyyy"):"-") + " From : " +
                                            (Eval("DateFrom")!=DBNull.Value?System.Convert.ToDateTime(Eval("DateFrom")).ToString("dd-MMM-yyyy"):"-" )+ " To : "
                                            + (Eval("DateTo")!=DBNull.Value?System.Convert.ToDateTime(Eval("DateTo")).ToString("dd-MMM-yyyy"):"-" ) %>'
                                            runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- ====== Activity=== --%>
                                <asp:TemplateField HeaderText="Activity">
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("Activity") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- == File View== --%>
                                <asp:TemplateField HeaderText="PDF">
                                    <ItemTemplate>
                                        <%#string.Format("<a href='#' onClick='window.open(\"{0}\", \"_blank\")'> <span class='fa fa-eye' title='View File'></span></a>",Convert.ToString(Eval("PdfFileUrl"))) %>
                                        <br />
                                        <%# Convert.ToString(Eval("PdfFileUrl"))!=string.Empty?"<span class=\"w3-text-blue\">Available</span>":"<span class=\"w3-text-red\">Not Available</span>"%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Image">
                                    <ItemTemplate>
                                        <%#string.Format("<a href='#' onClick='window.open(\"{0}\", \"_blank\")'> <span class='fa fa-eye' title='View File'></span></a>",Convert.ToString(Eval("ImageFileUrl"))) %>
                                        <br />
                                        <%# Convert.ToString(Eval("ImageFileUrl"))!=string.Empty?"<span class=\"w3-text-blue\">Available</span>":"<span class=\"w3-text-red\">Not Available</span>"%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--= ==Column Edit and Delete=== --%>
                                <asp:TemplateField HeaderText="Action">

                                    <ItemTemplate>
                                        <asp:ImageButton ImageUrl="~/Admin/images/Alter.png" ID="imgAlter" CommandName="EditRow" ToolTip="Edit" CommandArgument='<%# Eval("Id") %>' runat="server"></asp:ImageButton>

                                        <asp:ImageButton ImageUrl="~/Admin/images/Delete.png" ID="imgDelete" CommandName="DeleteRow" ToolTip="Delete" CommandArgument='<%# Eval("Id") %>' runat="server"
                                            OnClientClick="return confirm('Are you sure you want to delete this event?');"></asp:ImageButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <%--= ==//Column Edit and Delete=== --%>
                            </Columns>

                        </asp:GridView>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </section>

        </div>

    </div>

      <!-- For Datatable -->
    <script type="text/javascript">  
        $(function () {
            bindDataTable(); // bind data table on first page load
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(bindDataTable); // bind data table on every UpdatePanel refresh
        });

        function bindDataTable() {
            var topProductsTable = $('#<%=gvView.ClientID%>').DataTable({ "scrollX": true });
        }
    </script>
</asp:Content>
