﻿
//    <% -----===========Script For file upload =======---%>

function FileOnchange(input) {
    var imgv = document.getElementById("<%=ImageOfUser.ClientID %>");
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(imgv)
                .attr('src', e.target.result)
                .width(150)
                .height(150);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
//<% ---============ Script For file upload =========== -----%>

