﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="NoticeBoard.aspx.cs" Inherits="SchoolWebApplication.Admin.NoticeBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- ===Save Messege=== --%>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- ===Save Messege=== --%>

    <asp:Panel ID="pnlNoticeBoard" runat="server">

        <div class="container-fluid">

            <%--==== insert Mode== --%>
            <button class="w3-button w3-xlarge w3-circle w3-teal w3-card pull-right" runat="server" id="btnAddMode" onserverclick="btnAddMode_ServerClick" title="Active Insert Mode">+</button>


            <%--==== Tab==== --%>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#NoticeEntry">Entry</a></li>
                <li><a data-toggle="tab" href="#NoticeView">View</a></li>
            </ul>
            <%--==== //Tab==== --%>

            <div class="tab-content">

                <%--========== Entry Form active===========--%>
                <section id="NoticeEntry" class="tab-pane fade in active">
                    <br />

                    <div class="container-fluid w3-border-blue">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>

                                <div class="row">

                                    <%--===Publisher Name and date===--%>
                                    <div class="form-group">

                                        <div class="col-md-9">
                                            <label class="label label-info" for="txtPublisherName">Publisher Name : </label>
                                            <asp:TextBox ID="txtPublisherName" class="form-control" runat="server" Placeholder="**Enter Publisher Name (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPublisherName" runat="server" ErrorMessage="Publisher Name is require" ControlToValidate="txtPublisherName" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertSave"></asp:RequiredFieldValidator>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="label label-info" for="txtDate">Publish Date : </label>
                                            <asp:TextBox ID="txtDate" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvDate" runat="server" ErrorMessage="Date is Require" ControlToValidate="txtDate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertSave"></asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Date is Require" ControlToValidate="txtDateFrom" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertSave"></asp:RequiredFieldValidator>
                                            <%-- ajax --%>
                                            <asp:RegularExpressionValidator runat="server" ControlToValidate="txtDate" ValidationExpression="^[0-9y]{4}-[0-9m]{1,2}-[0-9d]{1,2}$"
                                                ErrorMessage="Invalid date format(yyyy-MM-dd)" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertSave" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" PopupButtonID="imgPopup" runat="server" TargetControlID="txtDate" Format="yyyy-MM-dd" />

                                        </div>
                                    </div>
                                    <%--===//Publisher Name and date===--%>
                                </div>

                                <br />

                                <div class="row">

                                    <div class="form-group">

                                        <%--// Subjects--%>
                                        <div class="col-md-6">
                                            <label class="label label-info" for="pwd">Subjects:</label>
                                            <asp:TextBox ID="txtSubjects" class="form-control" runat="server" Placeholder="**Enter Notice Subjects (limit of 200 characters)" MaxLength="200"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvSubject" runat="server" ControlToValidate="txtSubjects" ErrorMessage="Subject is Require" ValidationGroup="InsertSave" ForeColor="Red" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>

                                        <%--// FileUpload1--%>
                                        <div class="col-md-6">
                                            <label class="label label-info" for="FileUpload1">PDF File : </label>
                                            <asp:FileUpload ID="FileUpload1" runat="server" class="form-control1" />
                                            <%-- Reular expression --%>
                                            <asp:RegularExpressionValidator ID="rvfuFileCheck" runat="server" Display="Dynamic"
                                                ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.pdf|.PDF)$" ErrorMessage="Only .pdf Files are allowed;" ControlToValidate="FileUpload1" SetFocusOnError="True" ForeColor="Red" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                                            <%-- Size Validation --%>
                                            <asp:CustomValidator ID="VCFFileSizeCheck" ValidationGroup="InsertSave" runat="server" ErrorMessage="File size cannot be greater than 200 KB " Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUpload1" OnServerValidate="VCFFileSizeCheck_ServerValidate"></asp:CustomValidator>

                                        </div>

                                    </div>

                                </div>

                                <br />

                                <%--// Content--%>
                                <div class="row">

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="label label-info" for="txtContent">Content:</label>
                                            <asp:TextBox ID="txtContent" class="form-control" Rows="5" runat="server" TextMode="MultiLine" Placeholder="Enter Notice Content (limit of 500 characters)" MaxLength="500"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <br />

                                <%--//footer Date ferom Date to--%>
                                <div class="row">
                                    <div class="form-group">
                                        <%--// Footer--%>
                                        <div class="col-md-6">
                                            <label class="label label-info" for="txtFooter">Footer : </label>
                                            <asp:TextBox ID="txtFooter" class="form-control" runat="server" Placeholder="Enter Notice Footer (limit of 200 characters)" MaxLength="200"></asp:TextBox>
                                        </div>

                                        <%--// Date From--%>
                                        <div class="col-md-3">
                                            <label class="label label-info" for="txtDateFrom">Date From : </label>
                                            <asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
                                            <%-- ajax --%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Date is Require" ControlToValidate="txtDateFrom" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertSave"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator runat="server" ControlToValidate="txtDateFrom" ValidationExpression="^[0-9y]{4}-[0-9m]{1,2}-[0-9d]{1,2}$"
                                                ErrorMessage="Invalid date format(yyyy-MM-dd)" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertSave" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" PopupButtonID="imgPopup" runat="server" TargetControlID="txtDateFrom" Format="yyyy-MM-dd" />
                                        </div>

                                        <%--// Date To--%>
                                        <div class="col-md-3">
                                            <label class="label label-info" for="txtDateTo">Date To : </label>
                                            <asp:TextBox ID="txtDateTo" runat="server" class="form-control"></asp:TextBox>
                                            <%-- ajax --%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Date is Require" ControlToValidate="txtDateFrom" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertSave"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator runat="server" ControlToValidate="txtDateTo" ValidationExpression="^[0-9y]{4}-[0-9m]{1,2}-[0-9d]{1,2}$"
                                                ErrorMessage="Invalid date format.(yyyy-MM-dd)" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ValidationGroup="InsertSave" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupButtonID="imgPopup" runat="server" TargetControlID="txtDateTo" Format="yyyy-MM-dd" />


                                        </div>
                                    </div>
                                </div>

                                <br />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%---== Button==--%>
                        <div class="form-group">
                            <asp:Button ID="btnSave" Style="float: right; width: 97px;" class="w3-button w3-white w3-border w3-border-red w3-round-large" runat="server" Text="Save" Width="102px" OnClick="btnSave_Click" ValidationGroup="InsertSave"></asp:Button>
                        </div>
                        <span class="fa fa-info-circle alert alert-info">&nbsp; Date Formate is (yyyy-MM-dd)</span>
                        <%---== //Button==--%>
                    </div>

                </section>
                <%--==============Entry form============== --%>


                <%-- ==============View  form===============--%>
                <section id="NoticeView" class="tab-panel fade">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Notice View::</div>
                        <div class="panel-body">

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>

                                    <asp:GridView ID="gvNoticeView" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="table table-striped table-bordered" OnRowCommand="gvNoticeView_RowCommand">

                                        <%-- ==Columns== --%>
                                        <Columns>

                                            <%-- ====== Id=== --%>
                                            <asp:TemplateField HeaderText="Id" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%#Eval("Id") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%-- ====== Subjects=== --%>
                                            <asp:TemplateField HeaderText="Subjects">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%#Eval("Subjects") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%-- ====== Contents=== --%>
                                            <asp:TemplateField HeaderText="Contents">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%#Eval("Contents") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%-- ====== Footer=== --%>
                                            <asp:TemplateField HeaderText="Footer">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%#Eval("Footer") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%-- ====== Date=== --%>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%#  "Publish Date : " 
                                                        + String.Format("{0:d}", Eval("PublishDate")) + "<br/> From : " +
                                                         String.Format("{0:d}", Eval("DateFrom"))+ " || To : "
                                                        + String.Format("{0:d}", Eval("DateTo")) %>'
                                                        runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%-- ====== Activity=== --%>
                                            <asp:TemplateField HeaderText="Activity">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%#Eval("Activity") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%-- ==Notice File View== --%>
                                            <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <%#string.Format("<a href='#' onClick='window.open(\"{0}\", \"_blank\")'> <span class='fa fa-eye' title='View File'></span></a>",Convert.ToString(Eval("NoticeFileUrl"))) %>
                                                    <br />
                                                    <%# Convert.ToString(Eval("NoticeFileUrl"))!=string.Empty?"<span class=\"w3-text-blue\">Available</span>":"<span class=\"w3-text-red\">Not Available</span>"%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--= ==Column Edit and Delete=== --%>
                                            <asp:TemplateField HeaderText="Image">

                                                <ItemTemplate>
                                                    <asp:ImageButton ImageUrl="~/Admin/images/Alter.png" ID="imgAlter" CommandName="EditRow" ToolTip="Edit" CommandArgument='<%# Eval("Id") %>' runat="server"></asp:ImageButton>

                                                    <asp:ImageButton ImageUrl="~/Admin/images/Delete.png" ID="imgDelete" CommandName="DeleteRow" ToolTip="Delete" CommandArgument='<%# Eval("Id") %>' runat="server"
                                                        OnClientClick="return confirm('Are you sure you want to delete this event?');"></asp:ImageButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <%--= ==//Column Edit and Delete=== --%>
                                        </Columns>

                                    </asp:GridView>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>

                </section>
                <%-- ================View Form==============--%>
            </div>

        </div>

    </asp:Panel>

    <%-- For Datatable --%>
    <script type="text/javascript">  
       
        //default call
        $(document).ready(function () {
            datatableload();
        });

        //Update panel load
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            // re-bind your jQuery events here
            datatableload();
        });

        function datatableload() {
            $('#<%=gvNoticeView.ClientID%>').DataTable({ "scrollX": true });
        }
    </script>
</asp:Content>
