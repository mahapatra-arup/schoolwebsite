﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="StaffEntryWndow.aspx.cs" Inherits="SchoolWebApplication.StaffEntryWndow" ValidateRequest="false" %><%-- EnableEventValidation="False"--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">

    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="Panel1" runat="server">

        <%--== Save Messege== --%>
        <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
        <%-- ==//Save Messege ==--%>

        <%-- Save Messege --%>
        <asp:Literal ID="ltrGriedMessage" runat="server"></asp:Literal>
        <%-- Save Messege --%>

        <!-----View Form----============= -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <a id="btnnavopenclose" runat="server" onserverclick="btnnavopenclose_Click" style="cursor: pointer; float: right;" href="javascript:void(0);" class="btn btn-warning"><span class="fa fa-plus-square"></span>&nbsp;Create</a>
                <span class="fa fa-desktop"></span>&nbsp;Details
            </div>

            <div class="panel-body">

                <asp:UpdatePanel ID="UpdatePanelGried" runat="server">
                    <ContentTemplate>

                        <asp:GridView ID="gvStaffView" runat="server" AutoGenerateColumns="False" DataKeyNames="EmpCode" CssClass="table table-striped table-bordered bootstrap-datatable datatable" OnRowCommand="gvStaffView_RowCommand" Width="100%">

                            <%-- Columns --%>
                            <Columns>

                                <%-- Column Emp Id --%>
                                <asp:TemplateField HeaderText="Emp Id">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("EmpCode") %>' runat="server"></asp:Label>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEmpCode" Text='<%#Eval("EmpCode") %>' runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <%-- Footer --%>
                                    <FooterTemplate>
                                    </FooterTemplate>

                                </asp:TemplateField>

                                <%-- Column Emp Name --%>
                                <asp:TemplateField HeaderText="Name">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("EmpName") %>' runat="server"></asp:Label>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEmpName" Text='<%#Eval("EmpName") %>' runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <%-- Footer --%>
                                    <FooterTemplate>
                                    </FooterTemplate>

                                </asp:TemplateField>

                                <%-- Column Gender --%>
                                <asp:TemplateField HeaderText="Gender">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("Gender") %>' runat="server"></asp:Label>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtGender" Text='<%#Eval("Gender") %>' runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <%-- Footer --%>
                                    <FooterTemplate>
                                    </FooterTemplate>

                                </asp:TemplateField>

                                <%-- Column Dob --%>
                                <asp:TemplateField HeaderText="DOB">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("Dob") %>' runat="server"></asp:Label>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtDob" Text='<%#Eval("Dob") %>' runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <%-- Footer --%>
                                    <FooterTemplate>
                                    </FooterTemplate>

                                </asp:TemplateField>

                                <%-- Column Address --%>
                                <asp:TemplateField HeaderText="Address">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <asp:Label Text='<%#Eval("Address") %>' runat="server"></asp:Label>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtAddress" Text='<%#Eval("Address") %>' runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <%-- Footer --%>
                                    <FooterTemplate>
                                    </FooterTemplate>

                                </asp:TemplateField>

                                <%-- Column Image --%>
                                <asp:TemplateField HeaderText="Image">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <asp:Image ImageUrl='<%#Eval("ImagePath") %>' runat="server" CssClass="img-responsive" Width="80px" Height="80px"></asp:Image>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                        <asp:Image ID="empImg" ImageUrl='<%#Eval("ImagePath") %>' runat="server"></asp:Image>
                                    </EditItemTemplate>
                                    <%-- Footer --%>
                                    <FooterTemplate>
                                    </FooterTemplate>

                                </asp:TemplateField>

                                <%-- Column Edit and Delete --%>
                                <asp:TemplateField HeaderText="Image">

                                    <%-- Simple Template --%>
                                    <ItemTemplate>
                                        <asp:ImageButton ImageUrl="~/Admin/images/Alter.png" ID="imgAlter" CommandName="EditRow" ToolTip="Edit" CommandArgument='<%# Eval("EmpCode") %>' runat="server"></asp:ImageButton>

                                        <asp:ImageButton ImageUrl="~/Admin/images/Delete.png" ID="imgDelete" CommandName="DeleteRow" ToolTip="Delete" CommandArgument='<%# Eval("EmpCode") %>' runat="server"
                                            OnClientClick="return confirm('Are you sure you want to delete this event?');"></asp:ImageButton>
                                    </ItemTemplate>

                                    <%-- Edit Template --%>
                                    <EditItemTemplate>
                                    </EditItemTemplate>

                                    <%-- Footer --%>
                                    <FooterTemplate>
                                    </FooterTemplate>

                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
        <!--//View Form  =================-->


        <!--Staff entry form============-->
        <div id="pnlStaffEntryform" class="container-fluid">

            <asp:UpdatePanel ID="UpdatePnllUpdateForm" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <%-- First slot --%>
                    <div class="row">

                        <%-- ----------Left Side-------- --%>
                        <div class="col-sm-8 col-md-8 col-lg-8">
                            <div class="form-group">

                                <%-- EmppId --%>
                                <asp:UpdatePanel ID="UpdatePaneleMpid" runat="server">
                                    <ContentTemplate>
                                        <label class="label label-primary" for="txtEmpCode">Employee Id : </label>
                                        <asp:TextBox ID="txtEmpCode" class="form-control" runat="server" AutoPostBack="True" OnTextChanged="txtEmpCode_TextChanged" Placeholder="**Enter Employee Id (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmpCode" Display="Dynamic" ErrorMessage="Employee Code is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="GStaffEntry"></asp:RequiredFieldValidator>
                                        <asp:Label ID="lblEmpIdExistMessege" runat="server" Text=" " ForeColor="Red"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <br />
                                <%--// Name name--%>
                                <label class="label label-primary" for="txtName">Employee Name : </label>
                                <asp:TextBox ID="txtName" class="form-control" runat="server" Placeholder="**Enter Employee Name (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtName" Display="Dynamic" ErrorMessage="Employee Name is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="GStaffEntry"></asp:RequiredFieldValidator>

                                <br />

                                <%-- dob --%>
                                <label class="label label-primary" for="txtDOBDate">DOB :</label>
                                <asp:TextBox ID="txtDOBDate" runat="server" class="form-control" TextMode="Date"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDOB" runat="server" ControlToValidate="txtDOBDate" Display="Dynamic" ErrorMessage="D.O.B. is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="GStaffEntry"></asp:RequiredFieldValidator>
                            </div>
                        </div>


                        <%-----------Right SIde---------- --%>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                                <%-----Image-----%>
                                <label class="label label-primary" for="imgUserImage">Image File : </label>
                                <div class="content-box bg-success" style="padding: 5px">

                                    <%--------IMAGE pART--------%>
                                    <asp:Image runat="server" ID="ImageOfUser" CssClass="img-responsive" Height="150px" Width="150px" ImageUrl="#" AlternateText="Staff Image" />

                                    <%--// FileUpload1--%>
                                    <asp:FileUpload ID="FileUpload1" class="form-control btn-file" onchange="FileOnchangeforSiteMeta(this);" runat="server" />



                                    <%-- Reular expression --%>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorFileUpload" runat="server" Display="Dynamic"
                                        ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
                                        ControlToValidate="FileUpload1" SetFocusOnError="True" ForeColor="Red" ValidationGroup="GStaffEntry"></asp:RegularExpressionValidator>

                                    <%-- File Size Check --%>
                                    <asp:CustomValidator ID="validatorCheckFileSize" ControlToValidate="FileUpload1" ValidationGroup="GStaffEntry" runat="server" ErrorMessage="File size cannot be greater than 200 KB " OnServerValidate="validatorCheckFileSize_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"></asp:CustomValidator>

                                </div>
                            </div>
                        </div>

                    </div>


                    <%-- Second row --%>
                    <div class="row">

                        <%-- ----------Left Side-------- --%>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                                <%-- Group --%>

                                <label class="label label-primary" for="ddGroup">Group : </label>
                                <asp:DropDownList ID="ddGroup" runat="server" class="form-control" Width="100%">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorGroup" runat="server" ControlToValidate="ddGroup" Display="Dynamic" ErrorMessage="Group is require" ForeColor="Red" ValidationGroup="GStaffEntry" SetFocusOnError="True"></asp:RequiredFieldValidator>

                                <br />

                                <%--//Employee Address --%>
                                <label class="label label-primary" for="txtAddress">Address :</label>
                                <asp:TextBox ID="txtAddress" runat="server" class="form-control" Placeholder="Enter Employee Name (limit of 200 characters)" MaxLength="200"></asp:TextBox>

                                <br />
                                <%--    Qualifiction --%>
                                <label class="label label-primary" for="txtEduQualification">Education Qualification : </label>
                                <asp:TextBox ID="txtEduQualification" class="form-control" runat="server" Placeholder="Enter Employee Education Qualification (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorEduQualification" runat="server" ControlToValidate="txtEduQualification" Display="Dynamic" ErrorMessage="Education Qualification is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="GStaffEntry"></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <%------------Middle Section----------- --%>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">

                                <%--//Employee Designation  --%>
                                <label class="label label-primary" for="txtdesignation">Designation :</label>
                                <asp:TextBox ID="txtdesignation" class="form-control" runat="server" Placeholder="Enter Employee Designation (limit of 100 characters)" MaxLength="100"></asp:TextBox>

                                <br />
                                <%-- and  Subjects --%>
                                <label class="label label-primary" for="txtSubject">Subjects : </label>
                                <asp:TextBox ID="txtSubject" class="form-control" runat="server" Placeholder="Enter Employee Education Subject (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorSubjects" runat="server" ControlToValidate="txtSubject" Display="Dynamic" ErrorMessage="Subjects is required" ForeColor="Red" SetFocusOnError="True" ValidationGroup="GStaffEntry"></asp:RequiredFieldValidator>--%>
                                <br />

                                <%-- PhNo --%>

                                <label class="label label-primary" for="txtPhNo">Ph. No. :</label>
                                <asp:TextBox ID="txtPhNo" class="form-control" AutoCompleteType="HomePhone" runat="server" TextMode="SingleLine" Placeholder="Enter Employee Phone Number" MaxLength="12"></asp:TextBox>
                                <%-- Expression --%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhNo" runat="server" ErrorMessage="Invalid PhNo" ControlToValidate="txtPhNo" ForeColor="#FF6600" SetFocusOnError="True"
                                    ValidationExpression="^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}|(\d[ -]?){10}\d$" ValidationGroup="GStaffEntry"></asp:RegularExpressionValidator>

                            </div>
                        </div>

                        <%-- -------Right SIde--------- --%>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group">
                                <%-- Email --%>

                                <label class="label label-primary" for="txtMail">Email Address : </label>
                                <asp:TextBox ID="txtMail" class="form-control" runat="server" TextMode="SingleLine" Placeholder="Enter Employee Mail Address (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorMail" runat="server" ErrorMessage="Invalid Email" ControlToValidate="txtMail" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="GStaffEntry"></asp:RegularExpressionValidator>
                                <br />

                                <%-- Gender --%>
                                <label class="label label-primary" for="rbtnGender">Gende : </label>
                                <asp:RadioButtonList ID="rbtnGender" class="form-control" runat="server" RepeatDirection="Horizontal" Style="text-align: center; vertical-align: central">
                                    <asp:ListItem Selected="True">Male</asp:ListItem>
                                    <asp:ListItem>Female</asp:ListItem>
                                    <asp:ListItem>Others</asp:ListItem>
                                </asp:RadioButtonList>


                                <%--// Footer Description--%>

                                <label class="label label-primary" for="txtDescription">Description : </label>
                                <asp:TextBox ID="txtDescription" class="form-control text-area" runat="server" TextMode="MultiLine" Placeholder="Enter Employee Description (limit of 200 characters)" MaxLength="200"></asp:TextBox>
                            </div>
                        </div>

                    </div>


                    <%-- third Row --%>
                    <div class="row">
                        <div class="col-lg-12">
                            <%--Opnion--%>
                            <div class="form-group">
                                <label class="label label-primary" for="comment">Opinion :</label>
                                <asp:TextBox ID="txtOpinion" class="form-control" Style="text-align: left" runat="server" placeholder="Enter Opinion (limit of 2000 characters)" Rows="6" aria-valuemax="2000" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>


                </ContentTemplate>
            </asp:UpdatePanel>

            <%--// Save Button--%>
            <div class="form-group">
                <asp:Button ID="btnSave" runat="server" Style="display: none" ValidationGroup="GStaffEntry" OnClick="btnSave_Click"></asp:Button>
            </div>

        </div>
        <!--//Entry  Form=========== -->


        
    </asp:Panel>

    <script type="text/javascript">

        //=======For Dialog=============>
        $(function () {
            $("#pnlStaffEntryform").dialog({

                height: 620,
                width: 900,
                autoOpen: false, //For Auto Post Back
                appendTo: 'form',
                buttons: {
                    "Save": function () {
                        $("#<%:btnSave.ClientID%>").click();

                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        });
        //open
        function openResultDialog() {
            $(function () {
                $("#pnlStaffEntryform").dialog("open");
            });
        }


        //====For Datatable===================>
        //Call
        $(function () {
            CreateDataTable();
            UpdatePanelRefresh();
        });
        //--For Datatable-- %>
        function CreateDataTable() {
            $(function () {
                $('#<%=gvStaffView.ClientID%>').DataTable({ "scrollX": true });
            });
        }
        //////On UpdatePanel Refresh.
        function UpdatePanelRefresh() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        CreateDataTable();
                    }
                });
            };
        }


   //<!--Script For file upload =================-->
        function FileOnchangeforSiteMeta(input) {
            var imgv = document.getElementById("<%=ImageOfUser.ClientID %>");
             if (input.files && input.files[0]) {
                 var reader = new FileReader();

                 reader.onload = function (e) {
                     $(imgv)
                         .attr('src', e.target.result)
                         .width(80)
                         .height(80);
                 };

                 reader.readAsDataURL(input.files[0]);
             }
         }
    </script>
    <script src="js/Staff.js"></script>
</asp:Content>
