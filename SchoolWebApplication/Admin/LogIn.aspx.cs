﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Threading;
using System.Web.Security;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin
{
    public partial class LogIn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page.User.Identity.IsAuthenticated)
            {
                Response.Redirect(FormsAuthentication.DefaultUrl);
            }

            //Fill
            RememberMeFill();
        }
        private void RememberMeFill()
        {
            try
            {
                if (Request.Cookies["uname"] != null && Request.Cookies["upws"] != null)
                {
                    var pws = CryptorEngine.Decrypt(Request.Cookies["upws"].Value.ConvertObjectToString(), true);
                    txtUsername.Text = Request.Cookies["uname"].Value.ConvertObjectToString();
                    txtPassword.Text = pws;
                    chkRememberMe.Checked = Request.Cookies["uremember"].Value.ConvertObjectToBool();
                }
            }
            catch (Exception)
            {

            }
        }
        private void RememberMeSet()
        {
            try
            {
                if (chkRememberMe.Checked)
                {
                    var pws = CryptorEngine.Encrypt(txtPassword.Text, true);
                    Request.Cookies["uname"].Value = txtUsername.Text;
                    Request.Cookies["upws"].Value = pws;
                    Request.Cookies["uremember"].Value = chkRememberMe.Checked.ConvertObjectToString();
                }
            }
            catch (Exception)
            {

            }
        }
        private void LogInCheck()
        {
            //User Name or mail
            string _UserName = txtUsername.Text;
            //Password
            string _Password = txtPassword.Text;

            string _Messege = string.Empty;
            //Login Check
            if (UserUtils.IsValid(_UserName, _Password, out _Messege))
            {
                RememberMeSet();
                Show(_Messege, "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                Thread.Sleep(5000);
                if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
                {
                    FormsAuthentication.SetAuthCookie(_UserName, chkRememberMe.Checked);
                    Response.Redirect(Request.QueryString["ReturnUrl"]);
                }
                else
                {
                    FormsAuthentication.RedirectFromLoginPage(_UserName, chkRememberMe.Checked);
                }

            }
            else
            {
                Show(_Messege, "Invalid!!", _messegeType.danger, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }
        protected void btnLogIn_Click(object sender, EventArgs e)
        {
            LogInCheck();
        }
    }
}