﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)


namespace SchoolWebApplication.Admin
{
    public partial class Gallery : System.Web.UI.Page
    {
        #region <------Variable------->
        double _lMaxFileSize = 1048576;
        string _SavePath = DefultPaths.Gallery_Folder_Path;
        private static bool mUpdateTime = false;
        private static string _GalleryId = "";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ltrSaveMessege.Text = "";

            if (!IsPostBack)
            {
                cmbThambnailGroup.AddThumbnailGroup();//Add Group
                ddlGroupNames.AddThumbnailGroup();
                this.FillGried();
            }
        }
        private void InsertData()
        {
            if (Page.IsValid)
            {
                string _Path = FileUpload1.GenerateFileUploadPath(_SavePath);

                #region <---------Var--------->
                string subject = txtSubject.Text;
                string content = txtContent.Text;
                string AlternateText = txtAlterNateText.Text;
                string ToolTip = txtToolTip.Text;
                string GalleryThumbnailId = cmbThambnailGroup.SelectedValue;
                string ImgPath = _Path.ISNullOrWhiteSpace() ? string.Empty : _Path;
                bool Activity = true;
                int Slno = (GalleryUtils.GetMaxGallerySlno() + 1);

                List<SqlParameter> lstParem = new List<SqlParameter>();
                #endregion

                //Query
                string qry = GalleryUtils.InsertGalleryQry(ImgPath, Slno.ToString(), Activity, ToolTip, AlternateText,
                    DBNull.Value, content, GalleryThumbnailId, subject, out lstParem);

                SqlHelper.MParameterList = lstParem;
                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    //File Upload
                    FileUpload(ImgPath);

                    //Clear 
                    Clear();

                    //Save Messege
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrSaveMessege);
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not save", "!Error", _messegeType.danger, _alartTopIndex.Topindex, ref ltrSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "!Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltrSaveMessege);
            }
        }
        private void Clear()
        {
            txtAlterNateText.Text = string.Empty;
            txtToolTip.Text = string.Empty;
            mUpdateTime = false;
        }
        private void FileUpload(string path)
        {
            if (!path.ISNullOrWhiteSpace())
            {
                // Notify the user that the file name was changed.
                FileUpload1.SaveAs(Server.MapPath(path));
                //Image Show
            }
        }


        #region <------------View-------->
        private void FillGried()
        {
            if (gvDetails.DataSource != null)
            {
                gvDetails.DataSource = null;
            }

            if (ddlGroupNames.SelectedIndex >= 0)
            {
                var dt = GalleryUtils.GetGalleryByGroup(ddlGroupNames.SelectedItem.Text);

                if (dt.IsValidList())
                {
                    gvDetails.DataSource = dt;
                    gvDetails.DataBind();
                    //This replaces <td> with <th> and adds the scope attribute
                    gvDetails.UseAccessibleHeader = true;

                    //This will add the <thead> and <tbody> elements
                    gvDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                else
                {
                    gvDetails.DataSource = null;
                    gvDetails.DataBind();
                }
            }

            // UpdatePanelGallery.Update();
        }

        private void GetDetails(string Id)
        {
            DataTable dt = GalleryUtils.GetGallery(Id);
            if (dt.IsValidDataTable())
            {
                try
                {
                    txtAlterNateText.Text = Convert.ToString(dt.Rows[0]["Gallery_AlternateText"]);
                    txtContent.Text = Convert.ToString(dt.Rows[0]["Gallery_Contents"]);
                    txtSubject.Text = Convert.ToString(dt.Rows[0]["Gallery_Subjects"]);
                    txtToolTip.Text = Convert.ToString(dt.Rows[0]["Gallery_ToolTip"]);

                    //Select Index
                    string vlu = Convert.ToString(dt.Rows[0]["Thumbnail_GroupName"]);
                    cmbThambnailGroup.ClearSelection(); //making sure the previous selection has been cleared
                    cmbThambnailGroup.SelectedIndex = cmbThambnailGroup.Items.IndexOf(cmbThambnailGroup.Items.FindByText(vlu));
                }
                catch (Exception)
                {

                }
            }

        }


        #endregion

        #region <----------Update-------->

        protected void gvDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                string vlu = e.CommandArgument.ToString();
                _GalleryId = vlu;

                //old Image Path
                string path = GalleryUtils.GetImagePath(vlu);
                if (GalleryUtils.DeleteAbouts(vlu))
                {

                    if (!path.ISNullOrWhiteSpace())
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }

                    Show("Delete Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrSaveMessege);
                    //Refresh Gried
                    FillGried();
                }
            }
            else if (e.CommandName == "EditRow")
            {
                string vlu = e.CommandArgument.ToString();
                _GalleryId = vlu;
                if (!vlu.ISNullOrWhiteSpace())
                {
                    GetDetails(vlu);
                    mUpdateTime = true;
                }
                else
                {
                    Show("Select Valid Row", "!Warning", _messegeType.warning, _alartTopIndex.Topindex, ref ltrSaveMessege);
                }
            }
        }

        private void UpdatetData(string id)
        {
            if (Page.IsValid)
            {
                #region <----------Var----------->
                string subject = txtSubject.Text;
                string content = txtContent.Text;
                string AlternateText = txtAlterNateText.Text;
                string ToolTip = txtToolTip.Text;
                string GalleryThumbnailId = cmbThambnailGroup.SelectedValue;
                bool Activity = true;

                //new Image path
                string _Path = FileUpload1.GenerateFileUploadPath(_SavePath);
                string ImgPath = _Path.ISNullOrWhiteSpace() ? string.Empty : _Path;

                List<SqlParameter> lstParem = new List<SqlParameter>();

                //.......old path for Image Delete......
                string Oldpath = GalleryUtils.GetImagePath(id);
                #endregion

                //Query
                string qry = GalleryUtils.UpdateGalleryQry(id, Activity, ToolTip, AlternateText,
                    DBNull.Value, content, GalleryThumbnailId, subject, out lstParem);

                SqlHelper.MParameterList = lstParem;
                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    //If Image path is available then
                    if (!ImgPath.ISNullOrWhiteSpace())
                    {
                        try
                        {
                            if (!Oldpath.ISNullOrWhiteSpace())
                            {
                                System.IO.File.Delete(Server.MapPath(Oldpath));
                            }
                        }
                        catch (Exception)
                        {
                        }

                        //.....Image Update.......
                        FileUpload(ImgPath);
                        //Db Update
                        GalleryUtils.UpdateGalleryImgPath(id, ImgPath);
                    }
                    else
                    {
                    }

                    //Clear
                    Clear();

                    //Messege
                    Show("Data Save Successfull", "!Success", _messegeType.info, _alartTopIndex.Simple, ref ltrSaveMessege);

                    Response.Write("<script>alert('Data Save SuccessFull');</script>");

                    //Refresh Gried
                    FillGried();
                }
                else
                {
                    Show("Data are not save", "!Error", _messegeType.danger, _alartTopIndex.Topindex, ref ltrSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "!Warning", _messegeType.warning, _alartTopIndex.Simple, ref ltrSaveMessege);
            }
        }

        #endregion

        #region <---------Event---------->
        protected void validatorCheckFileSize_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileUpload1.FileContent.Length;
            if (filesize > _lMaxFileSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            btnSave.Enabled = false;
            btnSave.Text = "Please Wait.......";
            if (!mUpdateTime)
            {
                if (FileUpload1.HasFile)
                {
                    InsertData();
                }
                else
                {
                    Show("Image are Require", "!Warning", _messegeType.warning, _alartTopIndex.Simple, ref ltrSaveMessege);
                }
            }
            else
            {
                UpdatetData(_GalleryId);
            }
            this.FillGried();
            btnSave.Enabled = true;
            btnSave.Text = "Save";

        }

        protected void ddlGroupNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FillGried();
        }

        protected void btnInsetAndUpdateMode_Click(object sender, EventArgs e)
        {
            Clear();
        }

        #endregion
    }
}