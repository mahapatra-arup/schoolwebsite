﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin
{
    public partial class UserProfile : System.Web.UI.Page
    {
        #region <-------------Var------------->
        double _lMaxFileSize = 204800;
        string _fileName = string.Empty;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillDetails();
            }
        }

        private void FillDetails()
        {
            //UserName
            string username = HttpContext.Current.User.Identity.Name;
            var User = UserUtils.GetUserDetails(username);
            if (User.ISValidObject())
            {
                lblUserName.InnerText = User.user_Name;
                txtFirstName.Text = User.user_FirstName;
                txtLastName.Text = User.User_LastName;
                txtDisplayName.Text = User.display_name;
                txtPostalCode.Text = User.User_PostalCode.ConvertObjectToString();
                txtUserAbout.Text = User.User_Abouts;
                txtMail.Text = User.user_email;

                if (User.User_Image != null && User.User_Image.Length > 0)
                {
                    Image img1 = System_Web_UI_WebControls_Imager.byteArrayToWebImage(User.User_Image);
                    ImageOfUser.ImageUrl = img1.ImageUrl;
                }
            }
        }

        private void UpdateUserData()
        {
            var User = UserUtils.GetUserDetails(lblUserName.InnerHtml);
            if (Page.IsValid)
            {
                SC_Users _SC_Users = User;
                _SC_Users.Id = UserUtils.GetUserDetails(lblUserName.InnerText).Id;
                _SC_Users.user_Name = lblUserName.InnerText;
                _SC_Users.user_FirstName = txtFirstName.Text;
                _SC_Users.User_PostalCode = txtPostalCode.Text.ConvertObjectToInt();
                _SC_Users.User_Abouts = txtUserAbout.Text;
                _SC_Users.display_name = txtDisplayName.Text;
                _SC_Users.User_LastName = txtLastName.Text;
                _SC_Users.User_Address = txtUserAddress.Text;
                _SC_Users.user_email = txtMail.Text;
                _SC_Users.Activity = true;
                _SC_Users.User_Country = "India";

                #region <----Image---->
                byte[] User_Image = null;
                //read from File upload control
                if (FileUploadImage.HasFile)
                {
                    using (Stream fs = FileUploadImage.PostedFile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            User_Image = br.ReadBytes((Int32)fs.Length);
                        }
                    }
                }
                else
                {
                    //read from image control
                    if (ImageOfUser.ImageUrl != null)
                    {
                        User_Image = System_Web_UI_WebControls_Imager.ImageControlToByte(ref ImageOfUser);
                    }
                }

                #endregion

                //Image Fill
                if (User_Image != null && User_Image.Length > 0)
                {
                    _SC_Users.User_Image = User_Image;
                }

                //Execute
                var issucess = UserUtils.UpdateUserDetails(_SC_Users);
                if (issucess)
                {
                    FillDetails();

                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrSaveMessege);
                    //Java script
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltrSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltrSaveMessege);
            }
        }

        #region <--------------Event------------>
        protected void cvNewPws_ServerValidate(object source, ServerValidateEventArgs args)
        {

        }

        protected void cvFilesizeCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileUploadImage.FileContent.Length;
            if (filesize > _lMaxFileSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            UpdateUserData();
        }
        protected void btnImageRefresh_Click(object sender, EventArgs e)
        {
            if (FileUploadImage.HasFile)
            {
                System.IO.Stream fs = FileUploadImage.PostedFile.InputStream;
                System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                ImageOfUser.ImageUrl = "data:image/png;base64," + base64String;
            }
            //else
            //{
            //    if (UserUtils.GUserImage != null && UserUtils.GUserImage.Length > 0)
            //    {
            //        Image img1 = System_Web_UI_WebControls_Imager.byteArrayToWebImage(UserUtils.GUserImage);
            //        ImageOfUser.ImageUrl = img1.ImageUrl;
            //    }
            //}
        }
        #endregion

        #region <-------Changing password------->
        private void UpdateUserPasswordData()
        {
            var db = SqlHelper.DbContext().SC_Users.FirstOrDefault();

            if (db.ISValidObject())
            {
                //User
                string user_Name = db.user_Name;
                long UserId = UserUtils.GetUserDetails(user_Name).Id;

                #region <--Current Password-->
                //Current (Old Password)
                string _Cuerrentpwd = txtCurrentPassword.Text;
                //Encode
                string _EncodeCuerrentpwd = HashHelper.EncodePassword(_Cuerrentpwd, db.SALT.ConvertObjectToString());
                #endregion

                #region <-----New Password--->
                //Generate Salt
                var GenerateSaltKey = HashHelper.GenerateRandomSalt(17);

                //New
                string Newpwd = txtNewPassword.Text;
                //Encode
                string _EncodeNewpwd = HashHelper.EncodePassword(Newpwd, GenerateSaltKey);
                #endregion

                //Check
                if (_EncodeCuerrentpwd != db.user_passWord)
                {
                    Show("Old Password are not currect", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltrSaveMessege);
                    return;
                }


                //Update Data
                SC_Users _SC_Users = new SC_Users
                {
                    Id = UserId,
                    user_Name = user_Name,
                    user_passWord = _EncodeNewpwd,
                    SALT = GenerateSaltKey,
                    ModifyDate = DateTime.Now,
                    Activity = true
                };

                //Query
                bool isSuccess = UserUtils.UpdateUserPassword(_SC_Users);

                if (isSuccess)
                {
                    Show("Password Change Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrSaveMessege);

                    //Java script
                    Response.Write("<script>alert('Password Change Successfull');</script>");

                    //#region <-----LogOut---->
                    ////Log Out
                    //Session.Clear();
                    //Session.Abandon();
                    //Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                    //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //Response.Cache.SetNoStore();

                    //try
                    //{
                    //    Session.Abandon();
                    //    FormsAuthentication.SignOut();
                    //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //    Response.Buffer = true;
                    //    Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                    //    Response.Expires = -1000;
                    //    Response.CacheControl = "no-cache";
                    //    //Response.Redirect("login.aspx", true);
                    //}
                    //catch (Exception ex)
                    //{
                    //    Response.Write(ex.Message);
                    //}
                    ////Redirect
                    //Response.Redirect("Login.aspx"); 
                    //#endregion
                }
                else
                {
                    Show("Password Change Not Successfull", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltrSaveMessege);
                }
            }
            else
            {
                Show("Some Problems, User Details Are not correct", "Invalid!", _messegeType.warning, _alartTopIndex.Simple, ref ltrSaveMessege);
            }

        }
        protected void btnChangePasswordPushButton_Click(object sender, EventArgs e)
        {
            UpdateUserPasswordData();
        }

        protected void ChangePassword1_ChangingPassword(object sender, LoginCancelEventArgs e)
        {

        }
        #endregion

    }
}