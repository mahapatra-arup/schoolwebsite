﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="SchoolProfiles.aspx.cs" Inherits="SchoolWebApplication.Admin.SchoolProfiles" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%---------//Message ---------%>
    <asp:Literal ID="ltrSaveMessege" runat="server"></asp:Literal>
    <%---------//Message ---------%>

    <asp:Panel ID="PanelSchool" runat="server">

        <div class="panel panel-primary">
            <div class="panel-heading"><span class="fa fa-home"></span>School Profiles</div>
            <div class="panel-body">
                <div class="container-fluid">

                    <div class="row">

                        <div class="col-sm-12">
                            <%--Group--%>
                            <div class="form-group">

                                <%--//----------------------- School Name-------------------------%>
                                <label class="label label-info" for="txtSchoolName">School Name : </label>
                                <asp:TextBox ID="txtSchoolName" class="form-control" runat="server" Placeholder="Enter School Name (limit of 200 characters)" MaxLength="200"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="School Name is requir" ControlToValidate="txtSchoolName" Display="Dynamic" SetFocusOnError="True" ValidationGroup="InsertSave"></asp:RequiredFieldValidator>
                                <br />

                                <%--//----------------------- AT-------------------------%>
                                <label class="label label-info" for="txtAt">AT : </label>
                                <asp:TextBox ID="txtAt" class="form-control" runat="server" Placeholder="Enter AT (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />

                                <%--//----------------------- po-------------------------%>
                                <label class="label label-info" for="txtPo">PO : </label>
                                <asp:TextBox ID="txtPo" class="form-control" runat="server" Placeholder="Enter PO (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <%-- First column --%>
                        <div class="col-sm-4">

                            <%--Group--%>
                            <div class="form-group">

                                <%--//----------------------- Block-------------------------%>
                                <label class="label label-info" for="txtBlock">Block : </label>
                                <asp:TextBox ID="txtBlock" class="form-control" runat="server" Placeholder="Enter Block (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />

                                <%--//----------------------- Sub Division-------------------------%>
                                <label class="label label-info" for="txtSubDivision">Sub Division : </label>
                                <asp:TextBox ID="txtSubDivision" class="form-control" runat="server" Placeholder="Enter Sub Division (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />

                                <%--//----------------------- PS-------------------------%>
                                <label class="label label-info" for="txtPS">PS : </label>
                                <asp:TextBox ID="txtPS" class="form-control" runat="server" Placeholder="Enter PS (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />

                                <%--//----------------------- District-------------------------%>
                                <label class="label label-info" for="txtDist">District : </label>
                                <asp:TextBox ID="txtDist" class="form-control" runat="server" Placeholder="Enter District (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />

                                <%--//----------------------- DistrictCOde-------------------------%>
                                <label class="label label-info" for="txtDistCode">District Code : </label>
                                <asp:TextBox ID="txtDistCode" class="form-control" runat="server" Placeholder="Enter District Code (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />

                            </div>

                        </div>
                        <%-- //First column --%>

                        <%-- Second column --%>
                        <div class="col-sm-4">
                            <div class="form-group">

                                <%--//----------------------- GP-------------------------%>
                                <label class="label label-info" for="txtGp">GP : </label>
                                <asp:TextBox ID="txtGp" class="form-control" runat="server" Placeholder="Enter GP (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />


                                <%--//----------------------- Pin-------------------------%>
                                <label class="label label-info" for="txtPin">Pin : </label>
                                <asp:TextBox ID="txtPin" class="form-control" runat="server" Placeholder="Enter Pin (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <%-- Expression --%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPostalCode" runat="server" ErrorMessage="Invalid Postal Code" ControlToValidate="txtPin" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                                    ValidationExpression="^[1-9][0-9]{5}$" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>
                                <br />



                                <%--//----------------------- PH-------------------------%>
                                <label class="label label-info" for="txtPh">PH : </label>
                                <asp:TextBox ID="txtPh" class="form-control" runat="server" Placeholder="Enter PH (limit of 100 characters)" MaxLength="100" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                <%-- Expression --%>
                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhNo" runat="server" ErrorMessage="Invalid PhNo" ControlToValidate="txtPh" ForeColor="#FF6600" SetFocusOnError="True"
                            ValidationExpression="^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}|(\d[ -]?){10}\d$" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>--%>

                                <br />


                                <%--//----------------------- Email-------------------------%>
                                <label class="label label-info" for="txtEmail">Email Address : </label>
                                <asp:TextBox ID="txtEmail" class="form-control" runat="server" Placeholder="Enter Email Address (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <%-- Expression --%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorMail" runat="server" ErrorMessage="Invalid Email" ControlToValidate="txtEmail" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>
                                <br />

                                <%--//----------------------- Website-------------------------%>
                                <label class="label label-info" for="txtWebsite">Website : </label>
                                <asp:TextBox ID="txtWebsite" class="form-control" runat="server" Placeholder="Enter Website (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <%-- Expression --%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid Website" ControlToValidate="txtWebsite" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                                    ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>
                                <br />
                            </div>
                        </div>
                        <%--// Second column --%>

                        <%-- Third column --%>
                        <div class="col-sm-4" style="background-color: white;">

                            <div class="form-group">

                                <%--//----------------------- Institution Code-------------------------%>
                                <label class="label label-info" for="txtInstitutionCode">Institution Code : </label>
                                <asp:TextBox ID="txtInstitutionCode" class="form-control" runat="server" Placeholder="Enter Institution Code (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />

                                <%--//----------------------- Dise Code-------------------------%>
                                <label class="label label-info" for="txtDiseCode">Dise Code : </label>
                                <asp:TextBox ID="txtDiseCode" class="form-control" runat="server" Placeholder="Enter Dise Code (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />

                                <%--//----------------------- ESTD-------------------------%>
                                <label class="label label-info" for="txtESTD">ESTD : </label>
                                <asp:TextBox ID="txtESTD" class="form-control" runat="server" Placeholder="Enter ESTD (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                <br />


                                <%--//----------------------- Google map url-------------------------%>
                                <label class="label label-info" for="txtMapUrl">Google map url : </label>
                                <asp:TextBox ID="txtMapUrl" CssClass="form-control" runat="server" Placeholder="Enter Google map url (limit of 500 characters)" MaxLength="500" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                <br />

                            </div>
                        </div>
                        <%-- //Third column --%>
                    </div>

                    <div class="row">


                        <%-- Logo :: :Left--%>
                        <div class="col-md-6">
                            <div class="panel panel-info  text-center">
                                <div class="panel-heading"><span class="fa fa-image"></span>&nbsp;Logo</div>
                                <div class="panel-body text-capitalize">
                                    <div class="form-group">
                                        <%-- Image--%>
                                        <label class="label label-warning" for="imgUserImage">
                                            School Logo : >>  100*100 - 150*150
                                        </label>
                                        <br />
                                        <asp:LinkButton ID="hlLogoRefresh" ToolTip="Refresh Image" runat="server" OnClick="hlLogoRefresh_Click" BorderStyle="None">
                                            <asp:Image ID="imgLogo" runat="server" class="img-responsive" />
                                            <i class="fa fa-refresh fa-spin" style="font-size: 30px; color: red"></i>
                                        </asp:LinkButton>

                                        <asp:FileUpload ID="fuLogo" runat="server" class="form-control btn-file" />

                                        <%-- Reular expression --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                            ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
                                            ControlToValidate="fuLogo" SetFocusOnError="True" ForeColor="Red" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                                        <%-- File Size Check --%>
                                        <asp:CustomValidator ID="cvLogosizeCheck" ValidationGroup="InsertSave" runat="server" ErrorMessage="" OnServerValidate="cvLogosizeCheck_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="fuLogo"></asp:CustomValidator>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <%-- Logo 1 :: Right--%>
                        <div class="col-md-6">
                            <div class="panel panel-info  text-center">
                                <div class="panel-heading"><span class="fa fa-image"></span>&nbsp;Logo-I</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <%-- Image--%>
                                        <label class="label label-warning" for="imgUserImage">
                                            School Logo 1 : >>  100*100 - 150*150</label>
                                        <br />
                                        <asp:LinkButton ID="hlLogo1Refresh" ToolTip="Refresh Image" runat="server" OnClick="hlLogo1Refresh_Click" BorderStyle="None">
                                            <asp:Image ID="imgLogo1" runat="server" class="img-responsive" />
                                            <i class="fa fa-refresh fa-spin" style="font-size: 30px; color: red"></i>
                                        </asp:LinkButton>

                                        <asp:FileUpload ID="fuLogo1" runat="server" class="form-control btn-file" />

                                        <%-- Reular expression --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic"
                                            ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only jpg or jpeg or png Files are allowed"
                                            ControlToValidate="fuLogo1" SetFocusOnError="True" ForeColor="Red" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                                        <%-- File Size Check --%>
                                        <asp:CustomValidator ID="cvLogo1SizeCheck" ValidationGroup="InsertSave" runat="server" ErrorMessage="" OnServerValidate="cvLogo1SizeCheck_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="fuLogo1"></asp:CustomValidator>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <%-- Banner--%>
                        <div class="col-md-12">
                            <div class="panel panel-info  text-center">
                                <div class="panel-heading"><span class="fa fa-image"></span>&nbsp;Banner</div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <%-- Image--%>
                                        <label class="label label-warning" for="imgUserImage">
                                            >> Banner -: 1920*200 - 2560*300</label>
                                        <br />
                                        <asp:LinkButton ID="hlLogo2Refresh" ToolTip="Refresh Image" runat="server" OnClick="hlLogo2Refresh_Click" BorderStyle="None">
                                            <asp:Image ID="imgLogo2" runat="server" class="img-responsive" />
                                            <i class="fa fa-refresh fa-spin" style="font-size: 30px; color: red"></i>
                                        </asp:LinkButton>

                                        <asp:FileUpload ID="fuLogo2" runat="server" class="form-control btn-file" />

                                        <%-- Reular expression --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic"
                                            ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
                                            ControlToValidate="fuLogo2" SetFocusOnError="True" ForeColor="Red" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                                        <%-- File Size Check --%>
                                        <asp:CustomValidator ID="cvLogo2SizeCheck" ValidationGroup="InsertSave" runat="server" ErrorMessage="" OnServerValidate="cvLogo2SizeCheck_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="fuLogo2"></asp:CustomValidator>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%-- Save--%>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <%--// Button--%>
                            <asp:Button ID="btnSave" align="right" CssClass="w3-button w3-border w3-border-red w3-round-large" runat="server" Text="Save" ValidationGroup="InsertSave" OnClick="btnSave_Click"></asp:Button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </asp:Panel>

    <%--File uploaded Input Image Show (Jquery) =================--%>
    <script>
        function FileUploadImageShow(input, img_ClientId) {
            var imgv = document.getElementById(img_ClientId);
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(imgv)
                        .attr('src', e.target.result);
                        //.width(100)
                        //.height(100);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        //Jquery
        $("document").ready(function () {

            //Logo -
            $("#<%=fuLogo.ClientID %>").change(function () {
                var img = "<%=imgLogo.ClientID %>";
               FileUploadImageShow(this,img)
            });

            //Logo - I
            $("#<%=fuLogo1.ClientID %>").change(function () {
                var img = "<%=imgLogo1.ClientID %>";
               FileUploadImageShow(this,img)
            });

             //Banner -
            $("#<%=fuLogo2.ClientID %>").change(function () {
                var img = "<%=imgLogo2.ClientID %>";
               FileUploadImageShow(this,img)
            });
        });
    </script>
    <%--File uploaded Input Image Show (Jquery) =================--%>
</asp:Content>
