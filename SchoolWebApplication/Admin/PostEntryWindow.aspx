﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="PostEntryWindow.aspx.cs" Inherits="SchoolWebApplication.Admin.PostEntryWindow" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <%----------===Message==== --------%>
    <asp:Literal ID="ltrSaveMessege" runat="server"></asp:Literal>
    <%----------===//Message==== ------%>

    <ajaxToolkit:TabContainer ID="TabContainer2" runat="server"></ajaxToolkit:TabContainer>    
    <div class="container-fluid">
        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Height="100%" Width="100%">

            <%-----=====================View Panel=================------%>

            <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3">
                <HeaderTemplate>
                    Post / Events
                </HeaderTemplate>

                <ContentTemplate>
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="background: linear-gradient(#50d4eb,#10c6c6)">View ::</div>
                        <div class="panel-body">

                            <label class="label label-info" for="ddlPostType">
                                Post Type :
                            </label>
                            <asp:DropDownList ID="ddlPostTypes002" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPostTypes002_SelectedIndexChanged">
                               <asp:ListItem Selected="True" Value="--Select--"> --Select-- </asp:ListItem>
                                <asp:ListItem  Value="Post"> Post </asp:ListItem>
                                <asp:ListItem Value="Event"> Event </asp:ListItem>
                            </asp:DropDownList>
                            <br />
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <%--//REF :https://www.youtube.com/watch?v=vuoJeQ4L3WI--%>
                                        <asp:GridView ID="gvPostView" runat="server" AutoGenerateColumns="False" 
                                            DataKeyNames="Id" CssClass="table table-striped table-bordered"  OnRowCommand="gvPostView_RowCommand" >


                                            <%-- ==Columns== --%>
                                            <Columns>

                                                <%-- ====== Id=== --%>
                                                <asp:TemplateField HeaderText="Id" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%#Eval("Id") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- ====== post_title=== --%>
                                                <asp:TemplateField HeaderText="Post Title">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%#Eval("post_title") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- ====== Post_name=== --%>
                                                <asp:TemplateField HeaderText="Post Name">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%#Eval("Post_name") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- ====== post_content=== --%>
                                                <asp:TemplateField HeaderText="Post Content">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%#Eval("post_content") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                  <%-- ====== Date=== --%>
                                                <asp:TemplateField HeaderText="Date">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%# "Publish Date : "+
                                                               (Eval("Publish_Date")!=DBNull.Value? System.Convert.ToDateTime(Eval(" Publish_Date")).ToString("dd-MMM-yyyy") :"-" ) %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- ====== Activity=== --%>
                                                <asp:TemplateField HeaderText="Post Author">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%#Eval(" post_author") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- ==post File View== --%>
                                                <asp:TemplateField HeaderText="View">
                                                    <ItemTemplate>
                                                        <%#string.Format("<a href='#' onClick='window.open(\"{0}\", \"_blank\")'> <span class='fa fa-eye' title='View File'></span></a>",Convert.ToString(Eval(" Post_ImgPath"))) %>
                                                        <br />
                                                        <%# Convert.ToString(Eval(" Post_ImgPath"))!=string.Empty?"<span class=\"w3-text-blue\">Available</span>":"<span class=\"w3-text-red\">Not Available</span>"%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--= ==Column Edit and Delete=== --%>
                                                <asp:TemplateField HeaderText="Image">

                                                    <ItemTemplate>
                                                        <%--<asp:ImageButton ImageUrl="~/Admin/images/Alter.png" ID="imgAlter" CommandName="EditRow" ToolTip="Edit" CommandArgument='<%# Eval("Id") %>' runat="server"></asp:ImageButton>--%>

                                                        <asp:ImageButton ImageUrl="~/Admin/images/Delete.png" ID="imgDelete" CommandName="DeleteRow" ToolTip="Delete" CommandArgument='<%# Eval("Id") %>' runat="server"
                                                            OnClientClick="return confirm('Are you sure you want to delete this event?');"></asp:ImageButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <%--= ==//Column Edit and Delete=== --%>
                                            </Columns>

                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                          
                        </div>
                    </div>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>


            <%-- ====================Entry form panel=====================--%>
            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                <HeaderTemplate>
                    Entry Form
                </HeaderTemplate>

                <ContentTemplate>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label label-info" for="txtTitle">
                                        Title :
                                    </label>
                                    <asp:TextBox ID="txtTitle" runat="server" class="form-control" MaxLength="100" Placeholder="**Enter Title (limit of 100 characters)" ToolTip="Title of Post"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvTitle" runat="server" ErrorMessage="Title is requir" ForeColor="Red" ControlToValidate="txtTitle" Display="Dynamic" ValidationGroup="InsertSave"></asp:RequiredFieldValidator>
                                    <br />
                                    <label class="label label-info" for="txtPostName">
                                        Post Name :
                                    </label>
                                    <asp:TextBox ID="txtPostName" runat="server" class="form-control" MaxLength="100" Placeholder="**Enter Post Name (limit of 100 characters)"></asp:TextBox>
                                    <br />
                                    <label class="label label-info" for="txtPostContent">
                                        Post Content :
                                    </label>
                                    <asp:TextBox ID="txtPostContent" class="form-control" runat="server" placeholder="Enter Content (limit of 4000 characters)" Rows="6" aria-valuemax="4000" TextMode="MultiLine"></asp:TextBox>

                                    <%-- Editing View ref: https://www.aspsnippets.com/Articles/ASPNet-AJAX-Control-Toolkit-HtmlEditorExtender-Example.aspx --%>
                                    <ajaxToolkit:HtmlEditorExtender ID="AjaxTextEditor" runat="server" TargetControlID="txtPostContent" EnableSanitization="false"
                                        DisplayPreviewTab="True" DisplaySourceTab="True">
                                    </ajaxToolkit:HtmlEditorExtender>

                                    <br />
                                    <label class="label label-info" for="ddlPostType">
                                        Post Type :
                                    </label>
                                    <asp:DropDownList ID="ddlPostType" runat="server" class="form-control">
                                        <asp:ListItem Selected="True" Value="Post"> Post </asp:ListItem>
                                        <asp:ListItem Value="Event"> Event </asp:ListItem>

                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="label label-info" for="txtPostAuther">
                                        Auther :
                                    </label>
                                    <asp:TextBox ID="txtPostAuther" runat="server" class="form-control" MaxLength="100" Placeholder="Enter Auther (limit of 100 characters)"></asp:TextBox>
                                    <br />
                                    <label class="label label-info" for="txtPublishDate">
                                        Publish Date :
                                    </label>
                                    <asp:TextBox ID="txtPublishDate" runat="server" class="form-control" TextMode="Date"></asp:TextBox>

                                    <br />
                                    <label class="label label-info" for="txtPostImage">
                                        Image :
                                    </label>
                                    <asp:FileUpload ID="ImageFileUpload" CssClass="form-control" runat="server" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                        ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
                                        ControlToValidate="ImageFileUpload" SetFocusOnError="True" ForeColor="Red" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                                    <asp:CustomValidator ID="cvFilesizeCheck" ValidationGroup="InsertSave" runat="server" ErrorMessage="Image size cannot be greater than 200 KB " OnServerValidate="cvFilesizeCheck_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="ImageFileUpload"></asp:CustomValidator>
                                    <br />

                                    <label class="label label-info" for="txtPostTag">
                                        Post Tag :
                                    </label>
                                    <asp:TextBox ID="txtPostTag" runat="server" class="form-control" MaxLength="200" Placeholder="Enter Tages (limit of 200 characters)"></asp:TextBox>
                                    <br />
                                </div>
                            </div>


                        </div>

                        <div class="form-group">
                            <asp:Button ID="btnSave" Style="float: right" class="w3-button w3-white w3-border w3-border-red w3-round-large" runat="server" Text="Save"  ValidationGroup="InsertSave" OnClick="btnSave_Click"></asp:Button>
                        </div>
                        <br />
                    </div>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%-- ====================//Entry form panel=====================--%>

        </ajaxToolkit:TabContainer>
    </div>


     <%-- For Datatable --%>
    <script type="text/javascript">  
        //Call
        $(function () {
            CreateDataTable();
            UpdatePanelRefresh();
        });

        //--For Datatable-- %>
        function CreateDataTable() {
            $(function () {
                $('#<%=gvPostView.ClientID%>').DataTable({ "scrollX": true });
           });
        }

        ////On UpdatePanel Refresh.
        function UpdatePanelRefresh() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        CreateDataTable();
                    }
                });
            };
        }
    </script>
</asp:Content>

