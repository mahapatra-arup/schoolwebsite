﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication
{
    public partial class StaffEntryWndow : System.Web.UI.Page
    {

        #region <----------Var---------->
        // double _lMaxFileSize = 204800;
        private static bool mUpdateTime = false;
        string _SavePath = DefultPaths.Staff_Folder_Path;
        #endregion

        #region <--Prevent Page Refresh in C#-->

        //For refresh
        private bool _refreshState;
        private bool _isRefresh;
        protected override void LoadViewState(object savedState)
        {
            object[] AllStates = (object[])savedState;
            base.LoadViewState(AllStates[0]);
            _refreshState = bool.Parse(AllStates[1].ToString());
            _isRefresh = _refreshState == bool.Parse(Session["__ISREFRESH"].ToString());
        }

        protected override object SaveViewState()
        {
            Session["__ISREFRESH"] = _refreshState;
            object[] AllStates = new object[2];
            AllStates[0] = base.SaveViewState();
            AllStates[1] = !(_refreshState);
            return AllStates;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            lblEmpIdExistMessege.Text = "";

            //=====Text Editor Width Set dynamically====
            int width = (Request.Browser.ScreenPixelsWidth) * 2;
            txtOpinion.Width = (width / 2);
            //=====//Text Editor Width Set====

            if (!IsPostBack)
            {
                ddGroup.AddStaffSubGroup();//Add Group
                DefaultSet();

                ltSaveMessege.Text = "";
            }
        }
        private void InsertData()
        {
            if (Page.IsValid)
            {
                #region <------------Var--------->
                string _Path = FileUpload1.GenerateFileUploadPath(_SavePath);

                string EmpName = txtName.Text;
                string EmpCode = txtEmpCode.Text;

                string Dob = txtDOBDate.Text;
                string EducationalQualification = txtEduQualification.Text;
                string Subjects = txtSubject.Text;
                string Address = txtAddress.Text;
                string Description = txtDescription.Text;
                string opinion = txtOpinion.Text;
                string PhNo = txtPhNo.Text;
                string designation = txtdesignation.Text;
                string Email = txtMail.Text;
                string imgpath = string.Empty;
                imgpath = _Path.ISNullOrWhiteSpace() ? string.Empty : _Path;
                string grupId = ddGroup.SelectedItem.Value;

                string SlNo = (StaffUtils.GetMaxMenuSlno() + 1).ToString();
                bool Activity = true;

                string Gender = rbtnGender.SelectedValue;
                List<SqlParameter> lstParem = new List<SqlParameter>();
                #endregion

                //Query
                string qry = StaffUtils.QInsertryStaffDetails(EmpName, EmpCode, Gender, Dob,
                EducationalQualification, Subjects, Address, Description,
                SlNo, Activity, PhNo, designation, Email, grupId, imgpath, opinion, out lstParem);
                SqlHelper.MParameterList = lstParem;

                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    //File Upload
                    FileUpload(imgpath);

                    DefaultSet();

                    //Save Mesesage
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Data Save SuccessFull');", true);
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }
        private void UpdateData()
        {
            if (Page.IsValid)
            {
                #region <--------Var--------->
                string EmpName = txtName.Text;
                string EmpCode = txtEmpCode.Text;

                string Dob = txtDOBDate.Text;
                string EducationalQualification = txtEduQualification.Text;
                string Subjects = txtSubject.Text;
                string Address = txtAddress.Text;
                string Description = txtDescription.Text;
                string PhNo = txtPhNo.Text;
                string opinion = txtOpinion.Text;

                string designation = txtdesignation.Text;
                string Email = txtMail.Text;
                string imgpath = string.Empty;
                //new image path
                string _Path = FileUpload1.GenerateFileUploadPath(_SavePath);
                imgpath = _Path.ISNullOrWhiteSpace() ? string.Empty : _Path;
                string grpId = ddGroup.SelectedValue;
                bool Activity = true;
                string Gender = rbtnGender.SelectedValue;

                //.......old Image path......
                string path = StaffUtils.GetImagePath(EmpCode);
                List<SqlParameter> lstParem = new List<SqlParameter>();
                #endregion

                //Query
                string qry = StaffUtils.QUpdateStaffDetails(EmpName, EmpCode, Gender, Dob,
                EducationalQualification, Subjects, Address, Description,
                Activity, PhNo, designation, Email, grpId, opinion, out lstParem);
                SqlHelper.MParameterList = lstParem;

                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    if (!imgpath.ISNullOrWhiteSpace())
                    {
                        try
                        {
                            //Old File delete
                            if (!path.ISNullOrWhiteSpace())
                            {
                                System.IO.File.Delete(Server.MapPath(path));
                            }
                        }
                        catch (Exception)
                        {
                        }

                        //.....Update.......
                        FileUpload(imgpath);
                        StaffUtils.UpdateStaffImgPath(EmpCode, imgpath);
                    }
                    else
                    {
                    }

                    DefaultSet();

                    //Mesege
                    Show("Data Update Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    Response.Write("<script>alert('Data Update SuccessFull');</script>");

                }
                else
                {
                    Show("Data are not Update", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not Update because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }

        private void DefaultSet()
        {
            Clear();
            FillGried();
        }

        private void Clear()
        {
            txtEmpCode.Text = string.Empty;
            txtName.Text = string.Empty;
            txtPhNo.Text = string.Empty;
            txtdesignation.Text = string.Empty;
            txtEduQualification.Text = string.Empty;
            txtSubject.Text = string.Empty;
            txtDescription.Text = string.Empty;
            txtDOBDate.Text = string.Empty;
            txtOpinion.Text = string.Empty;
            txtMail.Text = string.Empty;
            txtAddress.Text = string.Empty;
            ImageOfUser.ImageUrl = "";
            ddGroup.ClearSelection();
            //Current Date Set
            txtDOBDate.SetCurrentDateInTextBox();

            mUpdateTime = false;
            //UpdateData 
            UpdatePnllUpdateForm.Update();
        }

        private bool IsvalidEmpCodeCheck()
        {
            //If Exist then Same valuecompare
            if (StaffUtils.IsExistEmployee(txtEmpCode.Text))
            {
                lblEmpIdExistMessege.Text = "!!Employee Id already exists";
                btnSave.Enabled = false;
                return true;
            }
            else
            {
                lblEmpIdExistMessege.Text = "";
                btnSave.Enabled = true;
                //Compare null
            }
            return false; ;
        }

        #region <---------file--------->
        private void FileUpload(string path)
        {
            try
            {
                if (!path.ISNullOrWhiteSpace())
                {
                    //Access the File using the Name of HTML INPUT File.
                    FileUpload1.PostedFile.SaveAs(Server.MapPath(path));
                }
            }
            catch (Exception)
            {

            }
        }

        #endregion

        #region <........Event.........>

        protected void txtEmpCode_TextChanged(object sender, EventArgs e)
        {
            IsvalidEmpCodeCheck();
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!_isRefresh) // check that page is not refreshed by browser.
            {
                btnSave.Enabled = false;
                btnSave.Text = "Please Wait.......";

                if (!mUpdateTime)
                {
                    if (!IsvalidEmpCodeCheck())
                    {
                        InsertData();
                    }
                }
                else
                {
                    UpdateData();
                }

                btnSave.Enabled = true;
                btnSave.Text = "Save";
            }

        }
        #endregion

        #region <----------View-------->
        private void FillGried()
        {
            var dt = StaffUtils.GetStaffDetails();

            gvStaffView.DataSource = dt;
            gvStaffView.DataBind();
            if (dt.IsValidList())
            {
                gvStaffView.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        private void GetDetails(string EmpId)
        {
            txtEmpCode.TextChanged -= txtEmpCode_TextChanged;

            var dt = StaffUtils.GetStaffDetails(EmpId);
            if (dt.ISValidObject())
            {
                txtEmpCode.Text = dt.EmpCode;
                txtName.Text = dt.EmpName;
                txtPhNo.Text = dt.PhNo;
                txtdesignation.Text = dt.designation;
                txtEduQualification.Text = dt.EducationalQualification;
                txtSubject.Text = dt.Subjects;
                txtDescription.Text = dt.Description;

                txtMail.Text = dt.Email;
                txtAddress.Text = dt.Address;
                txtOpinion.Text = dt.Opinion;

                //**Date Time must be set formate of yyyy-MM-dd**
                DateTime dttime = dt.Dob;
                txtDOBDate.Text = dttime.Date.ToString("yyyy-MM-dd");

                string imgpath = dt.ImagePath;

                if (!imgpath.ISNullOrWhiteSpace())
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ImageOfUser1", "ImageSet1();", true);
                }
                ImageOfUser.ImageUrl = imgpath;

                //Grou set use grouyp id
                //Select Index
                string vlu = dt.SubGroupId.ConvertObjectToString();
                ddGroup.ClearSelection(); //making sure the previous selection has been cleared
                ddGroup.SelectedIndex = ddGroup.Items.IndexOf(ddGroup.Items.FindByValue(vlu));
            }
            //Only update time
            UpdatePnllUpdateForm.Update();
            txtEmpCode.TextChanged += txtEmpCode_TextChanged;
        }


        #endregion

        #region <---------Event-------->
        protected void gvStaffView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                string vlu = e.CommandArgument.ToString();
                //Image path
                string path = StaffUtils.GetImagePath(vlu);
                if (StaffUtils.DeleteStaff(vlu))
                {
                    if (!path.ISNullOrWhiteSpace())
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }

                    Show("Delete Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrGriedMessage);
                    FillGried();
                }
            }
            else if (e.CommandName == "EditRow")
            {
                string vlu = e.CommandArgument.ToString();
                if (!vlu.ISNullOrWhiteSpace())
                {
                    GetDetails(vlu);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scr", "openResultDialog();", true);
                    mUpdateTime = true;
                }
                else
                {
                    Show("Select Valid Row", "Warning!", _messegeType.warning, _alartTopIndex.Topindex, ref ltrGriedMessage);
                }
            }
        }
        protected void btnnavopenclose_Click(object sender, EventArgs e)
        {
            Clear();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scr", "openResultDialog();", true);
        }
        protected void validatorCheckFileSize_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;

            double filesize = FileUpload1.PostedFile.ContentLength;
            if (filesize > 204800)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
        #endregion
    }
}