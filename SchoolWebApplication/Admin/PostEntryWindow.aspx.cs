﻿
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin
{
    public partial class PostEntryWindow : System.Web.UI.Page
    {
        #region <--------------Var-------------->
        enum _typemd
        {
            _event, _post
        }
        string _SavePath = DefultPaths.Post_Folder_Path;
        private static _typemd _mode;
        double _lMaxFileSize = 204800;
        private static string _Noticeid = string.Empty;
        private static bool mUpdateTime = false;
        #endregion

        #region <---Prevent Page Refresh in C#-->

        //For refresh
        private bool _refreshState;
        private bool _isRefresh;
        protected override void LoadViewState(object savedState)
        {
            object[] AllStates = (object[])savedState;
            base.LoadViewState(AllStates[0]);
            _refreshState = bool.Parse(AllStates[1].ToString());
            _isRefresh = _refreshState == bool.Parse(Session["__ISREFRESH"].ToString());
        }

        protected override object SaveViewState()
        {
            Session["__ISREFRESH"] = _refreshState;
            object[] AllStates = new object[2];
            AllStates[0] = base.SaveViewState();
            AllStates[1] = !(_refreshState);
            return AllStates;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Defult Event
                _mode = _typemd._event;
                //Date Time must be set formate of yyyy-MM-dd
                DateTime dttime = DateTime.Now.Date;
                txtPublishDate.Text = dttime.Date.ToString("yyyy-MM-dd");
                FillGried();
            }
        }

        private void InsertData()
        {
            if (Page.IsValid)
            {
                #region <-----------Var--------->
                string _Path = ImageFileUpload.GenerateFileUploadPath(_SavePath);
                string post_author = txtPostAuther.Text;
                string Publish_Date = txtPublishDate.Text;
                object post_dateFrom = DBNull.Value;
                object Post_Date_to = DBNull.Value;
                string post_content = txtPostContent.Text;
                string post_title = txtTitle.Text;
                string Post_Header = txtTitle.Text;//title and header are same
                object post_password = DBNull.Value;
                string post_name = txtPostName.Text;
                string post_content_filtered = txtPostTag.Text;
                object post_parent = DBNull.Value;
                string menu_order = (PostUtils.GetMaxPostSlno() + 1).ToString();
                string post_type = ddlPostType.Text;
                object Post_Link_Id = DBNull.Value;
                string Post_ImgPath = _Path.ISNullOrWhiteSpace() ? string.Empty : _Path;
                bool Activity = true;
                List<SqlParameter> lstParem = new List<SqlParameter>();
                #endregion

                //Query
                string qry = PostUtils.InsertQryPost(post_author, Publish_Date, post_dateFrom,
                     Post_Date_to, post_content, post_title, Post_Header, Activity, post_password, post_name,
                 post_content_filtered, post_parent, menu_order, post_type, Post_ImgPath, Post_Link_Id, out lstParem);
                SqlHelper.MParameterList = lstParem;

                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    //File Upload
                    FileUpload(Post_ImgPath);

                    //Clear
                    Clear();
                    //Refresh Gried
                    FillGried();

                    //Messege
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrSaveMessege);
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltrSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltrSaveMessege);
            }
        }
        private void Clear()
        {
            txtTitle.Text = string.Empty;
            txtPostAuther.Text = string.Empty;

            txtPostContent.Text = string.Empty;
            txtPostTag.Text = string.Empty;
            txtPostName.Text = string.Empty;
            mUpdateTime = false;//importent
        }
        private void FileUpload(string path)
        {
            if (!path.ISNullOrWhiteSpace())
            {
                // Notify the user that the file name was changed.
                ImageFileUpload.SaveAs(Server.MapPath(path));
                //Image Show
            }
        }
        private void FillGried()
        {
            DataTable dt = new DataTable();
            switch (_mode)
            {
                case _typemd._event:
                    //Source
                    dt = PostUtils.GetTop10PostDetails(PostUtils._PostType.Event);
                    break;
                case _typemd._post:
                    //Source
                    dt = PostUtils.GetTop10PostDetails(PostUtils._PostType.Post);
                    break;
            }
            if (dt.IsValidDataTable())
            {
                gvPostView.DataSource = dt;
                gvPostView.DataBind();
                gvPostView.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvPostView.DataSource = null;
                gvPostView.DataBind();
            }
        }

        #region <-----------Event----------->
        protected void gvPostView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                string vlu = e.CommandArgument.ToString();
                //Image path
                string path = PostUtils.GetPostFilePath(vlu);
                if (PostUtils.DeletePost(vlu))
                {
                    if (!path.ISNullOrWhiteSpace())
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }

                    Show("Delete Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrSaveMessege);
                    FillGried();
                }
            }

            else if (e.CommandName == "EditRow")
            {
                //string vlu = e.CommandArgument.ToString();
                //if (!vlu.ISNullOrWhiteSpace())
                //{
                //    GetDetails(vlu);
                //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Navfn0007", "$('.nav-tabs a[href=\"#NoticeEntry\"]').tab('show')", true);

                //    mUpdateTime = true;
                //}
                //else
                //{
                //    Show("Select Valid Row", "Warning!", _messegeType.warning, _alartTopIndex.Topindex, ref ltSaveMessege);
                //}
            }
        }
        protected void ddlPostTypes002_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPostTypes002.SelectedIndex >= 0)
            {
                switch (ddlPostTypes002.SelectedItem.Text.ToLower().Trim())
                {
                    case "post":
                        _mode = _typemd._post;
                        break;
                    case "event":
                        _mode = _typemd._event;
                        break;
                }
                FillGried();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!_isRefresh) // check that page is not refreshed by browser.
            {
                btnSave.Enabled = false;
                btnSave.Text = "Please Wait.......";

                if (!mUpdateTime)
                {
                    InsertData();
                }
                else
                {
                    //UpdateData();
                }

                btnSave.Enabled = true;
                btnSave.Text = "Save";
            }
        }
        protected void cvFilesizeCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = ImageFileUpload.FileContent.Length;
            if (filesize > _lMaxFileSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }


        }
        #endregion
    }
}