﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Linq;

namespace SchoolWebApplication.Admin
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            iframDashboard.Attributes.Add("src", SchoolWebLibrary.WebsiteAnalyticsTools.GetWebsiteAnalyticsPath());
            FillData();
        }

        private void FillData()
        {
            var db = SqlHelper.DbContext();
            lblTotalNotic.Text = db.Notices.Count().ToString();
            lblTotalStaff.Text = db.Staff_Details.Count().ToString();
            lblTotalPost.Text = db.SC_Posts.Count().ToString();
            lblTotalStudentResult.Text = "I-X-->" + db.RStudent_MpResult.Count().ToString()
                + "\n XI-XII-->" + db.RStudent_HSResult.Count().ToString()
            + "\n Total MarkSheet-->" + db.RStudentsMarksheetReports.Count().ToString();
        }
    }
}