﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="SchoolWebApplication.Admin.UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Literal ID="ltrSaveMessege" runat="server"></asp:Literal>

    <asp:Panel ID="pnlUserProfile" CssClass="container-fluid" runat="server">
        <div class="row">

            <!---Left sIDE------>
            <div class="col-md-8 panel panel-primary" style="margin-right: 10px;">

                <div class="panel-heading">Profile</div>
                <div class="panel-body">
                    <div class="form-group">

                        <!--For table bootstrap-->
                        <table class="table">
                            <tr>
                                <%-- ------Left Side------- --%>
                                <td style="width: 60%">
                                    <%--// user name--%>
                                           User Name : 
                                            <label class="label label-success" id="lblUserName" runat="server"></label>

                                    <br />
                                    <br />

                                    <%--// First name--%>
                                    <label class="label label-success" for="txtFirstName">First Name : </label>
                                    <asp:TextBox ID="txtFirstName" class="form-control" runat="server" TextMode="SingleLine" Placeholder="Enter First Name (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                    <br />

                                    <%--//------ Last Name--------%>
                                    <label class="label label-success" for="txtLastName">Last Name : </label>
                                    <asp:TextBox ID="txtLastName" class="form-control" runat="server" TextMode="SingleLine" Placeholder="Enter Last Name (limit of 100 characters)" MaxLength="100"></asp:TextBox>

                                    <br />
                                    <%--//----- Display Name------%>
                                    <label class="label label-success" for="txtDisplayName">Display Name : </label>
                                    <asp:TextBox ID="txtDisplayName" class="form-control" runat="server" TextMode="SingleLine" Placeholder="Enter Display Name (limit of 100 characters)" MaxLength="100"></asp:TextBox>
                                </td>


                                <%------Right SIde---- --%>
                                <td style="text-align: center">
                                    <%-----Image-----%>
                                    <label class="label label-success" for="imgUserImage">Image File : </label>
                                    <div style="background-color: #DDDDCC">

                                        <asp:LinkButton ID="hlRefresh" ToolTip="Refresh Image" runat="server" OnClick="btnImageRefresh_Click" BackColor="#DDDDCC" BorderStyle="None">
                                            <asp:Image ID="ImageOfUser" runat="server" class="img-responsive" Height="178px" Width="184px" />
                                            &nbsp;

                                                  <span class="glyphicon glyphicon-refresh"></span>
                                        </asp:LinkButton>
                                        <asp:FileUpload ID="FileUploadImage" runat="server" class="form-control btn-file" BackColor="#DDDDCC" BorderStyle="None" />

                                        <%-- Reular expression --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                            ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
                                            ControlToValidate="FileUploadImage" SetFocusOnError="True" ForeColor="Red" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                                        <%-- File Size Check --%>
                                        <asp:CustomValidator ID="cvFilesizeCheck" ValidationGroup="InsertSave" runat="server" ErrorMessage="Image size cannot be greater than 200 KB " OnServerValidate="cvFilesizeCheck_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUploadImage"></asp:CustomValidator>

                                    </div>
                                </td>
                            </tr>
                        </table>


                        <%--//----------------------- mail-------------------------%>
                        <label class="label label-success" for="txtMail">Mail Address :</label>
                        <asp:TextBox ID="txtMail" class="form-control" AutoCompleteType="Email" runat="server" TextMode="SingleLine" Placeholder="Enter Mail Address (limit of 100 characters)" MaxLength="100"></asp:TextBox>

                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorMail" runat="server" ErrorMessage="Invalid Email" ControlToValidate="txtMail" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                        <br />

                        <%--//----------------------- address-------------------------%>
                        <label class="label label-success" for="txtUserAddress">User Address :</label>
                        <asp:TextBox ID="txtUserAddress" class="form-control" runat="server" TextMode="SingleLine" Placeholder="Enter Address (limit of 100 characters)" MaxLength="100"></asp:TextBox>

                        <br />

                        <%--//----------------------- PostalCode-------------------------%>
                        <label class="label label-success" for="txtPostalCode">PostalCode :</label>
                        <asp:TextBox ID="txtPostalCode" class="form-control" runat="server" TextMode="Number" Placeholder="Enter Postal Code (limit of 6 characters)" MaxLength="6"></asp:TextBox>
                        <%-- Expression --%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorPostalCode" runat="server" ErrorMessage="Invalid Postal Code" ControlToValidate="txtPostalCode" Display="Dynamic" ForeColor="Red" SetFocusOnError="True"
                            ValidationExpression="^[1-9][0-9]{5}$" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>
                        <br />

                        <%--//----------------------- UserAbout-------------------------%>
                        <label class="label label-success" for="txtUserAbout">User About : </label>
                        <asp:TextBox ID="txtUserAbout" class="form-control" runat="server" TextMode="MultiLine" Placeholder="Enter User About (limit of 500 characters)" MaxLength="500" Rows="5"></asp:TextBox>
                        <br />
                        <%--// Button--%>
                        <div class="col-sm-12 ">
                            <asp:Button ID="btnSave" CssClass="w3-button w3-white w3-border w3-border-red w3-round-large" runat="server" Text="Save" ValidationGroup="InsertSave" OnClick="btnSave_Click"></asp:Button>
                        </div>
                        <br />
                    </div>
                </div>
            </div>
            <!--///End---->

            <!--Right Side for password change Setting---> 
            <div class="col-md-3 ">
                <div class="panel panel-primary">
                    <div class="panel-heading">Change Your Password</div>
                    <div class="panel-body">
                        <div class="col-md-12">

                            <%-- Old --%>
                            <asp:Label ID="CurrentPasswordLabel" runat="server">Password:</asp:Label><p>
                                <asp:TextBox ID="txtCurrentPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="CurrentPasswordRequired" runat="server" ControlToValidate="txtCurrentPassword" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1" Display="Dynamic"></asp:RequiredFieldValidator>
                                <br />
                                <%-- New --%>
                                <asp:Label ID="NewPasswordLabel" runat="server">New Password:</asp:Label>
                                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="NewPasswordRequired" runat="server" ControlToValidate="txtNewPassword" ErrorMessage="New Password is required." ToolTip="New Password is required." ValidationGroup="ChangePassword1" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvNewPws" OnServerValidate="cvNewPws_ServerValidate" runat="server" ForeColor="Red" ControlToValidate="txtNewPassword" ErrorMessage="CustomValidator" ValidationGroup="ChangePassword1" Display="Dynamic">Invalid Password Only support are :- abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~!@#$%^&*()_+=-.,;:|</asp:CustomValidator>

                                <br />
                                <%-- Confram --%>
                                <asp:Label ID="Label1" runat="server">Confirm New Password:</asp:Label>

                                <asp:TextBox ID="txtConfirmNewPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="txtConfirmNewPassword" ErrorMessage="Confirm New Password is required." ForeColor="Red" ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>

                        <div class="col-md-12">
                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry." ValidationGroup="ChangePassword1" ForeColor="Red"></asp:CompareValidator>

                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>

                            <asp:Button ID="ChangePasswordPushButton" CssClass="btn btn-primary" runat="server" CommandName="ChangePassword" Text="Change Password" ValidationGroup="ChangePassword1" OnClick="btnChangePasswordPushButton_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <!--///End-->

        </div>
    </asp:Panel>


</asp:Content>
