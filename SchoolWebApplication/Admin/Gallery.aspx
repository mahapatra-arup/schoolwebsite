﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="SchoolWebApplication.Admin.Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%-- ==Save Messege== --%>
    <asp:Literal ID="ltrSaveMessege" runat="server"></asp:Literal>
    <%--== Save Messege== --%>



    <asp:Panel ID="pnlGallery" CssClass="container-fluid" Style="width: 100%" runat="server">

        <div class="row">

            <!---------------Left sIDE------------->
            <div class="col-sm-6">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#GaleryEntry">Entry</a></li>
                </ul>

                <%--insert Mode --%>
                <button class="w3-button w3-xlarge w3-circle w3-red w3-card pull-right" runat="server" id="btnInsetAndUpdateMode" onserverclick="btnInsetAndUpdateMode_Click" title="Active Insert Mode">+</button>
              
                <div class="tab-content">
                    <%-- Menue 1 active--%>
                    <div id="GaleryEntry" class="tab-panel fade in active">

                        <div style="padding: 10px; border: thin solid #0a2bf1;">
                            <div class="form-group">

                                <asp:UpdatePanel ID="UpdatePanelInsert" runat="server">
                                    <ContentTemplate>
                                        <%--//----------------------- ToolTip-------------------------%>
                                        <label class="label label-warning" for="txtToolTip">ToolTip : </label>
                                        <asp:TextBox ID="txtToolTip" class="form-control" runat="server" Placeholder="Enter ToolTip (limit of 100 characters)" MaxLength="100"></asp:TextBox>

                                        <br />
                                        <%-- -----------------AlterNate Text -----------------------------------%>
                                        <label class="label label-warning" for="txtAlterNateText">AlterNateText : </label>
                                        <asp:TextBox ID="txtAlterNateText" class="form-control" runat="server" Placeholder="Enter AlterNateText (limit of 100 characters)" MaxLength="100"></asp:TextBox>

                                        <br />

                                        <%-- -----------------Subject -----------------------------------%>
                                        <label class="label label-warning" for="txtSubject">Subject : </label>
                                        <asp:TextBox ID="txtSubject" class="form-control" runat="server" Placeholder="Enter Subject (limit of 50 characters)" MaxLength="50"></asp:TextBox>

                                        <br />

                                        <%-- -----------------Content -----------------------------------%>
                                        <label class="label label-warning" for="txtContent">Content : </label>
                                        <asp:TextBox ID="txtContent" class="form-control" runat="server" Placeholder="Enter Content (limit of 100 characters)" MaxLength="100"></asp:TextBox>

                                        <br />
                                        <%-- --------------------------------Image------------------------- --%>
                                        <%--// FileUpload1--%>

                                        <label class="label label-warning" for="FileUpload1">File : </label>
                                        <asp:FileUpload ID="FileUpload1" runat="server" class="form-control1" />


                                        <%-- Reular expression --%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorFileUpload" runat="server" Display="Dynamic"
                                            ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.png|.PNG)$" ErrorMessage="!!Only .jpg or jpeg or png Files are allowed"
                                            ControlToValidate="FileUpload1" SetFocusOnError="True" ForeColor="Red" ValidationGroup="InsertSave"></asp:RegularExpressionValidator>

                                        <%-- File Size Check --%>
                                        <asp:CustomValidator ID="validatorCheckFileSize" ValidationGroup="InsertSave" runat="server" ErrorMessage="Image size cannot be greater than 1MB " OnServerValidate="validatorCheckFileSize_ServerValidate" Display="Dynamic" ForeColor="Red" SetFocusOnError="True" ControlToValidate="FileUpload1" ValidateEmptyText="True"></asp:CustomValidator>

                                        <br />

                                        <%-------------------------------// Thumbnail Group--------------------------%>

                                        <%--// Thumbnail group dropdown--%>
                                        <label class="label label-warning" for="cmbThambnailGroup">Thumbnail group : </label>
                                        <asp:DropDownList ID="cmbThambnailGroup" class="form-control" runat="server"></asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorGroup" runat="server" ControlToValidate="cmbThambnailGroup" Display="Dynamic" InitialValue="-1" ErrorMessage="Group is require" ForeColor="Red" ValidationGroup="InsertSave" SetFocusOnError="True"></asp:RequiredFieldValidator>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>


                            <div class="text-right col-sm-12">
                                <%-- Button --%>
                                <asp:Button ID="btnSave" CssClass="w3-button w3-white w3-border w3-border-red w3-round-large" OnClientClick="this.value='Please wait...';" runat="server" Text="Save" ValidationGroup="InsertSave" OnClick="btnSave_Click"></asp:Button>
                            </div>
                        </div>

                    </div>

                </div>

            </div>


            <%-- ---------------Right Side---------- --%>
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><span>>> Details</span></div>

                    <div class="panel-body">
                        <asp:UpdatePanel ID="UpdatePanelGallery" runat="server">
                            <%--UpdateMode="Conditional"--%>
                            <ContentTemplate>

                                <%-- Drop down list for search --%>
                                <div class="form-group">
                                    <label class="label label-info" for="contain">Serach :</label>
                                    <asp:DropDownList ID="ddlGroupNames" class="form-control" runat="server" OnSelectedIndexChanged="ddlGroupNames_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                </div>


                                <%--Gried View--%>
                                <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" DataKeyNames="Id"
                                    OnRowCommand="gvDetails_RowCommand">

                                    <%-- Columns --%>
                                    <Columns>

                                        <%-- Column  Id --%>
                                        <asp:TemplateField HeaderText="Id" Visible="False">

                                            <%-- Simple Template --%>
                                            <ItemTemplate>
                                                <asp:Label ID="lblId" Text='<%#Eval("Id") %>' runat="server"></asp:Label>
                                            </ItemTemplate>

                                            <%-- Edit Template --%>
                                            <EditItemTemplate>
                                            </EditItemTemplate>

                                            <%-- Footer --%>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:TemplateField>

                                        <%-- Column  Subjects --%>
                                        <asp:TemplateField HeaderText="Subjects" ItemStyle-HorizontalAlign="Center">

                                            <%-- Simple Template --%>
                                            <ItemTemplate>
                                                <%--//Imagezoom--%>
                                                <asp:Image ID="img_Gallery" CssClass="img-responsive img-rounded img_zoom_5" Width="40px" Height="40px" ImageUrl='<%#Eval("ImgPath") %>' runat="server"></asp:Image>
                                            </ItemTemplate>

                                            <%-- Edit Template --%>
                                            <EditItemTemplate>
                                            </EditItemTemplate>

                                            <%-- Footer --%>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:TemplateField>

                                        <%-- Column Edit and Delete --%>
                                        <asp:TemplateField HeaderText="" ItemStyle-Width="10%">

                                            <%-- Simple Template --%>
                                            <ItemTemplate>
                                                <asp:ImageButton ImageUrl="~/Admin/images/Alter.png" ID="imgAlter" CommandName="EditRow" ToolTip="Edit" CommandArgument='<%# Eval("Id") %>' runat="server"></asp:ImageButton>
                                                <asp:ImageButton ImageUrl="~/Admin/images/Delete.png" ID="imgDelete" CommandName="DeleteRow" ToolTip="Delete" CommandArgument='<%# Eval("Id") %>' runat="server"
                                                    OnClientClick="return confirm('Are you sure you want to delete this event?');"></asp:ImageButton>
                                            </ItemTemplate>

                                            <%-- Edit Template --%>
                                            <EditItemTemplate>
                                            </EditItemTemplate>

                                            <%-- Footer --%>
                                            <FooterTemplate>
                                            </FooterTemplate>

                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>
    
     <%-- For Datatable --%>
    <script type="text/javascript">  
        //Call
        $(function () {
            CreateDataTable();
            //Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(CreateDataTable);
            UpdatePanelRefresh();
        });

        //--For Datatable-- %>
        function CreateDataTable() {
            $(function () {
                $('#<%=gvDetails.ClientID%>').DataTable({ "scrollX": true });
            });
        }

        ////On UpdatePanel Refresh.
        function UpdatePanelRefresh() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        CreateDataTable();
                    }
                });
            };
        }
    </script>
</asp:Content>
