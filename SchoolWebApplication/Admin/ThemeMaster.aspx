﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Sp-Admin.Master" AutoEventWireup="true" CodeBehind="ThemeMaster.aspx.cs" Inherits="SchoolWebApplication.Admin.ThemeMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <!-- css For Color Picker -->
    <link href="../GlobalTools/ColorPicker/bootstrap-colorpicker.min.css" rel="stylesheet" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%-------//Message ---------%>
    <asp:Literal ID="ltrMessege" runat="server"></asp:Literal>
    <%-------//Message ---------%>

    <asp:Panel ID="Panelradio" runat="server">
        <%--  <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>--%>
        <div class="ui-widget w3-round-large w3-center w3-card w3-animate-top bg-warning">
            <fieldset>
                <legend><span class="w3-badge w3-round-xlarge w3-card">&nbsp;&nbsp;Select a Design Section :-</span> </legend>
                <label for="<%=radio_1.ClientID %>">Header Section</label>
                <input class="radio00112" type="radio" name="radio_1" id="radio_1" runat="server" onchange="return raiseEvent()" />
                <label for="<%=radio_2.ClientID %>">Menu Section&nbsp;&nbsp;</label>
                <input class="radio00112" type="radio" name="radio_1" id="radio_2" runat="server" onchange="return raiseEvent()" />
                <label for="<%=radio_3.ClientID %>">Body Section&nbsp;&nbsp;</label>
                <input class="radio00112" type="radio" name="radio_1" id="radio_3" runat="server" onchange="return raiseEvent()" />
                <label for="<%=radio_4.ClientID %>">Footer Section</label>
                <input class="radio00112" type="radio" name="radio_1" id="radio_4" runat="server" onchange="return raiseEvent()" />
            </fieldset>

        </div>

        <asp:Button ID="btnTemp" Text="radion button click" runat="server" OnClick="btnTemp_Click" CssClass="hidden" />
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </asp:Panel>



    <asp:Panel ID="Panel1" runat="server">
        <div class="w3-panel">
            <%-- Tab-001 --%>
            <div id="ThemeMaster_tabs_001">
                <ul>
                    <li><a href="#tabs-1">Background</a></li>
                    <li><a id="btnForecolorDialogOpenButton" href='#tabs-1'>ForeGround</a></li>

                </ul>

                <%-- ===================Tab -1 Back Ground================ --%>
                <div id="tabs-1">
                    <div class="form-group w3-padding-large">
                        <div class="w3-row">

                            <div class="w3-col m6">
                                <%-- ==================Color================= --%>
                                <div class="panel panel-success">
                                    <div class="panel-heading">Background Color : </div>
                                    <div class="panel-body">
                                        <%-- First Color --%>
                                        <label for="txtFirstBackColor">Color - I : </label>
                                        <div class="dvColorPicker input-group colorpicker-component">
                                            <asp:TextBox ID="txtFirstBackColor" value="#00AABB" class="form-control" runat="server"></asp:TextBox>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>

                                        <br />
                                        <%-- Second Color --%>
                                        <label for="txtSecondBackColor">Color - II : </label>
                                        <div class="dvColorPicker input-group colorpicker-component">
                                            <asp:TextBox ID="txtSecondBackColor" value="#00AABB" class="form-control" runat="server"></asp:TextBox>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>

                                        <br />
                                        <%-- third Color --%>
                                        <label for="txtThirdBackColor">Color - III : </label>
                                        <div class="dvColorPicker input-group colorpicker-component">
                                            <asp:TextBox ID="txtThirdBackColor" value="#00AABB" class="form-control" runat="server"></asp:TextBox>
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </div>
                                <%-- ==================//Color================= --%>

                                <%-- ==================Color Range================= --%>
                                <div class="panel panel-danger w3-margin-top">
                                    <div class="panel-heading">Color Range: </div>

                                    <div class="panel-body">
                                        <label for="txtFirstBackColor_Range">Range - I : </label>
                                        <asp:TextBox ID="txtFirstBackColor_Range" class="form-control" TextMode="Number" min="0" max="100" value="0" runat="server"></asp:TextBox>

                                        <br />
                                        <label for="txtSecondBackColor_Range">Range - II : </label>
                                        <asp:TextBox ID="txtSecondBackColor_Range" class="form-control" TextMode="Number" min="0" max="100" value="100" runat="server"></asp:TextBox>

                                    </div>
                                </div>
                                <%-- ==================//Color Range================= --%>
                            </div>

                            <%-- ==================Color View================= --%>
                            <div class="w3-col m6 w3-padding-small">
                                <%-- <asp:UpdatePanel ID="UpdatePanel_View" runat="server">
                                    <ContentTemplate>--%>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <span class="w3-btn">Color View:</span>
                                        <span class="panel-title" style="float: right">
                                            <button id="btnBackcolorRefresh" runat="server" class="ui-button ui-widget ui-corner-all w3-round-large" onserverclick="btnBackcolorRefresh_ServerClick"><span class="fa fa-refresh"></span></button>
                                        </span>

                                    </div>
                                    <div class="panel-body text-center">
                                        <asp:Panel ID="pnlBackColorView" runat="server">
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <hr />
                                            <asp:Label ID="lblSectionForeColor" runat="server" Text="Selected Color"></asp:Label><hr />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                        </asp:Panel>
                                    </div>
                                    <div class="panel-footer">
                                        <label for="cmbBackColorDirestion">Direction : </label>
                                        <asp:DropDownList ID="cmbBackColorDirestion" CssClass="form-control" runat="server">
                                            <asp:ListItem Enabled="true" Text="to bottom"></asp:ListItem>
                                            <asp:ListItem Enabled="true" Text="to right"></asp:ListItem>
                                            <asp:ListItem Enabled="true" Text="to top left"></asp:ListItem>
                                            <asp:ListItem Enabled="true" Text="to top right"></asp:ListItem>

                                            <asp:ListItem Enabled="true" Text="to top"></asp:ListItem>
                                            <asp:ListItem Enabled="true" Text="to left"></asp:ListItem>
                                            <asp:ListItem Enabled="true" Text="to bottom left"></asp:ListItem>
                                            <asp:ListItem Enabled="true" Text="to bottom right"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <%-- </ContentTemplate>

                                </asp:UpdatePanel>--%>
                            </div>
                            <%-- ==================Color View================= --%>
                        </div>

                        <div class="w3-row w3-right-align">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="ui-button ui-widget ui-corner-all" OnClick="btnSubmit_Click" Text="Submit" />
                        </div>
                    </div>
                </div>
                <%-- ===================//Tab -1================ --%>
            </div>
        </div>
    </asp:Panel>



    <%-- ================ Fore Ground ========================--%>
    <asp:Panel runat="server" ID="pnl1111">

        <!-- ModalPopupExtender -->
        <asp:Panel runat="server" ID="pnldialogForForeColor001" title="Basic dialog">
            <asp:UpdatePanel ID="UpdatePanel200" runat="server">
                <ContentTemplate>
                    <div>
                        <div class="modal-body">
                            <label for="txtForeColorCode">Color : </label>
                            <div class="dvColorPicker input-group colorpicker-component">
                                <asp:TextBox ID="txtForeColorCode" CssClass="form-control" runat="server"></asp:TextBox>
                                <span class="input-group-addon"><i></i></span>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <asp:Button ID="btnForeGroundColorSubmit" runat="server" CssClass="ui-icon-arrow-4-diag ui-button ui-widget ui-corner-all w3-round-large"></asp:Button>
                        </div>
                    </div>

                </ContentTemplate>

            </asp:UpdatePanel>

        </asp:Panel>
        <!-- ModalPopupExtender -->

        <asp:HiddenField ID="hfforecolor" runat="server" />
    </asp:Panel>


    <%-- ================ External css ========================--%>
    <asp:Panel ID="Panel2222" runat="server">

        <div class="panel panel-primary">
            <%-- Header --%>
            <div class="panel-heading">
                <span style="color: white">External CSS >></span>
            </div>


            <%-- Body --%>
            <div class="panel-body">

                <div class="col-md-9 text-left">
                    <div class="col-md-6">
                        <label class="label label-success" for="txtExternalAttributeKey">Attribute Key (ex : 'border') : </label>
                        <asp:TextBox ID="txtExternalAttributeKey" runat="server" class="form-control"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidatorGroup" runat="server" ControlToValidate="txtExternalAttributeKey" ErrorMessage="Attribute Key (ex : 'border') is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="externalcss"></asp:RequiredFieldValidator>
                        --%>
                    </div>

                    <div class="col-md-6">
                        <label class="label label-success" for="txtExternalAttributevalue">Attribute Value (ex : '1px solid black !important') : </label>
                        <asp:TextBox ID="txtExternalAttributevalue" runat="server" class="form-control"></asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtExternalAttributevalue" ErrorMessage="Attribute Value (ex : 'border') is require" ForeColor="Red" SetFocusOnError="True" ValidationGroup="externalcss"></asp:RequiredFieldValidator>
                        --%>
                    </div>
                    <asp:Label ID="lblAlreadyExist" runat="server" ForeColor="Red" Text="Already Exist" Visible="False"></asp:Label>

                </div>

                <div class="col-md-3">
                    <%-- Save Button --%>
                    <br />
                    <asp:Button ID="btnExternalCssSubmit" runat="server" Text="ADD" OnClick="btnExternalCssSubmit_Click"
                        ValidationGroup="externalcss" class="btn btn-primary" />
                </div>

                <div class="col-md-12">
                    <br />
                    <%-- =-===View==== --%>
                    <asp:GridView ID="gvExternalCssView" runat="server" AutoGenerateColumns="false" OnRowUpdating="gvExternalCssView_RowUpdating" OnRowEditing="gvExternalCssView_RowEditing" OnRowCancelingEdit="gvExternalCssView_RowCancelingEdit" CssClass="w3-table-all">
                        <Columns>



                            <%--Attribute key  --%>
                            <asp:TemplateField HeaderText="Attribute Key">
                                <ItemTemplate>
                                    <asp:Label ID="label00111" runat="server" Text='<%# Eval("AttributeKey") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lbl_ExternalAttcssKey" Style="display: none" runat="server" Text='<%# Eval("AttributeKey") %>'></asp:Label>
                                    <asp:TextBox ID="txt_ExternalcssKey" runat="server" class="form-control" Text='<%#Eval("AttributeKey") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>


                            <%--Attribute value --%>
                            <asp:TemplateField HeaderText="Attribute Value">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Externalcssvalue" runat="server" Text='<%# Eval("AttributeValue") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txt_ExternalcssValue" runat="server" class="form-control" Text='<%#Eval("AttributeValue") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <%-- Button --%>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:Button ID="btn_Edit" runat="server" CssClass="btn btn-primary" Text="Edit" CommandName="Edit" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btn_Update" runat="server" CssClass="btn btn-primary" Text="Update" CommandName="Update" />
                                    <br />
                                    <asp:Button ID="btn_Cancel" runat="server" CssClass="btn btn-primary" Text="Cancel" CommandName="Cancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
            </div>
        </div>
    </asp:Panel>


    <%-- ===================Script Section============= --%>
    <script src="../GlobalTools/ColorPicker/bootstrap-colorpicker.min.js"></script>

    <script>
        function raiseEvent() {
            $('#<%= btnTemp.ClientID %>').click();
            return false;
        }
    </script>
    <script type="text/javascript">
        //Radio Button---------
        $(function () {
            $(".radio00112").checkboxradio();
        });


        //----------Tab Control---------
        $(function () {
            $("#ThemeMaster_tabs_001").tabs();
        });

        // ======Script For Color Picker=======
        $(function () {
            $('.dvColorPicker').colorpicker();
        });


        //======Script For jquery UI Dialog=====
        $(function () {
            $("#<%=pnldialogForForeColor001.ClientID%>").dialog({
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 1000
                }
            });
            //=================Open Diallog=============
            $("#btnForecolorDialogOpenButton").on("click", function () {
                $("#<%=pnldialogForForeColor001.ClientID%>").dialog("open");
            });
            //===============Close Dialog or Fore Color Send to Server SIde==============
            $("#<%=btnForeGroundColorSubmit.ClientID%>").on("click", function () {
                $("#<%=pnldialogForForeColor001.ClientID%>").dialog("close");
                $("#<%:this.hfforecolor.ClientID %>").val($("#<%=txtForeColorCode.ClientID%>").val());
            });
        });


    </script>
</asp:Content>
