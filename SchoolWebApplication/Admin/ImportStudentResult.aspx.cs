﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin
{
    public partial class ImportStudentResult : System.Web.UI.Page
    {
        #region <-------------Variable-------------->
        double _lMaxFileSize = 524288;//512kb
        string[] _clsMp = StudentClassTools.ClassMp.ToArray();
        string[] _clsHs = StudentClassTools.ClassHs.ToArray();
        string Marksheet_Folder_Path = DefultPaths.Marksheet_Folder_Path;
        // Specify the path to save the uploaded file to.
        string Temp_Folder_Path = DefultPaths.Temp_Folder_Path;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGridview();
            }
        }

        #region <================View=================>

        private void AddColumn()
        {
            gvStudentResult.Columns.Clear();
            if (_clsMp.Contains(ddlClass.SelectedItem.Text))
            {
                #region mp
                BoundField ida1 = new BoundField();
                ida1.HeaderText = "ID";
                ida1.DataField = "ID";
                ida1.Visible = false;
                gvStudentResult.Columns.Add(ida1);


                BoundField RollNoa3 = new BoundField();
                RollNoa3.HeaderText = "Roll No";
                RollNoa3.DataField = "RollNo";
                gvStudentResult.Columns.Add(RollNoa3);

                BoundField Sectiona2 = new BoundField();
                Sectiona2.HeaderText = "Section";
                Sectiona2.DataField = "Section";
                gvStudentResult.Columns.Add(Sectiona2);

                BoundField Namea4 = new BoundField();
                Namea4.HeaderText = "Name";
                Namea4.DataField = "Name";
                gvStudentResult.Columns.Add(Namea4);

                BoundField FathersNamea5 = new BoundField();
                FathersNamea5.HeaderText = "Fathers Name";
                FathersNamea5.DataField = "FathersName";
                gvStudentResult.Columns.Add(FathersNamea5);

                BoundField MothersNamea6 = new BoundField();
                MothersNamea6.HeaderText = "Mothers Name";
                MothersNamea6.DataField = "MothersName";
                gvStudentResult.Columns.Add(MothersNamea6);

                BoundField DOBa7 = new BoundField();
                DOBa7.HeaderText = "DOB";
                DOBa7.DataField = "DOB";
                gvStudentResult.Columns.Add(DOBa7);

                BoundField SubjectsNamea8 = new BoundField();
                SubjectsNamea8.HeaderText = "Subjects Name";
                SubjectsNamea8.DataField = "SubjectsName";
                gvStudentResult.Columns.Add(SubjectsNamea8);

                BoundField Unit1_Marksa9 = new BoundField();
                Unit1_Marksa9.HeaderText = "Unit-I";
                Unit1_Marksa9.DataField = "Unit1_Marks";
                gvStudentResult.Columns.Add(Unit1_Marksa9);

                BoundField Unit2_Marksa10 = new BoundField();
                Unit2_Marksa10.HeaderText = "Unit-II";
                Unit2_Marksa10.DataField = "Unit2_Marks";
                gvStudentResult.Columns.Add(Unit2_Marksa10);

                BoundField Unit3_Marksa11 = new BoundField();
                Unit3_Marksa11.HeaderText = "Unit-III";
                Unit3_Marksa11.DataField = "Unit3_Marks";
                gvStudentResult.Columns.Add(Unit3_Marksa11);

                BoundField Totala12 = new BoundField();
                Totala12.HeaderText = "Total";
                Totala12.DataField = "Total";
                gvStudentResult.Columns.Add(Totala12);

                BoundField AvgAndGradea13 = new BoundField();
                AvgAndGradea13.HeaderText = "Avg And Grade";
                AvgAndGradea13.DataField = "AvgAndGrade";
                gvStudentResult.Columns.Add(AvgAndGradea13);

                BoundField GrandTotala14 = new BoundField();
                GrandTotala14.HeaderText = "Grand Total";
                GrandTotala14.DataField = "GrandTotal";
                gvStudentResult.Columns.Add(GrandTotala14);

                BoundField Resulta15 = new BoundField();
                Resulta15.HeaderText = "Result";
                Resulta15.DataField = "Result";
                gvStudentResult.Columns.Add(Resulta15);

                //================Date time formate setting=============
                DOBa7.DataFormatString = "{0:D}";
                //=====Support Html Encode like<Pre> </Pre>(if it not use the html tag are not support .the tag formate is change like : &lt;pre&gt; )=-======
                //Ref:https://msdn.microsoft.com/en-us/library/system.web.ui.webcontrols.boundfield.htmlencode(v=vs.110).aspx

                Sectiona2.HtmlEncode = false;
                RollNoa3.HtmlEncode = false;
                Namea4.HtmlEncode = false;
                FathersNamea5.HtmlEncode = false;
                MothersNamea6.HtmlEncode = false;
                DOBa7.HtmlEncode = false;
                SubjectsNamea8.HtmlEncode = false;
                Unit1_Marksa9.HtmlEncode = false;
                Unit2_Marksa10.HtmlEncode = false;
                Unit3_Marksa11.HtmlEncode = false;

                Totala12.HtmlEncode = false;
                AvgAndGradea13.HtmlEncode = false;
                GrandTotala14.HtmlEncode = false;
                Resulta15.HtmlEncode = false;
                #endregion
            }
            else
            {
                #region hs
                BoundField id1 = new BoundField();
                id1.HeaderText = "ID";
                id1.DataField = "ID";
                id1.Visible = false;
                gvStudentResult.Columns.Add(id1);

                BoundField RollNo3 = new BoundField();
                RollNo3.HeaderText = "Roll No";
                RollNo3.DataField = "RollNo";
                gvStudentResult.Columns.Add(RollNo3);

                BoundField Section2 = new BoundField();
                Section2.HeaderText = "Section";
                Section2.DataField = "Section";
                gvStudentResult.Columns.Add(Section2);

                BoundField Name4 = new BoundField();
                Name4.HeaderText = "Name";
                Name4.DataField = "Name";
                gvStudentResult.Columns.Add(Name4);

                BoundField FathersName5 = new BoundField();
                FathersName5.HeaderText = "Fathers Name";
                FathersName5.DataField = "FathersName";
                gvStudentResult.Columns.Add(FathersName5);

                BoundField MothersName6 = new BoundField();
                MothersName6.HeaderText = "Mothers Name";
                MothersName6.DataField = "MothersName";
                gvStudentResult.Columns.Add(MothersName6);

                BoundField DOB7 = new BoundField();
                DOB7.HeaderText = "DOB";
                DOB7.DataField = "DOB";
                gvStudentResult.Columns.Add(DOB7);

                BoundField SubjectsName8 = new BoundField();
                SubjectsName8.HeaderText = "Subjects Name";
                SubjectsName8.DataField = "SubjectsName";
                gvStudentResult.Columns.Add(SubjectsName8);

                //BoundField Marks9 = new BoundField();
                //Marks9.HeaderText = "Marks";
                //Marks9.DataField = "Marks";
                //gvStudentResult.Columns.Add(Marks9);

                //BoundField Total10 = new BoundField();
                //Total10.HeaderText = "Total";
                //Total10.DataField = "Total";
                //gvStudentResult.Columns.Add(Total10);

                BoundField PercentageandGrade11 = new BoundField();
                PercentageandGrade11.HeaderText = "Marks and Grade";
                PercentageandGrade11.DataField = "PercentageandGrade";
                gvStudentResult.Columns.Add(PercentageandGrade11);

                BoundField GrandTotal11 = new BoundField();
                GrandTotal11.HeaderText = "Grand Total";
                GrandTotal11.DataField = "GrandTotal";
                gvStudentResult.Columns.Add(GrandTotal11);

                BoundField Result12 = new BoundField();
                Result12.HeaderText = "Result";
                Result12.DataField = "Result";
                gvStudentResult.Columns.Add(Result12);


                //================Date time formate setting=============
                DOB7.DataFormatString = "{0:D}";
                //=====Support Html Encode like<Pre> </Pre>(if it not use the html tag are not support .the tag formate is change like : &lt;pre&gt; )=-======
                //Ref:https://msdn.microsoft.com/en-us/library/system.web.ui.webcontrols.boundfield.htmlencode(v=vs.110).aspx

                Section2.HtmlEncode = false;
                RollNo3.HtmlEncode = false;
                Name4.HtmlEncode = false;
                FathersName5.HtmlEncode = false;
                MothersName6.HtmlEncode = false;
                DOB7.HtmlEncode = false;
                SubjectsName8.HtmlEncode = false;
                //Marks9.HtmlEncode = false;
                //Total10.HtmlEncode = false;
                PercentageandGrade11.HtmlEncode = false;
                GrandTotal11.HtmlEncode = false;
                Result12.HtmlEncode = false;
                #endregion
            }
        }
        private void BindGridview()
        {
            AddColumn();
            // gvStudentResult.HeaderRow.TableSection = TableRowSection.TableHeader;
            string cls = ddlClass.SelectedItem.Text;
            if (_clsMp.Contains(cls))
            {
                gvStudentResult.DataSource = StudentResultImportTools.GetMpresultDetails(cls);
            }
            else
            {
                gvStudentResult.DataSource = StudentResultImportTools.GetHSresultDetails(cls);
            }
            gvStudentResult.DataBind();

        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Progress001", "document.getElementById('Progress001').style.display = 'block';", true);

            BindGridview();
            ShowReport();
            ScriptManager.RegisterStartupScript(this, GetType(), "Progress0012", "document.getElementById(\"Progress001\").style.display = \"none\";", true);
        }
        #endregion

        #region <=============Excel process===========>
        private void UploadExcel()
        {
            if (Page.IsValid)
            {
                lblMessage.Text = "";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                string cls = ddlClassForExcel.Text;

                if (FileUploadForExcel.PostedFile != null && !cls.ISNullOrWhiteSpace())
                {
                    bool isInsertSuccess = false;
                    //Temp Path
                    string _Path = FileUploadForExcel.GenerateFileUploadPath(Temp_Folder_Path);
                    string fileExtension = System.IO.Path.GetExtension(FileUploadForExcel.FileName);

                    if (!_Path.ISNullOrWhiteSpace())
                    {
                        //Delete old Data
                        if (DeleteOldExcelResultData(cls))
                        {
                            // Notify the user that the file name was changed.
                            FileUploadForExcel.SaveAs(Server.MapPath(_Path));

                            //Inser Result data into database
                            if (_clsMp.Contains(cls))
                            {
                                isInsertSuccess = StudentResultImportTools.ImportMpResult(Server.MapPath(_Path));
                            }
                            else
                            {
                                isInsertSuccess = StudentResultImportTools.ImportHSResult(Server.MapPath(_Path));
                            }

                            // ====== Delete upload Temp Excel File=====
                            if (!_Path.ISNullOrWhiteSpace())
                            {
                                System.IO.File.Delete(Server.MapPath(_Path));
                            }

                            //====if Data Not Insert Then===
                            if (!isInsertSuccess)
                            {
                                lblMessage.Text = "Data are not import || Insert Problem -" + StudentResultImportTools._ImportErrorMessageShow + ",\n (DB ERROR :- " + SqlHelper.Msg + ")";
                                Show(lblMessage.Text, "!Error", _messegeType.danger, _alartTopIndex.Simple, ref ltSaveMessege);
                            }
                            else
                            {

                                lblMessage.Text = "Excel Data Upload Successfull";
                                lblMessage.ForeColor = System.Drawing.Color.Green;
                                Show(lblMessage.Text, "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);

                            }
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Data are not import || Path Invalid";
                        Show(lblMessage.Text, "!Error", _messegeType.danger, _alartTopIndex.Simple, ref ltSaveMessege);
                    }
                }
                else
                {
                    lblMessage.Text = "Data are not import || File Invalid";
                    Show(lblMessage.Text, "!Error", _messegeType.danger, _alartTopIndex.Simple, ref ltSaveMessege);
                }
            }
        }
        protected void validatorCheckFileSize_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileUploadForExcel.FileContent.Length;
            if (filesize > _lMaxFileSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void btnExcelUpload_Click(object sender, EventArgs e)
        {
            UploadExcel();
        }

        private bool DeleteOldExcelResultData(string clas)
        {
            bool isDeleteSuccess = false;
            if (_clsMp.Contains(clas))
            {
                isDeleteSuccess = StudentResultImportTools.DeleteMpresultDetails(clas);
            }
            else
            {
                isDeleteSuccess = StudentResultImportTools.DeleteHSpresultDetails(clas);
            }
            return isDeleteSuccess;
        }
        #endregion

        #region <-----RStudentsMarksheetReports------->

        protected void cv_ReportFilecheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // Check File Prasent or not  
            if (FileUploadReport.HasFiles)
            {
                foreach (HttpPostedFile postfiles in FileUploadReport.PostedFiles)
                {
                    //Get The File Extension  
                    string filetype = Path.GetExtension(postfiles.FileName);
                    if (filetype.ToLower() == ".pdf")
                    {
                        //Get The File Size In Byte  
                        double filesize = postfiles.ContentLength;
                        if (filesize > _lMaxFileSize)
                        {
                            cv_ReportFilecheck.ErrorMessage = "file size cannot be greater than 500KB/pdf";
                            args.IsValid = false;
                            break;
                        }
                        else
                        {
                            args.IsValid = true;
                        }
                    }
                    else
                    {
                        cv_ReportFilecheck.ErrorMessage = "Only allow .pdf Files";
                        args.IsValid = false;
                        break;
                    }
                }
            }
            else
            {
                cv_ReportFilecheck.ErrorMessage = "Files are required";
                args.IsValid = false;
            }

        }

        protected void btnReportUpload_Click(object sender, EventArgs e)
        {
            lblMessage.ForeColor = System.Drawing.Color.Red; lblMessage.Text = "";

            btnReportUpload.Enabled = false;
            btnReportUpload.Text = "Please Wait...";
            pdfFileInsert();
            btnReportUpload.Enabled = true;
            btnReportUpload.Text = "Upload";
        }

        private bool pdfFileUpload()
        {
            if (FileUploadReport.HasFiles)
            {
                string path = Marksheet_Folder_Path + "/";
                foreach (HttpPostedFile postfiles in FileUploadReport.PostedFiles)
                {
                    //Get The File Extension  
                    string filetype = Path.GetExtension(postfiles.FileName);
                    string fileName = Path.GetFileName(postfiles.FileName);
                    if (!path.ISNullOrWhiteSpace())
                    {
                        string filename = path + fileName;

                        // Notify the user that the file name was changed.
                        postfiles.SaveAs(Server.MapPath(filename));
                    }
                    else
                    {

                        lblMessage.Text = "Data are not upload || invalid file upload path";
                        Show(lblMessage.Text, "!Error", _messegeType.danger, _alartTopIndex.Simple, ref ltSaveMessege);
                        return false;

                    }
                }
            }
            else
            {
                lblMessage.Text = "Data are not upload || Files are not available";
                Show(lblMessage.Text, "!Error", _messegeType.danger, _alartTopIndex.Simple, ref ltSaveMessege);
                return false;
            }

            return true;
        }

        private bool SplitStudentDetails(string filename, out string clas, out string sec, out string rollno, out DateTime dob)
        {
            //out variable
            clas = "";
            sec = "";
            rollno = "";
            dob = DateTime.MinValue;
            filename = Path.GetFileNameWithoutExtension(filename);

            //--=Series  are Class_Sec_Rollno,_Dob=--
            string[] result = filename.Split('_');

            try
            {
                //1. Class
                clas = result[0];
                //2. sec
                sec = result[1];
                //3. rollno
                rollno = result[2];
                //4. dob 
                dob = DateTime.ParseExact(result[3], "dd-MM-yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private void DeleteOldMarksheetReportData(string clas)
        {
            DataTable dt = StudentResultImportTools.GetRStudentsMarksheetReports(clas);
            if (dt.IsValidDataTable())
            {
                foreach (DataRow item in dt.Rows)
                {
                    string cls = Convert.ToString(item["Class"]);
                    string sec = Convert.ToString(item["Section"]);
                    string rollno = Convert.ToString(item["RollNo"]);
                    string dob = Convert.ToString(item["DOB"]);
                    string MarksheetPath = Convert.ToString(item["MarksheetPath"]);
                    if (StudentResultImportTools.DeleteRStudentsMarksheetReports(cls, sec, rollno, dob))
                    {
                        // ====== Delete Marks Report=====
                        if (!MarksheetPath.ISNullOrWhiteSpace())
                        {

                            if (File.Exists(MarksheetPath))
                            {
                                System.IO.File.Delete(Server.MapPath(MarksheetPath));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Main Upload process method
        /// </summary>
        private void pdfFileInsert()
        {
            if (Page.IsValid)
            {
                List<string> lstQuery = new List<string>();
                List<List<SqlParameter>> sqlparammain = new List<List<SqlParameter>>();
                string comboClassName = ddlclassForMarksheet.SelectedItem.Text;

                if (FileUploadReport.HasFiles)
                {
                    //=====Delete Old Data=====
                    DeleteOldMarksheetReportData(comboClassName);
                    //=====Delete Old Data=====

                    foreach (HttpPostedFile postfiles in FileUploadReport.PostedFiles)
                    {
                        //Get The File Extension  
                        string filetype = Path.GetExtension(postfiles.FileName);
                        string fileName = postfiles.FileName;
                        string filename = Marksheet_Folder_Path + "/" + fileName;

                        //for out variable
                        string clas = "";
                        string sec = "";
                        string rollno = "";
                        DateTime dob = DateTime.MinValue;
                        if (SplitStudentDetails(fileName, out clas, out sec, out rollno, out dob))
                        {
                            List<SqlParameter> ParamList = new List<SqlParameter>();
                            string qry = StudentResultImportTools.InsertRStudentsMarksheetReportsQry(DBNull.Value, DBNull.Value, clas, sec, rollno, DBNull.Value, DBNull.Value, DBNull.Value, DBNull.Value, dob, filename, out ParamList);

                            //==Assign list==
                            lstQuery.Add(qry);
                            sqlparammain.Add(ParamList);
                        }
                        else
                        {
                            lblMessage.Text = "Data are not upload || File Upload Problem";
                            Show(lblMessage.Text, "!Error", _messegeType.danger, _alartTopIndex.Simple, ref ltSaveMessege);
                        }
                    }
                    //==insert process==
                    if (SqlHelper.GetInstance().ExecuteTransection(lstQuery, sqlparammain))
                    {
                        if (pdfFileUpload())
                        {
                            lblMessage.Text = "Data Save Successfull";
                            lblMessage.ForeColor = System.Drawing.Color.Green;
                            Show(lblMessage.Text, "!Success", _messegeType.success, _alartTopIndex.Simple, ref ltSaveMessege);
                        }

                    }
                    else
                    {
                        lblMessage.Text = "Data are not Save || Insert Problem";
                        Show(lblMessage.Text, "!Error", _messegeType.danger, _alartTopIndex.Simple, ref ltSaveMessege);
                    }
                }
                else
                {
                    lblMessage.Text = "Data are not upload || Files are not available";
                    Show(lblMessage.Text, "!Error", _messegeType.danger, _alartTopIndex.Simple, ref ltSaveMessege);
                }
            }
        }

        private void ShowReport()
        {
            Literal LT = new Literal();
            int i = 0;
            StringBuilder ster = new StringBuilder();
            DataTable dt = StudentResultImportTools.GetRStudentsMarksheetReports(ddlClass.Text);
            if (dt.IsValidDataTable())
            {
                foreach (DataRow item in dt.Rows)
                {
                    string cls = Convert.ToString(item["Class"]);
                    string sec = Convert.ToString(item["Section"]);
                    string roll = Convert.ToString(item["RollNo"]);
                    string DOB = Convert.ToString(item["DOB"]) != string.Empty ? Convert.ToDateTime(item["DOB"]).ToString("dd-MMM-yyyy") : "-";

                    //Note :Do not support Phycal full path like use  Server.MapPath()
                    string pat = Convert.ToString(item["MarksheetPath"]);

                    string sbobj = "<object	data=\"" + pat + "\"  type=\"application/pdf\" style=\"width:100%;height:720px\">" +
                        "</object> ";

                    //======marksheet design==========
                    string sbCarousel = " <div class=\"w3-display-container\"> " +
        //-------------------------------------Header --------------------
        "                                            <div class=\"label label-danger w3-display-topmiddle\"> " +
                                                         cls + "/" + sec + "/" + roll + "/" + DOB +
        "                                            </div> " +

                                           sbobj +
        "                                        </div> ";
                    //======//marksheet design==========

                    ster.Append(sbCarousel);
                    i++;
                }
            }
            else
            {
                ster.Append("No MarkSheet Are Available");
            }
            LT.Text = ster.ToString();
            PlaceHoldermarksheet.Controls.Add(LT);
            UpdatePanelviewmarksheet.Update();
        }
        #endregion

        protected void btnDeleteMarksReport_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string cls = ddlClass.Text;

                var confirmValue = Request.Form["confirmClassDelete_value"].Split(',');
                var yesNo = confirmValue.ISValidObject() ? confirmValue[0] : "NO";
                if (yesNo.ToUpper().Trim() == "YES")
                {
                    //=====Delete Old Data=====
                    DeleteOldMarksheetReportData(cls);
                    //=====Delete Old Data=====

                    //Delete old Data
                    if (DeleteOldExcelResultData(cls))
                    {
                        Show("Delete Class Data", "Delete", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    }
                    //Refresh 
                    BindGridview();
                }
                else
                {
                    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked NO!')", true);
                }

            }
        }
    }
}