﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin
{
    public partial class SchoolProfiles : System.Web.UI.Page
    {

        double _lMaxFileSize = 512000;//500KB
        string _fileName = string.Empty;
        enum logoType { logo, Banner }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowDetails();
            }
        }

        private void ShowDetails()
        {
            SchoolWebLibrary.Models.SchoolProfile _SchoolDetails = SqlHelper.DbContext().SchoolProfiles.FirstOrDefault();
            if (_SchoolDetails.ISValidObject())
            {
                #region Data Fill
                txtAt.Text = _SchoolDetails.At;
                txtBlock.Text = _SchoolDetails.Block;
                txtDiseCode.Text = _SchoolDetails.DISECode;
                txtDist.Text = _SchoolDetails.DIST;
                txtDistCode.Text = _SchoolDetails.DistrictCode;
                txtEmail.Text = _SchoolDetails.Email;
                txtESTD.Text = _SchoolDetails.ESTD;

                txtGp.Text = _SchoolDetails.GP;
                txtInstitutionCode.Text = _SchoolDetails.InstitutionalCode;
                txtMapUrl.Text = _SchoolDetails.GoogleMapUrl;
                txtPh.Text = _SchoolDetails.Ph;
                txtPin.Text = _SchoolDetails.PIN;
                txtPo.Text = _SchoolDetails.PO;
                txtPS.Text = _SchoolDetails.PS;
                txtSchoolName.Text = _SchoolDetails.SchoolName;
                txtSubDivision.Text = _SchoolDetails.Website;
                #endregion

                #region Logo Fill 
                //logo
                if (_SchoolDetails.Logo != null && _SchoolDetails.Logo.Length > 0)
                {
                    imgLogo.ImageUrl = (System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo)).ImageUrl;
                }

                //logo1
                if (_SchoolDetails.Logo1 != null && _SchoolDetails.Logo1.Length > 0)
                {
                    imgLogo1.ImageUrl = (System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo1)).ImageUrl;
                }

                //logo2
                if (_SchoolDetails.Logo2 != null && _SchoolDetails.Logo2.Length > 0)
                {
                    imgLogo2.ImageUrl = (System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo2)).ImageUrl;
                }
                #endregion
            }
        }

        private void UpdateUserData()
        {
            if (Page.IsValid)
            {
                SchoolProfile schoolProfile = new SchoolProfile();

                #region -=Fill Data=-
                schoolProfile.At = txtAt.Text;
                schoolProfile.Block = txtBlock.Text;
                schoolProfile.DISECode = txtDiseCode.Text;
                schoolProfile.DIST = txtDist.Text;
                schoolProfile.DistrictCode = txtDistCode.Text;
                schoolProfile.Email = txtEmail.Text;
                schoolProfile.ESTD = txtESTD.Text;

                schoolProfile.GP = txtGp.Text;
                schoolProfile.InstitutionalCode = txtInstitutionCode.Text;
                schoolProfile.GoogleMapUrl = txtMapUrl.Text;
                schoolProfile.Ph = txtPh.Text;
                schoolProfile.PIN = txtPin.Text;
                schoolProfile.PO = txtPo.Text;
                schoolProfile.PS = txtPS.Text;
                schoolProfile.SchoolName = txtSchoolName.Text;
                schoolProfile.SubDivission = txtSubDivision.Text;
                schoolProfile.Website = txtWebsite.Text;


                //logo
                schoolProfile.Logo = GetLogoDetails(fuLogo, ref imgLogo);

                //logo1
                schoolProfile.Logo1 = GetLogoDetails(fuLogo1, ref imgLogo1);

                //logo2
                schoolProfile.Logo2 = GetLogoDetails(fuLogo2, ref imgLogo2);

                #endregion


                //Save
                var issucess = SchoolUtils.UpdateSchoolProfile(schoolProfile);

                if (issucess)
                {
                    ShowDetails();

                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltrSaveMessege);
                    //Java script
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");
                    //  Response.Redirect(Request.Url.AbsoluteUri);
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltrSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltrSaveMessege);
            }
        }

        private byte[] GetLogoDetails(FileUpload fu, ref Image logo_Image)
        {
            byte[] img_Data = null;
            //read from File upload control
            if (fu.HasFile)
            {
                System.IO.Stream fs = fu.PostedFile.InputStream;
                System.IO.BinaryReader _br = new System.IO.BinaryReader(fs);
                // Set Position to the beginning of the stream.
                _br.BaseStream.Position = 0;

                var data = _br.ReadBytes((int)fs.Length);
                img_Data = data;
            }

            else
            {
                //read from image control
                if (!logo_Image.ImageUrl.ISNullOrWhiteSpace())
                {
                    img_Data = System_Web_UI_WebControls_Imager.ImageControlToByte(ref logo_Image);

                }
            }
            //Java script
            return img_Data;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            UpdateUserData();
        }

        #region --Logo Refresh Button--
        protected void hlLogoRefresh_Click(object sender, EventArgs e)
        {
            RefreshLogoProcess();
        }
        protected void hlLogo2Refresh_Click(object sender, EventArgs e)
        {
            RefreshLogoProcess();
        }
        protected void hlLogo1Refresh_Click(object sender, EventArgs e)
        {
            RefreshLogoProcess();
        }
        #endregion

        #region --Logo Size Check--
        protected void cvLogosizeCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            LogoSizeCheck(ref args, ref cvLogosizeCheck, fuLogo, logoType.logo);
        }
        protected void cvLogo1SizeCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            LogoSizeCheck(ref args, ref cvLogo1SizeCheck, fuLogo1, logoType.logo);
        }
        protected void cvLogo2SizeCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Banner
            LogoSizeCheck(ref args, ref cvLogo2SizeCheck, fuLogo2, logoType.Banner);
        }
        #endregion

        private void LogoSizeCheck(ref ServerValidateEventArgs args, ref CustomValidator cvFilesize_Check, FileUpload fu, logoType logoType)
        {
            //data
            //Ref:http://converter.elliotbeken.com/

            string data = args.Value;
            args.IsValid = false;
            double filesize = fu.FileContent.Length;
            double height = 0d, width = 0d;
            System_Web_UI_WebControls_Imager.GetFileUploadImageHeightWidth(fu, out height, out width);

            //Check

            if (filesize > _lMaxFileSize)//Yes
            {
                cvFilesize_Check.ErrorMessage = "Image size cannot be greater than 500 KB";
                args.IsValid = false;
            }
            else//No
            {

                switch (logoType)
                {
                    case logoType.logo:
                        if ((height <= 150 && height >= 100) && (width <= 150 && width >= 100))
                        {
                            args.IsValid = true;
                        }
                        else
                        {
                            cvFilesize_Check.ErrorMessage = "Logo Can not be Under of <br/> School Logo : 100*100 - 150*150 <br /> ";
                            args.IsValid = false;
                        }
                        break;
                    case logoType.Banner:
                        //Banner Size Check
                        if ((height <= 300 && height >= 200) && (width <= 2560 && width >= 1920))
                        {
                            args.IsValid = true;
                        }
                        else
                        {
                            cvFilesize_Check.ErrorMessage = "Banner Can not be Under of <br/>  Banner :- 1920 * 200 - 2560 * 300";

                            args.IsValid = false;
                        }
                        break;
                }
            }
        }
        private void RefreshLogoProcess()
        {
            SchoolWebLibrary.Models.SchoolProfile _SchoolDetails = SqlHelper.DbContext().SchoolProfiles.FirstOrDefault();

            //Logo 
            #region Logo
            if (fuLogo.HasFile)
            {
                imgLogo.ImageUrl = FileUploadToImageUrl(fuLogo);
            }
            else
            {
                if (_SchoolDetails.Logo != null && _SchoolDetails.Logo.Length > 0)
                {
                    imgLogo.ImageUrl = (System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo)).ImageUrl;
                }
            }
            #endregion

            //Logo - 1
            #region Logo 1
            if (fuLogo1.HasFile)
            {
                imgLogo1.ImageUrl = FileUploadToImageUrl(fuLogo1);
            }
            else
            {
                if (_SchoolDetails.Logo1 != null && _SchoolDetails.Logo1.Length > 0)
                {
                    imgLogo1.ImageUrl = (System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo1)).ImageUrl;
                }
            }
            #endregion

            //Banner
            #region Banner
            if (fuLogo2.HasFile)
            {
                imgLogo2.ImageUrl = FileUploadToImageUrl(fuLogo2);
            }
            else
            {
                if (_SchoolDetails.Logo2 != null && _SchoolDetails.Logo2.Length > 0)
                {
                    imgLogo2.ImageUrl = (System_Web_UI_WebControls_Imager.byteArrayToWebImage(_SchoolDetails.Logo2)).ImageUrl;
                }
            }
            #endregion
        }
        private string FileUploadToImageUrl(FileUpload fu)
        {
            string data_Image = string.Empty;
            if (fu.HasFile)
            {
                using (Stream fs = fu.PostedFile.InputStream)
                {
                    using (BinaryReader _br = new BinaryReader(fs))
                    {
                        string contentType = fu.PostedFile.ContentType;
                        // Set Position to the beginning of the stream.
                        _br.BaseStream.Position = 0;

                        byte[] bytes = _br.ReadBytes((int)fs.Length);
                        string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                        data_Image = "data:image/png;base64," + base64String;
                    }
                }
            }
            return data_Image;
        }
    }
}