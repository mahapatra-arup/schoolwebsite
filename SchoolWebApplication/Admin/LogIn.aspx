﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="SchoolWebApplication.Admin.LogIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Log on as an Administrator</title>

    <%-- Web site ico --%>
    <link runat="server" rel="shortcut icon" id="titleico1" href="#" type="image/png" />
    <link runat="server" rel="icon" id="titleico2" href="#" type="image/png" />

    <%--//Meta--%>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="School Web Admin Panel, Admin Panel" />

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/jquery") %>
        <%: Styles.Render("~/Content/css/bootstrap") %>
        <!--jqueryUi-->
        <link href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.12.1/themes/blitzer/jquery-ui.css" rel="stylesheet" />
        <!--Remove Jquery Ui Close Button(Login Window)-->
        <%: Styles.Render("~/Content/css/JqueryUI_CloseButton_Remove") %>
    </asp:PlaceHolder>


</head>
<body>

    <form id="loginform" runat="server">

        <div id="dialogforlogin" title="LogIn">

            <%-- Save Messege --%>
            <asp:Literal ID="ltSaveMessege" runat="server"></asp:Literal>
            <%-- Save Messege --%>

            <%-- User Name --%>
            <label for="txtUsername">Username</label>
            <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" placeholder="Enter Username.." />
            <br />

            <%-- Password --%>
            <label for="txtPassword">Password</label>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control"
                placeholder="Enter Password" />
            <br />

            <%--Submit Button --%>
            <div class="text-right">
                <asp:CheckBox ID="chkRememberMe" CssClass="checkbox-inline " Text=" Remember Me" runat="server" />

                <asp:Button ID="btnLogIn" runat="server" OnClick="btnLogIn_Click" Text="LogIn" Style="display: none" />
            </div>

        </div>

    </form>

   
    <script type="text/javascript">
        //Dialog
        $(function () {
            var dlg = $("#dialogforlogin").dialog({
                modal: true,
                //For Auto Post Back
                appendTo: 'form',
                buttons: [
                    {
                        text: "Log In",
                        click: function () {
                            $("#<%:btnLogIn.ClientID%>").click();
                        }
                    }
                ]
            });

        });
    </script>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/bootstrap") %>
    </asp:PlaceHolder>
    <!--jqueryUi-->
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.12.1/jquery-ui.min.js"></script>
</body>
</html>
