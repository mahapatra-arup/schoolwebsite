﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)


namespace SchoolWebApplication.Admin
{
    public partial class NoticeBoard : System.Web.UI.Page
    {
        #region <---------var--------->
        string _SavePath = DefultPaths.Notice_Folder_Path;
        double _lMaxFileSize = 2097152;
        private static long _Noticeid = 0;
        private static bool mUpdateTime = false;
        #endregion

        #region Prevent Page Refresh in C#

        //For refresh
        private bool _refreshState;
        private bool _isRefresh;
        protected override void LoadViewState(object savedState)
        {
            object[] AllStates = (object[])savedState;
            base.LoadViewState(AllStates[0]);
            _refreshState = bool.Parse(AllStates[1].ToString());
            _isRefresh = _refreshState == bool.Parse(Session["__ISREFRESH"].ToString());
        }

        protected override object SaveViewState()
        {
            Session["__ISREFRESH"] = _refreshState;
            object[] AllStates = new object[2];
            AllStates[0] = base.SaveViewState();
            AllStates[1] = !(_refreshState);
            return AllStates;
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDateFrom.SetCurrentDateInTextBox("yyyy-MM-dd");
                txtDateTo.SetCurrentDateInTextBox("yyyy-MM-dd");
                txtDate.SetCurrentDateInTextBox("yyyy-MM-dd");
                FillGried();
            }
            ltSaveMessege.Text = "";
        }
        private void InsertData()
        {
            if (Page.IsValid)
            {
                #region <--------Var------->
                string _Path = FileUpload1.GenerateFileUploadPath(_SavePath);

                string Publisher = txtPublisherName.Text;
                string PublishDate = txtDate.Text;
                string dateFrom = txtDateFrom.Text;
                string dateTo = txtDateTo.Text;
                string Subjects = txtSubjects.Text;
                string Contents = txtContent.Text;
                string Footer = txtFooter.Text;
                string ENoticeFileUrl = _Path.ISNullOrWhiteSpace() ? string.Empty : _Path;
                bool Activity = true;
                int Slno = (NoticeUtils.GetMaxNoticeSlno() + 1);
                List<SqlParameter> lstParem = new List<SqlParameter>();
                #endregion

                //Query
                string qry = NoticeUtils.InsertNoticeQry(ENoticeFileUrl, Subjects, Contents,
                Footer, PublishDate, Publisher, Slno.ToString(), Activity, DBNull.Value, dateFrom, dateTo, out lstParem);

                SqlHelper.MParameterList = lstParem;
                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    //File upload
                    FileUpload(ENoticeFileUrl);

                    //  Default Set
                    Clear();
                    FillGried();

                    //Update  in gried Data
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    //Java script
                    Response.Write("<script>alert('Data Save SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }
        private void Clear()
        {
            txtPublisherName.Text = string.Empty;
            txtDate.Text = string.Empty;

            txtSubjects.Text = string.Empty;
            txtContent.Text = string.Empty;
            txtFooter.Text = string.Empty;

            //Importent
            mUpdateTime = false;
        }

        private void FileUpload(string path)
        {
            if (!path.ISNullOrWhiteSpace())
            {
                // Notify the user that the file name was changed.
                FileUpload1.SaveAs(Server.MapPath(path));
                //Image Show
            }
        }

        #region <............Event............>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!_isRefresh) // check that page is not refreshed by browser.
            {
                btnSave.Enabled = false;
                btnSave.Text = "Please Wait.......";

                if (!mUpdateTime)
                {
                    InsertData();
                }
                else
                {
                    UpdateData();
                }

                btnSave.Enabled = true;
                btnSave.Text = "Save";
            }
        }

        protected void VCFFileSizeCheck_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;
            double filesize = FileUpload1.FileContent.Length;
            if (filesize > _lMaxFileSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }


        protected void gvNoticeView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteRow")
            {
                string vlu = e.CommandArgument.ToString();

                //Image path
                string path = NoticeUtils.GetNoticeFilePath(vlu.ConvertObjectToLong());
                if (NoticeUtils.DeleteNotice(vlu))
                {
                    if (!path.ISNullOrWhiteSpace())
                    {
                        System.IO.File.Delete(Server.MapPath(path));
                    }
                    FillGried();
                    Show("Delete Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                }
            }

            else if (e.CommandName == "EditRow")
            {
                string vlu = e.CommandArgument.ToString();
                if (!vlu.ISNullOrWhiteSpace())
                {
                    GetDetails(vlu.ConvertObjectToLong());
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Navfn0007", "$('.nav-tabs a[href=\"#NoticeEntry\"]').tab('show')", true);

                    mUpdateTime = true;
                }
                else
                {
                    Show("Select Valid Row", "Warning!", _messegeType.warning, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
        }
        protected void btnAddMode_ServerClick(object sender, EventArgs e)
        {
            Clear();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Navfn0006", "$('.nav-tabs a[href=\"#NoticeEntry\"]').tab('show')", true);
        }
        #endregion

        private void FillGried()
        {
            var dt = NoticeUtils.GetAllNotice();
            if (dt.IsValidList())
            {
                gvNoticeView.DataSource = dt;
                gvNoticeView.DataBind();
                if (dt.IsValidList())
                {
                    gvNoticeView.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            else
            {
                gvNoticeView.DataSource = null;
                gvNoticeView.DataBind();
            }

        }

        private void GetDetails(long id)
        {
            var dt = NoticeUtils.GetNotice(id);
            if (dt.ISValidObject())
            {
                try
                {
                    _Noticeid = id;
                    txtSubjects.Text = dt.Subjects;
                    txtContent.Text = dt.Contents;
                    txtFooter.Text = dt.Footer;
                    txtPublisherName.Text = dt.Publisher;


                    //Publisher date
                    txtDate.Text = dt.PublishDate.ConvertNullableToDateTime().Date.ToString("yyyy-MM-dd");

                    //Date from
                    txtDateFrom.Text = dt.DateFrom.ConvertNullableToDateTime().Date.ToString("yyyy-MM-dd");

                    //Date To
                    txtDateTo.Text = dt.DateTo.ConvertNullableToDateTime().Date.ToString("yyyy-MM-dd");
                }
                catch (Exception)
                {

                }
            }
        }

        private void UpdateData()
        {
            if (Page.IsValid)
            {
                #region <----------Var---------->
                string Publisher = txtPublisherName.Text;
                string PublishDate = txtDate.Text;
                string dateFrom = txtDateFrom.Text;
                string dateTo = txtDateTo.Text;
                string Subjects = txtSubjects.Text;
                string Contents = txtContent.Text;
                string Footer = txtFooter.Text;
                string pdfpath = string.Empty;

                //new pdf path
                string _Path = FileUpload1.GenerateFileUploadPath(_SavePath);
                pdfpath = _Path.ISNullOrWhiteSpace() ? string.Empty : _Path;
                bool Activity = true;
                #endregion

                //==old pdf Path======......
                string path = NoticeUtils.GetNoticeFilePath(_Noticeid.ConvertObjectToLong());
                List<SqlParameter> lstParem = new List<SqlParameter>();

                //Query
                string qry = NoticeUtils.UpdateNoticeQry(_Noticeid, Subjects, Contents, Footer, PublishDate, Publisher, Activity, DBNull.Value, dateFrom, dateTo, out lstParem);
                SqlHelper.MParameterList = lstParem;

                if (SqlHelper.GetInstance().ExcuteQuery(qry))
                {
                    if (!pdfpath.ISNullOrWhiteSpace())
                    {
                        try
                        {
                            //Delete old File
                            if (!path.ISNullOrWhiteSpace())
                            {
                                System.IO.File.Delete(Server.MapPath(path));
                            }
                        }
                        catch (Exception)
                        {
                        }

                        //.Upload file.......
                        FileUpload(pdfpath);
                        NoticeUtils.UpdateNoticeFilePath(_Noticeid, pdfpath);
                    }
                    else
                    {
                    }

                    //  Default Set
                    Clear();
                    FillGried();

                    //Messege
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Navfn0006", "$('.nav-tabs a[href=\"#NoticeView\"]').tab('show')", true);
                    Show("Data Update Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    Response.Write("<script>alert('Data Update SuccessFull');</script>");
                }
                else
                {
                    Show("Data are not Update", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not Update because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }
    }
}
