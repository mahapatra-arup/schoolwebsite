﻿#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.DataAccesTools;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using SchoolWebLibrary.Models;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
#pragma warning disable CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)
using static SchoolWebLibrary.MessageBoxAlart;
#pragma warning restore CS0246 // The type or namespace name 'SchoolWebLibrary' could not be found (are you missing a using directive or an assembly reference?)

namespace SchoolWebApplication.Admin
{
    public partial class AddNavigation : System.Web.UI.Page
    {
        #region <-------------Var------------->
        int _IconSize = 3072;//3kb
        static bool mUpdateTime = false;
        static string _gUpdateModule_Id;
        #endregion

        #region Prevent Page Refresh in C#

        //For refresh
        private bool _refreshState;
        private bool _isRefresh;
        protected override void LoadViewState(object savedState)
        {
            try
            {
                object[] AllStates = (object[])savedState;
                base.LoadViewState(AllStates[0]);
                _refreshState = bool.Parse(AllStates[1].ToString());
                _isRefresh = _refreshState == bool.Parse(Session["__ISREFRESH"].ToString());
            }
            catch (Exception)
            {
            }
        }

        protected override object SaveViewState()
        {
            Session["__ISREFRESH"] = _refreshState;
            object[] AllStates = new object[2];
            AllStates[0] = base.SaveViewState();
            AllStates[1] = !(_refreshState);
            return AllStates;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Get pages title----------
                DefaultSet();
                ltSaveMessege.Text = "";
            }
        }

        private void BindGrid()
        {
            var data = SqlHelper.DbContext().SC_MenuMaster.OrderBy(s => s.SlNo);

            gvDetails.DataSource = data.ToList();
            gvDetails.DataBind();
            if (data.IsValidIEnumerable())
            {
                gvDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void UpdatePreference()
        {
            if (Page.IsValid)
            {
                //GroupIds
                var v = Request.Form["MenuMasterId"];
                var Ids = (from Q in v.Split(',')
                           select Q).ToArray();

                int SlNo = 1;
                foreach (var Id in Ids)
                {
                    DynamicPageUtils.UpdateMenuSlNo(Id, SlNo);
                    SlNo += 1;
                }

                //Refresh Time Save Problem Solve
                Response.Redirect(Request.Url.AbsoluteUri);
            }
        }
        private void UpdateData()
        {
            //Check
            if (Page.IsValid && !_gUpdateModule_Id.ISNullOrWhiteSpace())
            {
                //Db Connection
                var db = SqlHelper.DbContext();

                //Url
                string ModifyPageName = Regex.Replace(txtPageName.Text, @"[^0-9a-zA-Z]+", "_");
                var newurl = string.Format("~/Pages/{0}", ModifyPageName);

                //DataFill
                #region <----DataFill---->

                //MenuMaster
                var slNo = SqlHelper.DbContext().SC_MenuMaster.Max(sl => sl.SlNo) + 1;
                var parantModule = ddlPages.SelectedIndex > -1 ? ddlPages.SelectedValue != "0" ? ddlPages.SelectedValue : null : null;

                var sMenuMaster = new SC_MenuMaster();
                sMenuMaster.Module_Id = _gUpdateModule_Id;
                sMenuMaster.Title = txtPageName.Text;
                sMenuMaster.Parent_Module_Id = parantModule;
                sMenuMaster.ToolsTrip = txtPageName.Text;
                sMenuMaster.Activity = chkVisibility.Checked;
                sMenuMaster.IsImageShow = chkimg_Show.Checked;
                sMenuMaster.IsTitleShow = chkShow_Title.Checked;

                #region ICO
                //image ico
                //read from File upload control
                if (FileUpload_Menuico.HasFile)
                {
                    using (Stream fs = FileUpload_Menuico.PostedFile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            sMenuMaster.Image = br.ReadBytes((Int32)fs.Length);
                        }
                    }
                }
                else
                {
                    //read from image control
                    if (ImageOfMenuIcon.ImageUrl != null)
                    {
                        sMenuMaster.Image = System_Web_UI_WebControls_Imager.ImageControlToByte(ref ImageOfMenuIcon);
                    }
                }
                #endregion

                //sLink

                var sLink = new SC_links();
                sLink.link_url = newurl;
                sLink.link_name = txtPageName.Text;
                #endregion

                //add
                var isSuccess = DynamicPageUtils.UpdateNavigation(sLink, sMenuMaster);
                if (isSuccess)
                {
                    //Update  in gried Data
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);
                    DefaultSet();
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }

        private void InsertData()
        {
            //Check
            if (Page.IsValid)
            {

                //Modify remove spaicial character
                string ModifyPageName = Regex.Replace(txtPageName.Text, @"[^0-9a-zA-Z]+", "_");
                var newurl = string.Format("~/Pages/{0}", ModifyPageName);

                //DataFill
                #region <----DataFill---->
                var sLink = new SC_links();
                sLink.Link_Id = Guid.NewGuid().ConvertObjectToString();
                sLink.link_url = newurl;
                sLink.link_name = txtPageName.Text;
                sLink.link_visible = true;

                //MenuMaster
                var slNo = SqlHelper.DbContext().SC_MenuMaster.Max(sl => sl.SlNo) + 1;
                var parantModule = ddlPages.SelectedIndex > -1 ? ddlPages.SelectedValue != "0" ? ddlPages.SelectedValue : null : null;

                var sMenuMaster = new SC_MenuMaster();
                sMenuMaster.Module_Id = Guid.NewGuid().ConvertObjectToString();
                sMenuMaster.Title = txtPageName.Text;
                sMenuMaster.Link_Id = sLink.Link_Id;
                sMenuMaster.Parent_Module_Id = parantModule;
                sMenuMaster.ToolsTrip = txtPageName.Text;
                sMenuMaster.SlNo = slNo;
                sMenuMaster.Activity = chkVisibility.Checked;
                sMenuMaster.IsDynamicPage = true;
                sMenuMaster.IsImageShow = chkimg_Show.Checked;
                sMenuMaster.IsTitleShow = chkShow_Title.Checked;

                #region ICO
                //image ico
                //read from File upload control
                if (FileUpload_Menuico.HasFile)
                {
                    using (Stream fs = FileUpload_Menuico.PostedFile.InputStream)
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            sMenuMaster.Image = br.ReadBytes((Int32)fs.Length);
                        }
                    }
                }
                else
                {
                    //read from image control
                    if (ImageOfMenuIcon.ImageUrl != null)
                    {
                        sMenuMaster.Image = System_Web_UI_WebControls_Imager.ImageControlToByte(ref ImageOfMenuIcon);
                    }
                }
                #endregion

                //DynamicPage
                var dynamicPage = new DynamicPage();
                dynamicPage.Id = Guid.NewGuid();
                dynamicPage.PageName = txtPageName.Text;
                dynamicPage.PageContent = "Hi i am Your New Page !!";
                dynamicPage.PageTitle = txtPageName.Text;
                dynamicPage.ModuleId = sMenuMaster.Module_Id;
                dynamicPage.css = "";

                #endregion

                //add
                var isSuccess = DynamicPageUtils.AddNavigation(sLink, sMenuMaster, dynamicPage);
                if (isSuccess)
                {
                    //Update  in gried Data
                    Show("Data Save Successfull", "Success!", _messegeType.info, _alartTopIndex.Simple, ref ltSaveMessege);

                    //Default set
                    DefaultSet();
                }
                else
                {
                    Show("Data are not save", "Error!", _messegeType.danger, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
            else
            {
                Show("Data are not save because all require field are not correct", "Warning!", _messegeType.warning, _alartTopIndex.Simple, ref ltSaveMessege);
            }
        }

        private void GetDetails()
        {
            Clear();

            var db = SqlHelper.DbContext();
            var dt = db.SC_MenuMaster.Where(a => a.Module_Id == _gUpdateModule_Id).FirstOrDefault();
            if (dt.ISValidObject())
            {
                try
                {
                    ddlPages.ClearSelection(); //making sure the previous selection has been cleared
                    ddlPages.SelectedIndex = ddlPages.Items.IndexOf(ddlPages.Items.FindByValue(dt.Parent_Module_Id));

                    txtPageName.Text = dt.Title;
                    chkVisibility.Checked = dt.Activity;
                    chkimg_Show.Checked = dt.IsImageShow.ConvertObjectToBool();
                    chkShow_Title.Checked = dt.IsTitleShow.ConvertObjectToBool();
                    byte[] ico_img = null;

                    //===================ico==========
                    try
                    {
                        if (dt.Image.ISValidObject())
                        {
                            Byte[] imageData = (byte[])dt.Image;
                            if (imageData != null)
                            {
                                ico_img = imageData;
                            }
                            if (ico_img.Length > 0 && ico_img != null)
                            {
                                Image img = System_Web_UI_WebControls_Imager.byteArrayToWebImage(ico_img);
                                ImageOfMenuIcon.ImageUrl = img.ImageUrl;
                            }
                        }
                    }
                    catch (Exception)
                    {
                        ico_img = null;
                    }
                }
                catch (Exception)
                {

                }
            }

        }
        private void DefaultSet()
        {
            Clear();
            ddlPages.CmbParentPages();
            ddlPages.Items.Insert(0, new ListItem("--root--", "0"));
            BindGrid();
        }

        private void Clear()
        {
            //must be
            mUpdateTime = false;

            txtPageName.Text = string.Empty;
            ImageOfMenuIcon.ImageUrl = "";
            ddlPages.ClearSelection(); //making sure the previous selection has been cleared
        }

        private bool IsvalidMenuCheck()
        {
            //If Exist then Same valuecompare
            if (_gUpdateModule_Id.ISNullOrWhiteSpace())
            {
                if (MenuMaster.IsExistMenu(txtPageName.Text))
                {
                    lblEmpIdExistMessege.Text = "!!Page Name already exist";
                    btnSave.Enabled = false;
                    return true;
                }
            }
            else
            {
                if (MenuMaster.IsExistMenu(txtPageName.Text, _gUpdateModule_Id))
                {
                    lblEmpIdExistMessege.Text = "!!Update Page Name already exist";
                    btnSave.Enabled = false;
                    return true;
                }
            }

            lblEmpIdExistMessege.Text = "";
            btnSave.Enabled = true;
            //Compare null

            return false; ;
        }

        #region <=====Event=====>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!_isRefresh) // check that page is not refreshed by browser.
            {
                btnSave.Enabled = false;
                btnSave.Text = "Please Wait.......";

                if (!IsvalidMenuCheck())
                {
                    if (!mUpdateTime)
                    {
                        InsertData();
                    }
                    else
                    {
                        UpdateData();
                    }

                }
                btnSave.Enabled = true;
                btnSave.Text = "Save";
            }
        }

        protected void txtPageName_TextChanged(object sender, EventArgs e)
        {
            IsvalidMenuCheck();
        }

        protected void btnUpdateMenuslNo_Click(object sender, EventArgs e)
        {
            UpdatePreference();
        }

        protected void gvDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditRow")
            {
                string vlu = e.CommandArgument.ToString();
                _gUpdateModule_Id = vlu;
                if (!vlu.ISNullOrWhiteSpace())
                {
                    GetDetails();
                    mUpdateTime = true;
                }
                else
                {
                    Show("Select Valid Row", "Warning!", _messegeType.warning, _alartTopIndex.Topindex, ref ltSaveMessege);
                }
            }
        }

        protected void btnAddMode_ServerClick(object sender, EventArgs e)
        {
            Clear();
        }

        protected void cv_MenuIco_FileUpload_ServerValidate(object source, ServerValidateEventArgs args)
        {
            //Ref:http://converter.elliotbeken.com/
            string data = args.Value;
            args.IsValid = false;

            double filesize = FileUpload_Menuico.PostedFile.ContentLength;
            if (filesize > _IconSize)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
        #endregion
    }
}